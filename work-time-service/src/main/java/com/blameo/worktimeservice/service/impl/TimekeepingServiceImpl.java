package com.blameo.worktimeservice.service.impl;

import com.blameo.worktimeservice.constant.Constant;
import com.blameo.worktimeservice.controller.CallOtherService;
import com.blameo.worktimeservice.dto.request.TimekeepingDTO;
import com.blameo.worktimeservice.dto.response.*;
import com.blameo.worktimeservice.model.AvailableDayOff;
import com.blameo.worktimeservice.model.Holiday;
import com.blameo.worktimeservice.model.Timekeeping;
import com.blameo.worktimeservice.model.WorkTimeInDay;
import com.blameo.worktimeservice.modelUser.Department;
import com.blameo.worktimeservice.modelUser.UserDTOResponse;
import com.blameo.worktimeservice.repository.AvailableDayOffRepository;
import com.blameo.worktimeservice.repository.TimekeepingRepository;
import com.blameo.worktimeservice.service.HolidayService;
import com.blameo.worktimeservice.service.TimekeepingService;
import com.blameo.worktimeservice.service.WorkTimeInDayService;
import com.blameo.worktimeservice.utils.CommonUtil;
import com.blameo.worktimeservice.utils.DataUtil;
import com.blameo.worktimeservice.utils.ExcelUtil;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.blameo.worktimeservice.constant.Constant.PATH.GET_ALL_REQUEST;
import static com.blameo.worktimeservice.utils.DataUtil.calculateDaysBetween;
import static com.blameo.worktimeservice.utils.DataUtil.formatToYYYYMMDD;

@Component
@Transactional
public class TimekeepingServiceImpl implements TimekeepingService {

    @Autowired
    MessageSource messageSource;

    @Autowired
    TimekeepingRepository timekeepingRepository;

    @Autowired
    AvailableDayOffRepository availableDayOffRepository;

    @Autowired
    WorkTimeInDayService workTimeInDayService;

    @Autowired
    HolidayService holidayService;

    Logger logger = Logger.getLogger(TimekeepingServiceImpl.class);





    @Override
    public Date importExcel(HttpServletRequest request, MultipartFile file) throws IOException {
        List<UserDTOResponse> listUser = CallOtherService.getAllFromIdService(UserDTOResponse.class, Constant.PATH.GET_ALL_USER, new HashMap<>(), request);
        List<RequestDTOResponse> allRequest = getAllWorktimeRequestFromErpService(null, null, request);
        return readDataFromExcel(file, listUser, allRequest);
    }

    @Override
    public Map<String, Object> getStaticAllUser(Integer page, Integer pageSize, String keyword,String userCode, String department, Date month, HttpServletRequest request) {
        try {
            Map<String, Object> requestParam = new HashMap<>();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            requestParam.put("page", page);
            requestParam.put("pageSize", pageSize);
            requestParam.put("keyword", keyword);
            requestParam.put("userCode", userCode);
            requestParam.put("department", department);
            requestParam.put("status", Constant.ACTIVE);
            requestParam.put("severanveDate", dateFormat.format(month));
            Map<String, Object> dataUserFromId = CallOtherService.getAllUserWithRequestParamFromIdService(Constant.PATH.GET_USER_WITH_PAGING, requestParam, request);
            List<UserDTOResponse> listUser = (List<UserDTOResponse>) dataUserFromId.get("data");
            List<Timekeeping> listTimekeeping = timekeepingRepository.findAllByUserIdInAndTimekeepingDateBetweenOrderByTimekeepingDateAsc(listUser.stream().map(UserDTOResponse::getUserId).collect(Collectors.toList()), DataUtil.getFirstDateInMonth(month), DataUtil.getLastDateInMonth(month));
            Map<String, List<Timekeeping>> data = listTimekeeping.stream().collect(Collectors.groupingBy(Timekeeping::getUserId));
            WorkTimeInDay workTimeInDay = workTimeInDayService.getWorkTimeInDay(month);
            generateStatic(month, listUser, data, holidayService.countAllHolidayInMonth(month), workTimeInDay,request,holidayService.getAllHolidayInMonth(month));
            dataUserFromId.put("data", listUser);
            return dataUserFromId;
        } catch (RuntimeException runtimeException) {
            throw new RuntimeException(messageSource.getMessage("unknownError", null, CommonUtil.getLocale()));
        }
    }

    public Map<String, Object> getTimekeepingUser(List<UserDTOResponse> users) {
        try {
            LocalDate localDate = LocalDate.now().minusDays(1);
            Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
            List<String> listUser = users.stream().map(UserDTOResponse::getUserId).collect(Collectors.toList());
            List<Timekeeping> listTimekeeping = timekeepingRepository.findAllByUserIdInAndTimekeepingDateBetweenOrderByTimekeepingDateAsc(listUser, DataUtil.getFirstDateInMonth(date), DataUtil.getLastDateInMonth(date));
            Map<String, List<Timekeeping>> data = listTimekeeping.stream().collect(Collectors.groupingBy(Timekeeping::getUserId));
            WorkTimeInDay workTimeInDay = workTimeInDayService.getWorkTimeInDay(date);
            List<TimekeepingUser> list = generateTimekeepingService(date, users, data, workTimeInDay,holidayService.getAllHolidayInMonth(date));
            Map<String, Object> dataUserFromId = new HashMap<>();
            dataUserFromId.put("data", list);
            return dataUserFromId;
        } catch (RuntimeException runtimeException) {
            throw new RuntimeException(messageSource.getMessage("unknownError", null, CommonUtil.getLocale()));
        }
    }

    private List<TimekeepingUser> generateTimekeepingService(Date month, List<UserDTOResponse> listUser, Map<String, List<Timekeeping>> data, WorkTimeInDay workTimeInDay,List<Holiday> holidays) {
        List<TimekeepingUser> timekeepingUsers = new ArrayList<>();
        for (UserDTOResponse user : listUser) {
            List<Timekeeping> timekeepingOfUser = data.get(user) != null ?
                    data.get(user)
                            .stream()
                            .filter(timekeeping -> timekeeping.isValidTimekeeping(workTimeInDay, holidays))
                            .collect(Collectors.toList()) : new ArrayList<>();

            List<RequestDTOResponse> requestInMonth = getAllWorktimeRequestFromErpServiceSchedule(month,user);
            List<RequestDTOResponse> requestDTOResponsesDayRemote = requestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE)).collect(Collectors.toList());

            Long countCutPaidMinus = 0L;
            Double countWorkPaid = timekeepingOfUser.parallelStream().reduce(0D, (aDouble, timekeeping) -> aDouble + timekeeping.countWorkPaid(workTimeInDay), Double::sum);
            for (Timekeeping timekeeping : timekeepingOfUser) {
                countCutPaidMinus = countCutPaidMinus + timekeeping.countCutPaidMinus(workTimeInDay, holidays);
            }

            // ngày công thực: Ngày công thực tế đi làm + Remote đã được phê duyệt trong tháng
            timekeepingUsers.add(new TimekeepingUser(user.getUserId(), countWorkPaid + requestDTOResponsesDayRemote.size()));
        }
        return timekeepingUsers;
    }

    private void generateStatic(Date month, List<UserDTOResponse> listUser, Map<String, List<Timekeeping>> data, Long countHoliday, WorkTimeInDay workTimeInDay,HttpServletRequest request,List<Holiday> holidays) {
        for(UserDTOResponse user : listUser){
            UserDTOResponse.StaticTimekeepingOfMonth staticTimekeeping = new UserDTOResponse.StaticTimekeepingOfMonth();

            List<RequestDTOResponse> requestInMonth = getAllWorktimeRequestFromErpService(month,user, request);
            updateWorkingPaid(data.get(user.getUserId()) != null ?
                    data.get(user.getUserId()) : new ArrayList<>(), requestInMonth, workTimeInDay, holidays);

            List<Timekeeping> timekeepingOfUser = data.get(user.getUserId()) != null ?
                    data.get(user.getUserId())
                            .stream()
                            .filter(timekeeping -> timekeeping.isValidTimekeeping(workTimeInDay, holidays))
                            .collect(Collectors.toList()) : new ArrayList<>();

            List<Timekeeping> timekeepingLateOfUser = timekeepingOfUser.stream()
                    .filter(timekeeping -> (timekeeping.isLate(workTimeInDay)))
                    .collect(Collectors.toList());

            Long countCutPaidMinus = 0L;
            Double countWorkPaid = timekeepingOfUser.parallelStream().reduce(0D, (aDouble, timekeeping) -> aDouble + timekeeping.countWorkPaid(workTimeInDay), Double::sum);
            for (Timekeeping timekeeping:timekeepingOfUser) {
                countCutPaidMinus = countCutPaidMinus + timekeeping.countCutPaidMinus(workTimeInDay, holidays);
            }

            List<RequestDTOResponse> requestDTOResponsesDayOff = requestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)).collect(Collectors.toList());
            List<RequestDTOResponse> requestDTOResponsesOT = requestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_OT)).collect(Collectors.toList());
            // ngày công thực: Ngày công thực tế đi làm
            staticTimekeeping.setCountTimekeeping(String.format("%.1f", countWorkPaid));

            staticTimekeeping.setCountHoliday(String.format("%02d", countHoliday));

            staticTimekeeping.setCountLate(String.format("%02d", timekeepingLateOfUser.size()));

            // số giờ OT đã được duyệt trong tháng

            Double countHoursOT = 0D;
            for (RequestDTOResponse requestDTOResponse:requestDTOResponsesOT) {
                if(requestDTOResponse.getRequestWorktimeContents().size() > 0 && requestDTOResponse.getRequestWorktimeContents().stream().findFirst().get().getCountHour() != null){
                    countHoursOT += requestDTOResponse.getRequestWorktimeContents().stream().findFirst().get().getCountHour();
                }
            }
            staticTimekeeping.setCountOTHour(String.format("%1$,.1f", countHoursOT));

            // số ngày nghỉ đã được duyệt trong tháng

            List<Date> dateHolidayInYearCurrent = holidayService.getAllHolidayBetween(DataUtil.getFirstDayOfYear(month),DataUtil.getLastDateInMonth(month))
                    .stream().map(Holiday::getHolidayDate).collect(Collectors.toList());


            AtomicReference<Double> countDayOff = new AtomicReference<>((double) 0);
            //số ngày phép đã nghỉ trong tháng
            AtomicReference<Double> countDayOffInMonthPaid = new AtomicReference<>((double) 0);
            updateDayOff(requestDTOResponsesDayOff, countDayOff, dateHolidayInYearCurrent, month, countDayOffInMonthPaid);
            staticTimekeeping.setCountDayOff(DataUtil.roundToString(countDayOff.get(),1));

            // Ngày lương: Ngày công thực + Ngày lễ
            // cần check thêm ngày phép đã nghỉ trong tháng(trừ những ngày nghỉ không có phép)
            // hiện tại để mặc định là không nghỉ ngày nào
            // số ngày nghỉ được tính lương của các phiếu yêu cầu nghỉ phép trong tháng
            AtomicReference<Double> countWorkPaidFromTimekeeping = new AtomicReference<>(0D);
            if(data.get(user.getUserId()) != null){
                data.get(user.getUserId()).forEach(x -> {
                    if(x.getWorkingTimePaid() != null){
                        countWorkPaidFromTimekeeping.updateAndGet(v -> v + x.getWorkingTimePaid());
                    }
                });
            }
            staticTimekeeping.setCountPaidTimekeeping(String.format("%.1f", countWorkPaidFromTimekeeping.get() + countHoliday));

            // số phép còn lại: tính tới tháng hiện tại
            LocalDate localDate = month.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            AvailableDayOff availableDayOff = availableDayOffRepository.findFirstByUserIdAndMonthAndYear(user.getUserId(), localDate.getMonth().getValue(), localDate.getYear());
            Double countPaidLeave = 0D;
            if(availableDayOff != null && availableDayOff.getAvailableDayOff() != null){
                countPaidLeave += availableDayOff.getAvailableDayOff();
            }
            if(availableDayOff != null && availableDayOff.getAvailableDayOffOld() != null){
                countPaidLeave += availableDayOff.getAvailableDayOffOld();
            }
            staticTimekeeping.setCountPaidLeave(String.format("%1$,.1f", countPaidLeave + countDayOffInMonthPaid.get()));

            // số phút trừ lương: Số phút muộn quá quy định + Số phút về sớm trong tháng
            staticTimekeeping.setCountCutPaidMinus(DataUtil.autoFormat2or3StringLength(countCutPaidMinus));

            user.setStaticTimekeeping(staticTimekeeping);

        };
    }

    private void generateStaticUser(Date month, UserDTOResponse user, List<Timekeeping> data, Long countHoliday, WorkTimeInDay workTimeInDay,HttpServletRequest request,List<Holiday> holidays, Map<String, Object> map) {
        List<RequestDTOResponse> requestInMonth = getAllWorktimeRequestFromErpService(month,user, request);
        updateWorkingPaid(data, requestInMonth, workTimeInDay, holidays);
        List<Timekeeping> timekeepingOfUser =  data.stream()
                        .filter(timekeeping -> timekeeping.isValidTimekeeping(workTimeInDay, holidays))
                        .collect(Collectors.toList());

        Long countCutPaidMinus = 0L;
        Double countWorkPaid = timekeepingOfUser.parallelStream().reduce(0D, (aDouble, timekeeping) -> aDouble + timekeeping.countWorkPaid(workTimeInDay), Double::sum);
        for (Timekeeping timekeeping:timekeepingOfUser) {
            countCutPaidMinus = countCutPaidMinus + timekeeping.countCutPaidMinus(workTimeInDay, holidays);
        }


        List<RequestDTOResponse> requestDTOResponsesDayOff = requestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)).collect(Collectors.toList());

        // ngày công thực: Ngày công thực tế đi làm
        map.put("countTimeKeeping", String.format("%.1f", countWorkPaid));

        map.put("countHolidays", String.format("%02d", countHoliday));

        // số giờ OT đã được duyệt trong tháng

        List<RequestDTOResponse> requestDTOResponsesOT = requestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_OT)).collect(Collectors.toList());
        Double countHoursOT = 0D;
        for (RequestDTOResponse requestDTOResponse:requestDTOResponsesOT) {
            if(requestDTOResponse.getRequestWorktimeContents().size() > 0 && requestDTOResponse.getRequestWorktimeContents().stream().findFirst().get().getCountHour() != null){
                countHoursOT += requestDTOResponse.getRequestWorktimeContents().stream().findFirst().get().getCountHour();
            }
        }
        map.put("countOTHour", String.format("%1$,.1f", countHoursOT));

        // số ngày nghỉ đã được duyệt trong tháng
        // cần call sang erp

        List<Date> dateHolidayInYearCurrent = holidayService.getAllHolidayBetween(DataUtil.getFirstDayOfYear(month),DataUtil.getLastDateInMonth(month))
                .stream().map(Holiday::getHolidayDate).collect(Collectors.toList());

        //số ngày phép đã nghỉ trong tháng
        AtomicReference<Double> countDayOffInMonthPaid = new AtomicReference<>((double) 0);
        AtomicReference<Double> countDayOff = new AtomicReference<>((double) 0);
        updateDayOff(requestDTOResponsesDayOff, countDayOff, dateHolidayInYearCurrent, month, countDayOffInMonthPaid);
        map.put("countDayOff", DataUtil.roundToString(countDayOff.get(),1));

        // số phép còn lại: tính tới tháng hiện tại
        LocalDate localDate = month.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        AvailableDayOff availableDayOff = availableDayOffRepository.findFirstByUserIdAndMonthAndYear(user.getUserId(), localDate.getMonth().getValue(), localDate.getYear());
        Double countPaidLeave = 0D;
        if(availableDayOff != null && availableDayOff.getAvailableDayOff() != null){
            countPaidLeave += availableDayOff.getAvailableDayOff();
        }
        if(availableDayOff != null && availableDayOff.getAvailableDayOffOld() != null){
            countPaidLeave += availableDayOff.getAvailableDayOffOld();
        }
        map.put("countAllDayOff", String.format("%1$,.1f", countPaidLeave + countDayOffInMonthPaid.get()));

        // Ngày lương: Ngày công thực + Phép trong tháng + Ngày lễ
        // cần check thêm ngày phép đã nghỉ trong tháng(trừ những ngày nghỉ không có phép)
        // số ngày nghỉ được tính lương của các phiếu yêu cầu nghỉ phép trong tháng
        AtomicReference<Double> countWorkPaidFromTimekeeping = new AtomicReference<>(0D);
        if(data.size() > 0){
            data.forEach(x -> {
                if(x.getWorkingTimePaid() != null){
                    countWorkPaidFromTimekeeping.updateAndGet(v -> v + x.getWorkingTimePaid());
                }
            });
        }
        map.put("countPaidTimekeeping", String.format("%.1f", countWorkPaidFromTimekeeping.get() + countHoliday));

        // số phút trừ lương: Số phút muộn quá quy định + Số phút về sớm trong tháng
        map.put("countCutPaidMinus", DataUtil.autoFormat2or3StringLength(countCutPaidMinus));

    }

    private void updateWorkingPaid(List<Timekeeping> data, List<RequestDTOResponse> requestInMonth, WorkTimeInDay workTimeInDay, List<Holiday> holidays){
        for (Timekeeping timekeeping : data){
            if(timekeeping.isValidTimekeepingWithWeekendAndHoliday(holidays)){
                List<RequestDTOResponse> requestInDay = requestInMonth.stream().filter((request) ->
                        request.getRequestWorktimeContents()
                                .stream()
                                .anyMatch(
                                        worktimeContentDTO -> (request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE) &&
                                                DateUtils.isSameDay(worktimeContentDTO.getRemoteDate(), timekeeping.getTimekeepingDate())) ||
                                                (request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE) &&
                                                        DataUtil.checkDateBetween(worktimeContentDTO.getDayOffBegin(), worktimeContentDTO.getDayOffEnd(), timekeeping.getTimekeepingDate()))
                                )
                ).collect(Collectors.toList());
                Set<String> workShift = new HashSet<>();
                List<RequestDTOResponse> requestRemote = new ArrayList<>();
                List<RequestDTOResponse> requestDayOff = new ArrayList<>();
                if(requestInDay.size() > 0){
                    requestRemote = requestInDay.stream().filter(x -> x.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE)).collect(Collectors.toList());
                    requestDayOff = requestInDay.stream().filter(x -> x.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)).collect(Collectors.toList());
                }
                if(requestRemote.size() > 0){
                    for(RequestDTOResponse requestDTOResponse : requestRemote){
                        if(requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_OT)){
                            RequestWorktimeContentDTO requestWorktimeContentDTO =  requestDTOResponse.getRequestWorktimeContents().stream().findFirst().orElse(null);
                            if(requestWorktimeContentDTO != null){
                                if(requestWorktimeContentDTO.getRemoteType().equals(Constant.REMOTE_TYPE.ALL)){
                                    workShift.add(Constant.WORK_SHIFT.MORNING);
                                    workShift.add(Constant.WORK_SHIFT.AFTERNOON);
                                }else if(requestWorktimeContentDTO.getRemoteType().equals(Constant.REMOTE_TYPE.AM)){
                                    workShift.add(Constant.WORK_SHIFT.MORNING);
                                }else{
                                    workShift.add(Constant.WORK_SHIFT.AFTERNOON);
                                }
                            }
                        }
                    }
                }
                if(workShift.size() < 2 && timekeeping.isValidTimekeepingWithTime(workTimeInDay)){
                    if(DataUtil.compareTimesWorkTime(timekeeping.getArrivalTime(), timekeeping.getBackTime(), workTimeInDay.getWorkTimeEndTimeAm()) == 1){
                        workShift.add(Constant.WORK_SHIFT.AFTERNOON);
                    }else if(DataUtil.compareTimesWorkTime(timekeeping.getArrivalTime(), timekeeping.getBackTime(), workTimeInDay.getWorkTimeStartTimePm()) == -1){
                        workShift.add(Constant.WORK_SHIFT.MORNING);
                    }else{
                        workShift.add(Constant.WORK_SHIFT.MORNING);
                        workShift.add(Constant.WORK_SHIFT.AFTERNOON);
                    }
                }
                if(requestDayOff.size() > 0){
                    for(RequestDTOResponse requestDTOResponse : requestDayOff){
                        RequestWorktimeContentDTO requestWorktimeContentDTO =  requestDTOResponse.getRequestWorktimeContents().stream().findFirst().orElse(null);
                        if(requestWorktimeContentDTO.getDayOffType().equals(Constant.DAY_OFF_TYPE.AM)){
                            if(requestWorktimeContentDTO.getRequestDayOffType().equals(Constant.REQUEST_DAY_OFF_TYPE.NO_PAID_LEAVE)){
                                workShift.remove(Constant.WORK_SHIFT.MORNING);
                            }else{
                                workShift.add(Constant.WORK_SHIFT.MORNING);
                            }
                        }else if(requestWorktimeContentDTO.getDayOffType().equals(Constant.REMOTE_TYPE.PM)){
                            if(requestWorktimeContentDTO.getRequestDayOffType().equals(Constant.REQUEST_DAY_OFF_TYPE.NO_PAID_LEAVE)){
                                workShift.remove(Constant.WORK_SHIFT.AFTERNOON);
                            }else{
                                workShift.add(Constant.WORK_SHIFT.AFTERNOON);
                            }
                        }else{
                            if(requestWorktimeContentDTO.getRequestDayOffType().equals(Constant.REQUEST_DAY_OFF_TYPE.NO_PAID_LEAVE)){
                                workShift.remove(Constant.WORK_SHIFT.MORNING);
                                workShift.remove(Constant.WORK_SHIFT.AFTERNOON);
                            }else{
                                workShift.add(Constant.WORK_SHIFT.MORNING);
                                workShift.add(Constant.WORK_SHIFT.AFTERNOON);
                            }
                        }
                    }
                }
                if(workShift.contains(Constant.WORK_SHIFT.MORNING) && workShift.contains(Constant.WORK_SHIFT.AFTERNOON)){
                    timekeeping.setWorkingTimePaid(1D);
                }else if(workShift.contains(Constant.WORK_SHIFT.MORNING) || workShift.contains(Constant.WORK_SHIFT.AFTERNOON)){
                    timekeeping.setWorkingTimePaid(0.5D);
                }else{
                    timekeeping.setWorkingTimePaid(0D);
                }
            }
        }
    }

    private void updateDayOff(List<RequestDTOResponse> requestDTOResponsesDayOff,  AtomicReference<Double> countDayOff,
                              List<Date> dateHolidayInYearCurrent, Date month, AtomicReference<Double> countDayOffInMonthPaid){
        Set<Date> listDateRequest = new HashSet<>();
        if(requestDTOResponsesDayOff.size()>0){
            requestDTOResponsesDayOff.stream()
                .filter(requestDTOResponse -> (requestDTOResponse.getRequestWorktimeContents() != null && requestDTOResponse.getRequestWorktimeContents().size() > 0))
                .collect(Collectors.toList())
                .forEach(requestDTOResponse -> {
                    requestDTOResponse.getRequestWorktimeContents().forEach(requestWorktimeContentDTO -> {
                        double multiplier = requestWorktimeContentDTO.getDayOffType() == 0? 1.0 : 0.5;
                        listDateRequest.addAll(DataUtil.getAllDateBetween(requestWorktimeContentDTO.getDayOffBegin(), requestWorktimeContentDTO.getDayOffEnd()));
                        //Nếu ngày kết thúc trước này cuối tháng thi trừ ngày bắt đầu nghỉ - ngày kết thúc
                        if (requestWorktimeContentDTO.getDayOffEnd().before(DataUtil.getLastDateInMonth(month))){
                            Double countDay;
                            if(requestWorktimeContentDTO.getDayOffBegin().before(DataUtil.getFirstDateInMonth(month))){
                                countDay = multiplier * (calculateDaysBetween(DataUtil.getFirstDateInMonth(month), requestWorktimeContentDTO.getDayOffEnd()) + 1
                                        //trừ ngày cuối tuần
                                        - DataUtil.countWeekendDays(DataUtil.getFirstDateInMonth(month),requestWorktimeContentDTO.getDayOffEnd())
                                        //trừ ngày nghỉ lễ
                                        - DataUtil.countDatesInRange(dateHolidayInYearCurrent,DataUtil.getFirstDateInMonth(month),requestWorktimeContentDTO.getDayOffEnd()));
                            }else{
                                countDay = multiplier * (calculateDaysBetween(requestWorktimeContentDTO.getDayOffBegin(), requestWorktimeContentDTO.getDayOffEnd()) + 1
                                        //trừ ngày cuối tuần
                                        - DataUtil.countWeekendDays(requestWorktimeContentDTO.getDayOffBegin(),requestWorktimeContentDTO.getDayOffEnd())
                                        //trừ ngày nghỉ lễ
                                        - DataUtil.countDatesInRange(dateHolidayInYearCurrent,requestWorktimeContentDTO.getDayOffBegin(),requestWorktimeContentDTO.getDayOffEnd()));
                            }
                            countDayOff.updateAndGet(v -> v + countDay);
                            if(requestWorktimeContentDTO.getCountDaySalary() != 0 && requestWorktimeContentDTO.getCountDaySalary() != null){
                                if(requestWorktimeContentDTO.getRequestDayOffType().equals(Constant.REQUEST_DAY_OFF_TYPE.PAID_LEAVE)){
                                    countDayOffInMonthPaid.updateAndGet(v -> v + requestWorktimeContentDTO.getCountDaySalary());
                                }
                            }
                        }
                        //Nếu ngày kết thúc sau tháng thì trừ ngày bắt đầu nghỉ - ngày cuối tháng
                        else {
                            Double countDay;
                            if(requestWorktimeContentDTO.getDayOffBegin().before(DataUtil.getFirstDateInMonth(month))){
                                countDay = multiplier * (calculateDaysBetween(DataUtil.getFirstDateInMonth(month), DataUtil.getLastDateInMonth(month)) + 1
                                        //trừ ngày cuối tuần
                                        - DataUtil.countWeekendDays(DataUtil.getFirstDateInMonth(month),DataUtil.getLastDateInMonth(month))
                                        //trừ ngày nghỉ lễ
                                        - DataUtil.countDatesInRange(dateHolidayInYearCurrent,DataUtil.getFirstDateInMonth(month),DataUtil.getLastDateInMonth(month)));
                            }else{
                                countDay = multiplier * (calculateDaysBetween(requestWorktimeContentDTO.getDayOffBegin(), DataUtil.getLastDateInMonth(month)) + 1
                                        //trừ ngày cuối tuần
                                        - DataUtil.countWeekendDays(requestWorktimeContentDTO.getDayOffBegin(),DataUtil.getLastDateInMonth(month))
                                        //trừ ngày nghỉ lễ
                                        - DataUtil.countDatesInRange(dateHolidayInYearCurrent,requestWorktimeContentDTO.getDayOffBegin(),DataUtil.getLastDateInMonth(month)));
                            }
                            countDayOff.updateAndGet(v -> v + countDay);
                            if(requestWorktimeContentDTO.getCountDaySalary() != 0 && requestWorktimeContentDTO.getCountDaySalary() != null){
                                if(requestWorktimeContentDTO.getRequestDayOffType().equals(Constant.REQUEST_DAY_OFF_TYPE.PAID_LEAVE)){
                                    countDayOffInMonthPaid.updateAndGet(v -> v + requestWorktimeContentDTO.getCountDaySalary());
                                }
                            }
                        }

                    });
                });
        }
    }

    @Override
    public InputStreamResource export(String keyword,String userCode, String department, Date month, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        try{
            Map<String, Object> requestParam = new HashMap<>();
            requestParam.put("keyword", keyword);
            requestParam.put("userCode", userCode);
            requestParam.put("department", department);
            requestParam.put("status", Constant.ACTIVE);
            List<UserDTOResponse> listUser = CallOtherService.getAllFromIdService(UserDTOResponse.class, Constant.PATH.GET_ALL_USER, requestParam, request);

            Map<Department, List<UserDTOResponse>> dataUser = listUser.stream().collect(Collectors.groupingBy(UserDTOResponse::getDepartment));

            List<Timekeeping> listTimekeeping = timekeepingRepository.findAllByUserIdInAndTimekeepingDateBetweenOrderByTimekeepingDateAsc(listUser.stream().map(UserDTOResponse::getUserId).collect(Collectors.toList()), DataUtil.getFirstDateInMonth(month), DataUtil.getLastDateInMonth(month));
            Map<String, List<Timekeeping>> data = listTimekeeping.stream().collect(Collectors.groupingBy(Timekeeping::getUserId));
            List<Holiday> listHoliday = holidayService.getAllHolidayInMonth(month);
            WorkTimeInDay workTimeInDay = workTimeInDayService.getWorkTimeInDay(month);
            List<RequestDTOResponse> requestInMonth = getAllWorktimeRequestFromErpService(month,null, request);

            generateStatic(month, listUser, data,  holidayService.countAllHolidayInMonth(month), workTimeInDay,request,listHoliday);
            InputStream templateStream = TimekeepingServiceImpl.class.getResourceAsStream("/template/" + Constant.TEMPLATE_STATIC_TIMEKEEPING);
            XSSFWorkbook workbook = new XSSFWorkbook(templateStream);

            //style basic
            XSSFCellStyle style = ExcelUtil.getStyleBasic(workbook);
            style.setWrapText(true);

            //style basic blod
            XSSFCellStyle styleBold = ExcelUtil.getStyleCenterBasicBold(workbook);
            styleBold.setWrapText(true);

            //style center basic
            XSSFCellStyle styleCenter = ExcelUtil.getStyleCenterBasic(workbook);
            styleCenter.setWrapText(true);

            //style date basic
            XSSFCellStyle styleDate = ExcelUtil.getStyleDateBasic(workbook, "dd-mm-yyyy");
            styleDate.setWrapText(true);

            XSSFCellStyle styleHoliday = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorHoliday = new XSSFColor(new java.awt.Color(255, 255, 0));
            styleHoliday.setFillForegroundColor(colorHoliday);
            styleHoliday.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleHoliday.setWrapText(true);

            //style center basic
            XSSFCellStyle styleSeveranve = ExcelUtil.getStyleCenterBasic(workbook);
            styleSeveranve.setFillForegroundColor(colorHoliday);
            styleSeveranve.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleSeveranve.setWrapText(true);

            //style bold Header
            XSSFCellStyle styleHeader = ExcelUtil.getStyleCenterBasicBold(workbook);
            XSSFColor colorHeader = new XSSFColor(new java.awt.Color(153, 153, 153));
            styleHeader.setFillForegroundColor(colorHeader);
            styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleHeader.setWrapText(true);

            //style bold gray text red
            XSSFFont font1 = workbook.createFont();
            font1.setFontName("Times New Roman");
            font1.setBold(true);
            XSSFColor colorHeaderRed = new XSSFColor(new java.awt.Color(255, 0, 0));
            XSSFColor colorGray= new XSSFColor(new java.awt.Color(243, 243, 243));
            font1.setColor(colorHeaderRed);
            XSSFCellStyle styleGrayRed = workbook.createCellStyle();
            styleGrayRed.setBorderBottom(BorderStyle.valueOf((short) 1));
            styleGrayRed.setBorderTop(BorderStyle.valueOf((short) 1));
            styleGrayRed.setBorderRight(BorderStyle.valueOf((short) 1));
            styleGrayRed.setBorderLeft(BorderStyle.valueOf((short) 1));
            styleGrayRed.setAlignment(HorizontalAlignment.CENTER);
            styleGrayRed.setFont(font1);
            styleGrayRed.setFillForegroundColor(colorGray);
            styleGrayRed.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleGrayRed.setWrapText(true);
            styleGrayRed.setFillForegroundColor(colorGray);
            styleGrayRed.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleGrayRed.setWrapText(true);

            XSSFFont font2 = workbook.createFont();
            font2.setFontName("Times New Roman");
            font2.setBold(true);
            XSSFCellStyle styleGray = workbook.createCellStyle();
            styleGray.setBorderBottom(BorderStyle.valueOf((short) 1));
            styleGray.setBorderTop(BorderStyle.valueOf((short) 1));
            styleGray.setBorderRight(BorderStyle.valueOf((short) 1));
            styleGray.setBorderLeft(BorderStyle.valueOf((short) 1));
            styleGray.setAlignment(HorizontalAlignment.CENTER);
            styleGray.setFont(font2);
            styleGray.setFillForegroundColor(colorGray);
            styleGray.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleGray.setWrapText(true);
            styleGray.setFillForegroundColor(colorGray);
            styleGray.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleGray.setWrapText(true);


            //style bold Header Yellow
            XSSFCellStyle styleHeaderYellow = ExcelUtil.getStyleCenterBasicBold(workbook);
            XSSFColor colorHeaderYellow = new XSSFColor(new java.awt.Color(255, 242, 204));
            styleHeaderYellow.setFillForegroundColor(colorHeaderYellow);
            styleHeaderYellow.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleHeaderYellow.setWrapText(true);

            //style bold Header red
            //create font
            XSSFFont font = workbook.createFont();
            font.setFontName("Times New Roman");
            font.setBold(true);
            font.setColor(colorHeaderRed);
            XSSFCellStyle styleHeaderRed = workbook.createCellStyle();
            styleHeaderRed.setBorderBottom(BorderStyle.valueOf((short) 1));
            styleHeaderRed.setBorderTop(BorderStyle.valueOf((short) 1));
            styleHeaderRed.setBorderRight(BorderStyle.valueOf((short) 1));
            styleHeaderRed.setBorderLeft(BorderStyle.valueOf((short) 1));
            styleHeaderRed.setAlignment(HorizontalAlignment.CENTER);
            styleHeaderRed.setFont(font);
            styleHeaderRed.setFillForegroundColor(colorHeaderYellow);
            styleHeaderRed.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleHeaderRed.setWrapText(true);


            //style basic Blue
            XSSFCellStyle styleLightBlue = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorBlue = new XSSFColor(new java.awt.Color(188, 212, 236));
            styleLightBlue.setFillForegroundColor(colorBlue);
            styleLightBlue.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleLightBlue.setWrapText(true);

            //style basic Pink
            XSSFCellStyle stylePink = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorPink = new XSSFColor(new java.awt.Color(252, 236, 228));
            stylePink.setFillForegroundColor(colorPink);
            stylePink.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            stylePink.setWrapText(true);

            //style basic Yellow
            XSSFCellStyle styleYellow = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorYellow = new XSSFColor(new java.awt.Color(252, 244, 204));
            styleYellow.setFillForegroundColor(colorYellow);
            styleYellow.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleYellow.setWrapText(true);

            //style basic Green
            XSSFCellStyle styleGreen = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorGreen = new XSSFColor(new java.awt.Color(228, 236, 220));
            styleGreen.setFillForegroundColor(colorGreen);
            styleGreen.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleGreen.setWrapText(true);

            //style basic Grey
            XSSFCellStyle styleGrey = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorGrey = new XSSFColor(new java.awt.Color(236, 236, 236));
            styleGrey.setFillForegroundColor(colorGrey);
            styleGrey.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleGrey.setWrapText(true);

            List<XSSFCellStyle> listStyle = Arrays.asList(styleLightBlue, stylePink, styleYellow, styleGreen, styleGrey);

            XSSFSheet sheet = workbook.getSheetAt(0);
            AtomicInteger rowIndex = new AtomicInteger(2); //  tiêu đề, bắt đầu từ row 5 để thêm dữ liệu
            AtomicInteger stt = new AtomicInteger(1); // đánh stt
            AtomicInteger countDept = new AtomicInteger(1); // đánh stt
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            List<String> header = Arrays.asList("Ngày công đi làm","Số buổi đi muộn","Số phút trừ lương ",
                    "Phép " + dateFormat.format(DataUtil.getFirstDateInMonth(month)),
                    "Số phép dùng T" + DataUtil.getMonthFromDate(month) + "/" + DataUtil.getYearFromDate(month),
                    "Số ngày tính lương ");
            // generate header
            Row row1 = sheet.createRow(rowIndex.get()-1);
            Integer countDayInMonth = DataUtil.countDayInMonth(month);
            for (int i = rowIndex.get(); i < countDayInMonth + header.size() + rowIndex.get() + 1; i++) {
                Cell cellheader = row1.createCell(i);
                if(i <  countDayInMonth + rowIndex.get() + 1){
                    cellheader.setCellStyle(styleHeader);
                    cellheader.setCellValue(i-rowIndex.get());
                }else{
                    if((i - countDayInMonth - rowIndex.get() - 1) == 2 || (i - countDayInMonth - rowIndex.get() - 1) == 3){
                        cellheader.setCellStyle(styleHeaderRed);
                    }else{
                        cellheader.setCellStyle(styleHeaderYellow);
                    }
                    cellheader.setCellValue(header.get(i - countDayInMonth - rowIndex.get() - 1));
                }
            }

            dataUser.forEach((department1, listUserDept) -> {
                 listUserDept.forEach(user -> {
                     List<RequestDTOResponse> requestDTOResponseDayOffFilterUser = requestInMonth.stream().
                             filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)).
                             filter(requestDTOResponse -> requestDTOResponse.getCreatedBy().equals(user.getUserId())).collect(Collectors.toList());
                     List<RequestDTOResponse> requestDTOResponseRemoteFilterUser = requestInMonth.stream().
                             filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE)).
                             filter(requestDTOResponse -> requestDTOResponse.getCreatedBy().equals(user.getUserId())).collect(Collectors.toList());
                     Row row = sheet.createRow(rowIndex.getAndIncrement());
                     XSSFCellStyle styleRandom = listStyle.get(countDept.get()%listStyle.size());

                     Cell cellStt = row.createCell(0);
                     cellStt.setCellStyle(styleRandom);
                     cellStt.setCellValue(stt.getAndIncrement());

                     Cell cellDeptName = row.createCell(1);
                     cellDeptName.setCellStyle(styleRandom);
                     cellDeptName.setCellValue(department1.getDepartmentName());


                     Cell cellUserFullname = row.createCell(2);
                     cellUserFullname.setCellStyle(styleRandom);
                     cellUserFullname.setCellValue(user.getFullName());
                     for (int i = 3; i < countDayInMonth + 9; i++) {
                         Cell cellData = row.createCell(i);
                         if(i == countDayInMonth + 3){
                             cellData.setCellStyle(styleGray);
                             cellData.setCellValue(user.getStaticTimekeeping().getCountTimekeeping());
                         } else if(i == countDayInMonth + 4){
                             cellData.setCellStyle(styleGray);
                             cellData.setCellValue(user.getStaticTimekeeping().getCountLate());
                         } else if(i == countDayInMonth + 5){
                             cellData.setCellStyle(styleGrayRed);
                             cellData.setCellValue(user.getStaticTimekeeping().getCountCutPaidMinus());
                         } else if(i == countDayInMonth + 6){
                             cellData.setCellStyle(styleGrayRed);
                             cellData.setCellValue(user.getStaticTimekeeping().getCountPaidLeave());
                         } else if(i == countDayInMonth + 7){
                             cellData.setCellStyle(styleGray);
                             cellData.setCellValue(user.getStaticTimekeeping().getCountDayOff());
                         } else if(i == countDayInMonth + 8){
                             cellData.setCellStyle(styleGray);
                             cellData.setCellValue(user.getStaticTimekeeping().getCountPaidTimekeeping());
                         } else {
                             Timekeeping timekeeping = getTimeKeepingBySttInMonthOfUer(String.valueOf(i-2), month, data.get(user.getUserId()));
                             if(timekeeping != null){
                                 int finalI = i;
                                 Date datework = DataUtil.generateDateFromSttInMonth(month, finalI -2);
                                 if(DataUtil.checkIsWeekend(month, i-2) || DataUtil.checkIsHoliday(month, i-2, listHoliday)) {
                                     cellData.setCellStyle(styleHoliday);
                                 }else if(user.getSeveranveDate() != null && datework.after(user.getSeveranveDate())) {
                                     cellData.setCellStyle(styleSeveranve);
                                     cellData.setCellValue("o");
                                 }else{
                                     AtomicBoolean workMorning = new AtomicBoolean(false);
                                     AtomicBoolean workAfternoon = new AtomicBoolean(false);
                                     if(timekeeping.isValidTimekeeping(workTimeInDay, listHoliday)){
                                         if(timekeeping.getArrivalTime().before(workTimeInDay.getWorkTimeEndTimeAm())){
                                             workMorning.set(true);
                                         }
                                         if(timekeeping.getBackTime().after(workTimeInDay.getWorkTimeStartTimePm())){
                                             workAfternoon.set(true);
                                         }
                                     }
                                     List<RequestDTOResponse> requestRemoteDTOResponses = requestDTOResponseRemoteFilterUser.stream()
                                             .filter(requestDTOResponse -> (requestDTOResponse.getRequestWorktimeContents() != null && requestDTOResponse.getRequestWorktimeContents().size() > 0))
                                             .filter(requestDTOResponse -> requestDTOResponse.getRequestWorktimeContents()
                                                             .stream().anyMatch(o -> (DateUtils.isSameDay(o.getRemoteDate(), datework)))).collect(Collectors.toList());
                                     if(requestRemoteDTOResponses.size() > 0){
                                         requestRemoteDTOResponses.forEach(x -> {
                                             if(x.getRequestWorktimeContents().stream().findFirst().get().getRemoteType() == Constant.REMOTE_TYPE.AM){
                                                 workMorning.set(true);
                                             }
                                             if(x.getRequestWorktimeContents().stream().findFirst().get().getRemoteType() == Constant.REMOTE_TYPE.PM){
                                                 workAfternoon.set(true);
                                             }
                                         });
                                     }
                                     List<RequestDTOResponse> requestDayOffDTOResponses = requestDTOResponseDayOffFilterUser.stream()
                                             .filter(requestDTOResponse -> (requestDTOResponse.getRequestWorktimeContents() != null && requestDTOResponse.getRequestWorktimeContents().size() > 0))
                                             .filter(requestDTOResponse ->
                                                     requestDTOResponse.getRequestWorktimeContents().stream()
                                                             .anyMatch(o -> ((o.getDayOffBegin().before(datework) || DateUtils.isSameDay(o.getDayOffBegin(), datework)) && (o.getDayOffEnd().after(datework) || DateUtils.isSameDay(o.getDayOffEnd(), datework))))).collect(Collectors.toList());
                                     if(requestDayOffDTOResponses.size() > 0){
                                         requestDayOffDTOResponses.stream().filter(requestDayOff -> (requestDayOff.getRequestWorktimeContents() != null && requestDayOff.getRequestWorktimeContents().size() > 0)).forEach(x -> {
                                             x.getRequestWorktimeContents().forEach(content -> {
                                                 if((content.getDayOffBegin().before(datework) || DateUtils.isSameDay(content.getDayOffBegin(), datework)) && (content.getDayOffEnd().after(datework) || DateUtils.isSameDay(content.getDayOffEnd(), datework))){
                                                     if(content.getDayOffType().equals(Constant.DAY_OFF_TYPE.ALL)){
                                                         workMorning.set(false);
                                                         workAfternoon.set(false);
                                                     }else if(content.getDayOffType().equals(Constant.DAY_OFF_TYPE.AM)){
                                                         workMorning.set(false);
                                                     }else if(content.getDayOffType().equals(Constant.DAY_OFF_TYPE.PM)){
                                                         workAfternoon.set(false);
                                                     }
                                                 }
                                             });
                                         });
                                     }
                                     cellData.setCellStyle(styleCenter);
                                     if(workMorning.get() && workAfternoon.get()){
                                         cellData.setCellValue(Constant.WORK_SHIFT.ALL_DAY);
                                     }else if(!workMorning.get() && workAfternoon.get()){
                                         cellData.setCellValue(Constant.WORK_SHIFT.AFTERNOON);
                                     }else if(workMorning.get() && !workAfternoon.get()){
                                         cellData.setCellValue(Constant.WORK_SHIFT.MORNING);
                                     }else{
                                         cellData.setCellValue("N");
                                     }
                                 }
                             }
                         }
                     }
                });

                 if(listUserDept.size() > 1){
                     sheet.addMergedRegion(new CellRangeAddress((rowIndex.get() -listUserDept.size()), rowIndex.get()-1,1,1));
                 }
                countDept.getAndIncrement();

            });


            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            workbook.close();
            InputStreamResource inputStream = new InputStreamResource(new ByteArrayInputStream(outputStream.toByteArray()));
            // Tạo Resource từ file Excel trong bộ nhớ tạm
            return inputStream;
        }catch (Exception e){
            throw new RuntimeException(messageSource.getMessage("exportFail", null, locale));
        }
    }

    @Override
    public Map<String, Object> getAllOfUser(String userId, Date month, Date startDate, Date endDate, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        Map<String, Object> data = new HashMap<>();
        List<TimekeepingDTOResponse> list = new ArrayList<>();
        UserDTOResponse user = CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_ID, userId, request);
        if(startDate == null){
            startDate = DataUtil.getFirstDateInMonth(month);
        }
        if(endDate == null){
            endDate = DataUtil.getLastDateInMonth(month);
        }
        List<Holiday> listHoliday = holidayService.getAllHolidayInMonth(month);
        List<Timekeeping> listTimekeeping = timekeepingRepository.findAllByUserIdInAndTimekeepingDateBetweenOrderByTimekeepingDateAsc(Arrays.asList(user.getUserId()), startDate, endDate);
        List<RequestDTOResponse> allRequestInMonth = getAllWorktimeRequestFromErpService(month, user, request);
        WorkTimeInDay workTimeInDay = workTimeInDayService.getWorkTimeInDay(month);
        generateStaticUser(month, user, listTimekeeping, holidayService.countAllHolidayInMonth(month), workTimeInDay,request,holidayService.getAllHolidayInMonth(month), data);
        listTimekeeping.forEach(timekeeping -> {
            list.add(new TimekeepingDTOResponse(timekeeping, workTimeInDay, listHoliday, locale, month, allRequestInMonth));
        });
        data.put("data", list);
        return data;
    }

    @Override
    public RequestDTOResponse updateByRequest(RequestDTOResponse requestEntity, HttpServletRequest request) {
        if(requestEntity.getRequestWorktimeContents() != null && !requestEntity.getRequestWorktimeContents().isEmpty()
                && requestEntity.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.WORK_CONFIMATION)){
            RequestWorktimeContentDTO requestWorktimeContentDTO = requestEntity.getRequestWorktimeContents().iterator().next();

            Timekeeping timekeeping = timekeepingRepository
                    .findByTimekeepingDateAndUserId(DataUtil.formatDate(requestWorktimeContentDTO.getRequestWorkTimeDate()), requestEntity.getCreatedBy())
                    .orElseThrow(
                            () -> new RuntimeException(messageSource.getMessage("notFound", null, CommonUtil.getLocale()))
                    );
            if(requestWorktimeContentDTO.getRequestWorkTimeCheckout() != null){
                timekeeping.setBackTime(requestWorktimeContentDTO.getRequestWorkTimeCheckout());
            }
            if(requestWorktimeContentDTO.getRequestWorkTimeCheckin() != null){
                timekeeping.setArrivalTime(requestWorktimeContentDTO.getRequestWorkTimeCheckin());
            }
            timekeepingRepository.save(timekeeping);
        }else if(requestEntity.getRequestWorktimeContents() != null && !requestEntity.getRequestWorktimeContents().isEmpty()
                && requestEntity.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)){
            //tính lại số ngày phép của user
            //update vào content số ngày nghỉ được tính lương
            AvailableDayOff availableDayOff = availableDayOffRepository.findFirstByUserIdOrderByMonthDescYearDesc(requestEntity.getCreatedBy());
            AtomicReference<Double> countDayOffRequest = new AtomicReference<>((double) 0);
            if(availableDayOff != null){
                Double countDayOffCurrent = availableDayOff.getAvailableDayOffOld() + availableDayOff.getAvailableDayOff();
                requestEntity.getRequestWorktimeContents().forEach(requestDayOffContents -> {
                    if(!requestDayOffContents.getRequestDayOffType().equals(Constant.REQUEST_DAY_OFF_TYPE.PAID_LEAVE)){
                        requestDayOffContents.setCountDaySalary(0D);
                    }else{
                        List<Date> dateHolidayInYearCurrent = holidayService.getAllHolidayBetween(requestDayOffContents.getDayOffBegin(),requestDayOffContents.getDayOffEnd())
                                .stream().map(Holiday::getHolidayDate).collect(Collectors.toList());
                        double multiplier = requestDayOffContents.getDayOffType() == 0 ? 1.0 : 0.5;
                        Double countDayOffRequestContent = multiplier * (calculateDaysBetween(requestDayOffContents.getDayOffBegin(), requestDayOffContents.getDayOffEnd()) + 1)
                                //trừ ngày cuối tuần
                                - DataUtil.countWeekendDays(requestDayOffContents.getDayOffBegin(),requestDayOffContents.getDayOffEnd())
                                //trừ ngày nghỉ lễ
                                - DataUtil.countDatesInRange(dateHolidayInYearCurrent,requestDayOffContents.getDayOffBegin(),requestDayOffContents.getDayOffEnd());
                        requestDayOffContents.setCountDaySalary(countDayOffRequestContent);
                        countDayOffRequest.updateAndGet(v -> v + countDayOffRequestContent);
                    }
                });
                CallOtherService.updateRequestWorktimeContents(requestEntity.getRequestWorktimeContents(), request);
                if(countDayOffRequest.get() <= availableDayOff.getAvailableDayOffOld()){
                    availableDayOff.setAvailableDayOffOld(availableDayOff.getAvailableDayOffOld() - countDayOffRequest.get());
                    requestEntity.setDayOffLastYear(availableDayOff.getAvailableDayOffOld() - countDayOffRequest.get());
                    requestEntity.setDayOffCurrentYear(0D);
                }else if(countDayOffRequest.get() > availableDayOff.getAvailableDayOffOld() && countDayOffRequest.get() < countDayOffCurrent){
                    availableDayOff.setAvailableDayOffOld(0D);
                    availableDayOff.setAvailableDayOff(countDayOffCurrent - countDayOffRequest.get());
                    requestEntity.setDayOffLastYear(0D);
                    requestEntity.setDayOffCurrentYear(countDayOffCurrent - countDayOffRequest.get());
                }else if(countDayOffRequest.get() >= countDayOffCurrent){
                    availableDayOff.setAvailableDayOffOld(0D);
                    availableDayOff.setAvailableDayOff(0D);
                    requestEntity.setDayOffLastYear(0D);
                    requestEntity.setDayOffCurrentYear(0D);
                }
                availableDayOffRepository.save(availableDayOff);
            }else{
                throw new RuntimeException(messageSource.getMessage("validate.avaiable_valid", null, CommonUtil.getLocale()));
            }
        }else{
            throw new RuntimeException(messageSource.getMessage("validate.invalid", null, CommonUtil.getLocale()));
        }
        return requestEntity;
    }

    @Transactional
    public Date readDataFromExcel(MultipartFile file, List<UserDTOResponse> listUser, List<RequestDTOResponse> allRequestInMonth) {
        List<RequestDTOResponse> listRequestWorktimeConfirm = allRequestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.WORK_CONFIMATION)).collect(Collectors.toList());
//        List<RequestDTOResponse> listRequestRemote = allRequestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE)).collect(Collectors.toList());
        try{
        InputStream inputStream = file.getInputStream();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.US);
        Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet sheet = workbook.getSheetAt(0); // Assuming you are reading the first sheet
            Date monthInExcel = formatDate.parse(getCellValueAsString(sheet.getRow(1).getCell(3)));
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                //check row trống | thừa dữ liệu
                if (row.getLastCellNum() > Constant.NUMBER_CELL_TIMEKEEPING) {
                    throw new RuntimeException(messageSource.getMessage("excel.error.redundant", new Object[]{i + 1}, CommonUtil.getLocale()));
                } else {
                    for (int j = 0; j < Constant.NUMBER_CELL_TIMEKEEPING; j++) {
                        if (j == Constant.CELL_BACK_TIME || j == Constant.CELL_ARRIVAL_TIME || j == Constant.CELL_NOTE)
                            continue;
                        if (getCellValueAsString(row.getCell(j)).equals("")) {
                            throw new RuntimeException(messageSource.getMessage("excel.error.emptyRow", new Object[]{i + 1, j + 1}, CommonUtil.getLocale()));
                        }
                    }
                }
                //check giờ vào trước giờ ra
                if (!getCellValueAsString(row.getCell(Constant.CELL_ARRIVAL_TIME)).equals("")
                        && !getCellValueAsString(row.getCell(Constant.CELL_BACK_TIME)).equals("")
                        && formatDate.parse(getCellValueAsString(row.getCell(Constant.CELL_ARRIVAL_TIME)))
                        .after(formatDate.parse(getCellValueAsString(row.getCell(Constant.CELL_BACK_TIME))))) {
                    throw new RuntimeException(messageSource.getMessage("excel.error.timeAttendance", new Object[]{i + 1}, CommonUtil.getLocale()));
                }
                Timekeeping timekeeping = new Timekeeping();
                String userId = listUser.stream()
                        .filter(user -> Objects.equals(user.getAttendanceCode(), new Double(Double.parseDouble(getCellValueAsString(row.getCell(1)))).intValue()))
                        .findFirst()
                        .orElse(new UserDTOResponse())
                        .getUserId();
                if(userId != null){
                    timekeeping.setUserId(userId);
                    try {
                        timekeeping.setTimekeepingDate(formatDate.parse(getCellValueAsString(row.getCell(3))));
                    } catch (java.text.ParseException e) {
                        timekeeping.setTimekeepingDate(new Date());
                    }
                    if(!DataUtil.isSameMonth(monthInExcel, timekeeping.getTimekeepingDate())){
                        throw new RuntimeException(messageSource.getMessage("timekeeping.errorSameMonth", null,  CommonUtil.getLocale()));
                    }
                    try {
                        timekeeping.setArrivalTime(formatDate.parse(getCellValueAsString(row.getCell(6))));
                    } catch (java.text.ParseException e) {
                        if(!Objects.equals(getCellValueAsString(row.getCell(6)), "")){
                            timekeeping.setArrivalTime(new Date());
                        }
                    }
                    try {
                        timekeeping.setBackTime(formatDate.parse(getCellValueAsString(row.getCell(7))));
                    } catch (java.text.ParseException e) {
                        if(!Objects.equals(getCellValueAsString(row.getCell(7)), "")){
                            timekeeping.setBackTime(new Date());
                        }
                    }
                    Optional<Timekeeping> oldData = timekeepingRepository.findByTimekeepingDateAndUserId(timekeeping.getTimekeepingDate(), timekeeping.getUserId());
                    if(oldData.isPresent()){
                        timekeeping.setTimekeepingId(oldData.get().getTimekeepingId());
                    }

//                    RequestDTOResponse requestRemote = listRequestRemote.stream()
//                            .filter(requestDTOResponse -> requestDTOResponse.getCreatedBy().equals(userId))
//                            .filter(requestDTOResponse -> requestDTOResponse.getRequestWorktimeContents() != null && requestDTOResponse.getRequestWorktimeContents().size() > 0)
//                            .filter(requestDTOResponse ->
//                                    requestDTOResponse.getRequestWorktimeContents()
//                                            .stream()
//                                            .filter(requestWorkConfirmContentDTO -> DateUtils.isSameDay(requestWorkConfirmContentDTO.getRemoteDate(), timekeeping.getTimekeepingDate()))
//                                            .count() > 0).findFirst().orElse(null);

                    RequestDTOResponse requestWorktimeConfirm = listRequestWorktimeConfirm.stream()
                            .filter(requestDTOResponse -> requestDTOResponse.getCreatedBy().equals(userId))
                            .filter(requestDTOResponse -> requestDTOResponse.getRequestWorktimeContents() != null && requestDTOResponse.getRequestWorktimeContents().size() > 0)
                            .filter(requestDTOResponse ->
                                    requestDTOResponse.getRequestWorktimeContents()
                                            .stream()
                                            .filter(requestWorkConfirmContentDTO -> DateUtils.isSameDay(requestWorkConfirmContentDTO.getRequestWorkTimeDate(), timekeeping.getTimekeepingDate()))
                                            .count() > 0).findFirst().orElse(null);
                    if(requestWorktimeConfirm != null){
                        if(requestWorktimeConfirm.getRequestWorktimeContents().iterator().next().getRequestWorkTimeCheckout() != null){
                            timekeeping.setBackTime(requestWorktimeConfirm.getRequestWorktimeContents().iterator().next().getRequestWorkTimeCheckout());
                        }
                        if(requestWorktimeConfirm.getRequestWorktimeContents().iterator().next().getRequestWorkTimeCheckin() != null){
                            timekeeping.setArrivalTime(requestWorktimeConfirm.getRequestWorktimeContents().iterator().next().getRequestWorkTimeCheckin());
                        }
                    }
                    timekeepingRepository.save(timekeeping);
                }else {
                    throw new RuntimeException(messageSource.getMessage("excel.error.attendanceCode",new Object[]{i + 1},CommonUtil.getLocale()));
                }
            }
            return monthInExcel;
        }catch (IOException e){
            throw new RuntimeException(messageSource.getMessage("jsonConvertExcelFail", null, CommonUtil.getLocale()));
        }
        catch (ParseException e){
            throw new RuntimeException(messageSource.getMessage("jsonConvertExcelFail", null, CommonUtil.getLocale()));
        }
    }

    private String getCellValueAsString(Cell cell) {
        if(Objects.isNull(cell))
            return "";
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue().trim();
            case NUMERIC:
                return String.valueOf(cell.getNumericCellValue());
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            default:
                return "";
        }
    }

    private Timekeeping getTimeKeepingBySttInMonthOfUer(String sttOfDate, Date date,  List<Timekeeping> data){
        try{
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int year = localDate.getYear();
            Month month = localDate.getMonth();
            LocalDate localDate1 = LocalDate.of(year, month, Integer.parseInt(sttOfDate));
            Date dateWork = Date.from(localDate1.atStartOfDay(ZoneId.systemDefault()).toInstant());
            return data.stream()
                    .filter(timekeeping1 -> timekeeping1.getTimekeepingDate().compareTo(dateWork) == 0)
                    .findFirst()
                    .orElse(new Timekeeping());
        }catch (Exception  e){
            return new Timekeeping();
        }
    }
    
    private List<RequestDTOResponse> getAllWorktimeRequestFromErpService(Date month, UserDTOResponse user, HttpServletRequest request) {
        Map<String,Object> requestParamCountDayOff = new HashMap<>();
        requestParamCountDayOff.put("status", Constant.REQUEST_STATUS.APPROVED);
        if(month != null){
            requestParamCountDayOff.put("worktimeFromDate",formatToYYYYMMDD(DataUtil.getFirstDateInMonth(month)));
            requestParamCountDayOff.put("worktimeToDate",formatToYYYYMMDD(DataUtil.getLastDateInMonth(month)));
        }
        List<Integer> listProcessType = Arrays.asList(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE, Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE, Constant.PROCESS_TYPE.REGISTER_FOR_OT, Constant.PROCESS_TYPE.WORK_CONFIMATION);
        requestParamCountDayOff.put("processType",listProcessType.stream().map(Object::toString).collect(Collectors.joining(",")));
        if(user != null){
            requestParamCountDayOff.put("userId", user.getUserId());
        }
        return CallOtherService.getAllFromIdService(RequestDTOResponse.class, GET_ALL_REQUEST, requestParamCountDayOff, request);
    }

    private List<RequestDTOResponse> getAllWorktimeRequestFromErpServiceSchedule(Date month, UserDTOResponse user) {
        Map<String,Object> requestParamCountDayOff = new HashMap<>();
        requestParamCountDayOff.put("status", Constant.REQUEST_STATUS.APPROVED);
        if(month != null){
            requestParamCountDayOff.put("worktimeFromDate",formatToYYYYMMDD(DataUtil.getFirstDateInMonth(month)));
            requestParamCountDayOff.put("worktimeToDate",formatToYYYYMMDD(DataUtil.getLastDateInMonth(month)));
        }
        List<Integer> listProcessType = Arrays.asList(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE, Constant.PROCESS_TYPE.REGISTER_FOR_OT ,Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE ,Constant.PROCESS_TYPE.WORK_CONFIMATION);
        requestParamCountDayOff.put("processType",listProcessType.stream().map(Object::toString).collect(Collectors.joining(",")));
        if(user != null){
            requestParamCountDayOff.put("userId", user.getUserId());
        }
        return CallOtherService.getAllFromIdService(RequestDTOResponse.class, Constant.PATH.GET_REQUEST_SCHEDULE, requestParamCountDayOff);
    }

    @Override
    public TimekeepingDTOResponse getTimekeepingUser(String userId, Date date, HttpServletRequest request){
        TimekeepingDTOResponse timekeepingDTOResponse = new TimekeepingDTOResponse();
        Timekeeping timekeeping = timekeepingRepository
                .findByTimekeepingDateAndUserId(DataUtil.formatDate(date), userId)
                .orElseThrow(
                        () -> new RuntimeException(messageSource.getMessage("notFound", null, CommonUtil.getLocale()))
                );
        BeanUtils.copyProperties(timekeeping, timekeepingDTOResponse);
        return timekeepingDTOResponse;
    }

    @Override
    public void backTimekeeping(TimekeepingDTO timekeepingDTO, HttpServletRequest request){
        Optional<Timekeeping> timekeeping = timekeepingRepository.findByTimekeepingDateAndUserId(timekeepingDTO.getTimekeeppingDate(), timekeepingDTO.getUserId());
        if(timekeeping.isPresent()){
            Timekeeping timekeepingSave = timekeeping.get();
            timekeepingSave.setArrivalTime(timekeepingDTO.getArrivalTime());
            timekeepingSave.setBackTime(timekeepingDTO.getBackTime());
            timekeepingRepository.save(timekeepingSave);
        }else{
            throw new RuntimeException(messageSource.getMessage(Constant.TIMEKEEPING, null, CommonUtil.getLocale()) + " " + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));
        }
    }

}
