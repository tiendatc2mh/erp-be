package com.blameo.worktimeservice.service.impl;

import com.blameo.worktimeservice.model.WorkTimeInDay;
import com.blameo.worktimeservice.repository.WorkTimeInDayRepository;
import com.blameo.worktimeservice.service.WorkTimeInDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class WorkTimeInDayServiceImpl implements WorkTimeInDayService {

    @Autowired
    WorkTimeInDayRepository workTimeInDayRepository;

    @Override
    public WorkTimeInDay getWorkTimeInDay(Date date) {
        try{
            return workTimeInDayRepository.findByWorkTimeEndApplyIsAfterAndAndWorkTimeStartApplyIsBeforeAndType(date, date,1);
        }catch (Exception e){
            return new WorkTimeInDay();
        }

    }
}
