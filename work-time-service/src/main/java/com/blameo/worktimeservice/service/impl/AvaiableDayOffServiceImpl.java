package com.blameo.worktimeservice.service.impl;

import com.blameo.worktimeservice.dto.request.AvailableOffDTO;
import com.blameo.worktimeservice.dto.response.AvaiableDayResponse;
import com.blameo.worktimeservice.model.AvailableDayOff;
import com.blameo.worktimeservice.modelUser.UserDTOResponse;
import com.blameo.worktimeservice.repository.AvailableDayOffRepository;
import com.blameo.worktimeservice.service.AvaiableDayOffService;
import com.blameo.worktimeservice.utils.CommonUtil;
import com.blameo.worktimeservice.utils.CoppyObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@Service
public class AvaiableDayOffServiceImpl implements AvaiableDayOffService {

    @Autowired
    AvailableDayOffRepository availableDayOffRepository;

    @Override
    public AvaiableDayResponse getAll(HttpServletRequest httpRequest) {
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
        AvailableDayOff availableDayOff = availableDayOffRepository.findFirstByUserIdOrderByMonthDescYearDesc(userDTOResponse.getUserId());
        AvaiableDayResponse avaiableDayResponse = new AvaiableDayResponse();
        if(availableDayOff != null){
            CoppyObject.copyNonNullProperties(availableDayOff, avaiableDayResponse);
        }
        return avaiableDayResponse;
    }

    @Override
    public void updateDayOffFromERP(AvailableOffDTO availableOffDTO, HttpServletRequest request){
        //update lại số ngày nghỉ phép còn lại của user đó khi phiếu nghỉ phép bị huỷ
        AvailableDayOff availableDayOff = availableDayOffRepository.findFirstByUserIdOrderByMonthDescYearDesc(availableOffDTO.getUserId());
        availableDayOff.setAvailableDayOffOld(availableDayOff.getAvailableDayOffOld() + availableOffDTO.getDayOffLastYear());
        availableDayOff.setAvailableDayOff(availableDayOff.getAvailableDayOff() + availableOffDTO.getDayOffCurrentYear());
        availableDayOffRepository.save(availableDayOff);
    }
}
