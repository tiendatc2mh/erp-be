package com.blameo.worktimeservice.service;

import com.blameo.worktimeservice.model.Holiday;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public interface HolidayService {
    Long countAllHolidayInMonth(Date date);
    List<Holiday> getAllHolidayInMonth(Date date);
    List<Holiday> getAllHolidayBetween(Date startDate, Date endDate);
}
