package com.blameo.worktimeservice.service;

import io.minio.*;
import io.minio.errors.*;
import io.minio.messages.VersioningConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 5/9/2023, Tuesday
 **/
@Component
public class MinIOService {


    @Value("${minio.bucket.name}")
    String bucketName;
    private final MinioClient minioClient;

    public MinIOService(MinioClient minioClient) {
        this.minioClient = minioClient;
    }

    public ObjectWriteResponse uploadFile(MultipartFile file, String bucketName, String objectName)
            throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, InsufficientDataException, ErrorResponseException {
        if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
            minioClient.setBucketPolicy(SetBucketPolicyArgs.builder().bucket(bucketName)
                    .config("{\n" +
                            "    \"Version\": \"2012-10-17\",\n" +
                            "    \"Statement\": [\n" +
                            "        {\n" +
                            "            \"Effect\": \"Allow\",\n" +
                            "            \"Principal\": {\n" +
                            "                \"AWS\": [\n" +
                            "                    \"*\"\n" +
                            "                ]\n" +
                            "            },\n" +
                            "            \"Action\": [\n" +
                            "                \"s3:GetObject\"\n" +
                            "            ],\n" +
                            "            \"Resource\": [\n" +
                            "                \"arn:aws:s3:::"+bucketName+"/*\"\n" +
                            "            ]\n" +
                            "        }\n" +
                            "    ]\n" +
                            "}").build());
            minioClient.setBucketVersioning(SetBucketVersioningArgs.builder().bucket(bucketName).config(new VersioningConfiguration(VersioningConfiguration.Status.ENABLED,false)).build());
        }
        PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                .bucket(bucketName)
                .object(objectName)
                .stream(file.getInputStream(), file.getSize(), -1)
                .contentType(file.getContentType())
                .build();
        ObjectWriteResponse response = minioClient.putObject(putObjectArgs);
        return response;
    }

}
