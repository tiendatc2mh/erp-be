package com.blameo.worktimeservice.service;

import com.blameo.worktimeservice.dto.request.AvailableOffDTO;
import com.blameo.worktimeservice.dto.response.AvaiableDayResponse;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public interface AvaiableDayOffService {

    AvaiableDayResponse getAll(HttpServletRequest httpRequest);
    void updateDayOffFromERP(AvailableOffDTO availableOffDTO, HttpServletRequest request);
}
