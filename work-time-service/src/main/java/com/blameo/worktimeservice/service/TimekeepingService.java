package com.blameo.worktimeservice.service;

import com.blameo.worktimeservice.dto.request.TimekeepingDTO;
import com.blameo.worktimeservice.dto.response.RequestDTOResponse;
import com.blameo.worktimeservice.dto.response.TimekeepingDTOResponse;
import com.blameo.worktimeservice.modelErp.Request;
import com.blameo.worktimeservice.modelUser.UserDTOResponse;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public interface TimekeepingService {
    Date importExcel(HttpServletRequest request, MultipartFile file) throws IOException;

    Map<String, Object> getStaticAllUser(Integer page, Integer pageSize, String keyword,String userCode, String department, Date month, HttpServletRequest request);
    InputStreamResource export(String keyword, String userCode, String department, Date month, HttpServletRequest request);

    Map<String, Object> getAllOfUser(String userId, Date month, Date startDate, Date endDate, HttpServletRequest request);

    RequestDTOResponse updateByRequest(RequestDTOResponse requestEntity, HttpServletRequest request);

    TimekeepingDTOResponse getTimekeepingUser(String userId, Date date, HttpServletRequest request);

    void backTimekeeping(TimekeepingDTO requestEntity, HttpServletRequest request);

}
