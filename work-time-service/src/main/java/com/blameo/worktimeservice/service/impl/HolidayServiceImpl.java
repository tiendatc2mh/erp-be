package com.blameo.worktimeservice.service.impl;

import com.blameo.worktimeservice.model.Holiday;
import com.blameo.worktimeservice.repository.HolidayRepository;
import com.blameo.worktimeservice.service.HolidayService;
import com.blameo.worktimeservice.utils.DataUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Transactional
public class HolidayServiceImpl implements HolidayService {

    @Autowired
    HolidayRepository holidayRepository;
    @Override
    public Long countAllHolidayInMonth(Date date) {
        try{
            return holidayRepository.countHolidayByHolidayDateBetween(DataUtil.getFirstDateInMonth(date), DataUtil.getLastDateInMonth(date));
        }catch (Exception e){
            return 0L;
        }
    }

    @Override
    public List<Holiday> getAllHolidayInMonth(Date date) {
        try{
            return holidayRepository.getAllByHolidayDateBetween(DataUtil.getFirstDateInMonth(date), DataUtil.getLastDateInMonth(date));
        }catch (Exception e){
            return new ArrayList<>();
        }
    }

    @Override
    public List<Holiday> getAllHolidayBetween(Date startDate, Date endDate) {
        try{
            return holidayRepository.getAllByHolidayDateBetween(startDate, endDate);
        }catch (Exception e){
            return new ArrayList<>();
        }
    }
}
