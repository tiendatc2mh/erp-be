package com.blameo.worktimeservice.service;

import com.blameo.worktimeservice.model.WorkTimeInDay;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public interface WorkTimeInDayService {

    WorkTimeInDay getWorkTimeInDay(Date date);
}
