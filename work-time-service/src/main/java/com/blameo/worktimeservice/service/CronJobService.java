package com.blameo.worktimeservice.service;

import com.blameo.worktimeservice.constant.Constant;
import com.blameo.worktimeservice.controller.CallOtherService;
import com.blameo.worktimeservice.dto.response.TimekeepingUser;
import com.blameo.worktimeservice.model.AvailableDayOff;
import com.blameo.worktimeservice.modelUser.UserDTOResponse;
import com.blameo.worktimeservice.repository.AvailableDayOffRepository;
import com.blameo.worktimeservice.service.impl.TimekeepingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Configuration
public class CronJobService {

    @Autowired
    AvailableDayOffRepository availableDayOffRepository;

    @Autowired
    TimekeepingServiceImpl timekeepingService;

    @Scheduled(cron = "0 0 0 1 * ?")
    public void runTaskUpdateHolidaysNew() {
        //Mỗi tháng nếu user làm trên 14 ngày công thì sẽ cộng thêm 1 ngày phép cho user đó
        //Tháng 12 sẽ chuyển ngày công mới này sang ngày công cũ và đưa ngày công mới về 0
        LocalDate localDate = LocalDate.now().minusDays(1);
        List<UserDTOResponse> listUser = CallOtherService.getAllFromIdService(UserDTOResponse.class, Constant.PATH.GET_ALL_USER_NO_AUTHEN);
        Map<String, Object> map = timekeepingService.getTimekeepingUser(listUser);
        List<TimekeepingUser> timekeepingUsers = (List<TimekeepingUser>) map.get("data");
        List<AvailableDayOff> availableDayOffs = new ArrayList<>();
        listUser.forEach(item -> {
            AvailableDayOff availableDayOffLastMonth = availableDayOffRepository.findFirstByUserIdAndMonthAndYear(item.getUserId(), localDate.getMonth().getValue(), localDate.getYear());
            AvailableDayOff availableDayOffNew = new AvailableDayOff();
            TimekeepingUser timekeepingUser = timekeepingUsers.stream().filter(x -> x.getUserId().equals(item.getUserId())).findFirst().orElse(new TimekeepingUser());
            //nếu là tháng 3
            Double dayOffOldLastMonth = 0D;
            Double dayOffLastMonth = 0D;
            if(availableDayOffLastMonth != null && availableDayOffLastMonth.getAvailableDayOffOld() != null){
                dayOffOldLastMonth = availableDayOffLastMonth.getAvailableDayOffOld();
            }else if(availableDayOffLastMonth != null && availableDayOffLastMonth.getAvailableDayOff() != null){
                dayOffLastMonth = availableDayOffLastMonth.getAvailableDayOff();
            }
            //nếu tháng đó số ngày công lớn hơn hoặc bằng 14 ngày thì cộng thêm 1 ngày
            if(timekeepingUser.getCountHolidays() >= 14){
                dayOffLastMonth += 1;
            }

            availableDayOffNew.setUserId(item.getUserId());
            availableDayOffNew.setMonth(LocalDate.now().getMonth().getValue());
            availableDayOffNew.setYear(LocalDate.now().getYear());

            if(LocalDate.now().getMonth().getValue() == 1){
                availableDayOffNew.setAvailableDayOffOld(dayOffLastMonth);
                availableDayOffNew.setAvailableDayOff(0D);
            }else if(LocalDate.now().getMonth().getValue() == 4){
                availableDayOffNew.setAvailableDayOffOld(0D);
                availableDayOffNew.setAvailableDayOff(dayOffLastMonth);
            }else{
                availableDayOffNew.setAvailableDayOffOld(dayOffOldLastMonth);
                availableDayOffNew.setAvailableDayOff(dayOffLastMonth);
            }

            availableDayOffs.add(availableDayOffNew);
        });
        availableDayOffRepository.saveAll(availableDayOffs);
    }
}
