package com.blameo.worktimeservice.constant;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

public class Constant {
    private Constant() {
        throw new IllegalStateException("Utility class");
    }
    public static final Integer NUMBER_CELL_TIMEKEEPING = 13;
    public static final Integer CELL_ARRIVAL_TIME = 6;
    public static final Integer CELL_BACK_TIME = 7;
    public static final Integer CELL_NOTE = 12;

    //Không được bỏ ROLE_
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_SUPER_ADMIN = "SUPER_ADMIN";
    public static final String ROLE_USER = "USER";

    public static final String ASC = "asc";
    public static final String DESC = "desc";
    public static final int OFFICE_FILE = 1;
    public static final int IMAGE_FILE = 2;
    public static final int IMAGE_PDF_FILE = 3;
    public static final String TEMPLATE_USER = "Danh sách nhân sự.xlsx";
    public static final String TEMPLATE_ASSET = "Kiểm kê công cụ tài sản.xlsx";
    public static final String TEMPLATE_STATIC_TIMEKEEPING = "Bảng chấm Công Phép 2023.xlsx";

    public static final String ADMIN_EMAIL = "admin@blameo.com";
    public static final String SUPER_ADMIN_ROLE = "SUPER_ADMIN";
    public static final String DEFAULT_PASSWORD = "123@blameo";
    public static final String KEY = "Department ";
    public static final String UNKNOWN = "unknownError";
    public static final String IS_EXIST = "isExist";
    public static final String CREATE_SUCCESS = "createSuccess";
    public static final String UPDATE_SUCCESS = "updateSuccess";
    public static final String DELETE_SUCCESS = "updateSuccess";
    public static final String NOT_EXIST = "notExist";
    public static final String IS_USE = "inUse";
    public static final String GET_SUCCESS = "getDataSuccess";
    public  static final String KEY_HEADER_USER = "user";
    public  static final String TIMEKEEPING = "timekeeping";

    public static final Integer ACTIVE = 1;
    public static final Integer INACTIVE = 0;

    public static final Integer DELETED = -1;

    public static final String REGEX_CODE = "^[A-Z0-9-_]+$";
    public static final String REGEX_PASSWORD = "^[a-zA-Z0-9!@#$%^&*()-_=+]$";
    public static final String REGEX_ALPHA_NUMBER = "^[a-zA-Z0-9]+$";
    public static final String REGEX_ALPHA_NUMBER_QUOTA = "^[a-zA-Z0-9]*$";
    public static final String REGEX_VIETNAMESE_RESIDENCE = "^[0-9a-zA-ZÀ-ỹ\\/\\- ]*$";
    public static final String REGEX_VIETNAMESE = "^[0-9a-zA-ZÀ-ỹ ]*$";
    public static final String REGEX_PHONE_NUMBER = "0[0-9]{7,9}";
    public static final String REGEX_NUMBER = "^[0-9]*$";

    public static class ASSET_LOG_ACTION {
        public static final Integer ASSIGN = 1;
        public static final Integer RETRIEVE = 2;
        public static final Integer BOUGHT = 3;
    }

    public static class PATH {
        public static final String GET_ALL_USER = "/api/id/user/all";
        public static final String GET_ALL_USER_NO_AUTHEN = "/api/id/user/schedule";
        public static final String GET_REQUEST_SCHEDULE = "/api/erp/request/schedule";
        public static final String GET_USER_WITH_PAGING = "/api/id/user";
        public static final String GET_USER_BY_ID = "/api/id/user/";
        public static final String GET_ALL_REQUEST = "/api/erp/request/all";
        public static final String UPDATE_REQUEST_WORKTIME_CONTENTS = "/api/erp/request/worktime-contents";
    }
    public static class WORK_SHIFT {
        public static final String MORNING = "S";
        public static final String AFTERNOON = "C";
        public static final String ALL_DAY = "x";
    }

    public static String getMethodName(int methodNumber) {
        switch (methodNumber) {
            case 1:
                return "GET";
            case 2:
                return "POST";
            case 3:
                return "PUT";
            case 4:
                return "PATCH";
            case 5:
                return "DELETE";
            default:
                throw new RuntimeException("Invalid method number");
        }
    }
    public static class PROCESS_TYPE {
        public static final int REGISTER_FOR_LEAVE = 1; // đăng ký nghỉ phép
        public static final int REGISTER_FOR_OT = 2; // đăng ký OT
        public static final int REGISTER_FOR_REMOTE = 3; // đăng ký remote
        public static final int BUY_EQUIPMENT = 4; // mua trang thiết bị
        public static final int PAYMENT_REQUEST = 5; // đề nghị thanh toán
        public static final int WORK_CONFIMATION = 6; // xác nhận công
        public static final int RECRUITMENT_REQUEST = 7; // đề nghị tuyển dụng
    }

    public static class TIMEKEEPING_TYPE {
        public static final int NO_REMOTE = 0; // không remote
        public static final int REMOTE = 1; // remote
    }

    public static class REQUEST_STATUS {
        public static final Integer UNSENT = 0; // CHƯA GỬI
        public static final Integer WAITING_APPROVAL = 1; // CHỜ PHÊ DUYỆT
        public static final Integer APPROVED = 2; // ĐÃ PHÊ DUYỆT
        public static final Integer REFUSE = 3; // TỪ CHỐI
        public static final Integer DELETE = 4; // XOÁ
    }

    public static class REMOTE_TYPE {
        public static final Integer ALL = 0; // Cả ngày
        public static final Integer AM = 1; // Sáng
        public static final Integer PM = 2; // chiều
    }

    public static class DAY_OFF_TYPE {
        public static final Integer ALL = 0; // Cả ngày
        public static final Integer AM = 1; // Sáng
        public static final Integer PM = 2; // chiều
    }

    public static class REQUEST_DAY_OFF_TYPE {
        public static final int PAID_LEAVE = 1; // nghỉ có lương
        public static final int NO_PAID_LEAVE = 2; // nghỉ không lương
        public static final int MATERNITY_LEAVE = 3; // Nghỉ thai sản (180 ngày)
        public static final int FUNERAL_LEAVE = 4; // Nghỉ hiếu (3 ngày)
        public static final int WEDDING_LEAVE = 5; // Nghỉ kết hôn (bản thân 3 ngày, con đẻ 1 ngày)
    }

    public static class TIMEKEEPING_NOTE {
        public static final Integer ON_TIME = 1; // đúng giờ
        public static final Integer LATE = 2; // đi muộn
        public static final Integer EARLY = 3; // về sớm
        public static final Integer DAY_OFF_WITH_REQUEST_SALARY = 4; // nghỉ có phép có lương
        public static final Integer DAY_OFF_WITHOUT_REQUEST = 5; // nghỉ không phép
        public static final Integer HOLIDAY = 6; // nghỉ lễ
        public static final Integer WEEKEND = 7; // cuối tuần
        public static final Integer REMOTE = 8; // remote

        public static final Integer WORK_TIME_CONFIRM = 9; // xác nhận công

        public static final Integer INVALID_TIMEKEEPING = 10; // chấm công không hợp lệ

        public static final Integer DAY_OFF_WITH_REQUEST_NO_SALARY = 11; // nghỉ có phép không lương lương
    }
}
