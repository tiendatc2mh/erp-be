package com.blameo.worktimeservice.modelErp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Process {
    private String processId;

    private String processName;

    private Integer processStatus;

    private String processDescription;

    private Integer processType;

    private Integer isDefault;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

}
