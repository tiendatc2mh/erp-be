package com.blameo.worktimeservice.modelErp;

import lombok.Data;

import java.util.Date;

@Data
public class RequestWorktimeContent {

    private String requestWorktimeContentId;

    private Date requestWorkTimeDate;

    private Date requestWorkTimeCheckin;

    private Date requestWorkTimeCheckout;

    private Date requestWorkTimeCheckinOld;

    private Date requestWorkTimeCheckoutOld;

    private Integer requestWorkTimeType;

    private Integer requestDayOffType;

    private Integer dayOffType;

    private String requestDayOffUserTransfer;

    private Date dayOffBegin;

    private Date dayOffEnd;

    private int remoteType;

    private Integer requestWorktimeContentStatus;

}
