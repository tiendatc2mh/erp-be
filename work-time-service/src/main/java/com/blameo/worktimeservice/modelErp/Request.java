package com.blameo.worktimeservice.modelErp;

import com.blameo.worktimeservice.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {

    private String requestId;

    private Process process;

    private String content;

    private String reason;

    private Integer requestStatus;

    private String requestCode;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private Set<RequestWorktimeContent> requestWorktimeContents = new HashSet<>();

}
