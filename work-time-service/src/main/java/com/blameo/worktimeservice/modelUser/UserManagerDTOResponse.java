package com.blameo.worktimeservice.modelUser;

import lombok.Data;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 9/12/2023, Tuesday
 **/
@Data
public class UserManagerDTOResponse {
    private String userId;
    private String userCode;
    private String fullName;
}
