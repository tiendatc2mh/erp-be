package com.blameo.worktimeservice.modelUser;

import com.blameo.worktimeservice.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/3/2023, Thursday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class Action extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    private String actionId;

    private String actionCode;

    private String actionName;

    private Integer actionStatus;
}
