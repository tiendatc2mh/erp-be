package com.blameo.worktimeservice.modelUser;

import com.blameo.worktimeservice.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TitleDTOResponse extends BaseEntity {

    @NotNull
    private String titleId;

    @NotNull
    private String titleCode;

    @NotNull
    private String titleName;

    private String titleDescription;

    @NotNull
    private Integer titleStatus;

}
