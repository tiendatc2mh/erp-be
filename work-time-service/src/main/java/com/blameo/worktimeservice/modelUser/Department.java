package com.blameo.worktimeservice.modelUser;

import com.blameo.worktimeservice.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class Department extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    private String departmentId;

    private String departmentName;

    private String departmentCode;

    private String departmentDescription;

    private Integer departmentStatus;

}
