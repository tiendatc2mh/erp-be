package com.blameo.worktimeservice.modelUser;


import com.blameo.worktimeservice.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PermissionDTOResponse extends BaseEntity {

    private String permissionId;

    private String permissionCode;

    private String path;


    private String method;


    private Integer permissionStatus;
}
