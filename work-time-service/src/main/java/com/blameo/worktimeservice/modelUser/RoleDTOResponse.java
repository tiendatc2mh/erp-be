package com.blameo.worktimeservice.modelUser;

import com.blameo.worktimeservice.dto.response.BaseEntityDTOResponse;
import com.blameo.worktimeservice.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleDTOResponse extends BaseEntity {

    private String roleId;


    private String roleName;


    private String roleCode;


    private Integer roleStatus;


    private Set<PermissionDTOResponse> permissions;

}
