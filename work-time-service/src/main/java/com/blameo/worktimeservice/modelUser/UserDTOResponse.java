package com.blameo.worktimeservice.modelUser;

import com.blameo.worktimeservice.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTOResponse extends BaseEntity {
    private String userId;

    private String userCode;

    private String fullName;

    private String avatar;

    private Integer gender;

    private Date birthday;

    private String email;

    private String personalEmail;

    private String phoneNumber;

    private String permanentAddress;

    private String residence;

    private Department department;

    private Position position;

    private Title title;

    private Level level;

    private Integer userType;

    private Date internshipStartDate;

    private Date officialDate;

    private Date severanveDate;

    private Integer employeeStatus;

    private Set<RoleDTOResponse> roles;

    private Set<UserManagerDTOResponse> managers;


    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String identityCard;

    private String placeOfIdentity;

    private Date dateOfIdentity;

    private String taxCode;

    private String socialInsuranceCode;

    private Date probationaryStartDate;

    private Integer attendanceCode;

    private String bankAccountNumber;

    private String bankType;

    private StaticTimekeepingOfMonth staticTimekeeping;

    private Double countHolidaysOld;

    private Double countHolidaysNew;

    @Data
    public static class StaticTimekeepingOfMonth{
        // ngày công thực: Ngày công thực tế đi làm + Remote đã được phê duyệt trong tháng
        String countTimekeeping;

        // Ngày lương: Ngày công thực + Phép trong tháng
        String countPaidTimekeeping;

        // ngày nghỉ lễ trong tháng
        String countHoliday;

        // số giờ OT đã được duyệt trong tháng
        String countOTHour;

        // số buổi đi muộn trong tháng
        String countLate;

        // số ngày nghỉ đã được duyệt trong tháng
        String countDayOff;

        // số phép còn lại: tháng  - tổng số ngày nghỉ đã được duyệt từ đầu năm
        String countPaidLeave;

        // số phút trừ lương: Số phút muộn quá quy định + Số phút về sớm trong tháng
        String countCutPaidMinus;
    }
}
