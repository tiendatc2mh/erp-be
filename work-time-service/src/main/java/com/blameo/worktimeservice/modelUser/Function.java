package com.blameo.worktimeservice.modelUser;

import com.blameo.worktimeservice.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/3/2023, Thursday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class Function extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    private String functionId;

    private String functionCode;

    private String functionName;

    private Integer functionStatus;
}
