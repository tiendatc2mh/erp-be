package com.blameo.worktimeservice.modelUser;

import com.blameo.worktimeservice.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/2/2023, Wednesday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class Level extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    private String levelId;

    private String levelName;

    private Department department;

    private Double coefficients;

    private String levelDescription;

    private Integer levelStatus;

    private String levelCode;
}
