package com.blameo.worktimeservice.modelUser;

import com.blameo.worktimeservice.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class Title extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    private String titleId;

    private String titleCode;

    private String titleName;

    private String titleDescription;

    private Integer titleStatus;

}
