package com.blameo.worktimeservice.modelUser;

import com.blameo.worktimeservice.model.BaseEntity;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class Position extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    private String positionId;

    private String positionCode;

    private String positionName;

    private Department department;

    private String positionDescription;

    private Integer positionStatus;

}
