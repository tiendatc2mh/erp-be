package com.blameo.worktimeservice.utils;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.regex.Pattern;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 5/10/2023, Wednesday
 **/
public class FileUtil {
    public final static String EXT_PDF = "pdf";
    public final static String EXT_OFFICE = "xls,xlsx,doc,docx";
    public final static String EXT_DATAFILE = "xls,xlsx";
    public final static String EXT_IMAGE = "jpeg,jpg,png";
    public final static Pattern PATTERN = Pattern.compile("^[a-zA-Z0-9-_]+$");

    public static Boolean checkOfficeFile(MultipartFile file) {
        String nameFile = file.getOriginalFilename();
        if (EXT_DATAFILE.contains(FilenameUtils.getExtension(nameFile.toLowerCase()))) {
            return true;
        }
        return false;
    }

    public static Boolean checkFileImage(MultipartFile file) {
        String nameFile = file.getOriginalFilename();
        if (EXT_IMAGE.contains(FilenameUtils.getExtension(nameFile))) {
            return true;
        }
        return false;
    }
    public static Boolean checkFileImagePDF(MultipartFile file) {
        String nameFile = file.getOriginalFilename();
        if (EXT_IMAGE.contains(FilenameUtils.getExtension(nameFile))||EXT_PDF.contains(FilenameUtils.getExtension(nameFile))) {
            return true;
        }
        return false;
    }
}
