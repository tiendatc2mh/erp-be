package com.blameo.worktimeservice.utils;

import com.blameo.worktimeservice.model.Holiday;
import com.blameo.worktimeservice.model.WorkTimeInDay;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataUtil {

    public static String makeLikeQuery(String s) throws UnsupportedEncodingException {
        if (StringUtils.isEmpty(s)) return null;
        s = URLDecoder.decode(s, "UTF-8");
        s =
            s
                .trim()
                .toLowerCase()
                .replace("!", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "!")
                .replace("%", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "%")
                .replace("_", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "_");
        return  s ;
    }

    public static List<Long> parseMultiValueRequestParam(String param) {
        List<Long> listParam = new ArrayList<>();
        try{
            if(param.contains(",")){
                String[] list = param.split(",");
                for (String s: list) {
                    listParam.add(Long.valueOf(s));
                }
            }else {
                listParam.add(Long.valueOf(param));
            }
        }catch (Exception e){
            return new ArrayList<>();
        }
        return listParam;
    }

    public static List<Date> getAllWeekendInMonth(Date date){
        try{
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int year = localDate.getYear();
            Month month = localDate.getMonth();
            return IntStream.rangeClosed(1,YearMonth.of(year, month).lengthOfMonth())
                    .mapToObj(day -> LocalDate.of(year, month, day))
                    .filter(date1 -> date1.getDayOfWeek() == DayOfWeek.SATURDAY ||
                            date1.getDayOfWeek() == DayOfWeek.SUNDAY)
                    .map(localDate1 -> Date.from(localDate1.atStartOfDay(ZoneId.systemDefault()).toInstant())).collect(Collectors.toList());
        }catch (Exception e){
            return new ArrayList<>();
        }
    }

    public static List<Date> getAllDateBetween(Date fromDate, Date toDate){
        LocalDate dateStart = fromDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate dateEnd = toDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        List<Date> totalDates = new ArrayList<>();
        while (!dateStart.isAfter(dateEnd)) {
            Date date = Date.from(dateStart.atStartOfDay(ZoneId.systemDefault()).toInstant());
            totalDates.add(date);
            dateStart = dateStart.plusDays(1);
        }
        return totalDates;
    }

    public static boolean checkDateBetween(Date minDate, Date maxDate, Date checkDate){
        LocalDate dateStart = minDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate dateEnd = maxDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate date = checkDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return date.isAfter(dateStart) && date.isBefore(dateEnd) || date.isEqual(dateStart) || date.isEqual(dateEnd);
    }

    public static int compareTimesWorkTime(Date d1, Date d2, Date compare){
        int     t1;
        int     t2;
        int     tCompare;
        t1 = (int) (d1.getTime() % (24*60*60*1000L));
        t2 = (int) (d2.getTime() % (24*60*60*1000L));
        tCompare = (int) (compare.getTime() % (24*60*60*1000L));
        if(t1 - tCompare >= 0 && t2 - tCompare >= 0){
            return 1;
        }else if(t1 - tCompare <= 0 && t2 - tCompare <= 0){
            return -1;
        }
        return 0;
    }

    public static boolean checkIsWeekend(Date date, int sttOfDate){
        try{
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int year = localDate.getYear();
            Month month = localDate.getMonth();
            LocalDate localDate1 = LocalDate.of(year, month, sttOfDate);
           return localDate1.getDayOfWeek() == DayOfWeek.SATURDAY || localDate1.getDayOfWeek() == DayOfWeek.SUNDAY;
        }catch (Exception e){
            return false;
        }
    }

    public static boolean checkIsWeekend(Date date){
        try{
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            return localDate.getDayOfWeek() == DayOfWeek.SATURDAY || localDate.getDayOfWeek() == DayOfWeek.SUNDAY;
        }catch (Exception e){
            return false;
        }
    }

    public static boolean checkIsHoliday(Date date, int sttOfDate, List<Holiday> holidays){
        try{
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int year = localDate.getYear();
            Month month = localDate.getMonth();
            LocalDate localDate1 = LocalDate.of(year, month, sttOfDate);
            return holidays.stream()
                    .filter(holiday -> holiday.getHolidayDate().compareTo(DataUtil.convertToLocalDateToDate(localDate1)) == 0)
                    .collect(Collectors.toList())
                    .size() > 0;
        }catch (Exception e){
            return false;
        }
    }

    public static Date atStartOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(startOfDay);
    }

    public static Date atEndOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return localDateTimeToDate(endOfDay);
    }

    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static double convertHoursToWorktimeInDay(WorkTimeInDay workTimeInDay, Double hours){
        Long hoursAm = workTimeInDay.getWorkTimeEndTimeAm().getTime() - workTimeInDay.getWorkTimeStartTimeAm().getTime();
        Long hoursPm = workTimeInDay.getWorkTimeEndTimePm().getTime() - workTimeInDay.getWorkTimeStartTimePm().getTime();
        return hours*(60*60*1000)/(hoursAm + hoursPm);
    }

    public static boolean checkIsHoliday(Date date, List<Holiday> holidays){
        try{
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            return holidays.stream()
                    .filter(holiday -> holiday.getHolidayDate().compareTo(DataUtil.convertToLocalDateToDate(localDate)) == 0)
                    .collect(Collectors.toList())
                    .size() > 0;
        }catch (Exception e){
            return false;
        }
    }
    public static Date generateDateFromSttInMonth(Date date, Integer sttOfDate){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year = localDate.getYear();
        Month month = localDate.getMonth();
        LocalDate localDate1 = LocalDate.of(year, month, sttOfDate);
        Date dateWork = Date.from(localDate1.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return dateWork;
    }

    public static int getCountWorkdayInMonth(Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year = localDate.getYear();
        Month month = localDate.getMonth();
        return YearMonth.of(year, month).lengthOfMonth() - getAllWeekendInMonth(date).size();
    }

    public static int countDayInMonth(Date date){
        try{
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            return localDate.lengthOfMonth();
        }catch (Exception e){
            return 0;
        }
    }

    public static Boolean isSameMonth(Date date1, Date date2){
        try{
            return DateUtils.isSameDay(getFirstDateInMonth(date1), getFirstDateInMonth(date2));
        }catch (Exception e){
            return false;
        }
    }

    public static int getMonthFromDate(Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate.getMonthValue();

    }
    public static int getYearFromDate(Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate.getYear();

    }

    public static Date getFirstDayOfYear(Date date) {
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year = localDate.getYear();
        LocalDate firstDayOfYear = Year.of(year).atDay(1);
        return Date.from(firstDayOfYear.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static long calculateDaysBetween(Date dateStart, Date dateEnd) {
        if(dateStart.before(dateEnd)){
            LocalDate localdateStart = dateStart.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate localdateEnd = dateEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            return ChronoUnit.DAYS.between(localdateStart, localdateEnd);
        }
        return 0;
    }
    public static String roundToString(double number, int decimalPlaces) {
        double factor = Math.pow(10, decimalPlaces);
        double roundedNumber = Math.round(number * factor) / factor;
        return String.valueOf(roundedNumber);
    }
    public static String formatToYYYYMMDD(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(date);
    }

    public static Date formatDate(Date date) {
        if(date!= null){
            try{
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                return dateFormat.parse(dateFormat.format(date));
            }catch (ParseException e){
                return new Date();
            }
        }else{
            return null;
        }
    }


    public static int countWeekendDays(Date dateStart, Date dateEnd) {
        int totalWeekendDays = 0;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateStart);

        while (!calendar.getTime().after(dateEnd)) {
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
                totalWeekendDays++;
            }
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        return totalWeekendDays;
    }

    public static long countDatesInRange(List<Date> dateList, Date start, Date end) {
        LocalDate localStart = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localEnd = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        long count = dateList.stream()
                .map(date -> date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .filter(localDate -> !localDate.isBefore(localStart) && !localDate.isAfter(localEnd))
                .count();

        return count;
    }

    public static Date getFirstDateInMonth(Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        int year = localDate.getYear();
        int month = localDate.getMonthValue();
        // Create a YearMonth object
        YearMonth yearMonth = YearMonth.of(year, month);

        // Get the first day of the month
        LocalDate firstDay = yearMonth.atDay(1);
        return convertToLocalDateToDate(firstDay);

    }

    public static Date getLastDateInMonth(Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        int year = localDate.getYear();
        int month = localDate.getMonthValue();
        // Create a YearMonth object
        YearMonth yearMonth = YearMonth.of(year, month);

        // Get the first day of the month
        LocalDate lastDay = yearMonth.atEndOfMonth();
        return convertToLocalDateToDate(lastDay);

    }

    public static Date convertToLocalDateToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static String autoFormat2or3StringLength(Long number) {
        return number > 99L ? String.format("%03d", number) : String.format("%02d", number);
    }

    public static String autoFormat2or3StringLength(Integer number) {
        return number > 99L ? String.format("%03d", number) : String.format("%02d", number);
    }

    public static String autoFormat2or3StringLength(Double number) {
        return number > 99L ? String.format("%03d", number) : String.format("%02d", number);
    }

    public static String formatDouble(Double number, int count) {
        return String.format("%." + count + "f", number);
    }
}
