package com.blameo.worktimeservice.utils;

import com.blameo.worktimeservice.constant.Constant;
import com.blameo.worktimeservice.controller.CallOtherService;
import com.blameo.worktimeservice.dto.response.DtDataResponse;
import com.blameo.worktimeservice.model.DsData;
import com.blameo.worktimeservice.model.DtData;
import com.blameo.worktimeservice.model.Timekeeping;
import com.blameo.worktimeservice.modelUser.UserDTOResponse;
import com.blameo.worktimeservice.repository.TimekeepingRepository;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 11/1/2023, Wednesday
 **/
@Configuration
public class ReadXmlWorkTime {

    @Value("${fpt-server.ip}")
    private String server;

    @Value("${fpt-server.port}")
    private int port;

    @Value("${fpt-server.user}")
    private String username;

    @Value("${fpt-server.password}")
    private String password;

    @Value("${fpt-server.dir}")
    private String remoteDirectory;

    @Autowired
    TimekeepingRepository timekeepingRepository;

    @Autowired
    MessageSource messageSource;

    @Scheduled(cron = "0 0/30 * * * ?")
    public void runTaskSyncTimeKeeping(){
        try{
            FTPClient ftpClient = new FTPClient();
            List<FTPFile> ftpFiles = listFTPFiles(server, port, username, password, remoteDirectory, ftpClient);
            List<File> newestFiles = getTwoNewestFilesFromFTP(ftpFiles, ftpClient);
            JAXBContext jaxbContext = JAXBContext.newInstance(DsData.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            List<DtData> dataNew = new ArrayList<>();
            List<DtDataResponse> dataNewResponse = new ArrayList<>();
            if (newestFiles != null && newestFiles.size() >= 2) {
                List<DtData> dtDataListNewest = ((DsData) jaxbUnmarshaller.unmarshal(newestFiles.get(0))).getDtDataList();
                List<DtData> dtDataListOld = ((DsData) jaxbUnmarshaller.unmarshal(newestFiles.get(1))).getDtDataList();
                dataNew = dtDataListNewest.subList(dtDataListOld.size()-1,dtDataListNewest.size()-1);
            } else if (newestFiles != null) {
                List<DtData> dtDataListNewest = ((DsData) jaxbUnmarshaller.unmarshal(newestFiles.get(0))).getDtDataList();
                Calendar calendar = Calendar.getInstance();
                dataNew = dtDataListNewest.stream().filter(x->x.getDwMonth() == (calendar.get(Calendar.MONTH) + 1)
                        && x.getDwYear() == calendar.get(Calendar.YEAR)).collect(Collectors.toList());
            }
            if(dataNew.size() > 0){
                dataNew.forEach(x -> {
                    dataNewResponse.add(new DtDataResponse(x));
                });
            }
            saveListTimekeepingForUser(dataNewResponse);
            ftpClient.logout();
            ftpClient.disconnect();
        }catch (Exception e){
            throw new RuntimeException(messageSource.getMessage("syncFail", null, CommonUtil.getLocale()));
        }
    }

    public static List<FTPFile> listFTPFiles(String server, int port, String username, String password, String remoteDirectory, FTPClient ftpClient) {
        List<FTPFile> ftpFiles = new ArrayList<>();

        try {
            ftpClient.connect(server, port);
            ftpClient.login(username, password);

            ftpClient.changeWorkingDirectory(remoteDirectory);

            FTPFile[] files = ftpClient.listFiles();
            for (FTPFile file : files) {
                ftpFiles.add(file);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return ftpFiles;
    }

    private void saveListTimekeepingForUser(List<DtDataResponse> dtData){
        List<UserDTOResponse> listUser = CallOtherService.getAllFromIdService(UserDTOResponse.class, Constant.PATH.GET_ALL_USER_NO_AUTHEN);
        List<Timekeeping> timekeepings = timekeepingRepository.findAllByTimekeepingDateBetween(DataUtil.atStartOfDay(new Date()), DataUtil.atEndOfDay(new Date()));
        List<Timekeeping> save = new ArrayList<>();
        if(listUser != null && listUser.size() > 0){
            for(UserDTOResponse userDTOResponse : listUser){
                Timekeeping timeKeepingUser = timekeepings.stream().filter(x -> x.getUserId().equals(userDTOResponse.getUserId())).findFirst().orElse(null);
                if(timeKeepingUser!= null){
                    DtDataResponse dtDataResponse = dtData.stream().filter(x -> x.getDwEnrollNumber() == userDTOResponse.getAttendanceCode()).max((d1, d2) -> d1.getDwWorkTime().compareTo(d2.getDwWorkTime())).orElse(null);
                    if(dtDataResponse != null && DateUtils.isSameDay(dtDataResponse.getDwWorkTime(), new Date())){
                        timeKeepingUser.setBackTime(dtDataResponse.getDwWorkTime());
                        save.add(timeKeepingUser);
                    }
                }else{
                    DtDataResponse dtDataResponse = dtData.stream().filter(x -> x.getDwEnrollNumber() == userDTOResponse.getAttendanceCode()).min((d1, d2) -> d1.getDwWorkTime().compareTo(d2.getDwWorkTime())).orElse(null);
                    if(dtDataResponse != null && DateUtils.isSameDay(dtDataResponse.getDwWorkTime(), new Date())){
                        Timekeeping timekeeping = new Timekeeping(userDTOResponse.getUserId(), dtDataResponse.getDwWorkTime(), dtDataResponse.getDwWorkTime(), null);
                        save.add(timekeeping);
                    }
                }
            }
            System.out.println(save);
            if(save.size() > 0){
                timekeepingRepository.saveAll(save);
            }
        }
    }

    public static List<File> getTwoNewestFilesFromFTP(List<FTPFile> ftpFiles, FTPClient ftpClient) throws FileNotFoundException {
        List<File> localFiles = new ArrayList<>();
        if (ftpFiles != null && ftpFiles.size() >= 2) {
            Collections.sort(ftpFiles, new Comparator<FTPFile>() {
                @Override
                public int compare(FTPFile file1, FTPFile file2) {
                    return file2.getTimestamp().compareTo(file1.getTimestamp());
                }
            });
            int i = 0;
            for (FTPFile ftpFile : ftpFiles){
                if(i < 2){
                    File localFile = new File(ftpFile.getName());
                    try (FileOutputStream fos = new FileOutputStream(localFile)) {
                        if (ftpClient.retrieveFile(ftpFile.getName(), fos)) {
                            localFiles.add(localFile);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
            }
            return localFiles;
        }else if (ftpFiles != null) {
            File localFile = new File(ftpFiles.get(0).getName());
            try (FileOutputStream fos = new FileOutputStream(localFile)) {
                if (ftpClient.retrieveFile(ftpFiles.get(0).getName(), fos)) {
                    localFiles.add(localFile);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return localFiles;
        }

        return null;
    }
}
