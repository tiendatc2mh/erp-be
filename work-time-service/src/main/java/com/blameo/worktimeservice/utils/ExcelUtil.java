package com.blameo.worktimeservice.utils;


import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jetbrains.annotations.NotNull;

public class ExcelUtil {

    public static boolean isRowEmpty(Row row) {
        for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
            Cell cell = row.getCell(c);
            if (cell != null && cell.getCellType() != CellType.BLANK)
                return false;
        }
        return true;
    }

    public static XSSFCellStyle getStyleBasic(XSSFWorkbook workbook){
        //create font
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.valueOf((short) 1));
        style.setBorderTop(BorderStyle.valueOf((short) 1));
        style.setBorderRight(BorderStyle.valueOf((short) 1));
        style.setBorderLeft(BorderStyle.valueOf((short) 1));
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setVerticalAlignment(VerticalAlignment.TOP);
        style.setFont(font);
        return style;
    }

    public static XSSFCellStyle getStyleCenterBasic(XSSFWorkbook workbook){
        //create font
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.valueOf((short) 1));
        style.setBorderTop(BorderStyle.valueOf((short) 1));
        style.setBorderRight(BorderStyle.valueOf((short) 1));
        style.setBorderLeft(BorderStyle.valueOf((short) 1));
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(font);
        return style;
    }

    public static XSSFCellStyle getStyleCenterBasicBold(XSSFWorkbook workbook){
        //create font
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.valueOf((short) 1));
        style.setBorderTop(BorderStyle.valueOf((short) 1));
        style.setBorderRight(BorderStyle.valueOf((short) 1));
        style.setBorderLeft(BorderStyle.valueOf((short) 1));
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(font);
        return style;
    }

    public static XSSFCellStyle getStyleDateBasic(XSSFWorkbook workbook, String dateFormat){
        //create CreationHelper
        CreationHelper createHelper = workbook.getCreationHelper();
        //create font
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.valueOf((short) 1));
        style.setBorderTop(BorderStyle.valueOf((short) 1));
        style.setBorderRight(BorderStyle.valueOf((short) 1));
        style.setBorderLeft(BorderStyle.valueOf((short) 1));
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setVerticalAlignment(VerticalAlignment.TOP);
        style.setFont(font);
        style.setDataFormat(createHelper.createDataFormat().getFormat(dateFormat));
        return style;
    }
}
