package com.blameo.worktimeservice.controller;

import com.blameo.worktimeservice.constant.Constant;
import com.blameo.worktimeservice.dto.ResponseMessage;
import com.blameo.worktimeservice.dto.request.TimekeepingDTO;
import com.blameo.worktimeservice.dto.response.RequestDTOResponse;
import com.blameo.worktimeservice.dto.response.TimekeepingDTOResponse;
import com.blameo.worktimeservice.modelErp.Request;
import com.blameo.worktimeservice.modelUser.UserDTOResponse;
import com.blameo.worktimeservice.service.TimekeepingService;
import com.blameo.worktimeservice.utils.CommonUtil;
import com.blameo.worktimeservice.utils.DataUtil;
import com.blameo.worktimeservice.utils.FileUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("/api/worktime/timekeeping")
public class TimekeepingController {

    @Autowired
    TimekeepingService timekeepingService;

    @Autowired
    MessageSource messageSource;

    Logger logger = Logger.getLogger(TimekeepingController.class);

    @PostMapping(value = "/import", consumes= MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> create(@RequestPart(value = "file") MultipartFile file, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to import timekeeping : {}");
            if (file.getSize() > 1024 * 1024 * 5) {
                return new ResponseEntity<>(new ResponseMessage(false, messageSource.getMessage("limitSizeUpload",null,locale), null), HttpStatus.BAD_REQUEST);
            }
            if (!FileUtil.checkOfficeFile(file)) {
                return new ResponseEntity<>(new ResponseMessage(false,  messageSource.getMessage("validExtensionUpload",null,locale), null), HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("timekeeping.importSuccess",null,locale), timekeepingService.importExcel(request, file)),HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/static")
    public ResponseEntity<?> getStaticAllUser(@RequestParam(defaultValue = "1") Integer page,
                                              @RequestParam(defaultValue = "10") Integer pageSize,
                                              @RequestParam(name = "keyword", required = false) String keyword,
                                              @RequestParam(name = "userCode", required = false) String userCode,
                                              @RequestParam(name = "department", required = false) String department,
                                              @RequestParam(name = "month", required = false) Date month,
                                              @RequestParam(name = "isExport", required = false) Integer isExport,
                                              HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to import timekeeping : {}");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if(isExport != null && isExport == 1){
                InputStreamResource inputStream=  timekeepingService.export(keyword,userCode, department, month != null ? month : new Date(), request);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
                headers.setContentDispositionFormData("attachment", URLEncoder.encode(Constant.TEMPLATE_STATIC_TIMEKEEPING, StandardCharsets.UTF_8).replaceAll("\\+", "%20"));
                return ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .body(inputStream);
            }
            Map<String, Object> result = timekeepingService.getStaticAllUser(page, pageSize, keyword,userCode, department, month != null ? month : new Date(), request);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale), result),HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<?> getStaticAllUser(@PathVariable("userId") String userId,
                                              @RequestParam(name = "month", required = false) Date month,
                                              @RequestParam(name = "startDate", required = false) Date startDate,
                                              @RequestParam(name = "endDate", required = false) Date endDate,
                                              HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to import timekeeping : {}");
            Map<String, Object> result = timekeepingService.getAllOfUser(userId, month != null ? month : new Date(), DataUtil.formatDate(startDate), DataUtil.formatDate(endDate), request);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale), result),HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("")
    public ResponseEntity<?> update(@RequestBody RequestDTOResponse requestEntity, HttpServletRequest request){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            RequestDTOResponse requestDTOResponse = timekeepingService.updateByRequest(requestEntity, request);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale), requestDTOResponse),HttpStatus.OK);
        }catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("timekeeping-user")
    public ResponseEntity<?> getTimekeepingUser(@RequestParam(name = "userId") String userId,
                                                @RequestParam(name = "date") Date date,
                                                HttpServletRequest request){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to import timekeeping : {}");
            TimekeepingDTOResponse result = timekeepingService.getTimekeepingUser(userId, date, request);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale), result),HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/back-timekeeping")
    public ResponseEntity<?> backTimekeeping(@RequestBody TimekeepingDTO requestEntity, HttpServletRequest request){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            timekeepingService.backTimekeeping(requestEntity, request);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale), null),HttpStatus.OK);
        }catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
