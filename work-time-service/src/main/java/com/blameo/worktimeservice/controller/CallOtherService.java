package com.blameo.worktimeservice.controller;

import com.blameo.worktimeservice.constant.Constant;
import com.blameo.worktimeservice.dto.ResponseMessage;
import com.blameo.worktimeservice.dto.response.RequestWorktimeContentDTO;
import com.blameo.worktimeservice.dto.response.TimekeepingUser;
import com.blameo.worktimeservice.modelUser.UserDTOResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Service
public class CallOtherService {
    private static String gatewayHost;
    private static String gatewayPort;

    public CallOtherService(@Value("${gateway.host}") String gatewayHost, @Value("${gateway.port}") String gatewayPort) {
        this.gatewayHost = gatewayHost;
        this.gatewayPort = gatewayPort;
    }

    public static <T> T getDetailFromIdService(Class<T> entity, String path, String userId, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+path+userId,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            return mapper.convertValue(Objects.requireNonNull(response.getBody()).getData(), entity);
        } catch (Exception e) {
            try {
                return entity.getDeclaredConstructor().newInstance();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static ResponseMessage updateRequestWorktimeContents(Set<RequestWorktimeContentDTO> requestBody, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Set<RequestWorktimeContentDTO>> data = new HttpEntity<>(requestBody, headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+ Constant.PATH.UPDATE_REQUEST_WORKTIME_CONTENTS,
                    HttpMethod.PUT,
                    data,
                    ResponseMessage.class
            );
            return response.getBody();
        }catch (HttpClientErrorException httpClientErrorException){
            try{
                String res = httpClientErrorException.getResponseBodyAsString();
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(res, ResponseMessage.class);
            }catch (JsonProcessingException jsonProcessingException){
                return new ResponseMessage();
            }
        }
        catch (Exception e) {
            return new ResponseMessage();
        }
    }

    public static <T> List<T> getListFromOtherService(Class<T> entity, String path, Set<String> lstId, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            String pathParam = lstId.stream().map(Object::toString)
                    .collect(Collectors.joining(","));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+path+"?id="+pathParam,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            List<T> list = (List<T>) response.getBody().getData();
            List<T> dataLst = new ArrayList<>();
            for(T t : list){
                dataLst.add(mapper.convertValue(t, entity));
            }
            return dataLst;
        } catch (Exception e) {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static <T> List<T> getAllFromIdService(Class<T> entity, String path, Map<String, Object> requestParam,HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(gatewayHost+":"+gatewayPort+path);
            requestParam.forEach((key, value) -> {
                builder.queryParam(key, value);
            });
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            List<T> list = (List<T>) response.getBody().getData();
            List<T> dataLst = new ArrayList<>();
            for(T t : list){
                dataLst.add(mapper.convertValue(t, entity));
            }
            return dataLst;
        } catch (Exception e) {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static <T> List<T> getAllFromIdService(Class<T> entity, String path, Map<String, Object> requestParam) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(gatewayHost+":"+gatewayPort+path);
            requestParam.forEach((key, value) -> {
                builder.queryParam(key, value);
            });
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            List<T> list = (List<T>) response.getBody().getData();
            List<T> dataLst = new ArrayList<>();
            for(T t : list){
                dataLst.add(mapper.convertValue(t, entity));
            }
            return dataLst;
        } catch (Exception e) {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static <T> List<T> getAllFromIdService(Class<T> entity, String path) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(gatewayHost+":"+gatewayPort+path);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            List<T> list = (List<T>) response.getBody().getData();
            List<T> dataLst = new ArrayList<>();
            for(T t : list){
                dataLst.add(mapper.convertValue(t, entity));
            }
            return dataLst;
        } catch (Exception e) {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static Map<String, Object> getAllUserWithRequestParamFromIdService(String path, Map<String, Object> requestParam, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(gatewayHost+":"+gatewayPort+path);
            requestParam.forEach((key, value) -> {
                builder.queryParam(key, value);
            });
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = (Map<String, Object>) response.getBody().getData();
            List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("data");
            List<UserDTOResponse> dataLst = new ArrayList<>();
            for(Map<String, Object> t : list){
                dataLst.add(mapper.convertValue(t, UserDTOResponse.class));
            }
            map.put("data", dataLst);
            return map;
        } catch (Exception e) {
            try {
                return null;
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }
}
