package com.blameo.worktimeservice.controller;

import com.blameo.worktimeservice.constant.Constant;
import com.blameo.worktimeservice.dto.ResponseMessage;
import com.blameo.worktimeservice.model.Holiday;
import com.blameo.worktimeservice.model.WorkTimeInDay;
import com.blameo.worktimeservice.service.HolidayService;
import com.blameo.worktimeservice.service.WorkTimeInDayService;
import com.blameo.worktimeservice.utils.CommonUtil;
import com.blameo.worktimeservice.utils.DataUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/worktime/holiday")
public class HolidayController {

    @Autowired
    HolidayService holidayService;

    @Autowired
    MessageSource messageSource;

    Logger logger = Logger.getLogger(HolidayController.class);

    @GetMapping("")
    public ResponseEntity<?> getAllHolidayInMonth(@RequestParam(name = "date", required = false) Date date){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try{
            logger.debug("REST request to getAll Holiday: {}");
            List<Holiday> holidays = holidayService.getAllHolidayInMonth(date != null ? date : new Date());
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale), holidays), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllHolidayBetween(@RequestParam(name = "start_date", required = false) Date startDate,
                                           @RequestParam(name = "end_date", required = false) Date endDate){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try{
            logger.debug("REST request to getAll Holiday: {}");
            List<Holiday> holidays = holidayService.getAllHolidayBetween(startDate, endDate);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale), holidays), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
