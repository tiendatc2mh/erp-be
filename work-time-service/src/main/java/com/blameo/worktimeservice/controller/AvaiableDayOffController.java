package com.blameo.worktimeservice.controller;

import com.blameo.worktimeservice.constant.Constant;
import com.blameo.worktimeservice.dto.ResponseMessage;
import com.blameo.worktimeservice.dto.request.AvailableOffDTO;
import com.blameo.worktimeservice.dto.request.TimekeepingDTO;
import com.blameo.worktimeservice.dto.response.AvaiableDayResponse;
import com.blameo.worktimeservice.dto.response.RequestDTOResponse;
import com.blameo.worktimeservice.model.AvailableDayOff;
import com.blameo.worktimeservice.modelUser.UserDTOResponse;
import com.blameo.worktimeservice.repository.AvailableDayOffRepository;
import com.blameo.worktimeservice.service.AvaiableDayOffService;
import com.blameo.worktimeservice.utils.CommonUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/worktime/avaiable-day-off")
public class AvaiableDayOffController {

    @Autowired
    MessageSource messageSource;

    @Autowired
    AvaiableDayOffService avaiableDayOffService;

    @Autowired
    AvailableDayOffRepository availableDayOffRepository;


    Logger logger = Logger.getLogger(AvaiableDayOffController.class);

    Locale locale = CommonUtil.getLocale();


    @GetMapping("/all")
    public ResponseEntity<Object> getAll(HttpServletRequest httpRequest) {
        try {
            logger.debug("REST request to get all department");
            AvaiableDayResponse result = avaiableDayOffService.getAll(httpRequest);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("update_day_off")
    public ResponseEntity<?> update(@RequestBody AvailableOffDTO availableOffDTO, HttpServletRequest request){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            avaiableDayOffService.updateDayOffFromERP(availableOffDTO, request);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale), null),HttpStatus.OK);
        }catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
