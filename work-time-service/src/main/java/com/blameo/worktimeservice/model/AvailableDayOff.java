package com.blameo.worktimeservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "available_day_off")
public class AvailableDayOff {

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "available_day_off_id", nullable = false)
    private String availableDayOffId;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "month", nullable = false)
    private Integer month;

    @Column(name = "year", nullable = false)
    private Integer year;

    @Column(name = "available_day_off_old", nullable = false)
    private Double availableDayOffOld;

    @Column(name = "available_day_off", nullable = false)
    private Double availableDayOff;
}
