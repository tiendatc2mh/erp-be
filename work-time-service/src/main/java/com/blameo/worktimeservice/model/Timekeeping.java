package com.blameo.worktimeservice.model;

import com.blameo.worktimeservice.constant.Constant;
import com.blameo.worktimeservice.utils.DataUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.hibernate.annotations.GenericGenerator;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "timekeeping")
public class Timekeeping {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "timekeeping_id", nullable = false)
    private String timekeepingId;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "work_shift", nullable = true)
    private Integer workShift;

    @Column(name = "timekeeping_date", nullable = true)
    private Date timekeepingDate;

    @Column(name = "arrival_time", nullable = true)
    private Date arrivalTime;

    @Column(name = "back_time", nullable = true)
    private Date backTime;

    @Column(name = "working_time", nullable = true, length = 50)
    private String workingTime;

    @Column(name = "over_time", nullable = true, length = 50)
    private String overTime;

    @Column(name = "late_time", nullable = true, length = 50)
    private String lateTime;

    @Column(name = "early_time", nullable = true, length = 50)
    private String earlyTime;

    @Column(name = "note", nullable = true)
    private Integer note;

    @Column(name = "updated_by", nullable = true)
    private String updatedBy;

    @Transient
    private Double workingTimePaid;

    public Timekeeping(String userId, Date timekeepingDate, Date arrivalTime, Date backTime) {
        this.userId = userId;
        this.timekeepingDate = timekeepingDate;
        this.arrivalTime = arrivalTime;
        this.backTime = backTime;
    }

    public Boolean isValidTimekeeping(WorkTimeInDay workTimeInDay, List<Holiday> holidays){
        // can check them holiday
        if(DataUtil.checkIsWeekend(this.timekeepingDate)){
            return false;
        } else if(DataUtil.checkIsHoliday(this.timekeepingDate, holidays)){
            return false;
        }
        else{
            if(this.arrivalTime == null || this.backTime == null){
                return false;
            } else if (this.arrivalTime.after(this.backTime)) {
                return false;
            } else if (this.arrivalTime.after(workTimeInDay.getWorkTimeEndTimePm())) {
                return false;
            } else if (this.backTime.before(workTimeInDay.getWorkTimeStartTimeAm())) {
                return false;
            } else if ((this.arrivalTime.after(workTimeInDay.getWorkTimeEndTimeAm()) || this.arrivalTime.equals(workTimeInDay.getWorkTimeEndTimeAm()))
            && (this.backTime.before(workTimeInDay.getWorkTimeStartTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeStartTimePm()))){
                return false;
            } else{
                return true;
            }
        }
    }

    public Boolean isValidTimekeepingWithWeekendAndHoliday(List<Holiday> holidays){
        if(DataUtil.checkIsWeekend(this.timekeepingDate)){
            return false;
        } else if(DataUtil.checkIsHoliday(this.timekeepingDate, holidays)){
            return false;
        }else{
            return true;
        }
    }

    public Boolean isValidTimekeepingWithTime(WorkTimeInDay workTimeInDay){
        if(this.arrivalTime == null || this.backTime == null){
            return false;
        } else if (this.arrivalTime.after(this.backTime)) {
            return false;
        } else if (this.arrivalTime.after(workTimeInDay.getWorkTimeEndTimePm())) {
            return false;
        } else if (this.backTime.before(workTimeInDay.getWorkTimeStartTimeAm())) {
            return false;
        } else if ((this.arrivalTime.after(workTimeInDay.getWorkTimeEndTimeAm()) || this.arrivalTime.equals(workTimeInDay.getWorkTimeEndTimeAm()))
                && (this.backTime.before(workTimeInDay.getWorkTimeStartTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeStartTimePm()))){
            return false;
        } else{
            return true;
        }
    }

    public String makeWorkShift(@NotNull WorkTimeInDay workTimeInDay){
      try{
          // làm ca sáng
        if(this.backTime.before(workTimeInDay.getWorkTimeStartTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeStartTimePm())){
            return Constant.WORK_SHIFT.MORNING;
        }else if(this.arrivalTime.after(workTimeInDay.getWorkTimeEndTimeAm()) || this.arrivalTime.equals(workTimeInDay.getWorkTimeEndTimeAm())){
            // làm ca chiều
            return Constant.WORK_SHIFT.AFTERNOON;
        }else{
            // làm cả ngày
            return Constant.WORK_SHIFT.ALL_DAY;
        }
      }catch (Exception e){
          return null;
      }
    }

    public Boolean isLate(@NotNull WorkTimeInDay workTimeInDay){
        if(this.arrivalTime.after(DateUtils.addMinutes(workTimeInDay.getWorkTimeStartTimePm(), 1))){
            return true;
        }else if(this.arrivalTime.after(DateUtils.addMinutes(workTimeInDay.getWorkTimeStartTimeAm(), 1))){
            return true;
        }else{
            return  false;
        }

    }

    public Long countCutPaidMinus(WorkTimeInDay workTimeInDay, List<Holiday> holidays){
        if(this.isValidTimekeeping(workTimeInDay, holidays)){
            return countEarlyMinus(workTimeInDay, holidays) + countLateMinus(workTimeInDay, holidays);
        }else{
            return 0L;
        }

    }

    public Long countLateMinus(@NotNull WorkTimeInDay workTimeInDay, List<Holiday> holidays){
        Long countMinus = 0L;
        if(this.isValidTimekeeping(workTimeInDay, holidays)) {
            // số phút đi muộn
            if (this.arrivalTime.after(workTimeInDay.getWorkTimeStartTimePm()) && this.arrivalTime.before(workTimeInDay.getWorkTimeEndTimePm())) {
                Long timeDifferenceMillis = this.arrivalTime.getTime() - workTimeInDay.getWorkTimeStartTimePm().getTime();
                countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
            } else if (this.arrivalTime.after(workTimeInDay.getWorkTimeStartTimeAm()) && this.arrivalTime.before(workTimeInDay.getWorkTimeEndTimeAm())) {
                Long timeDifferenceMillis = this.arrivalTime.getTime() - workTimeInDay.getWorkTimeStartTimeAm().getTime();
                countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
            }
        }

        return countMinus;
    }

    public Long countEarlyMinus(@NotNull WorkTimeInDay workTimeInDay, List<Holiday> holidays){
        Long countMinus = 0L;
        if(this.isValidTimekeeping(workTimeInDay, holidays)) {
            // số phút về sớm
            if (this.backTime.before(workTimeInDay.getWorkTimeEndTimeAm()) && this.backTime.after(workTimeInDay.getWorkTimeStartTimeAm())) {
                Long timeDifferenceMillis = workTimeInDay.getWorkTimeEndTimeAm().getTime() - this.backTime.getTime();
                countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
            } else if (this.backTime.before(workTimeInDay.getWorkTimeEndTimePm()) && this.backTime.after(workTimeInDay.getWorkTimeStartTimePm())) {
                Long timeDifferenceMillis = workTimeInDay.getWorkTimeEndTimePm().getTime() - this.backTime.getTime();
                countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
            }
        }

        return countMinus;
    }

    public Long countWorkMinus(@NotNull WorkTimeInDay workTimeInDay, List<Holiday> holidays){
        Long countMinus = 0L;
        if(this.isValidTimekeeping(workTimeInDay, holidays)){
            if(this.arrivalTime.before(workTimeInDay.getWorkTimeStartTimeAm())){
                if((this.backTime.after(workTimeInDay.getWorkTimeStartTimeAm()) || this.backTime.equals(workTimeInDay.getWorkTimeStartTimeAm())) &&
                        (this.backTime.before(workTimeInDay.getWorkTimeEndTimeAm()) || this.backTime.equals(workTimeInDay.getWorkTimeEndTimeAm()))) {
                    Long timeDifferenceMillis = this.backTime.getTime() - workTimeInDay.getWorkTimeStartTimeAm().getTime();
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }else if(this.backTime.after(workTimeInDay.getWorkTimeEndTimeAm()) &&
                        (this.backTime.before(workTimeInDay.getWorkTimeStartTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeStartTimePm()))){
                    Long timeDifferenceMillis = workTimeInDay.getWorkTimeEndTimeAm().getTime() - workTimeInDay.getWorkTimeStartTimeAm().getTime();
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }else if(this.backTime.after(workTimeInDay.getWorkTimeStartTimePm()) &&
                        (this.backTime.before(workTimeInDay.getWorkTimeEndTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeEndTimePm()))){
                    Long timeDifferenceMillis = (workTimeInDay.getWorkTimeEndTimeAm().getTime() - workTimeInDay.getWorkTimeStartTimeAm().getTime()) +
                            (this.backTime.getTime() - workTimeInDay.getWorkTimeStartTimePm().getTime());
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }else if(this.backTime.after(workTimeInDay.getWorkTimeEndTimePm())){
                    Long timeDifferenceMillis = (workTimeInDay.getWorkTimeEndTimeAm().getTime() - workTimeInDay.getWorkTimeStartTimeAm().getTime()) +
                            (workTimeInDay.getWorkTimeEndTimePm().getTime() - workTimeInDay.getWorkTimeStartTimePm().getTime());
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }
            }else if((this.arrivalTime.after(workTimeInDay.getWorkTimeStartTimeAm()) || this.arrivalTime.equals(workTimeInDay.getWorkTimeStartTimeAm())) &&
                    (this.arrivalTime.before(workTimeInDay.getWorkTimeEndTimeAm()) || this.arrivalTime.equals(workTimeInDay.getWorkTimeEndTimeAm()))){
                if(this.backTime.before(workTimeInDay.getWorkTimeEndTimeAm()) || this.backTime.equals(workTimeInDay.getWorkTimeEndTimeAm())){
                    Long timeDifferenceMillis = this.backTime.getTime() - this.arrivalTime.getTime();
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }else if(this.backTime.after(workTimeInDay.getWorkTimeEndTimeAm()) &&
                        (this.backTime.before(workTimeInDay.getWorkTimeStartTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeStartTimePm()))){
                    Long timeDifferenceMillis = workTimeInDay.getWorkTimeEndTimeAm().getTime() - this.arrivalTime.getTime();
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }else if(this.backTime.after(workTimeInDay.getWorkTimeStartTimePm()) && (this.backTime.before(workTimeInDay.getWorkTimeEndTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeEndTimePm()))){
                    Long timeDifferenceMillis = (workTimeInDay.getWorkTimeEndTimeAm().getTime() - this.arrivalTime.getTime()) +
                            (this.backTime.getTime() - workTimeInDay.getWorkTimeStartTimePm().getTime());
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }else if(this.backTime.after(workTimeInDay.getWorkTimeEndTimePm())){
                    Long timeDifferenceMillis = (workTimeInDay.getWorkTimeEndTimeAm().getTime() - this.arrivalTime.getTime()) +
                            (workTimeInDay.getWorkTimeEndTimePm().getTime() - workTimeInDay.getWorkTimeStartTimePm().getTime());
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }
            } else if (this.arrivalTime.after(workTimeInDay.getWorkTimeEndTimeAm()) &&
                    (this.arrivalTime.before(workTimeInDay.getWorkTimeStartTimePm()) || this.arrivalTime.equals(workTimeInDay.getWorkTimeStartTimePm()))) {
                if((this.backTime.after(workTimeInDay.getWorkTimeStartTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeStartTimePm())) &&
                        (this.backTime.before(workTimeInDay.getWorkTimeEndTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeEndTimePm()))){
                    Long timeDifferenceMillis = this.backTime.getTime() - workTimeInDay.getWorkTimeStartTimePm().getTime();
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }else if(this.backTime.after(workTimeInDay.getWorkTimeEndTimePm())){
                    Long timeDifferenceMillis = workTimeInDay.getWorkTimeEndTimePm().getTime() - workTimeInDay.getWorkTimeStartTimePm().getTime();
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }
            }else if(this.arrivalTime.after(workTimeInDay.getWorkTimeStartTimePm()) &&
                    (this.arrivalTime.before(workTimeInDay.getWorkTimeEndTimePm()) || this.arrivalTime.equals(workTimeInDay.getWorkTimeEndTimePm()))){
                if((this.backTime.after(workTimeInDay.getWorkTimeStartTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeStartTimePm())) &&
                        (this.backTime.before(workTimeInDay.getWorkTimeEndTimePm()) || this.backTime.equals(workTimeInDay.getWorkTimeEndTimePm()))){
                    Long timeDifferenceMillis = this.backTime.getTime() - this.arrivalTime.getTime();
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }else if(this.backTime.after(workTimeInDay.getWorkTimeEndTimePm())){
                    Long timeDifferenceMillis = workTimeInDay.getWorkTimeEndTimePm().getTime() - this.arrivalTime.getTime();
                    countMinus = countMinus + timeDifferenceMillis / (60 * 1000);
                }
            }
        }
        return countMinus;
    }

    // count ngày công làm việc : 0.5 -> 1
    public Double countWorkPaid(@NotNull WorkTimeInDay workTimeInDay){
        switch (this.makeWorkShift(workTimeInDay)){
            case Constant.WORK_SHIFT.ALL_DAY:
                return 1D;
            case Constant.WORK_SHIFT.MORNING:
            case Constant.WORK_SHIFT.AFTERNOON:
                return 0.5D;
        }
        return 0D;
    }

}
