package com.blameo.worktimeservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "holiday")
public class Holiday {

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "holiday_id", nullable = false)
    private String holidayId;

    @Column(name = "holiday_date", nullable = false)
    private Date holidayDate;

    @Column(name = "holiday_coefficient", nullable = false, precision = 0)
    private double holidayCoefficient;

    @Column(name = "holiday_description", nullable = true, length = 500)
    private String holidayDescription;
}
