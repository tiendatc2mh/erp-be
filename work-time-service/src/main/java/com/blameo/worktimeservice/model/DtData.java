package com.blameo.worktimeservice.model;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 11/1/2023, Wednesday
 **/
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "dtData")
public class DtData {
    private int dwMachineNumber;
    private int dwEnrollNumber;
    private int dwVerifyMode;
    private int dwInOutMode;
    private int dwYear;
    private int dwMonth;
    private int dwDay;
    private int dwHour;
    private int dwMinute;
    private int dwSecond;
    private int dwWorkCode;

    public int getDwMachineNumber() {
        return dwMachineNumber;
    }

    @XmlElement(name = "dwMachineNumber")
    public void setDwMachineNumber(int dwMachineNumber) {
        this.dwMachineNumber = dwMachineNumber;
    }

    public int getDwEnrollNumber() {
        return dwEnrollNumber;
    }

    @XmlElement(name = "dwEnrollNumber")
    public void setDwEnrollNumber(int dwEnrollNumber) {
        this.dwEnrollNumber = dwEnrollNumber;
    }

    public int getDwVerifyMode() {
        return dwVerifyMode;
    }

    @XmlElement(name = "dwVerifyMode")
    public void setDwVerifyMode(int dwVerifyMode) {
        this.dwVerifyMode = dwVerifyMode;
    }

    public int getDwInOutMode() {
        return dwInOutMode;
    }

    @XmlElement(name = "dwInOutMode")
    public void setDwInOutMode(int dwInOutMode) {
        this.dwInOutMode = dwInOutMode;
    }

    public int getDwYear() {
        return dwYear;
    }

    @XmlElement(name = "dwYear")
    public void setDwYear(int dwYear) {
        this.dwYear = dwYear;
    }

    public int getDwMonth() {
        return dwMonth;
    }

    @XmlElement(name = "dwMonth")
    public void setDwMonth(int dwMonth) {
        this.dwMonth = dwMonth;
    }

    public int getDwDay() {
        return dwDay;
    }

    @XmlElement(name = "dwDay")
    public void setDwDay(int dwDay) {
        this.dwDay = dwDay;
    }

    public int getDwHour() {
        return dwHour;
    }

    @XmlElement(name = "dwHour")
    public void setDwHour(int dwHour) {
        this.dwHour = dwHour;
    }

    public int getDwMinute() {
        return dwMinute;
    }

    @XmlElement(name = "dwMinute")
    public void setDwMinute(int dwMinute) {
        this.dwMinute = dwMinute;
    }

    public int getDwSecond() {
        return dwSecond;
    }

    @XmlElement(name = "dwSecond")
    public void setDwSecond(int dwSecond) {
        this.dwSecond = dwSecond;
    }

    public int getDwWorkCode() {
        return dwWorkCode;
    }

    @XmlElement(name = "dwWorkCode")
    public void setDwWorkCode(int dwWorkCode) {
        this.dwWorkCode = dwWorkCode;
    }
}

