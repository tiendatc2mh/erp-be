package com.blameo.worktimeservice.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 11/1/2023, Wednesday
 **/
@XmlRootElement(name = "dsData")
public class DsData {
    private List<DtData> dtDataList;

    @XmlElement(name = "dtData")
    public List<DtData> getDtDataList() {
        return dtDataList;
    }

    public void setDtDataList(List<DtData> dtDataList) {
        this.dtDataList = dtDataList;
    }
}
