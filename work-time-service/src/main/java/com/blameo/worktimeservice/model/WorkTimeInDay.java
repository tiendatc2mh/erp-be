package com.blameo.worktimeservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "work_time_in_day")
public class WorkTimeInDay {

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "work_time_id", nullable = false)
    private String workTimeId;

    @Column(name = "work_time_start_time_am")
    @Temporal(TemporalType.TIMESTAMP)
    private Date workTimeStartTimeAm;

    @Column(name = "work_time_end_time_am")
    @Temporal(TemporalType.TIMESTAMP)
    private Date workTimeEndTimeAm;

    @Column(name = "work_time_start_apply")
    @Temporal(TemporalType.TIMESTAMP)
    private Date workTimeStartApply;

    @Column(name = "work_time_end_apply")
    @Temporal(TemporalType.TIMESTAMP)
    private Date workTimeEndApply;

    @Column(name = "work_time_end_time_pm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date workTimeEndTimePm;

    @Column(name = "work_time_start_time_pm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date workTimeStartTimePm;

    @Column(name = "work_time_late_allow")
    @Temporal(TemporalType.TIMESTAMP)
    private Date workTimeLateAllow;

    @Column(name = "work_time_late_allow_count")
    private Integer workTimeLateAllowCount;

    @Column(name = "`type`")
    private Integer type;

}
