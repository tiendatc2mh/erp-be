package com.blameo.worktimeservice.payload.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MessageResponse {
	private String message;
}
