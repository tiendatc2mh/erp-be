package com.blameo.worktimeservice.repository;

import com.blameo.worktimeservice.model.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface HolidayRepository extends JpaRepository<Holiday, String> {

    Long countHolidayByHolidayDateBetween(Date startDate, Date endDate);
    List<Holiday> getAllByHolidayDateBetween(Date startDate, Date endDate);
}