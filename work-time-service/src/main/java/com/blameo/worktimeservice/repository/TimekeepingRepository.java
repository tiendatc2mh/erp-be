package com.blameo.worktimeservice.repository;

import com.blameo.worktimeservice.model.Timekeeping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface TimekeepingRepository extends JpaRepository<Timekeeping, String> {

    Optional<Timekeeping> findByTimekeepingDateAndUserId(Date date, String userId);

    List<Timekeeping> findAllByTimekeepingDateBetween(Date startDate, Date endDate);


    List<Timekeeping> findAllByUserIdInAndTimekeepingDateBetweenOrderByTimekeepingDateAsc(List<String> userId, Date startDate, Date endDate);
}
