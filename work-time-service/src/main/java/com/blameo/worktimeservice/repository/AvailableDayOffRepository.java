package com.blameo.worktimeservice.repository;

import com.blameo.worktimeservice.model.AvailableDayOff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AvailableDayOffRepository extends JpaRepository<AvailableDayOff, String> {
    AvailableDayOff findFirstByUserIdAndMonthAndYear(String userId, Integer month, Integer year);

    AvailableDayOff findFirstByUserIdOrderByMonthDescYearDesc(String userId);
}
