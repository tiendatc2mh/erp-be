package com.blameo.worktimeservice.repository;

import com.blameo.worktimeservice.model.WorkTimeInDay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface WorkTimeInDayRepository extends JpaRepository<WorkTimeInDay, String> {

    WorkTimeInDay findByWorkTimeEndApplyIsAfterAndAndWorkTimeStartApplyIsBeforeAndType(Date startDate, Date endDate, Integer type);
}