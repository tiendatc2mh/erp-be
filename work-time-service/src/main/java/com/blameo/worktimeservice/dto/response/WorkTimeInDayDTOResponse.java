package com.blameo.worktimeservice.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.TimeZone;

@Data
public class WorkTimeInDayDTOResponse {

    private String workTimeId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date workTimeStartTimeAm;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date workTimeEndTimeAm;

    private Date workTimeStartApply;

    private Date workTimeEndApply;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date workTimeEndTimePm;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date workTimeStartTimePm;
}
