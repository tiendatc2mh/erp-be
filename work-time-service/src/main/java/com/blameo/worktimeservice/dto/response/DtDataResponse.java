package com.blameo.worktimeservice.dto.response;

import com.blameo.worktimeservice.model.DtData;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Data
@NoArgsConstructor
public class DtDataResponse {
    private int dwMachineNumber;
    private int dwEnrollNumber;
    private int dwVerifyMode;
    private int dwInOutMode;
    private int dwYear;
    private int dwMonth;
    private int dwDay;
    private int dwHour;
    private int dwMinute;
    private int dwSecond;
    private int dwWorkCode;
    private Date dwWorkTime;

    public DtDataResponse(DtData dtData){
        dwMachineNumber = dtData.getDwMachineNumber();
        dwEnrollNumber = dtData.getDwEnrollNumber();
        dwVerifyMode = dtData.getDwVerifyMode();
        dwInOutMode = dtData.getDwInOutMode();
        dwYear = dtData.getDwYear();
        dwMonth = dtData.getDwMonth();
        dwDay = dtData.getDwDay();
        dwHour = dtData.getDwHour();
        dwMinute = dtData.getDwMinute();
        dwSecond = dtData.getDwSecond();
        dwWorkCode = dtData.getDwWorkCode();
        Calendar calendar = new GregorianCalendar(dtData.getDwYear(),dtData.getDwMonth() - 1,dtData.getDwDay(),dtData.getDwHour(),dtData.getDwMinute(),dtData.getDwMinute());
        dwWorkTime = calendar.getTime();
    }
}
