package com.blameo.worktimeservice.dto.request;

import lombok.Data;

import java.util.Date;

@Data
public class TimekeepingDTO {

    private String userId;

    private Date timekeeppingDate;

    private Date arrivalTime;

    private Date backTime;

}
