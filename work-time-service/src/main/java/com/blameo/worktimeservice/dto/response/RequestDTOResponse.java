package com.blameo.worktimeservice.dto.response;

import com.blameo.worktimeservice.modelErp.Process;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import java.util.Date;
import java.util.Set;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestDTOResponse {
    private String requestId;

    private Integer requestStatus;

    private Integer requestStatusProcess;

    private String requestCode;

    private Process process;

    private String content;

    private String reason;

    private String description;

    private String action;

    private String roleId;

    private String requestApprove;

    private String requestApproveName;

    private String departmentId;

    private String departmentName;

    private String positionName;

    private Integer payType;

    private Integer dayOffMarryType;

    private String handlerUser;

    private String bankAccountNumber;

    private String bankType;

    private String handoverUser;

    private Date payDate;

    private String accountOwner;

    private String bank;

    private String accountNumber;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;

    private String handoverUsername;

    private Double dayOffLastYear;

    private Double dayOffCurrentYear;

    private Set<RequestWorktimeContentDTO> requestWorktimeContents;

}
