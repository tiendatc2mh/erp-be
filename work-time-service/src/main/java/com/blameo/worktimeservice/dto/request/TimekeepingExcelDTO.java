package com.blameo.worktimeservice.dto.request;

import lombok.Data;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

@Data
public class TimekeepingExcelDTO {
    private String userId;

    private Integer workShift;

    private Date timekeeppingDate;

    private Date arrivalTime;

    private Date backTime;

    private String workingTime;

    private String overTime;

    private String lateTime;

    private String earlyTime;

    private Integer note;

    private String updatedBy;

}
