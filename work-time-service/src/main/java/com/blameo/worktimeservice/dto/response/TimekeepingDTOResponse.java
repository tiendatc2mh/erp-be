package com.blameo.worktimeservice.dto.response;

import com.blameo.worktimeservice.constant.Constant;
import com.blameo.worktimeservice.model.Holiday;
import com.blameo.worktimeservice.model.Timekeeping;
import com.blameo.worktimeservice.model.WorkTimeInDay;
import com.blameo.worktimeservice.utils.DataUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.blameo.worktimeservice.controller.CallOtherService.getAllFromIdService;

@Data
@NoArgsConstructor
public class TimekeepingDTOResponse {


    private String timekeepingId;

    private String userId;

    private Integer workShift;

    private Date timekeepingDate;

    private Integer timekeepingDateStr;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date arrivalTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date backTime;

    private String workingTime;

    private String overTime;

    private String lateTime;

    private String earlyTime;

    private List<Integer> note = new ArrayList<>();

    private String updatedBy;

    private WorkTimeInDayDTOResponse workTimeInDay;

    private Integer remoteType;

    private Integer type;

    private String remoteTime;

    public TimekeepingDTOResponse(Timekeeping timekeeping, WorkTimeInDay workTimeInDay, List<Holiday> holidays, Locale locale, Date month, List<RequestDTOResponse> requestInMonth) {
        String[] days = {"monday","tuesday","wednesday","thursday","friday","saturday","sunday"};
        Locale localeEn = new Locale("en");
        timekeepingId = timekeeping.getTimekeepingId();
        userId = timekeeping.getUserId();
        timekeepingDate = timekeeping.getTimekeepingDate();
        DateFormat formatter = new SimpleDateFormat("EEEE", localeEn);
        timekeepingDateStr = Arrays.asList(days).indexOf(formatter.format(timekeepingDate).toLowerCase());
        arrivalTime = timekeeping.getArrivalTime();
        backTime = timekeeping.getBackTime();
        type = Constant.TIMEKEEPING_TYPE.NO_REMOTE;
        Long work = timekeeping.countWorkMinus(workTimeInDay, holidays);
        Long late = timekeeping.countLateMinus(workTimeInDay, holidays);
        Long early = timekeeping.countEarlyMinus(workTimeInDay, holidays);
//        workingTime = String.valueOf(((double) work / 60));
        lateTime = DataUtil.autoFormat2or3StringLength(late);
        earlyTime = DataUtil.autoFormat2or3StringLength(early);
        workingTime = String.valueOf(timekeeping.getWorkingTimePaid() != null ? ((double) timekeeping.getWorkingTimePaid())*8 : 0);
        WorkTimeInDayDTOResponse dto = new WorkTimeInDayDTOResponse();
        BeanUtils.copyProperties(workTimeInDay, dto);
        this.workTimeInDay = dto;
        List<RequestDTOResponse> remoteDateList =  requestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE)).collect(Collectors.toList());
        List<RequestDTOResponse> worktimeConfirmDateList =  requestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.WORK_CONFIMATION)).collect(Collectors.toList());
        List<RequestDTOResponse> worktimeDayoffList =  requestInMonth.stream().filter(requestDTOResponse -> requestDTOResponse.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)).collect(Collectors.toList());

        AtomicBoolean noteWithRequest = new AtomicBoolean(false);

        if (remoteDateList.size()>0 && !DataUtil.checkIsHoliday(timekeepingDate, holidays) && !DataUtil.checkIsWeekend(timekeepingDate)){
            AtomicReference<Double> countRemoteTime = new AtomicReference<>(0D);
            remoteDateList.forEach(requestDTOResponse -> {
                AtomicInteger i = new AtomicInteger();
                requestDTOResponse.getRequestWorktimeContents().forEach(requestWorktimeContentDTO -> {
                    if (DateUtils.isSameDay(timekeepingDate, requestWorktimeContentDTO.getRemoteDate())){
                        remoteType = requestWorktimeContentDTO.getRemoteType();
                        countRemoteTime.updateAndGet(v -> v + requestWorktimeContentDTO.getCountHour());
                        type = Constant.TIMEKEEPING_TYPE.REMOTE;
                    }
                });
            });
            remoteTime = String.format("%.1f", countRemoteTime.get());
        }

        if(type.equals(Constant.TIMEKEEPING_TYPE.REMOTE)){
            noteWithRequest.set(true);
            note.add(Constant.TIMEKEEPING_NOTE.REMOTE);
        }
        if (worktimeConfirmDateList.size()>0 && !DataUtil.checkIsHoliday(timekeepingDate, holidays) && !DataUtil.checkIsWeekend(timekeepingDate)){
            worktimeConfirmDateList.forEach(requestDTOResponse -> {
                requestDTOResponse.getRequestWorktimeContents().forEach(requestWorktimeContentDTO -> {
                    if (DateUtils.isSameDay(timekeepingDate, requestWorktimeContentDTO.getRequestWorkTimeDate())){
                        noteWithRequest.set(true);
                        note.add(Constant.TIMEKEEPING_NOTE.WORK_TIME_CONFIRM);
                    }
                });
            });
        }

        if (worktimeDayoffList.size()>0 && !DataUtil.checkIsHoliday(timekeepingDate, holidays) && !DataUtil.checkIsWeekend(timekeepingDate)){
            worktimeDayoffList.forEach(requestDTOResponse -> {
                requestDTOResponse.getRequestWorktimeContents().forEach(requestWorktimeContentDTO -> {
                    if ((timekeepingDate.after(requestWorktimeContentDTO.getDayOffBegin()) || DateUtils.isSameDay(timekeepingDate, requestWorktimeContentDTO.getDayOffBegin()))
                            && (timekeepingDate.before(requestWorktimeContentDTO.getDayOffEnd()) || DateUtils.isSameDay(timekeepingDate, requestWorktimeContentDTO.getDayOffEnd()))){
                        noteWithRequest.set(true);
                        if(requestWorktimeContentDTO.getRequestDayOffType().equals(Constant.REQUEST_DAY_OFF_TYPE.NO_PAID_LEAVE)){
                            note.add(Constant.TIMEKEEPING_NOTE.DAY_OFF_WITH_REQUEST_NO_SALARY);
                        }else{
                            note.add(Constant.TIMEKEEPING_NOTE.DAY_OFF_WITH_REQUEST_SALARY);
                        }
                    }
                });
            });
        }
        if(!noteWithRequest.get()){
            if(DataUtil.checkIsHoliday(timekeepingDate, holidays) || DataUtil.checkIsWeekend(timekeepingDate)){
                if(DataUtil.checkIsHoliday(timekeepingDate, holidays)) {
                    note.add(Constant.TIMEKEEPING_NOTE.HOLIDAY);
                }
                if(DataUtil.checkIsWeekend(timekeepingDate)) {
                    note.add(Constant.TIMEKEEPING_NOTE.WEEKEND);
                }
            } else if(timekeeping.isValidTimekeeping(workTimeInDay, holidays)){
                if(late.equals(0L) && early.equals(0L)){
                    note.add(Constant.TIMEKEEPING_NOTE.ON_TIME);
                }
                if(!late.equals(0L)) {
                    note.add(Constant.TIMEKEEPING_NOTE.LATE);
                }
                if(!early.equals(0L)) {
                    note.add(Constant.TIMEKEEPING_NOTE.EARLY);
                }
                if(DataUtil.checkIsHoliday(timekeepingDate, holidays)){
                    note.add(Constant.TIMEKEEPING_NOTE.HOLIDAY);
                }
                if(DataUtil.checkIsWeekend(timekeepingDate)){
                    note.add(Constant.TIMEKEEPING_NOTE.WEEKEND);
                }
                if(!DataUtil.checkIsHoliday(timekeepingDate, holidays) && !DataUtil.checkIsWeekend(timekeepingDate)){
                    if(timekeeping.getArrivalTime() == null && timekeeping.getBackTime() == null){
                        note.add(Constant.TIMEKEEPING_NOTE.DAY_OFF_WITHOUT_REQUEST);
                    }
                }
            }else{
                note.add(Constant.TIMEKEEPING_NOTE.INVALID_TIMEKEEPING);
            }
        }
    }
}
