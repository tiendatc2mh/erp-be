package com.blameo.worktimeservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;

@Data
public class AvaiableDayResponse {

    private String availableDayOffId;

    private String userId;

    private Integer month;

    private Integer year;

    private Double availableDayOffOld;

    private Double availableDayOff;
}
