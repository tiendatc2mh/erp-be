package com.blameo.worktimeservice.dto.request;

import lombok.Data;

@Data
public class AvailableOffDTO {

    private String userId;

    private Double dayOffLastYear;

    private Double dayOffCurrentYear;

    public AvailableOffDTO(String userId, Double dayOffLastYear, Double dayOffCurrentYear) {
        this.userId = userId;
        this.dayOffLastYear = dayOffLastYear;
        this.dayOffCurrentYear = dayOffCurrentYear;
    }
}
