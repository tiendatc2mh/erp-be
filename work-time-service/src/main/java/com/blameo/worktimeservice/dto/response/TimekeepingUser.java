package com.blameo.worktimeservice.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TimekeepingUser {
    private String userId;

    private Double countHolidays;

    public TimekeepingUser(String userId, Double countHolidays){
        this.userId = userId;
        this.countHolidays = countHolidays;
    }
}
