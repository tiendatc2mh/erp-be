package com.blameo.worktimeservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 5/9/2023, Tuesday
 **/
@Configuration
public class CommonsMultipart {

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        return multipartResolver;
    }
}
