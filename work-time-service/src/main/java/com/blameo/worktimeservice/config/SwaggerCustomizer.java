package com.blameo.worktimeservice.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.stereotype.Component;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/26/2023, Wednesday
 **/
@Component
public class SwaggerCustomizer implements OpenApiCustomiser {


    @Override
    public void customise(OpenAPI openApi) {
        SecurityScheme securityScheme = new SecurityScheme()
                .type(SecurityScheme.Type.HTTP)
                .scheme("bearer")
                .bearerFormat("JWT");
        openApi.getComponents().addSecuritySchemes("bearer-key", securityScheme);
        SecurityRequirement securityRequirement = new SecurityRequirement();
        securityRequirement.addList("bearer-key");
        openApi.addSecurityItem(securityRequirement);
    }

}
