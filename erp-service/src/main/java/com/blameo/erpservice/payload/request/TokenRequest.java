package com.blameo.erpservice.payload.request;

import javax.validation.constraints.NotBlank;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/24/2023, Monday
 **/
public class TokenRequest {
    @NotBlank
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
