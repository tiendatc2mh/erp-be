package com.blameo.erpservice.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@AllArgsConstructor
@NoArgsConstructor
@Data
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String refreshToken;
	private String username;
	private String email;
	private List<String> roles;

	public JwtResponse(String token, String refreshToken, String username, String email, List<String> roles) {
		this.token = token;
		this.refreshToken = refreshToken;
		this.username = username;
		this.email = email;
		this.roles = roles;
	}
}
