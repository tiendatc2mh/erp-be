package com.blameo.erpservice.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/25/2023, Tuesday
 **/
@Configuration
public class SwaggerConfig {

    @Autowired
    private  SwaggerCustomizer swaggerCustomizer;

    @Value("${gateway.host}")
    private String serverHost;

    @Value("${gateway.port}")
    private String serverPort;

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI().addServersItem(new Server().url(serverHost + ":" + serverPort));
    }

    @Bean
    public OpenApiCustomiser openApiCustomiser() {
         return openApi ->  swaggerCustomizer.customise(openApi);
    }

}
