package com.blameo.erpservice.exception;//package com.blameo.idservice.exception;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.context.request.WebRequest;
//import org.springframework.web.multipart.MultipartException;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author : Phạm Hòa
// * @mailto : phoavn96@gmail.com
// * @created : 3/15/2023, Wednesday
// **/
//
//@SuppressWarnings({"unchecked", "rawtypes"})
//@ControllerAdvice
//public class BadRequestException extends ResponseEntityExceptionHandler {
//
//    @ExceptionHandler(RecordNotFoundException.class)
//    public final ResponseEntity<Object> handleUserNotFoundException(RecordNotFoundException ex, WebRequest request) {
//        List<String> details = new ArrayList<>();
//        details.add(ex.getLocalizedMessage());
//        ErrorResponse error = new ErrorResponse("Record Not Found", details);
//        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
//    }
//
//    @ExceptionHandler(MultipartException.class)
//    @ResponseStatus(value = HttpStatus.PAYLOAD_TOO_LARGE)
//    @ResponseBody
//    public ResponseEntity<Object> handleMultipartException(MultipartException exception){
//        logger.error(exception.getMessage());
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tập tin quá dung lượng cho phép");
//    }
//}
