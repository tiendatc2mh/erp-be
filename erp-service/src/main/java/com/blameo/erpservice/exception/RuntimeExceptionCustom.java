package com.blameo.erpservice.exception;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
public class RuntimeExceptionCustom extends RuntimeException{
    public RuntimeExceptionCustom() {
        super();
    }

    public RuntimeExceptionCustom(String message) {
        super(message);
    }

    public RuntimeExceptionCustom(String message, Throwable cause) {
        super(message, cause);
    }

    public RuntimeExceptionCustom(Throwable cause) {
        super(cause);
    }
}
