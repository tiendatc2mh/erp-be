package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.Attachment;
import com.blameo.erpservice.model.RequestRecruitmentUserContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RequestRecruitmentUserContentRepository extends JpaRepository<RequestRecruitmentUserContent, String> {

    List<RequestRecruitmentUserContent> findAllByRequestRecruitmentUserContentIdIn(List<String> listId);

    List<RequestRecruitmentUserContent> findAllByRequest_RequestId(String requestId);
}
