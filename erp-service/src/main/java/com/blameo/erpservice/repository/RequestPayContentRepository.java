package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.RequestPayContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RequestPayContentRepository extends JpaRepository<RequestPayContent, String> {
    @Query(value = "select * from erp.request_pay_content rpc where rpc.request_id = ?1 and rpc.request_pay_content_status = 1 ", nativeQuery = true)
    List<RequestPayContent> findByRequestId(String requestId);

    List<RequestPayContent> findByRequestPayContentIdInAndRequestPayContentStatusEquals(List<String> requestId,Integer status);

}
