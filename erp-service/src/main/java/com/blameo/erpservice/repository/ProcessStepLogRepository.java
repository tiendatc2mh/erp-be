package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.Process;
import com.blameo.erpservice.model.ProcessStepLog;
import com.blameo.erpservice.model.RequestLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ProcessStepLogRepository extends JpaRepository<ProcessStepLog, String> {

    @Query(value = "select * from process_step_log psl where psl.request_id = :requestId and psl.is_delete <> 1", nativeQuery = true)
    List<ProcessStepLog> getByRequestId(@Param("requestId") String requestId);

    @Query(value = "select psl from ProcessStepLog psl left join ProcessConfig pc on psl.processConfigId = pc.processConfigId where (pc.processStepType <> 0 or psl.processStepStatus <> 0) and psl.requestId = :requestId and psl.processStepStatus <> 4 order by psl.handleDate desc")
    List<ProcessStepLog> getAllByProcessRequestLogId(String requestId);

    @Query(value = "select psl from ProcessStepLog psl where psl.requestId = :requestId and psl.isDelete <> 1 and psl.processStepStatus in (:lstStatus) and psl.receiveUserId = :receiveUserId")
    List<ProcessStepLog> getByRequestIdAndListStatusAndReceiveUserId(@Param("requestId") String requestId, @Param("lstStatus") List<Integer> lstStatus, @Param("receiveUserId") String receiveUserId);

    @Query(value = "select * from process_step_log psl where psl.request_id = :requestId and " +
            "psl.process_step_status in (:status) and psl.receive_user_id = :receiveUserId and psl.is_delete <> 1 order by psl.order_process desc " +
            " limit 1 offset 0", nativeQuery = true)
    ProcessStepLog getProcessByReceiveUserIdAndOrderMaxAndStatus(@Param("requestId") String requestId, @Param("status") List<Integer> status, @Param("receiveUserId") String receiveUserId);

    @Query(value = "select * from process_step_log psl where psl.request_id = :requestId and " +
            "psl.receive_user_id = :receiveUserId and psl.is_delete <> 1 order by psl.order_process desc " +
            " limit 1 offset 0", nativeQuery = true)
    ProcessStepLog getProcessByReceiveUserIdAndOrderMax(@Param("requestId") String requestId, @Param("receiveUserId") String receiveUserId);

    @Query(value = "select * from process_step_log psl where psl.process_config_id in (:processConfig) and " +
            "psl.process_step_status = :status and psl.is_delete <> 1 and psl.request_id = :requestId", nativeQuery = true)
    List<ProcessStepLog> getProcessStepLogByProcessConfig(@Param("processConfig") List<String> processConfig, @Param("status") Integer status, @Param("requestId") String requestId);

    @Query(value = "select * from process_step_log psl where psl.request_id = :requestId and psl.is_delete <> 1 order by psl.order_process desc limit 1 offset 0", nativeQuery = true)
    ProcessStepLog getProcessLogMax(@Param("requestId") String requestId);

    @Query(value = "select * from process_step_log psl where psl.request_id = :requestId and psl.process_config_id = :processConfig and psl.is_delete <> 1 order by psl.order_process desc limit 1 offset 0", nativeQuery = true)
    ProcessStepLog getProcessLogMaxAndProcessConfig(@Param("requestId") String requestId, @Param("processConfig") String processConfig);

    @Query(value = "select * from process_step_log psl where psl.receive_user_id = :receiveUserId and psl.request_id = :requestId and psl.process_step_status = :status", nativeQuery = true)
    List<ProcessStepLog> getProcessLogByStatus(@Param("receiveUserId") String receiveUserId, @Param("requestId") String requestId, @Param("status") Integer status);

    @Query(value = "select * from process_step_log psl where psl.request_id = :requestId and psl.process_step_status = :status", nativeQuery = true)
    List<ProcessStepLog> getAllProcessLogByStatus(@Param("requestId") String requestId, @Param("status") Integer status);

    @Query(value = "select t.* from (select request_id, max(order_process) as maxOrder from process_step_log where is_delete = 0 " +
            "and (:receiveUserId is null or receive_user_id = :receiveUserId)" +
            "group by request_id) r inner join process_step_log t on t.request_id = r.request_id " +
            "and r.maxOrder = t.order_process and (:receiveUserId is null or t.receive_user_id = :receiveUserId) " +
            "and t.request_id = :requestId limit 1 offset 0", nativeQuery = true)
    ProcessStepLog getProcessStepStatusByRequestIdAndReceiveUserId(@Param("receiveUserId") String receiveUserId, @Param("requestId") String requestId);

}
