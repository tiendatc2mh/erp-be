package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.ProcessConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface ProcessConfigRepository extends JpaRepository<ProcessConfig, String> {
    @Query(value = "select * from erp.process_config pc where pc.process_id = ?1", nativeQuery = true)
    List<ProcessConfig> getAll(String processId);

    @Query(value = "select pc.* from erp.process_config pc inner join process p on pc.process_id=p.process_id " +
            "inner join process_step pt on pt.process_step_id=pc.process_step_id  " +
            "where p.process_type=?1 and p.process_status=1 and p.is_default=1 and pt.process_step_code = ?2 and pc.process_step_type = ?3", nativeQuery = true)
    List<ProcessConfig> getFirstConfigByProcessType(Integer processType, String processCodeStart, Integer processStepType);

    @Query(value = "select pc.processConfigId from ProcessConfig pc inner join Process p on pc.process.processId = p.processId " +
            "where p.processId= ?1 and p.processStatus = 1 and p.isDefault = 1 and pc.processStepType = ?2")
    List<String> getProcessConfigByProcessTypeAndProcessStepType(String processId, Integer processStepType);

    @Query(value = "select pc2 from ProcessConfig pc inner join ProcessConfig pc2 on pc.processStepNext.processStepId = pc2.processStep.processStepId " +
            "where pc.processConfigId = ?1 and pc2.processStepType in (?2)")
    List<ProcessConfig> getProcessConfigNext(String processConfigId, List<Integer> processStepType);

    @Query(value = "select pc.* from process_config pc inner join process_config pc2 on pc.process_step_next = pc2.process_step_id " +
            "where pc2.process_config_id = ?1 and pc.process_step_type in (?2) limit 1 offset 0", nativeQuery = true)
    ProcessConfig getProcessConfigPrevious(String processConfigId, List<Integer> processStepType);

    @Query(value = "select * from erp.process_config pc where pc.process_step_id = :processStepId and pc.process_step_type = :processStepType limit 1 offset 0", nativeQuery = true)
    ProcessConfig getProcessStepByProcessStepType(@Param("processStepId") String processStepId, @Param("processStepType") Integer processStepType);

    @Query(value = "WITH RECURSIVE ProcessConfig AS (" +
            "    SELECT pc.process_config_id, pc.process_step_id , pc.process_step_next " +
            "    FROM process_config pc " +
            "    WHERE process_step_id = :processStepId and process_step_type = 1 " +
            "    UNION ALL " +
            "    SELECT pc.process_config_id, pc.process_step_id , pc.process_step_next " +
            "    FROM process_config pc " +
            "    JOIN ProcessConfig rh ON pc.process_step_id = rh.process_step_next" +
            "    WHERE process_step_type = 1 " +
            ")" +
            "SELECT DISTINCT process_config_id " +
            "FROM ProcessConfig;", nativeQuery = true)
    List<String> getAllDescendantProcessConfig(@Param("processStepId") String processStepId);

    @Query(value = "select pc from ProcessConfig pc where pc.processStep.processStepId = :processStepId and pc.processStepType <> :processStepType")
    ProcessConfig getProcessConfigByStepId(@Param("processStepId") String processStepId, @Param("processStepType") Integer processStepType);

    @Query(value = "select distinct pcf.* from process_config pcf inner join (select pc.process_step_next from process_step_log psl " +
            "inner join request r on r.request_id = psl.request_id " +
            "inner join process_config pc on psl.process_config_id = pc.process_config_id " +
            "WHERE psl.receive_user_id = :receiveUserId and psl.is_delete = 0 " +
            "and psl.process_step_status in (1, 2) and r.request_id = :requestId " +
            "ORDER by psl.order_process DESC LIMIT 1) psn on pcf.process_step_id = psn.process_step_next", nativeQuery = true)
    List<ProcessConfig> getProcessConfigByProcessLogMaxAndRequestIdAndReceiveUserId(@Param("requestId") String requestId, @Param("receiveUserId") String receiveUserId);
}
