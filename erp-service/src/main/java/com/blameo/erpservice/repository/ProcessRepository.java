package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.Process;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProcessRepository extends JpaRepository<Process, String> {
    Process getByProcessTypeAndAndIsDefaultEqualsAndProcessStatusEquals(Integer processTypeId, Integer isDefault, Integer status);

    boolean existsByProcessIdAndProcessStatusGreaterThanEqual(String id, Integer status);

    @Query(value = "select * from erp.process p where p.process_status != -1 order by p.created_date desc ", nativeQuery = true)
    List<Process> getAll();

    Optional<Process> findByProcessTypeAndProcessStatusAndIsDefault(Integer processType, Integer processStatus, Integer isDefault);

    @Query(value = " select * from erp.process p where lower(p.process_name) like lower(:keyword)  ESCAPE '!' " +
            "and (:processType is null or p.process_type = :processType) and p.process_status != -1 and (:isDefault is null or p.is_default = :isDefault) order by p.created_date desc", nativeQuery = true)
    Page<Process> findAll(Pageable paging, String keyword, Integer processType, Integer isDefault);

}
