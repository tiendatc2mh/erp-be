package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.RequestLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RequestLogRepository extends JpaRepository<RequestLog, String> {
    List<RequestLog> findAllByRequestIdOrderByRequestLogDateDescRequestLogActionDesc(String requestId);

    @Query(value = "select * from request_log r where r.request_id = :requestId and r.request_log_action = :requestLogAction and r.request_approve is not null order by r.request_log_date desc limit 1 offset 0", nativeQuery = true)
    RequestLog findRequestLogByRequestIdAndRequestLogAction(@Param("requestId") String requestId, @Param("requestLogAction") Integer requestLogAction);
}
