package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.Attachment;
import com.blameo.erpservice.model.RequestRecruitmentContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AttachmentRepository extends JpaRepository<Attachment, String> {
    @Query(value = "select a from Attachment a where a.objectId = :objectId and a.attachmentStatus = 1")
    List<Attachment> getByObjectIdAndAttachmentStatus(@Param("objectId") String objectId);

    List<Attachment> findAllByAttachmentIdIn(List<String> listId);
}
