package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.RequestRecruitmentContent;
import com.blameo.erpservice.model.RequestWorktimeContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RequestRecruitmentContentRepository extends JpaRepository<RequestRecruitmentContent, String> {

    List<RequestRecruitmentContent> findAllByRequestRecruitmentContentIdIn(List<String> listId);

    RequestRecruitmentContent findByRequest_RequestId(String requestId);
}
