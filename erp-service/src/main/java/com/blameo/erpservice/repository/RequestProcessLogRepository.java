package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.ProcessStepLog;
import com.blameo.erpservice.model.RequestLog;
import com.blameo.erpservice.model.RequestProcessLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RequestProcessLogRepository extends JpaRepository<RequestProcessLog, String> {

 @Query(value = "select * from request_process_log rpl where rpl.request_id = :requestId and " +
         "rpl.request_process_log_status in (:status) and rpl.receive_user_id = :receiveUserId order by rpl.order_process desc " +
         " limit 1 offset 0", nativeQuery = true)
 RequestProcessLog getProcessByReceiveUserIdAndOrderMaxAndStatus(String requestId, List<Integer> status, String receiveUserId);

 @Query(value = "select * from request_process_log rpl where rpl.request_id = :requestId order by rpl.order_process desc limit 1 offset 0", nativeQuery = true)
 RequestProcessLog getRequestProcessLogMax(String requestId);

 List<RequestProcessLog> findAllByRequestIdOrderByOrderProcessDesc(String id);
}
