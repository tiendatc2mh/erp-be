package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.Request;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface RequestRepository extends JpaRepository<Request, String> {
    Boolean existsByRequestId(String id);

    List<Request> findByRequestCodeIn(List<String> requestCode);

    @Query(value = "select distinct r.* from request r inner join (select t.request_id, t.process_step_status, t.process_config_id " +
                    "from (select request_id, max(order_process) as maxOrder from process_step_log where is_delete = 0 " +
                    "and (:receiveUserId is null or receive_user_id = :receiveUserId)" +
                    "group by request_id) r inner join process_step_log t on t.request_id = r.request_id " +
                    "and r.maxOrder = t.order_process and (:receiveUserId is null or t.receive_user_id = :receiveUserId)" +
                    ") y on r.request_id = y.request_id " +
                    "inner join process_config pcg on y.process_config_id = pcg.process_config_id " +
                    "inner join process_step psp on psp.process_step_id = pcg.process_step_next " +
                    "inner join process p on r.process_id = p.process_id and p.is_default = 1 and p.process_status = 1 and p.process_type = :processType " +
                    "left join request_worktime_content rwc on r.request_id = rwc.request_id and rwc.request_worktime_content_status = 1 " +
                    "left join request_pay_content rpc on r.request_id = rpc.request_id and rpc.request_pay_content_status = 1 " +
                    "left join request_recruitment_content rrc on r.request_id = rrc.request_id and rrc.request_recruitment_content_status = 1 " +
                    "where lower(r.request_code) like lower(:requestCode) ESCAPE '!' " +
                    "and (:fromDate is null or r.created_date >= :fromDate) " +
                    "and (:toDate is null or r.created_date <= :toDate) " +
                    "and (coalesce(:requestWorkTimeType) is null or rwc.request_work_time_type = :requestWorkTimeType) " +
                    "and (coalesce(:dayOffType) is null or rwc.request_day_off_type = :dayOffType) " +
                    "and (coalesce(:requestOtType) is null or rwc.request_ot_type = :requestOtType) " +
                    "and (coalesce(:payType) is null or r.pay_type = :payType) " +
                    "and (coalesce(:position) is null or :position = '' or rrc.position_id = :position) " +
                    "and (coalesce(:listUserStr) is null or r.created_by in (:listUserStr)) " +
                    "and r.request_status not in (4) and (coalesce(:status) is null or y.process_step_status in (:status))" +
                    "and (psp.process_step_code <> :stepCodeStart or y.process_step_status <> 4) order by r.created_date desc",
            countQuery = "select count(distinct r.request_id) from request r inner join (select t.request_id, t.process_step_status, t.process_config_id " +
                    "from (select request_id, max(order_process) as maxOrder from process_step_log where is_delete = 0 " +
                    "and (:receiveUserId is null or receive_user_id = :receiveUserId)" +
                    "group by request_id) r inner join process_step_log t on t.request_id = r.request_id " +
                    "and r.maxOrder = t.order_process and (:receiveUserId is null or t.receive_user_id = :receiveUserId)" +
                    ") y on r.request_id = y.request_id " +
                    "inner join process_config pcg on y.process_config_id = pcg.process_config_id " +
                    "inner join process_step psp on psp.process_step_id = pcg.process_step_next " +
                    "inner join process p on r.process_id = p.process_id and p.is_default = 1 and p.process_status = 1 and p.process_type = :processType " +
                    "left join request_worktime_content rwc on r.request_id = rwc.request_id and rwc.request_worktime_content_status = 1 " +
                    "left join request_pay_content rpc on r.request_id = rpc.request_id and rpc.request_pay_content_status = 1 " +
                    "left join request_recruitment_content rrc on r.request_id = rrc.request_id and rrc.request_recruitment_content_status = 1 " +
                    "where lower(r.request_code) like lower(:requestCode) ESCAPE '!' " +
                    "and (:fromDate is null or r.created_date >= :fromDate) " +
                    "and (:toDate is null or r.created_date <= :toDate) " +
                    "and (coalesce(:requestWorkTimeType) is null or rwc.request_work_time_type = :requestWorkTimeType) " +
                    "and (coalesce(:dayOffType) is null or rwc.request_day_off_type = :dayOffType) " +
                    "and (coalesce(:requestOtType) is null or rwc.request_ot_type = :requestOtType) " +
                    "and (coalesce(:payType) is null or r.pay_type = :payType) " +
                    "and (coalesce(:position) is null or :position = '' or rrc.position_id = :position) " +
                    "and (coalesce(:listUserStr) is null or r.created_by in (:listUserStr)) " +
                    "and r.request_status not in (4) and (coalesce(:status) is null or y.process_step_status in (:status))" +
                    "and (psp.process_step_code <> :stepCodeStart or y.process_step_status <> 4) order by r.created_date desc"
            , nativeQuery = true)
    Page<Request> getAllRequest(Pageable paging, @Param("processType") Integer processType, @Param("receiveUserId") String receiveUserId,
                                @Param("requestCode") String requestCode, @Param("status") List<Integer> status,
                                @Param("fromDate") Date fromDate, @Param("toDate") Date toDate,
                                @Param("requestWorkTimeType") Integer requestWorkTimeType, @Param("dayOffType") Integer dayOffType,
                                @Param("requestOtType") Integer requestOtType, @Param("payType") Integer payType,
                                @Param("position") String position, List<String> listUserStr, String stepCodeStart);

    @Query(value = "select distinct r.* from request r inner join (select t.request_id, t.process_step_status, t.process_config_id " +
            "from (select request_id, max(order_process) as maxOrder from process_step_log where is_delete = 0 " +
            "and (:receiveUserId is null or receive_user_id = :receiveUserId)" +
            "group by request_id) r inner join process_step_log t on t.request_id = r.request_id " +
            "and r.maxOrder = t.order_process and (:receiveUserId is null or t.receive_user_id = :receiveUserId)" +
            ") y on r.request_id = y.request_id " +
            "inner join process_config pcg on y.process_config_id = pcg.process_config_id " +
            "inner join process_step psp on psp.process_step_id = pcg.process_step_next " +
            "inner join process p on r.process_id = p.process_id and p.is_default = 1 and p.process_status = 1 and p.process_type = :processType " +
            "where r.request_status not in (4,6) and y.process_step_status <> 3 " +
            "and (psp.process_step_code <> :stepCodeStart or y.process_step_status <> 4) order by r.created_date desc", nativeQuery = true)
    List<Request> getAllRequestWithoutPaging(@Param("processType") Integer processType, @Param("receiveUserId") String receiveUserId,
                                String stepCodeStart);

    @Query(value = "select distinct r.* from request r inner join (select t.request_id, t.process_step_status, t.process_config_id " +
            "from (select request_id, max(order_process) as maxOrder from process_step_log where is_delete = 0 " +
            "and (:receiveUserId is null or receive_user_id = :receiveUserId)" +
            "group by request_id) r inner join process_step_log t on t.request_id = r.request_id " +
            "and r.maxOrder = t.order_process and (:receiveUserId is null or t.receive_user_id = :receiveUserId)" +
            ") y on r.request_id = y.request_id " +
            "inner join process_config pcg on y.process_config_id = pcg.process_config_id " +
            "inner join process_step psp on psp.process_step_id = pcg.process_step_next " +
            "inner join process p on r.process_id = p.process_id and p.is_default = 1 and p.process_status = 1 and p.process_type = :processType " +
            "left join request_worktime_content rwc on r.request_id = rwc.request_id and rwc.request_worktime_content_status = 1 " +
            "left join request_pay_content rpc on r.request_id = rpc.request_id and rpc.request_pay_content_status = 1 " +
            "left join request_recruitment_content rrc on r.request_id = rrc.request_id and rrc.request_recruitment_content_status = 1 " +
            "where lower(r.request_code) like lower(:requestCode) ESCAPE '!' " +
            "and (:fromDate is null or r.created_date >= :fromDate) " +
            "and (:toDate is null or r.created_date <= :toDate) " +
            "and (coalesce(:requestWorkTimeType) is null or rwc.request_work_time_type = :requestWorkTimeType) " +
            "and (coalesce(:dayOffType) is null or rwc.request_day_off_type = :dayOffType) " +
            "and (coalesce(:requestOtType) is null or rwc.request_ot_type = :requestOtType) " +
            "and (coalesce(:payType) is null or r.pay_type = :payType) " +
            "and (coalesce(:position) is null or :position = '' or rrc.position_id = :position) " +
            "and (coalesce(:listUserStr) is null or r.created_by in (:listUserStr)) " +
            "and r.request_status not in (4) and (coalesce(:status) is null or y.process_step_status in (:status))" +
            "and (psp.process_step_code <> :stepCodeStart or y.process_step_status <> 4) order by r.created_date desc", nativeQuery = true)
    List<Request> getAllRequestExport(Integer processType, String receiveUserId,
                                String requestCode, List<Integer> status,
                                Date fromDate, Date toDate,
                                Integer requestWorkTimeType, Integer dayOffType,
                                Integer requestOtType, Integer payType,
                                String position, List<String> listUserStr, String stepCodeStart);

    @Query(value = "select count(distinct r.request_id) from request r inner join (select t.request_id, t.process_step_status, t.process_config_id " +
            "from (select request_id, max(order_process) as maxOrder from process_step_log where is_delete = 0 " +
            "and (:receiveUserId is null or receive_user_id = :receiveUserId)" +
            "group by request_id) r inner join process_step_log t on t.request_id = r.request_id " +
            "and r.maxOrder = t.order_process and (:receiveUserId is null or t.receive_user_id = :receiveUserId)" +
            ") y on r.request_id = y.request_id " +
            "inner join process_config pcg on y.process_config_id = pcg.process_config_id " +
            "inner join process_step psp on psp.process_step_id = pcg.process_step_next " +
            "inner join process p on r.process_id = p.process_id and p.is_default = 1 and p.process_status = 1 and p.process_type = :processType " +
            "left join request_worktime_content rwc on r.request_id = rwc.request_id and rwc.request_worktime_content_status = 1 " +
            "left join request_pay_content rpc on r.request_id = rpc.request_id and rpc.request_pay_content_status = 1 " +
            "left join request_recruitment_content rrc on r.request_id = rrc.request_id and rrc.request_recruitment_content_status = 1 " +
            "where r.request_status not in (4) and (coalesce(:status) is null or y.process_step_status in (:status))" +
            "and (psp.process_step_code <> :stepCodeStart or y.process_step_status <> 4) order by r.created_date desc", nativeQuery = true)
    Long getCountAllRequest(Integer processType, String receiveUserId, List<Integer> status, String stepCodeStart);

    @Query(value = "select distinct r from Request r " +
            "inner join Process p on r.process.processId = p.processId and p.isDefault = 1 and p.processStatus = 1 " +
            "left join RequestWorktimeContent rwc on r.requestId = rwc.request.requestId and rwc.requestWorktimeContentStatus = 1 " +
            "left join RequestPayContent rpc on r.requestId = rpc.request.requestId and rpc.requestPayContentStatus = 1 " +
            "left join RequestRecruitmentContent rrc on r.requestId = rrc.request.requestId and rrc.requestRecruitmentContentStatus = 1 " +
            "WHERE p.processType = :processType and lower(r.requestCode) like lower(:requestCode) ESCAPE '!' " +
            "and (coalesce(:status) is null or r.requestStatus in (:status)) and (:fromDate is null or r.createdDate >= :fromDate) " +
            "and (:toDate is null or r.createdDate <= :toDate) and (:isAll = true or r.createdBy = :userId) and r.requestStatus not in (4) " +
            "and (lower(r.requestCode) like lower(:keyword) ESCAPE '!' or lower(r.content) like lower(:keyword)  ESCAPE '!') " +
            "and (coalesce(:requestWorkTimeType) is null or rwc.requestWorkTimeType = :requestWorkTimeType) " +
            "and (coalesce(:dayOffType) is null or rwc.requestDayOffType = :dayOffType) " +
            "and (coalesce(:requestOtType) is null or rwc.requestOtType = :requestOtType) " +
            "and (coalesce(:payType) is null or r.payType = :payType) " +
            "and (coalesce(:position) is null or :position = '' or rrc.positionId = :position) " +
            "and (coalesce(:listUserStr) is null or r.createdBy in (:listUserStr)) " +
            "order by r.createdDate desc")
    Page<Request> getAllRequestByRequestStatus(Pageable paging, Integer processType,
                                               String requestCode, List<Integer> status,
                                               Date fromDate, Date toDate, String userId,
                                               Integer requestWorkTimeType, Integer dayOffType,
                                               Integer requestOtType, Integer payType, String keyword,
                                               String position, List<String> listUserStr, Boolean isAll);

    @Query(value = "select distinct r from Request r " +
            "inner join Process p on r.process.processId = p.processId and p.isDefault = 1 and p.processStatus = 1 " +
            "left join RequestWorktimeContent rwc on r.requestId = rwc.request.requestId and rwc.requestWorktimeContentStatus = 1 " +
            "left join RequestPayContent rpc on r.requestId = rpc.request.requestId and rpc.requestPayContentStatus = 1 " +
            "left join RequestRecruitmentContent rrc on r.requestId = rrc.request.requestId and rrc.requestRecruitmentContentStatus = 1 " +
            "WHERE p.processType = :processType and lower(r.requestCode) like lower(:requestCode) ESCAPE '!' " +
            "and (coalesce(:status) is null or r.requestStatus in (:status)) and (:fromDate is null or r.createdDate >= :fromDate) " +
            "and (:toDate is null or r.createdDate <= :toDate) and (:isAll = true or r.createdBy = :userId) and r.requestStatus not in (4) " +
            "and (coalesce(:requestWorkTimeType) is null or rwc.requestWorkTimeType = :requestWorkTimeType) " +
            "and (lower(r.requestCode) like lower(:keyword) ESCAPE '!' or lower(r.content) like lower(:keyword)  ESCAPE '!') " +
            "and (coalesce(:dayOffType) is null or rwc.requestDayOffType = :dayOffType) " +
            "and (coalesce(:requestOtType) is null or rwc.requestOtType = :requestOtType) " +
            "and (coalesce(:payType) is null or r.payType = :payType) " +
            "and (coalesce(:position) is null or :position = '' or rrc.positionId = :position) " +
            "and (coalesce(:listUserStr) is null or r.createdBy in (:listUserStr)) " +
            "order by r.createdDate desc")
    List<Request> getAllRequestByRequestStatusExport(Integer processType,
                                                     String requestCode, List<Integer> status,
                                                     Date fromDate, Date toDate, String userId,
                                                     Integer requestWorkTimeType, Integer dayOffType,
                                                     Integer requestOtType, Integer payType, String keyword,
                                                     String position, List<String> listUserStr, Boolean isAll);

    @Query(value = "select count(distinct r.request_id) from request r " +
            "inner join process p on r.process_id = p.process_id and is_default = 1 and p.process_status = 1 " +
            "WHERE p.process_type = :processType and (coalesce(:status) is null or r.request_status in (:status)) " +
            "and r.request_status not in (4) and (:isAll = true or r.created_by = :userId) " +
            "order by r.created_date desc", nativeQuery = true)
    Long getCountAllRequestByRequestStatus(@Param("processType") Integer processType, @Param("status") List<Integer> status,
                                            @Param("userId") String userId, @Param("isAll") Boolean isAll);

    @Query(value = "select * from request r where r.request_code like :requestCode order by request_code desc limit 1 offset 0", nativeQuery = true)
    Request findByRequestCodeLikeFirst(@Param("requestCode") String requestCode);

    @Query(value = "select distinct r.* from request r " +
            "inner join process p on r.process_id = p.process_id and is_default = 1 and p.process_status = 1 " +
            "left join request_worktime_content rwc on r.request_id = rwc.request_id and rwc.request_worktime_content_status = 1 " +
            "WHERE p.process_type in (:processType) and lower(r.request_code) like lower(:keyword) ESCAPE '!' " +
            "and (:userId is null or r.created_by = :userId ) " +
            "and (:status is null or r.request_status in (:status)) and (:fromDate is null or r.created_date > :fromDate) " +
            "and (:toDate is null or r.created_date < :toDate)" +
            "and ((p.process_type = 1 and (DATE_FORMAT(rwc.day_off_begin, '%Y-%m-%d') between DATE_FORMAT(:worktimeToDate, '%Y-%m-%d') and DATE_FORMAT(:worktimeFromDate, '%Y-%m-%d') or " +
            "DATE_FORMAT(rwc.day_off_end, '%Y-%m-%d') between DATE_FORMAT(:worktimeToDate, '%Y-%m-%d') and DATE_FORMAT(:worktimeFromDate, '%Y-%m-%d'))) " +
            "or (p.process_type = 2 and DATE_FORMAT(rwc.request_ot_date, '%Y-%m-%d') between DATE_FORMAT(:worktimeToDate, '%Y-%m-%d') and DATE_FORMAT(:worktimeFromDate, '%Y-%m-%d'))" +
            "or (p.process_type = 3 and DATE_FORMAT(rwc.remote_date, '%Y-%m-%d') between DATE_FORMAT(:worktimeToDate, '%Y-%m-%d') and DATE_FORMAT(:worktimeFromDate, '%Y-%m-%d'))" +
            "or (p.process_type = 6 and DATE_FORMAT(rwc.request_work_time_date, '%Y-%m-%d') between DATE_FORMAT(:worktimeToDate, '%Y-%m-%d') and DATE_FORMAT(:worktimeFromDate, '%Y-%m-%d'))) " +
            "order by r.created_date desc", nativeQuery = true)
    List<Request> getAllRequestWithoutPaging(@Param("processType") List<Integer> processType, @Param("userId") String userId,
                                             @Param("keyword") String keyword, @Param("status") List<Integer> status,
                                             @Param("fromDate") Date fromDate, @Param("toDate") Date toDate,
                                             @Param("worktimeFromDate") Date worktimeFromDate, @Param("worktimeToDate") Date worktimeToDate);

    //chia 2 trường hợp 1 là request nằm trong request_advance: nếu request của request_advance đó có trạng thái bằng 0 và 3 thì vẫn lấy request_advance
    //trường hợp 2 là request chưa nằm trong request_advance nào thì lấy
    @Query(value = "select distinct rp.* from (select r.* from request r inner join process p on r.process_id = p.process_id " +
            "inner join request_advance ra on r.request_id = ra.request_id " +
            "inner join request_pay_content rpc on ra.request_pay_content_id = rpc.request_pay_content_id " +
            "inner join request r2 on rpc.request_id = r2.request_id " +
            "where p.process_type = 5 AND r2.request_status in (0,3) and r.request_status = 5 and r.pay_type = 1 " +
            "union all " +
            "select r.* from request r inner join process p on r.process_id = p.process_id " +
            "left join request_advance ra on r.request_id = ra.request_id " +
            "where ra.request_pay_content_id is null and r.pay_type = 1 and r.request_status = 5) " +
            "rp where rp.created_by = :userId", nativeQuery = true)
    List<Request> getLstRequestAdvancePaymentByUserIdAndRequestStatus(@Param("userId") String userId);

    @Query(value = "select r from Request r where r.process.processId = :processId and r.requestStatus in (1,2)")
    List<Request> findByProcess_ProcessIdAndRequestStatus(String processId);

    @Query(value = "select distinct r.* from request r " +
            "inner join process p on r.process_id = p.process_id and is_default = 1 and p.process_status = 1 " +
            "left join request_worktime_content rwc on r.request_id = rwc.request_id and rwc.request_worktime_content_status = 1 " +
            "WHERE (:userId is null or r.created_by = :userId ) " +
            "and r.request_status not in (0,4,6) " +
            "and ((p.process_type = 1 and ((DATE_FORMAT(rwc.day_off_begin, '%Y-%m-%d') between DATE_FORMAT(:worktimeToDate, '%Y-%m-%d') and DATE_FORMAT(:worktimeFromDate, '%Y-%m-%d') or " +
            "DATE_FORMAT(rwc.day_off_end, '%Y-%m-%d') between DATE_FORMAT(:worktimeToDate, '%Y-%m-%d') and DATE_FORMAT(:worktimeFromDate, '%Y-%m-%d')) and " +
            "(coalesce(:dayOffType) is null or rwc.day_off_type in (:dayOffType)))) " +
            "or (p.process_type = 3 and DATE_FORMAT(rwc.remote_date, '%Y-%m-%d') between DATE_FORMAT(:worktimeToDate, '%Y-%m-%d') and DATE_FORMAT(:worktimeFromDate, '%Y-%m-%d') " +
            "and (coalesce(:dayOffType) is null or rwc.remote_type in (:dayOffType)))" +
            "or (p.process_type = 6 and (DATE_FORMAT(rwc.request_work_time_date, '%Y-%m-%d') between DATE_FORMAT(:worktimeToDate, '%Y-%m-%d') and DATE_FORMAT(:worktimeFromDate, '%Y-%m-%d'))))" +
            "order by r.created_date desc", nativeQuery = true)
    List<Request> getAllRequestDuplicate(@Param("userId") String userId,
                                             @Param("worktimeFromDate") Date worktimeFromDate,
                                             @Param("worktimeToDate") Date worktimeToDate,
                                             @Param("dayOffType") List<Integer> dayOffType);

}
