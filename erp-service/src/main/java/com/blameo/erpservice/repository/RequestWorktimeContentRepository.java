package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.RequestWorktimeContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RequestWorktimeContentRepository extends JpaRepository<RequestWorktimeContent, String> {

    List<RequestWorktimeContent> findAllByRequest_RequestId(String requestId);

    List<RequestWorktimeContent> findAllByRequestWorktimeContentIdIn(List<String> listId);
}