package com.blameo.erpservice.repository;

import com.blameo.erpservice.model.ProcessStep;
import com.blameo.erpservice.model.RequestPayContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface ProcessStepRepository extends JpaRepository<ProcessStep, String> {

    @Query(value = "delete from erp.process_step where process_step_id in (?1)", nativeQuery = true)
    void deleteByLstProcessStepId(Set<String> id);

    @Query(value = "select * from erp.process_step ps where ps.process_step_id in (?1)", nativeQuery = true)
    List<ProcessStep> getAllByListId(List<String> requestId);
}
