package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.dto.request.ProcessStepDTO;
import com.blameo.erpservice.dto.response.ProcessStepDTOResponse;
import com.blameo.erpservice.model.ProcessConfig;
import com.blameo.erpservice.model.ProcessStep;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.repository.ProcessConfigRepository;
import com.blameo.erpservice.repository.ProcessStepRepository;
import com.blameo.erpservice.service.ProcessStepService;
import com.blameo.erpservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProcessStepServiceImpl implements ProcessStepService {

    @Autowired
    ProcessStepRepository processStepRepository;

    @Autowired
    ProcessConfigRepository processConfigRepository;

    @Override
    public ProcessStepDTOResponse save(ProcessStepDTO processStepDTO, UserDTOResponse userDTOResponse) {
        ProcessStepDTOResponse processTypeDTOResponse = new ProcessStepDTOResponse();
        ProcessStep processStep = new ProcessStep();
        CoppyObject.copyNonNullProperties(processStepDTO,processStep);
        processStep.setProcessStepId(null);
        processStep.setCreatedBy(userDTOResponse.getUserId());
        ProcessStep save = processStepRepository.save(processStep);
        BeanUtils.copyProperties(save, processTypeDTOResponse);
        return processTypeDTOResponse;
    }

    @Override
    public void deleteAll(Set<String> processId) {
        String pathParam = processId.stream().map(Object::toString)
                .collect(Collectors.joining(","));
        processStepRepository.deleteByLstProcessStepId(processId);
    }

}
