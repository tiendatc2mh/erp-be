package com.blameo.erpservice.service;

import com.blameo.erpservice.dto.request.AttachmentDTO;
import com.blameo.erpservice.model.Attachment;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AttachmentService {
    void saveAndDeleteAttachment(List<AttachmentDTO> lstSave, String objectId, HttpServletRequest request, String requestId, Integer attachType);
}
