package com.blameo.erpservice.service;

import com.blameo.erpservice.modeluser.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RequestPayContentService {
    void deleteList(List<String> id);
}
