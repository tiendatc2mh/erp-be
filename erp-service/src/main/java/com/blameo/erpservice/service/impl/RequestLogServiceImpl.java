package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.controller.CallOtherService;
import com.blameo.erpservice.dto.response.RequestLogDTOResponse;
import com.blameo.erpservice.model.RequestLog;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.repository.RequestLogRepository;
import com.blameo.erpservice.service.RequestLogService;
import com.blameo.erpservice.utils.CommonUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RequestLogServiceImpl implements RequestLogService {

    @Autowired
    RequestLogRepository requestLogRepository;

    @Override
    public List<RequestLogDTOResponse> save(List<RequestLog> requestLog) {
        List<RequestLog> saved = requestLogRepository.saveAll(requestLog);
        List<RequestLogDTOResponse> requestLogDTOResponses = new ArrayList<>();
        if(saved != null && saved.size() > 0){
            saved.forEach(x -> {
                RequestLogDTOResponse requestLogDTOResponse = new RequestLogDTOResponse();
                BeanUtils.copyProperties(x, requestLogDTOResponse);
                requestLogDTOResponses.add(requestLogDTOResponse);
            });
        }
        return requestLogDTOResponses;
    }

    @Override
    public List<RequestLogDTOResponse> getAll(String requestId, HttpServletRequest httpRequest) {
        List<RequestLog> requestLogs = requestLogRepository.findAllByRequestIdOrderByRequestLogDateDescRequestLogActionDesc(requestId);
        Set<String> listUserId = requestLogs.stream().map(requestLog -> requestLog.getRequestLogUser()).collect(Collectors.toSet());

        List<UserDTOResponse> listUser = CallOtherService.getListFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_MULTIPLE_ID, listUserId, httpRequest);

        List<RequestLogDTOResponse> res =  CommonUtil.mapList(requestLogs, RequestLogDTOResponse.class);
        res.stream().forEach(dto -> dto.setRequestLogUserFullname(
                listUser.stream()
                        .filter(userDTOResponse -> userDTOResponse.getUserId().equals(dto.getRequestLogUser()))
                        .findAny()
                        .orElse(new UserDTOResponse())
                        .getFullName()
        ));
        return res;
    }
}
