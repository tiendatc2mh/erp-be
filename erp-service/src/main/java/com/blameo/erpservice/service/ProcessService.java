package com.blameo.erpservice.service;

import com.blameo.erpservice.dto.response.ProcessDTOResponse;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Component
public interface ProcessService {
    ProcessDTOResponse save(Map<String, Object> processTypeDTO, UserDTOResponse userDTOResponse);
    ProcessDTOResponse get(Integer id, HttpServletRequest httpServletRequest);
    Map<String, Object> getAll(Integer id, HttpServletRequest httpServletRequest);
    void delete(String id, UserDTOResponse userDTOResponse);
    Map<String, Object> getAllProcess(String id, HttpServletRequest httpServletRequest);
    ProcessDTOResponse update(String id, Map<String, Object> processTypeDTO, UserDTOResponse userDTOResponse, HttpServletRequest httpServletRequest);
    ProcessDTOResponse updateDefault(String id, UserDTOResponse userDTOResponse, HttpServletRequest httpServletRequest);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer processType, Integer isDefault);

    List<ProcessDTOResponse> getAllProcessType();
}
