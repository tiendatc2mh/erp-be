package com.blameo.erpservice.service;

import com.blameo.erpservice.dto.request.ProcessConfigDTO;
import com.blameo.erpservice.dto.response.ProcessConfigDTOResponse;
import com.blameo.erpservice.dto.response.ProcessStepDTOResponse;
import com.blameo.erpservice.model.ProcessConfig;
import com.blameo.erpservice.model.ProcessStep;
import com.blameo.erpservice.modeluser.UserDTOResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface ProcessConfigService {
    ProcessConfigDTOResponse save(ProcessConfig processConfig);
    ProcessConfig convertProcessConfig(ProcessConfigDTO processConfigDTO, Map<String, ProcessStep> stringMap);
    List<ProcessConfigDTOResponse> getAll(String processTypeId, HttpServletRequest httpServletRequest);
    void deleteAll(String processId);
    List<UserDTOResponse> getListUserProcessConfigByRequestId(String requestId, Integer processType, HttpServletRequest httpServletRequest, Boolean isCreated);
}
