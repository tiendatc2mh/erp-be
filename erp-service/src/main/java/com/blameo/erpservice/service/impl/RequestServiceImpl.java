package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.controller.CallOtherService;
import com.blameo.erpservice.dto.ResponseMessage;
import com.blameo.erpservice.dto.request.*;
import com.blameo.erpservice.dto.response.*;
import com.blameo.erpservice.exception.RuntimeExceptionCustom;
import com.blameo.erpservice.model.*;
import com.blameo.erpservice.model.Process;
import com.blameo.erpservice.modeluser.*;
import com.blameo.erpservice.repository.*;
import com.blameo.erpservice.service.*;
import com.blameo.erpservice.utils.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class RequestServiceImpl implements RequestService {
    @Autowired
    RequestRepository requestRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    ProcessRepository processRepository;

    @Autowired
    ProcessConfigRepository processConfigRepository;

    @Autowired
    RequestLogService requestLogService;

    @Autowired
    RequestLogRepository requestLogRepository;

    @Autowired
    ProcessStepLogService processStepLogService;

    @Autowired
    RequestPayContentService requestPayContentService;

    @Autowired
    RequestWorktimeContentService requestWorktimeContentService;

    @Autowired
    AttachmentService attachmentService;

    @Autowired
    RequestRecruitmentContentService requestRecruitmentContentService;

    @Autowired
    RequestPayContentRepository requestPayContentRepository;

    @Autowired
    ProcessStepRepository processStepRepository;

    @Autowired
    ProcessStepLogRepository processStepLogRepository;

    @Autowired
    RequestProcessLogRepository requestProcessLogRepository;

    @Autowired
    RequestRecruitmentContentRepository requestRecruitmentContentRepository;

    @Autowired
    RequestRecruitmentUserContentRepository requestRecruitmentUserContentRepository;

    @Autowired
    RequestWorktimeContentRepository requestWorktimeContentRepository;

    @Autowired
    ProcessService processService;

    @Autowired
    MessageSource messageSource;

    Locale locale = CommonUtil.getLocale();

    @Override
    @Transactional
    public RequestDTOResponse save(RequestDTO requestDTO, HttpServletRequest httpRequest) {
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
        //save request
        Request request = new Request();
        CoppyObject.copyNonNullProperties(requestDTO, request);
        Process process = processRepository.findByProcessTypeAndProcessStatusAndIsDefault(requestDTO.getProcessType(), Constant.ACTIVE, Constant.IS_DEFAULT).orElseThrow(
            () -> new RuntimeException("Process " + messageSource.getMessage("notExist", null, locale)
        ));
        if((requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE) ||
                requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE) ||
                requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.WORK_CONFIRMATION))
            && requestDTO.getIsSend() != null && requestDTO.getIsSend().equals(Constant.REQUEST_SEND.SEND)){
            List<RequestWorktimeContent> requestWorktimeContents = new ArrayList<>();
            if(requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE)){
                requestWorktimeContents = CommonUtil.mapList(Arrays.asList(requestDTO.getRequestRemoteContents().toArray()), RequestWorktimeContent.class);
            }else if(requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)){
                requestWorktimeContents = CommonUtil.mapList(Arrays.asList(requestDTO.getRequestDayOffContents().toArray()), RequestWorktimeContent.class);
            }else{
                requestWorktimeContents = CommonUtil.mapList(Arrays.asList(requestDTO.getRequestWorkConfirmContent().toArray()), RequestWorktimeContent.class);
            }
            validateSendRequestWorkTime(requestDTO.getProcessType(), requestWorktimeContents, httpRequest, userDTOResponse);
        }
        switch (requestDTO.getProcessType()){
            case Constant.PROCESS_TYPE.BUY_EQUIPMENT:
                Set<RequestPayContent> requestBuyEquipmentContents = new HashSet<>();
                if(requestDTO.getRequestBuyEquipmentContents() != null && requestDTO.getRequestBuyEquipmentContents().size() > 0){
                    for(RequestBuyEquipmentContentDTO content : requestDTO.getRequestBuyEquipmentContents()){
                        RequestPayContent requestPayContent = new RequestPayContent();
                        CoppyObject.copyNonNullProperties(content, requestPayContent);
                        requestBuyEquipmentContents.add(requestPayContent);
                    }
                    request.setRequestPayContents(requestBuyEquipmentContents);
                }
                break;
            case Constant.PROCESS_TYPE.PAYMENT_REQUEST:
                Set<RequestPayContent> requestPaymentContents = new HashSet<>();
                if(requestDTO.getRequestPaymentContents() != null && requestDTO.getRequestPaymentContents().size() > 0){
                    for(RequestPaymentContentDTO content : requestDTO.getRequestPaymentContents()){
                        RequestPayContent requestPayContent = new RequestPayContent();
                        CoppyObject.copyNonNullProperties(content, requestPayContent);
                        if(content.getRequestAdvance() != null && content.getRequestAdvance().size() > 0){
                            Set<Request> requests = new HashSet<>();
                            for (String s : content.getRequestAdvance()){
                                Optional<Request> requestAdvanceItem = requestRepository.findById(s);
                                if(requestAdvanceItem.isPresent()){
                                    requests.add(requestAdvanceItem.get());
                                }
                            }
                            requestPayContent.setRequestAdvance(requests);
                        }
                        requestPaymentContents.add(requestPayContent);
                    }
                    request.setRequestPayContents(requestPaymentContents);
                }
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE:
                Set<RequestWorktimeContent> requestDayOffContent = new HashSet<>();
                if(requestDTO.getRequestDayOffContents() != null && requestDTO.getRequestDayOffContents().size() > 0){
                    for(RequestDayOffContentDTO content : requestDTO.getRequestDayOffContents()){
                        RequestWorktimeContent requestWorktimeContent = new RequestWorktimeContent();
                        CoppyObject.copyNonNullProperties(content, requestWorktimeContent);
                        requestDayOffContent.add(requestWorktimeContent);
                    }
                    request.setRequestWorktimeContents(requestDayOffContent);
                }
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE:
                Set<RequestWorktimeContent> requestRemoteContents = new HashSet<>();
                if(requestDTO.getRequestRemoteContents() != null && requestDTO.getRequestRemoteContents().size() > 0){
                    for(RequestRemoteContentDTO content : requestDTO.getRequestRemoteContents()){
                        RequestWorktimeContent requestWorktimeContent = new RequestWorktimeContent();
                        CoppyObject.copyNonNullProperties(content, requestWorktimeContent);
                        requestRemoteContents.add(requestWorktimeContent);
                    }
                    request.setRequestWorktimeContents(requestRemoteContents);
                }
                break;
            case Constant.PROCESS_TYPE.WORK_CONFIRMATION:
                Set<RequestWorktimeContent> requestWorkConfirmationContents = new HashSet<>();
                if(requestDTO.getRequestWorkConfirmContent() != null && requestDTO.getRequestWorkConfirmContent().size() > 0){
                    for(RequestWorkConfirmContentDTO content : requestDTO.getRequestWorkConfirmContent()){
                        RequestWorktimeContent requestWorktimeContent = new RequestWorktimeContent();
                        CoppyObject.copyNonNullProperties(content, requestWorktimeContent);
                        requestWorkConfirmationContents.add(requestWorktimeContent);
                    }
                    request.setRequestWorktimeContents(requestWorkConfirmationContents);
                }
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_OT:
                Set<RequestWorktimeContent> requestWorktimeOtContentDTOS = new HashSet<>();
                if(requestDTO.getRequestWorktimeOtContent() != null && requestDTO.getRequestWorktimeOtContent().size() > 0){
                    for(RequestWorktimeOtContentDTO content : requestDTO.getRequestWorktimeOtContent()){
                        RequestWorktimeContent requestWorktimeContent = new RequestWorktimeContent();
                        CoppyObject.copyNonNullProperties(content, requestWorktimeContent);
                        requestWorktimeOtContentDTOS.add(requestWorktimeContent);
                    }
                    request.setRequestWorktimeContents(requestWorktimeOtContentDTOS);
                }
                break;
            case Constant.PROCESS_TYPE.RECRUITMENT_REQUEST:
                Set<RequestRecruitmentContent> requestRecruitmentContents = new HashSet<>();
                if(requestDTO.getRequestRecruitmentContent() != null && requestDTO.getRequestRecruitmentContent().size() > 0){
                    for(RequestRecruitmentContentDTO content : requestDTO.getRequestRecruitmentContent()){
                        RequestRecruitmentContent requestRecruitmentContent = new RequestRecruitmentContent();
                        CoppyObject.copyNonNullProperties(content, requestRecruitmentContent);
                        requestRecruitmentContents.add(requestRecruitmentContent);
                    }
                    request.setRequestRecruitmentContents(requestRecruitmentContents);
                }
                break;
            default:
                break;
        }
        request.setProcess(process);
        if(requestDTO.getIsSend() != null && requestDTO.getIsSend().equals(Constant.REQUEST_SEND.SEND)){
            request.setRequestStatus(Constant.REQUEST_STATUS.WAITING_APPROVAL);
        }else{
            request.setRequestStatus(Constant.REQUEST_STATUS.UNSENT);
        }
        request.setDepartmentId(userDTOResponse.getDepartment().getDepartmentId());
        request.setRequestCode(initRequestCode(requestDTO.getProcessType()));
        request.setCreatedBy(userDTOResponse.getUserId());

        request.getRequestPayContents().forEach(x->{
            x.setRequest(request);
        });
        request.getRequestWorktimeContents().forEach(x->{
            x.setRequest(request);
        });
        request.getRequestRecruitmentContents().forEach(x->{
            x.setRequest(request);
        });
        Request saved = requestRepository.save(request);
        if(requestDTO.getListAttachment() != null && requestDTO.getListAttachment().size() > 0 &&
                (requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST)
                || requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST))){
            List<Attachment> attachments = new ArrayList<>();
            for(AttachmentDTO attachmentDTO : requestDTO.getListAttachment()){
                Attachment attachment = new Attachment();
                CoppyObject.copyNonNullProperties(attachmentDTO, attachment);
                attachment.setObjectId(saved.getRequestId());
                attachment.setCreatedBy(userDTOResponse.getUserId());
                attachment.setAttachmentStatus(Constant.ACTIVE);
                if(requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST)){
                    attachment.setAttachmentType(attachmentDTO.getAttachmentType());
                }else{
                    attachment.setAttachmentType(Constant.ATTACHMENT_TYPE.RECRUITMENT);
                }
                attachments.add(attachment);
            }
            attachmentRepository.saveAll(attachments);
        }
        List<RequestLog> requestLogs = new ArrayList<>();
        if(requestDTO.getIsSend() != null && requestDTO.getIsSend().equals(Constant.REQUEST_SEND.SEND)){
            requestLogs.add(new RequestLog(saved.getRequestId(), userDTOResponse.getUserId(), Constant.REQUEST_STATUS.UNSENT, Constant.REQUEST_ACTION.CREATE, requestDTO.getReceiveUserId()));
            requestLogs.add(new RequestLog(saved.getRequestId(), userDTOResponse.getUserId(), Constant.REQUEST_STATUS.WAITING_APPROVAL, Constant.REQUEST_ACTION.SEND));
            List<ProcessConfig> processConfig = processConfigRepository.getFirstConfigByProcessType(saved.getProcess().getProcessType(), Constant.PROCESS_STEP_CONFIG.START, Constant.PROCESS_CONFIG_TYPE.SEND);
            if(processConfig != null && processConfig.size() > 0){
                processConfig.forEach(x -> {
                    if(x.getProcessStepNext() != null && x.getProcessStepNext().getProcessStepId() != null){
                        ProcessStepLog processStepLog = new ProcessStepLog(saved.getRequestId(), Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL, x.getProcessConfigId(), 0, userDTOResponse.getUserId(), requestDTO.getReceiveUserId());
                        processStepLogRepository.save(processStepLog);
                        //lưu log requestProcess
                        RequestProcessLog requestProcessLog = new RequestProcessLog(saved.getRequestId(), Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL, null, userDTOResponse.getUserId(), requestDTO.getReceiveUserId(), 0);
                        requestProcessLogRepository.save(requestProcessLog);
                        //gửi notification
                        NotificationDTO notificationDTO = createNotification(requestDTO.getProcessType(), Constant.PROCESS_CONFIG_TYPE.SEND, saved, userDTOResponse, requestDTO.getReceiveUserId());
                        ResponseMessage responseMessage = CallOtherService.createNotification(notificationDTO, httpRequest);
                        if(responseMessage.isSuccess()){

                        }else{
                            throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.NOTIFICATION_ERROR, null, CommonUtil.getLocale()));
                        }
                        //send mail
                        UserDTOResponse userReceiveRequest = CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, requestDTO.getReceiveUserId(), httpRequest);
                        if(userReceiveRequest != null && userReceiveRequest.getEmail() != null){
                            String content = getContentOrSubjectEmail(request, userDTOResponse, "send", 1, httpRequest);
                            String subject = getContentOrSubjectEmail(request, userDTOResponse, "send", 2, httpRequest);
                            sendMail(subject, content, new String[]{userReceiveRequest.getEmail()}, null, null);
                        }
                    }
                });
            }else{
                throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.PROCESS, null, CommonUtil.getLocale()) + " " + messageSource.getMessage(Constant.NOT_EXIST, null, CommonUtil.getLocale()));
            }
        }else{
            requestLogs.add(new RequestLog(saved.getRequestId(), userDTOResponse.getUserId(), Constant.REQUEST_STATUS.UNSENT, Constant.REQUEST_ACTION.CREATE, requestDTO.getReceiveUserId()));
        }
        requestLogService.save(requestLogs);
        RequestDTOResponse requestDTOResponse = new RequestDTOResponse();
        BeanUtils.copyProperties(saved, requestDTOResponse);
        return requestDTOResponse;
    }

    private void validateSendRequestWorkTime(Integer processType, List<RequestWorktimeContent> requestWorktimeContents, HttpServletRequest httpRequest, UserDTOResponse userDTOResponse){
        Set<Request> requests = new HashSet<>();
        if(requestWorktimeContents != null && requestWorktimeContents.size() > 0){
            for (RequestWorktimeContent requestWorktimeContent : requestWorktimeContents) {
                List<Integer> requestType = new ArrayList<>();
                Date startTime = new Date();
                Date endTime = new Date();
                if(processType == Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE){
                    startTime = requestWorktimeContent.getDayOffBegin();
                    endTime = requestWorktimeContent.getDayOffEnd();
                    requestType.add(requestWorktimeContent.getDayOffType());
                }else if(processType == Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE){
                    startTime = requestWorktimeContent.getRemoteDate();
                    endTime = requestWorktimeContent.getRemoteDate();
                    requestType.add(requestWorktimeContent.getRemoteType());
                }else{
                    startTime = requestWorktimeContent.getRequestWorkTimeDate();
                    endTime = requestWorktimeContent.getRequestWorkTimeDate();
                    requestType.add(Constant.REMOTE_TYPE.ALL);
                }
                if(requestType.contains(Constant.REMOTE_TYPE.ALL)){
                    requestType.add(Constant.REMOTE_TYPE.AM);
                    requestType.add(Constant.REMOTE_TYPE.PM);
                }else if(requestType.contains(Constant.REMOTE_TYPE.AM)){
                    requestType.add(Constant.REMOTE_TYPE.ALL);
                }else{
                    requestType.add(Constant.REMOTE_TYPE.ALL);
                }
                requests.addAll(requestRepository.getAllRequestDuplicate(userDTOResponse.getUserId(),
                        startTime, endTime, requestType));
            }
        }
        if(requests.size() > 0){
            String requestCode = String.join(", ", requests.stream().map(Request::getRequestCode).collect(Collectors.toList()));
            requestCode = "\"" + requestCode + "\"";
            throw new RuntimeExceptionCustom(MessageFormat.format(messageSource.getMessage("exitsRequestApproved", null, locale), requestCode));
        }
    }

    private String getContentOrSubjectEmail(Request request, UserDTOResponse userDTOResponse, String action, int type, HttpServletRequest requestHttp){
        String namePropose = "";
        switch (request.getProcess().getProcessType()){
            case Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE:
                namePropose = "registerForLeave";
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_OT:
                namePropose = "registerForOt";
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE:
                namePropose = "registerForRemote";
                break;
            case Constant.PROCESS_TYPE.BUY_EQUIPMENT:
                namePropose = "buyEquipment";
                break;
            case Constant.PROCESS_TYPE.PAYMENT_REQUEST:
                namePropose = "paymentRequest";
                break;
            case Constant.PROCESS_TYPE.WORK_CONFIRMATION:
                namePropose = "workConfirmation";
                break;
            case Constant.PROCESS_TYPE.RECRUITMENT_REQUEST:
                namePropose = "recruitmentRequest";
                break;
            default:
                break;
        }
        String content = "";
        if(type == 1) {
            String tab = "&emsp;&emsp;";
            String systemNameRequest = messageSource.getMessage(namePropose, null, locale);
            systemNameRequest = systemNameRequest.substring(0, 1).toUpperCase() + systemNameRequest.substring(1);
            String line2 = messageSource.getMessage("contentEmailLine1", null, locale);
            if(action.equals("send")){
                line2 += " " + messageSource.getMessage("send", null, locale);
            }else if(action.equals("approval")){
                line2 += " " + messageSource.getMessage("approval", null, locale);
            }else if(action.equals("refuse")){
                line2 += " " + messageSource.getMessage("refuse", null, locale);
            }else if(action.equals("perform")){
                line2 += " " + messageSource.getMessage("perform", null, locale);
            }
            line2 += " " + messageSource.getMessage(namePropose, null, locale) + ".";
            content = "<h1>" + messageSource.getMessage("contentEmailRequestNameSystem", null, locale) + " - " + systemNameRequest +"</h1>\n" +
                    "    <p>" + line2 + "</p>" +
                    "    <p>" + messageSource.getMessage("contentEmailLine3", null, locale) + "</p>" +
                    "    <p>" + "- " + messageSource.getMessage("contentEmailLine4", null, locale) + " " + userDTOResponse.getFullName() + " - " + userDTOResponse.getDepartment().getDepartmentName() + "</p>";
            if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.WORK_CONFIRMATION)){
//                TimekeepingDTOResponse timekeepingDTOResponse = CallOtherService.getTimekeepingByUserIdAndDate(request.getCreatedBy(), request.getRequestWorktimeContents().stream().findFirst().get().getRequestWorkTimeDate(), requestHttp);
                String worktimeStart = "";
                String worktimeEnd = "";
                String worktimeConfirmStart = "";
                String worktimeConfirmEnd = "";
                RequestWorktimeContent requestWorktimeContent = request.getRequestWorktimeContents().stream().findFirst().orElse(new RequestWorktimeContent());
                if(requestWorktimeContent.getRequestWorkTimeCheckinOld() != null){
                    worktimeStart = CommonUtil.dateToStringFull(requestWorktimeContent.getRequestWorkTimeCheckinOld());
                }
                if(requestWorktimeContent.getRequestWorkTimeCheckoutOld() != null){
                    worktimeEnd = CommonUtil.dateToStringFull(requestWorktimeContent.getRequestWorkTimeCheckoutOld());
                }
                if(requestWorktimeContent.getRequestWorkTimeCheckin() != null){
                    worktimeConfirmStart = CommonUtil.dateToStringFull(requestWorktimeContent.getRequestWorkTimeCheckin());
                }
                if(requestWorktimeContent.getRequestWorkTimeCheckout() != null){
                    worktimeConfirmEnd = CommonUtil.dateToStringFull(requestWorktimeContent.getRequestWorkTimeCheckout());
                }
                content += "<p>" + "- " + messageSource.getMessage("contentEmailWorkComfirm1", null, locale) + " " + worktimeStart + " " + messageSource.getMessage("timeIn", null, locale) + " - " + worktimeEnd + " " + messageSource.getMessage("timeOut", null, locale) + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("contentEmailWorkComfirm2", null, locale) + " " + worktimeConfirmStart + " " + messageSource.getMessage("timeIn", null, locale) + " - " + worktimeConfirmEnd + " " + messageSource.getMessage("timeOut", null, locale) + "</p>";
            }else if (request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)){
                String typeDayOff;
                RequestWorktimeContent requestWorktimeContent = request.getRequestWorktimeContents().stream().findFirst().orElse(new RequestWorktimeContent());
                if(requestWorktimeContent.getRequestDayOffType().equals(Constant.DAY_OFF_TYPE.ON_LEAVE)){
                    typeDayOff = messageSource.getMessage("onLeave", null, locale);
                }else if(requestWorktimeContent.getRequestDayOffType().equals(Constant.DAY_OFF_TYPE.UNAUTHORIZED_LEAVE)){
                    typeDayOff = messageSource.getMessage("unauthorizedLeave", null, locale);
                }else if(requestWorktimeContent.getRequestDayOffType().equals(Constant.DAY_OFF_TYPE.MATERNITY_LEAVE)){
                    typeDayOff = messageSource.getMessage("maternityLeave", null, locale);
                }else if(requestWorktimeContent.getRequestDayOffType().equals(Constant.DAY_OFF_TYPE.FUNERAL_LEAVE)){
                    typeDayOff = messageSource.getMessage("funeralLeave", null, locale);
                }else{
                    typeDayOff = messageSource.getMessage("marriageLeave", null, locale);
                }
                content += "<p>" + "- " + messageSource.getMessage("dayOffType", null, locale) + " " + typeDayOff + "</p>" +
                        "<p>" +"- " + messageSource.getMessage("timeOff", null, locale) + "</p>";
                for(RequestWorktimeContent requestWorktimeContentItem : request.getRequestWorktimeContents()){
                    String dayOffStart = CommonUtil.dateToString(requestWorktimeContentItem.getDayOffBegin());
                    String dayOffEnd = CommonUtil.dateToString(requestWorktimeContentItem.getDayOffEnd());
                    content += "<p>" + tab + "+ " + typeDayOff + " " + messageSource.getMessage("fromDate", null, locale) + " " + dayOffStart + " " + messageSource.getMessage("toDate", null, locale) + " " + dayOffEnd + "</p>";
                }
                //chưa tính tổng số ngày nghỉ
                Double countDayOff = countDayOff(request, requestHttp);
                content += "<p>" + "- " + messageSource.getMessage("countDayOff", null, locale) + " " + countDayOff + "</p>";
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_OT)){
                String requestOtType;
                RequestWorktimeContent requestWorktimeContent = request.getRequestWorktimeContents().stream().findFirst().orElse(new RequestWorktimeContent());
                if(requestWorktimeContent.getRequestOtType().equals(Constant.OT_TYPE.IN_WEEK)){
                    requestOtType = messageSource.getMessage("otInWeek", null, locale);
                }else if(requestWorktimeContent.getRequestOtType().equals(Constant.OT_TYPE.WEEKEND)){
                    requestOtType = messageSource.getMessage("otWeekend", null, locale);
                }else{
                    requestOtType = messageSource.getMessage("otHoliday", null, locale);
                }
                String otStart = CommonUtil.dateToStringFull(requestWorktimeContent.getRequestOtBegin());
                String otEnd = CommonUtil.dateToStringFull(requestWorktimeContent.getRequestOtEnd());
                content += "<p>" + "- " + messageSource.getMessage("typeOT", null, locale) + " " + requestOtType + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("project", null, locale) + " " + requestWorktimeContent.getRequestOtProject() + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("otTime", null, locale) + " " + otStart + " - " + otEnd + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("countTime", null, locale) + " " + requestWorktimeContent.getCountHour() + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("workContent", null, locale) + " " + request.getContent() + "</p>";
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE)){
                RequestWorktimeContent requestWorktimeContent = request.getRequestWorktimeContents().stream().findFirst().orElse(new RequestWorktimeContent());
                String remoteType;
                if(requestWorktimeContent.getRemoteType() == Constant.REMOTE_TYPE.ALL){
                    remoteType = messageSource.getMessage("allDay", null, locale);
                }else if(requestWorktimeContent.getRemoteType() == Constant.REMOTE_TYPE.AM){
                    remoteType = messageSource.getMessage("am", null, locale);
                }else{
                    remoteType = messageSource.getMessage("pm", null, locale);
                }
                content += "<p>" + "- " + messageSource.getMessage("workingTime", null, locale) + " " + remoteType + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("countTime", null, locale) + " " + requestWorktimeContent.getCountHour() + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("workContent", null, locale) + " " + request.getContent() + "</p>";
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.BUY_EQUIPMENT)){
                Set<String> stringList = request.getRequestPayContents().stream().map(RequestPayContent::getAssetTypeId).collect(Collectors.toSet());
                List<AssetTypeDTOResponse> listAssetType = CallOtherService.getListFromIdService(AssetTypeDTOResponse.class, Constant.PATH.GET_ASSET_TYPE_MULTIPLE_ID, stringList, requestHttp);
                content += "<p>" + "- " + messageSource.getMessage("requestContent", null, locale) + " " + request.getContent() + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("assetRequest", null, locale) + "</p>";
                for(RequestPayContent requestPayContent : request.getRequestPayContents()){
                    AssetTypeDTOResponse assetTypeDTOResponse = listAssetType.stream().filter(x -> x.getAssetTypeId().equals(requestPayContent.getAssetTypeId())).findFirst().orElse(new AssetTypeDTOResponse());
                    content += "<p>" + tab + "+ " + assetTypeDTOResponse.getAssetTypeName() + " - " + requestPayContent.getQuantity() + "</p>";
                }
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST)){
                RequestPayContent requestPayContent = request.getRequestPayContents().stream().findFirst().orElse(new RequestPayContent());
                String payDate = CommonUtil.dateToString(request.getPayDate());
                String paymentType;
                if(request.getPayType().equals(Constant.PAY_TYPE.ADVANCE)){
                    paymentType = messageSource.getMessage("advance", null, locale);
                }else if(request.getPayType().equals(Constant.PAY_TYPE.PAYMENT)){
                    paymentType = messageSource.getMessage("pay", null, locale);
                }else{
                    paymentType = messageSource.getMessage("reimbursement", null, locale);
                }
                content += "<p>" + "- " + messageSource.getMessage("paymentType", null, locale) + " " + paymentType + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("paymentTerm", null, locale) + " " + payDate + "</p>";
                AtomicReference<Double> countPayment = new AtomicReference<>((double) 0);
                request.getRequestPayContents().forEach(x -> {
                    countPayment.updateAndGet(z -> z + x.getRemainingMoney());
                });
                if(requestPayContent.getReceivedMoney() != null){
                    countPayment.updateAndGet(z -> z - requestPayContent.getReceivedMoney());
                }
                content += "<p>" + "- " + messageSource.getMessage("totalPayment", null, locale) + " " + String.format("%,.2f", countPayment.get()) + " VND" + "</p>";
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST)){
                RequestRecruitmentContent requestRecruitmentContent = request.getRequestRecruitmentContents().stream().findFirst().orElse(new RequestRecruitmentContent());
                String onboardTimeStart = CommonUtil.dateToString(requestRecruitmentContent.getOnboardTimeStart());
                String onboardTimeEnd = CommonUtil.dateToString(requestRecruitmentContent.getOnboardTimeEnd());
                Set<String> listPositionId = new HashSet<>();
                listPositionId.add(requestRecruitmentContent.getPositionId());
                Set<String> listLevelId = new HashSet<>();
                listLevelId.add(requestRecruitmentContent.getLevelId());
                List<PositionDTOResponse> listPosition = CallOtherService.getListFromIdService(PositionDTOResponse.class, Constant.PATH.GET_POSITION_MULTIPLE_ID, listPositionId, requestHttp);
                List<LevelDTOResponse> listLevel = CallOtherService.getListFromIdService(LevelDTOResponse.class, Constant.PATH.GET_LEVEL_MULTIPLE_ID, listLevelId, requestHttp);
                content += "<p>" + "- " + messageSource.getMessage("vacancies", null, locale) + " " + listPosition.get(0).getPositionName() + " - " + listLevel.get(0).getLevelName() + " - " + messageSource.getMessage("quantity", null, locale) + " " + requestRecruitmentContent.getQuantity() + "</p>" +
                        "<p>" + "- " + messageSource.getMessage("estimatedTimeOnboard", null, locale) + " " + onboardTimeStart + " - " + onboardTimeEnd + "</p>";
            }
            String dateRequest = CommonUtil.dateToStringFull(new Date());
            content += "<p>" + "- " + messageSource.getMessage("contentEmailSubmit", null, locale) + " " + dateRequest + "</p>" +
                    "<p>" + MessageFormat.format(messageSource.getMessage("footerContent", null, locale), Constant.URL_SYSTEM) + "</p>";
        }else{
            String name = messageSource.getMessage(namePropose, null, locale);
            content = messageSource.getMessage("contentEmailRequestNameSystem", null, locale) + " - " + name.substring(0, 1).toUpperCase() + name.substring(1) + " - " + userDTOResponse.getFullName();
        }
        return content;
    }

    private Double countDayOff(Request request, HttpServletRequest httpRequest){
        AtomicReference<Double> countDayOff = new AtomicReference<>((double) 0);
        if(request.getRequestWorktimeContents() != null && request.getRequestWorktimeContents().size() > 0){
            for (RequestWorktimeContent requestWorktimeContentDTO : request.getRequestWorktimeContents()){
                double multiplier = requestWorktimeContentDTO.getDayOffType() == 0? 1.0 : 0.5;
                List<Date> holidayDTOResponses = CallOtherService.getListHolidayFromWorktime(requestWorktimeContentDTO.getDayOffBegin(),requestWorktimeContentDTO.getDayOffEnd(), httpRequest).stream().map(HolidayDTOResponse::getHolidayDate).collect(Collectors.toList());
                Double countDay = multiplier * (DataUtil.calculateDaysBetween(requestWorktimeContentDTO.getDayOffBegin(), requestWorktimeContentDTO.getDayOffEnd()) + 1
                        //trừ ngày cuối tuần
                        - DataUtil.countWeekendDays(requestWorktimeContentDTO.getDayOffBegin(),requestWorktimeContentDTO.getDayOffEnd())
                        //trừ ngày nghỉ lễ
                        - DataUtil.countDatesInRange(holidayDTOResponses,requestWorktimeContentDTO.getDayOffBegin(),requestWorktimeContentDTO.getDayOffEnd()));
                countDayOff.updateAndGet(v -> v + countDay);
            }
        }
        return countDayOff.get();
    }

    private NotificationDTO createNotification(Integer processType, Integer actionType, Request request, UserDTOResponse userRequest, String userReceiverId){
        String message = userRequest.getFullName() + " ";
        switch (actionType){
            case Constant.PROCESS_CONFIG_TYPE.SEND:
                message += Constant.NOTIFICATION_CODE.SEND;
                break;
            case Constant.PROCESS_CONFIG_TYPE.APPROVAL:
                message += Constant.NOTIFICATION_CODE.APPROVAL;
                break;
            case Constant.PROCESS_CONFIG_TYPE.REFUSE:
                message += Constant.NOTIFICATION_CODE.REFUSE;
                break;
            case Constant.PROCESS_CONFIG_TYPE.PERFORM:
                message += Constant.NOTIFICATION_CODE.PERFORM;
                break;
            default:
                break;
        }
        message += " ";
        switch (processType){
            case Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE:
                message += Constant.NOTIFICATION_CODE.REGISTER_FOR_LEAVE;
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_OT:
                message += Constant.NOTIFICATION_CODE.REGISTER_FOR_OT;
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE:
                message += Constant.NOTIFICATION_CODE.REGISTER_FOR_REMOTE;
                break;
            case Constant.PROCESS_TYPE.BUY_EQUIPMENT:
                message += Constant.NOTIFICATION_CODE.BUY_EQUIPMENT;
                break;
            case Constant.PROCESS_TYPE.PAYMENT_REQUEST:
                message += Constant.NOTIFICATION_CODE.PAYMENT_REQUEST;
                break;
            case Constant.PROCESS_TYPE.WORK_CONFIRMATION:
                message += Constant.NOTIFICATION_CODE.WORK_CONFIRMATION;
                break;
            case Constant.PROCESS_TYPE.RECRUITMENT_REQUEST:
                message += Constant.NOTIFICATION_CODE.RECRUITMENT_REQUEST;
                break;
            default:
                break;
        }
        message += " " + Constant.NOTIFICATION_CODE.CODE + " " + request.getRequestCode() + ".";
        NotificationDTO notificationDTO = new NotificationDTO(message, request.getRequestId(), request.getRequestStatus(), processType, userRequest.getUserId(), userReceiverId);
        return notificationDTO;
    }

    private String initRequestCode(Integer processType){
        String requestCodeInit = "DX" + processType + "_%";
        String requestCode = "";
        Request request = requestRepository.findByRequestCodeLikeFirst(requestCodeInit);
        if(request != null && request.getRequestCode() != null){
            String[] requestCodeSplit = request.getRequestCode().split("_");
            String numberCode = String.valueOf(Integer.parseInt(requestCodeSplit[1]) + 1);
            if(numberCode.length() == 1){
                requestCode = "DX" + processType + "_" + "000" + numberCode;
            }else if(numberCode.length() == 2){
                requestCode = "DX" + processType + "_" + "00" + numberCode;
            }else if(numberCode.length() == 3){
                requestCode = "DX" + processType + "_" + "0" + numberCode;
            }else{
                requestCode = "DX" + processType + "_" + numberCode;
            }
            return requestCode;
        }else{
            return "DX" + processType + "_" + "0001";
        }
    }

    @Override
    public RequestDTOResponse get(String id, HttpServletRequest requestHttp) {
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(requestHttp);
        RequestDTOResponse requestDTOResponse = new RequestDTOResponse();
        Optional<Request> result =  requestRepository.findById(id);
        if (result.isPresent()) {
            Request request = result.get();
            requestDTOResponse = getRequestDTO(result.get(), requestHttp);
            ProcessStepLog processStepLog = processStepLogRepository.getProcessByReceiveUserIdAndOrderMax(request.getRequestId(), userDTOResponse.getUserId());
            if(processStepLog != null){
                requestDTOResponse.setRequestStatusProcess(processStepLog.getProcessStepStatus());
                Optional<ProcessConfig> processConfig = processConfigRepository.findById(processStepLog.getProcessConfigId());
                if(processConfig.isPresent() && processConfig.get().getProcessStep().getRoleId() != null){
                    requestDTOResponse.setRoleId(processConfig.get().getProcessStep().getRoleId());
                }
            }
            if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST) ||
                    request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST)){
                List<Attachment> attachments = attachmentRepository.getByObjectIdAndAttachmentStatus(request.getRequestId());
                if(attachments != null && attachments.size() > 0){
                    if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST)){
                        List<Attachment> attachmentList = attachments.stream().filter(item -> item.getAttachmentType().equals(Constant.ATTACHMENT_TYPE.RECRUITMENT)).collect(Collectors.toList());
                        List<Attachment> attachmentResultList = attachments.stream().filter(item -> item.getAttachmentType().equals(Constant.ATTACHMENT_TYPE.RECRUITMENT_RESULT)).collect(Collectors.toList());
                        if(attachmentList.size() > 0){
                            requestDTOResponse.setListAttachment(CommonUtil.mapList(attachmentList, AttachmentDTOResponse.class));
                        }
                        if(attachmentResultList.size() > 0){
                            requestDTOResponse.setListAttachmentResult(CommonUtil.mapList(attachmentResultList, AttachmentDTOResponse.class));
                        }
                    }else{
                        requestDTOResponse.setListAttachment(CommonUtil.mapList(attachments, AttachmentDTOResponse.class));
                    }
                }
            }
            Set<String> listUserId = new HashSet<>();
            if(requestDTOResponse.getUpdatedBy() != null){
                listUserId.add(requestDTOResponse.getUpdatedBy());
            }
            if(requestDTOResponse.getCreatedBy() != null){
                listUserId.add(requestDTOResponse.getCreatedBy());
            }
            if(requestDTOResponse.getHandoverUser() != null){
                listUserId.add(requestDTOResponse.getHandoverUser());
            }
            RequestLog requestLog = requestLogRepository.findRequestLogByRequestIdAndRequestLogAction(request.getRequestId(), Constant.REQUEST_LOG_ACTION.CREATE);
            if(requestLog != null){
                listUserId.add(requestLog.getRequestApprove());
            }
            List<Attachment> attachments = attachmentRepository.getByObjectIdAndAttachmentStatus(request.getRequestId());
            if(attachments != null && attachments.size() > 0){
                List<AttachmentDTOResponse> attachmentDTOResponses = new ArrayList<>();
                attachmentDTOResponses = CommonUtil.mapList(attachments, AttachmentDTOResponse.class);
                requestDTOResponse.setListAttachment(attachmentDTOResponses);
            }
            if(requestDTOResponse.getRequestRecruitmentUserContent() != null && requestDTOResponse.getRequestRecruitmentUserContent().size() > 0){
                for (RequestRecruitmentUserContentResponseDTO recruitmentUserContentResponseDTO : requestDTOResponse.getRequestRecruitmentUserContent()){
                    List<Attachment> attachmentUserContent = attachmentRepository.getByObjectIdAndAttachmentStatus(recruitmentUserContentResponseDTO.getRequestRecruitmentUserContentId());
                    if(attachmentUserContent != null && attachmentUserContent.size() > 0){
                        recruitmentUserContentResponseDTO.setListAttachment(CommonUtil.mapList(attachmentUserContent, AttachmentDTOResponse.class));
                    }
                }
            }
            List<UserDTOResponse> listUser = CallOtherService.getListFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_MULTIPLE_ID, listUserId, requestHttp);
            RequestDTOResponse finalRequestDTOResponse = requestDTOResponse;

            UserDTOResponse userCreated = listUser.stream().filter(y -> y.getUserId().equals(finalRequestDTOResponse.getCreatedBy())).findFirst().orElse(new UserDTOResponse());
            UserDTOResponse userUpdated = listUser.stream().filter(y -> y.getUserId().equals(finalRequestDTOResponse.getUpdatedBy())).findFirst().orElse(new UserDTOResponse());
            UserDTOResponse handoverUser = listUser.stream().filter(y -> y.getUserId().equals(finalRequestDTOResponse.getHandoverUser())).findFirst().orElse(new UserDTOResponse());

            requestDTOResponse.setDepartmentName(userCreated.getDepartment().getDepartmentName());
            requestDTOResponse.setCreatedByName(userCreated.getFullName());
            requestDTOResponse.setUpdatedByName(userUpdated.getFullName());
            requestDTOResponse.setHandoverUsername(handoverUser.getFullName());
            if(requestLog != null){
                UserDTOResponse userDTOResponseApprove = listUser.stream().filter(y -> y.getUserId().equals(requestLog.getRequestApprove())).findFirst().orElse(new UserDTOResponse());
                requestDTOResponse.setRequestApprove(userDTOResponseApprove.getUserId());
                requestDTOResponse.setRequestApproveName(userDTOResponseApprove.getFullName());
            }
        }
        return requestDTOResponse;
    }

    @Override
    public List<RequestDTOResponse> getRequestAdvance(HttpServletRequest request) {
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        List<Request> requests = requestRepository.getLstRequestAdvancePaymentByUserIdAndRequestStatus(userDTOResponse.getUserId());
        if(requests != null && requests.size() > 0){
            List<RequestDTOResponse> requestDTOS = CommonUtil.mapList(requests, RequestDTOResponse.class);
            for (RequestDTOResponse requestDTOResponse : requestDTOS){
                requestDTOResponse.setRequestStatusProcess(null);
            }
            int i = 0;
            for (Request item : requests){
                Set<RequestPaymentContentResponseDTO> requestPaymentContentDTOS = new HashSet<>();
                item.getRequestPayContents().forEach(x -> {
                    RequestPaymentContentResponseDTO requestPaymentContentDTO = new RequestPaymentContentResponseDTO();
                    CoppyObject.copyNonNullProperties(x, requestPaymentContentDTO);
                    requestPaymentContentDTOS.add(requestPaymentContentDTO);
                });
                requestDTOS.get(i).setRequestPaymentContents(requestPaymentContentDTOS);
                i++;
            }
            return requestDTOS;
        }
        return new ArrayList<>();
    }

    @Override
    public void delete(String id, UserDTOResponse userDTOResponse) {
        Optional<Request> result = requestRepository.findById(id);
        if (result.isPresent()) {
            Request request = result.get();
            request.setRequestStatus(Constant.REQUEST_STATUS.DELETE);
            request.setUpdatedBy(userDTOResponse.getUserId());
            requestRepository.save(request);
            List<RequestLog> requestLogs = new ArrayList<>();
            processStepLogService.delete(id);
            requestLogs.add(new RequestLog(request.getRequestId(), userDTOResponse.getUserId(), result.get().getRequestStatus(), Constant.REQUEST_ACTION.DELETE));
            requestLogService.save(requestLogs);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.REQUEST, null, CommonUtil.getLocale()) + " " + messageSource.getMessage("unknownError", null, CommonUtil.getLocale()));
    }

    @Override
    @Transactional
    public RequestDTOResponse update(RequestDTO requestDTO, String id,  HttpServletRequest requestHttp) {
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(requestHttp);
        List<String> stringListIdDelete = new ArrayList<>();
        List<RequestPayContent> lstRequestPayContent = requestPayContentRepository.findByRequestId(id);
        List<RequestWorktimeContent> lstRequestWorktimeContent = requestWorktimeContentRepository.findAllByRequest_RequestId(id);
        lstRequestPayContent.forEach(x -> {
            stringListIdDelete.add(x.getRequestPayContentId());
        });
        lstRequestWorktimeContent.forEach(x -> {
            stringListIdDelete.add(x.getRequestWorktimeContentId());
        });
        Optional<Request> result = requestRepository.findById(id);
        Request request;
        RequestDTOResponse requestDTOResponse = new RequestDTOResponse();
        if (result.isPresent()) {
            request = result.get();
            if(requestDTO.getRequestCode() != null){
                request.setRequestCode(requestDTO.getRequestCode());
            }
            if(requestDTO.getContent() != null){
                request.setContent(requestDTO.getContent());
            }
            if(requestDTO.getReason() != null){
                request.setReason(requestDTO.getReason());
            }
            if(requestDTO.getPayType() != null){
                request.setPayType(requestDTO.getPayType());
            }
            if(requestDTO.getPayDate() != null){
                request.setPayDate(requestDTO.getPayDate());
            }
            if(requestDTO.getDescription() != null){
                request.setDescription(requestDTO.getDescription());
            }
            if(requestDTO.getHandoverUser() != null){
                request.setHandoverUser(requestDTO.getHandoverUser());
            }
            if(requestDTO.getAccountOwner() != null){
                request.setAccountOwner(requestDTO.getAccountOwner());
            }
            if(requestDTO.getAccountNumber() != null){
                request.setAccountNumber(requestDTO.getAccountNumber());
            }
            if(requestDTO.getBank() != null){
                request.setBank(requestDTO.getBank());
            }
            if(requestDTO.getDayOffMarryType() != null){
                request.setDayOffMarryType(requestDTO.getDayOffMarryType());
            }
            request.setUpdatedBy(userDTOResponse.getUserId());
            if(requestDTO.getReceiveUserId() != null){
                RequestLog requestLog = requestLogRepository.findRequestLogByRequestIdAndRequestLogAction(request.getRequestId(), Constant.REQUEST_LOG_ACTION.CREATE);
                requestLog.setRequestApprove(requestDTO.getReceiveUserId());
                requestLogRepository.save(requestLog);
            }
            switch (requestDTO.getProcessType()){
                case Constant.PROCESS_TYPE.BUY_EQUIPMENT:
                    Set<RequestPayContent> requestBuyEquipmentContents = new HashSet<>();
                    if(requestDTO.getRequestBuyEquipmentContents() != null && requestDTO.getRequestBuyEquipmentContents().size() > 0){
                        for(RequestBuyEquipmentContentDTO content : requestDTO.getRequestBuyEquipmentContents()){
                            if(content.getRequestPayContentId() != null && stringListIdDelete.contains(content.getRequestPayContentId())){
                                stringListIdDelete.remove(content.getRequestPayContentId());
                            }
                            RequestPayContent requestPayContent = new RequestPayContent();
                            CoppyObject.copyNonNullProperties(content, requestPayContent);
                            requestPayContent.setRequestPayContentStatus(Constant.REQUEST_CONTENT_STATUS.ACTIVE);
                            requestBuyEquipmentContents.add(requestPayContent);
                        }
                        request.setRequestPayContents(requestBuyEquipmentContents);
                    }
                    if(stringListIdDelete.size() > 0) {
                        requestPayContentService.deleteList(stringListIdDelete);
                    }
                    break;
                case Constant.PROCESS_TYPE.PAYMENT_REQUEST:
                    Set<RequestPayContent> requestPaymentContents = new HashSet<>();
                    if(requestDTO.getRequestPaymentContents() != null && requestDTO.getRequestPaymentContents().size() > 0){
                        for(RequestPaymentContentDTO content : requestDTO.getRequestPaymentContents()){
                            if(content.getRequestPayContentId() != null && stringListIdDelete.contains(content.getRequestPayContentId())){
                                stringListIdDelete.remove(content.getRequestPayContentId());
                            }
                            RequestPayContent requestPayContent = new RequestPayContent();
                            CoppyObject.copyNonNullProperties(content, requestPayContent);
                            requestPayContent.setRequestPayContentStatus(Constant.REQUEST_CONTENT_STATUS.ACTIVE);
                            if(content.getRequestAdvance() != null && content.getRequestAdvance().size() > 0){
                                Set<Request> requests = new HashSet<>();
                                for (String s : content.getRequestAdvance()){
                                    Optional<Request> requestAdvanceItem = requestRepository.findById(s);
                                    if(requestAdvanceItem.isPresent()){
                                        requests.add(requestAdvanceItem.get());
                                    }
                                }
                                requestPayContent.setRequestAdvance(requests);
                            }
                            requestPaymentContents.add(requestPayContent);
                        }
                        request.setRequestPayContents(requestPaymentContents);
                    }
                    if(stringListIdDelete.size() > 0) {
                        requestPayContentService.deleteList(stringListIdDelete);
                    }
                    break;
                case Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE:
                    Set<RequestWorktimeContent> requestDayOffContents = new HashSet<>();
                    if(requestDTO.getRequestDayOffContents() != null && requestDTO.getRequestDayOffContents().size() > 0){
                        for(RequestDayOffContentDTO content : requestDTO.getRequestDayOffContents()){
                            if(content.getRequestWorktimeContentId() != null && stringListIdDelete.contains(content.getRequestWorktimeContentId())){
                                stringListIdDelete.remove(content.getRequestWorktimeContentId());
                            }
                            RequestWorktimeContent requestWorktimeContent = new RequestWorktimeContent();
                            CoppyObject.copyNonNullProperties(content, requestWorktimeContent);
                            requestWorktimeContent.setRequestWorktimeContentStatus(Constant.REQUEST_CONTENT_STATUS.ACTIVE);
                            requestDayOffContents.add(requestWorktimeContent);
                        }
                        request.setRequestWorktimeContents(requestDayOffContents);
                    }
                    if(stringListIdDelete.size() > 0) {
                        requestWorktimeContentService.deleteList(stringListIdDelete);
                    }
                    break;
                case Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE:
                    Set<RequestWorktimeContent> requestRemoteContents = new HashSet<>();
                    if(requestDTO.getRequestRemoteContents() != null && requestDTO.getRequestRemoteContents().size() > 0){
                        for(RequestRemoteContentDTO content : requestDTO.getRequestRemoteContents()){
                            if(content.getRequestWorktimeContentId() != null && stringListIdDelete.contains(content.getRequestWorktimeContentId())){
                                stringListIdDelete.remove(content.getRequestWorktimeContentId());
                            }
                            RequestWorktimeContent requestWorktimeContent = new RequestWorktimeContent();
                            CoppyObject.copyNonNullProperties(content, requestWorktimeContent);
                            requestWorktimeContent.setRequestWorktimeContentStatus(Constant.REQUEST_CONTENT_STATUS.ACTIVE);
                            requestRemoteContents.add(requestWorktimeContent);
                        }
                        request.setRequestWorktimeContents(requestRemoteContents);
                    }
                    if(stringListIdDelete.size() > 0) {
                        requestWorktimeContentService.deleteList(stringListIdDelete);
                    }
                    break;
                case Constant.PROCESS_TYPE.WORK_CONFIRMATION:
                    Set<RequestWorktimeContent> requestTimeConfirmationContents = new HashSet<>();
                    if(requestDTO.getRequestWorkConfirmContent() != null && requestDTO.getRequestWorkConfirmContent().size() > 0){
                        for(RequestWorkConfirmContentDTO content : requestDTO.getRequestWorkConfirmContent()){
                            if(content.getRequestWorktimeContentId() != null && stringListIdDelete.contains(content.getRequestWorktimeContentId())){
                                stringListIdDelete.remove(content.getRequestWorktimeContentId());
                            }
                            RequestWorktimeContent requestWorktimeContent = new RequestWorktimeContent();
                            CoppyObject.copyNonNullProperties(content, requestWorktimeContent);
                            requestWorktimeContent.setRequestWorktimeContentStatus(Constant.REQUEST_CONTENT_STATUS.ACTIVE);
                            requestTimeConfirmationContents.add(requestWorktimeContent);
                        }
                        request.setRequestWorktimeContents(requestTimeConfirmationContents);
                    }
                    if(stringListIdDelete.size() > 0) {
                        requestWorktimeContentService.deleteList(stringListIdDelete);
                    }
                    break;
                case Constant.PROCESS_TYPE.REGISTER_FOR_OT:
                    Set<RequestWorktimeContent> requestWorktimeOtContentDTOS = new HashSet<>();
                    if(requestDTO.getRequestWorktimeOtContent() != null && requestDTO.getRequestWorktimeOtContent().size() > 0){
                        for(RequestWorktimeOtContentDTO content : requestDTO.getRequestWorktimeOtContent()){
                            if(content.getRequestWorktimeContentId() != null && stringListIdDelete.contains(content.getRequestWorktimeContentId())){
                                stringListIdDelete.remove(content.getRequestWorktimeContentId());
                            }
                            RequestWorktimeContent requestWorktimeContent = new RequestWorktimeContent();
                            CoppyObject.copyNonNullProperties(content, requestWorktimeContent);
                            requestWorktimeContent.setRequestWorktimeContentStatus(Constant.REQUEST_CONTENT_STATUS.ACTIVE);
                            requestWorktimeOtContentDTOS.add(requestWorktimeContent);
                        }
                        request.setRequestWorktimeContents(requestWorktimeOtContentDTOS);
                    }
                    if(stringListIdDelete.size() > 0) {
                        requestWorktimeContentService.deleteList(stringListIdDelete);
                    }
                    break;
                case Constant.PROCESS_TYPE.RECRUITMENT_REQUEST:
                    Set<RequestRecruitmentContent> requestRecruitmentContents = new HashSet<>();
                    if(requestDTO.getRequestRecruitmentContent() != null && requestDTO.getRequestRecruitmentContent().size() > 0){
                        for(RequestRecruitmentContentDTO content : requestDTO.getRequestRecruitmentContent()){
                            if(content.getRequestRecruitmentContentId() != null && stringListIdDelete.contains(content.getRequestRecruitmentContentId())){
                                stringListIdDelete.remove(content.getRequestRecruitmentContentId());
                            }
                            RequestRecruitmentContent requestRecruitmentContent = new RequestRecruitmentContent();
                            CoppyObject.copyNonNullProperties(content, requestRecruitmentContent);
                            requestRecruitmentContent.setRequestRecruitmentContentStatus(Constant.REQUEST_CONTENT_STATUS.ACTIVE);
                            requestRecruitmentContents.add(requestRecruitmentContent);
                        }
                        request.setRequestRecruitmentContents(requestRecruitmentContents);
                    }
                    if(stringListIdDelete.size() > 0){
                        requestRecruitmentContentService.deleteList(stringListIdDelete);
                    }
                    break;
                default:
                    break;
            }
            request.getRequestPayContents().forEach(x->{
                x.setRequest(request);
            });
            request.getRequestWorktimeContents().forEach(x->{
                x.setRequest(request);
            });
            request.getRequestRecruitmentContents().forEach(x->{
                x.setRequest(request);
            });
            Request saved = requestRepository.save(request);
            if(requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST)
                            || requestDTO.getProcessType().equals(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST)) {
                attachmentService.saveAndDeleteAttachment(requestDTO.getListAttachment(), request.getRequestId(), requestHttp, saved.getRequestId(), saved.getProcess().getProcessType());
            }
            requestDTOResponse = getRequestDTO(saved, requestHttp);
            List<RequestLog> requestLogs = new ArrayList<>();
            requestLogs.add(new RequestLog(request.getRequestId(), userDTOResponse.getUserId(), result.get().getRequestStatus(), Constant.REQUEST_ACTION.EDIT));
            requestLogService.save(requestLogs);
            if(requestDTO.getIsSend().equals(Constant.REQUEST_SEND.SEND) && (saved.getRequestStatus().equals(Constant.REQUEST_STATUS.UNSENT) || saved.getRequestStatus().equals(Constant.REQUEST_STATUS.REFUSE))){
                this.send(id, requestHttp);
            }
        }
        return requestDTOResponse;
    }

    private RequestDTOResponse getRequestDTO(Request request, HttpServletRequest requestHttp){
        RequestDTOResponse requestDTOResponse = new RequestDTOResponse();
        BeanUtils.copyProperties(request, requestDTOResponse);
        Set<String> listPositionId = new HashSet<>();
        Set<String> listLevelId = new HashSet<>();
        Set<String> listAssetTypeId = new HashSet<>();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(requestHttp);
        ProcessStepLog processStepLog = processStepLogRepository.getProcessStepStatusByRequestIdAndReceiveUserId(userDTOResponse.getUserId(), request.getRequestId());
        if(processStepLog != null){
            requestDTOResponse.setRequestStatusProcess(processStepLog.getProcessStepStatus());
            //chỉ có trạng thái là chờ phê duyệt và từ chối mới trả action để FE check
            if(processStepLog.getProcessStepStatus().equals(Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL) ||
                    processStepLog.getProcessStepStatus().equals(Constant.PROCESS_STEP_LOG_STATUS.DENIED)){
                List<ProcessConfig> processConfigs = processConfigRepository.getProcessConfigByProcessLogMaxAndRequestIdAndReceiveUserId(request.getRequestId(), userDTOResponse.getUserId());
                if(processConfigs.size() > 0){
                    List<Integer> listAction = new ArrayList<>();
                    for (ProcessConfig processConfig : processConfigs){
                        //nếu luồng phê duyệt tiếp theo tới ông cuối cùng trong luồng thì gửi về action COMPLETE để FE check không chọn  người phê duyệt
                        if(processConfig.getProcessStepType().equals(Constant.PROCESS_CONFIG_TYPE.APPROVAL) && processConfig.getProcessStepNext().getProcessStepCode().equals(Constant.PROCESS_STEP_CONFIG.END)){
                            listAction.add(Constant.PROCESS_CONFIG_TYPE.APPROVAL_END);
                            // nếu luồng phê duyệt là mua trang thiết bị, thanh toán hoặc tuyển dụng luồng tiếp theo là thực hiện thì trả về 4
                        }else if(processConfig.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.BUY_EQUIPMENT) ||
                                processConfig.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST) ||
                                processConfig.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST)){
                            List<Integer> lstStatus = new ArrayList<>();
                            lstStatus.add(Constant.PROCESS_CONFIG_TYPE.PERFORM);
                            List<ProcessConfig> list = processConfigRepository.getProcessConfigNext(processConfig.getProcessConfigId(), lstStatus);
                            if(list != null && list.size() > 0){
                                listAction.add(Constant.PROCESS_CONFIG_TYPE.APPROVAL_END);
                            }else{
                                listAction.add(processConfig.getProcessStepType());
                            }
                        }else{
                            listAction.add(processConfig.getProcessStepType());
                        }
                    }
                    String result = listAction.stream()
                            .map(Object::toString)
                            .collect(Collectors.joining(","));
                    requestDTOResponse.setAction(result);
                }
            }
        }
        switch (request.getProcess().getProcessType()) {
            case Constant.PROCESS_TYPE.BUY_EQUIPMENT:
                Set<RequestBuyEquipmentContentResponseDTO> requestBuyEquipmentContentDTOS = new HashSet<>();
                if (request.getRequestPayContents() != null && request.getRequestPayContents().size() > 0) {
                    request.getRequestPayContents().forEach(x -> {
                        RequestBuyEquipmentContentResponseDTO requestBuyEquipmentContentDTO = new RequestBuyEquipmentContentResponseDTO();
                        BeanUtils.copyProperties(x, requestBuyEquipmentContentDTO);
                        requestBuyEquipmentContentDTOS.add(requestBuyEquipmentContentDTO);
                        if(x.getAssetTypeId() != null){
                            listAssetTypeId.add(x.getAssetTypeId());
                        }
                    });
                    requestDTOResponse.setRequestBuyEquipmentContents(requestBuyEquipmentContentDTOS);
                }
                break;
            case Constant.PROCESS_TYPE.PAYMENT_REQUEST:
                Set<RequestPaymentContentResponseDTO> requestPaymentContentDTOS = new HashSet<>();
                if (request.getRequestPayContents() != null && request.getRequestPayContents().size() > 0) {
                    request.getRequestPayContents().forEach(x -> {
                        RequestPaymentContentResponseDTO requestPaymentContentDTO = new RequestPaymentContentResponseDTO();
                        BeanUtils.copyProperties(x, requestPaymentContentDTO);
                        requestPaymentContentDTOS.add(requestPaymentContentDTO);
                    });
                    requestDTOResponse.setRequestPaymentContents(requestPaymentContentDTOS);
                }
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE:
                Set<RequestDayOffContentResponseDTO> requestDayOffContentDTOS = new HashSet<>();
                if (request.getRequestWorktimeContents() != null && request.getRequestWorktimeContents().size() > 0) {
                    Set<String> listUserId = request.getRequestWorktimeContents().stream().map(requestWorktimeContent -> requestWorktimeContent.getRequestDayOffUserTransfer()).collect(Collectors.toSet());
                    List<UserDTOResponse> listUser = CallOtherService.getListFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_MULTIPLE_ID, listUserId, requestHttp);
                    request.getRequestWorktimeContents().forEach(x -> {
                        RequestDayOffContentResponseDTO requestBuyEquipmentContentDTO = new RequestDayOffContentResponseDTO();
                        BeanUtils.copyProperties(x, requestBuyEquipmentContentDTO);
                        if(requestBuyEquipmentContentDTO.getRequestDayOffUserTransfer() != null){
                            requestBuyEquipmentContentDTO.setRequestDayOffUserTransferFullname(listUser.stream()
                                    .filter(userDTOResponse1 -> userDTOResponse1.getUserId().equals(requestBuyEquipmentContentDTO.getRequestDayOffUserTransfer()))
                                    .findAny()
                                    .orElse(new UserDTOResponse())
                                    .getFullName());
                        }
                        requestDayOffContentDTOS.add(requestBuyEquipmentContentDTO);
                    });
                    requestDTOResponse.setRequestDayOffContents(requestDayOffContentDTOS);
                }
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE:
                Set<RequestRemoteContentResponseDTO> requestRemoteContentDTOS = new HashSet<>();
                if (request.getRequestWorktimeContents() != null && request.getRequestWorktimeContents().size() > 0) {
                    request.getRequestWorktimeContents().forEach(x -> {
                        RequestRemoteContentResponseDTO requestBuyEquipmentContentDTO = new RequestRemoteContentResponseDTO();
                        BeanUtils.copyProperties(x, requestBuyEquipmentContentDTO);
                        requestRemoteContentDTOS.add(requestBuyEquipmentContentDTO);
                    });
                    requestDTOResponse.setRequestRemoteContents(requestRemoteContentDTOS);
                }
                break;
            case Constant.PROCESS_TYPE.WORK_CONFIRMATION:
                Set<RequestWorkConfirmContentResponseDTO> requestWorktimeContentDTOS = new HashSet<>();
                if (request.getRequestWorktimeContents() != null && request.getRequestWorktimeContents().size() > 0) {
                    request.getRequestWorktimeContents().forEach(x -> {
                        RequestWorkConfirmContentResponseDTO requestBuyEquipmentContentDTO = new RequestWorkConfirmContentResponseDTO();
                        BeanUtils.copyProperties(x, requestBuyEquipmentContentDTO);
                        requestWorktimeContentDTOS.add(requestBuyEquipmentContentDTO);
                    });
                    requestDTOResponse.setRequestWorkConfirmContent(requestWorktimeContentDTOS);
                }
                break;
            case Constant.PROCESS_TYPE.REGISTER_FOR_OT:
                Set<RequestWorktimeOtContentResponseDTO> requestWorktimeOtContentDTOS = new HashSet<>();
                if (request.getRequestWorktimeContents() != null && request.getRequestWorktimeContents().size() > 0) {
                    request.getRequestWorktimeContents().forEach(x -> {
                        RequestWorktimeOtContentResponseDTO requestWorktimeOtContentDTO = new RequestWorktimeOtContentResponseDTO();
                        BeanUtils.copyProperties(x, requestWorktimeOtContentDTO);
                        requestWorktimeOtContentDTOS.add(requestWorktimeOtContentDTO);
                    });
                    requestDTOResponse.setRequestWorktimeOtContent(requestWorktimeOtContentDTOS);
                }
                break;
            case Constant.PROCESS_TYPE.RECRUITMENT_REQUEST:
                Set<RequestRecruitmentContentResponseDTO> requestRecruitmentContentDTOS = new HashSet<>();
                Set<RequestRecruitmentUserContentResponseDTO> recruitmentUserContentResponseDTOS = new HashSet<>();
                if (request.getRequestRecruitmentContents() != null && request.getRequestRecruitmentContents().size() > 0) {
                    request.getRequestRecruitmentContents().forEach(x -> {
                        RequestRecruitmentContentResponseDTO requestRecruitmentContentDTO = new RequestRecruitmentContentResponseDTO();
                        BeanUtils.copyProperties(x, requestRecruitmentContentDTO);
                        requestRecruitmentContentDTOS.add(requestRecruitmentContentDTO);
                        if(x.getPositionId() != null){
                            listPositionId.add(x.getPositionId());
                        }
                        if(x.getLevelId() != null){
                            listLevelId.add(x.getLevelId());
                        }
                    });
                    requestDTOResponse.setRequestRecruitmentContent(requestRecruitmentContentDTOS);
                }
                if (request.getRequestRecruitmentUserContents() != null && request.getRequestRecruitmentUserContents().size() > 0) {
                    request.getRequestRecruitmentUserContents().forEach(x -> {
                        RequestRecruitmentUserContentResponseDTO recruitmentUserContentResponseDTO = new RequestRecruitmentUserContentResponseDTO();
                        BeanUtils.copyProperties(x, recruitmentUserContentResponseDTO);
                        recruitmentUserContentResponseDTOS.add(recruitmentUserContentResponseDTO);
                        if(x.getRequestRecruitmentUserLevelId() != null){
                            listLevelId.add(x.getRequestRecruitmentUserLevelId());
                        }
                    });
                    requestDTOResponse.setRequestRecruitmentUserContent(recruitmentUserContentResponseDTOS);
                }
                break;
            default:
                break;
        }
        List<PositionDTOResponse> listPosition = new ArrayList<>();
        List<LevelDTOResponse> levelLevel = new ArrayList<>();
        List<AssetTypeDTOResponse> listAssetType = new ArrayList<>();
        if(listPositionId.size() > 0) {
            listPosition = CallOtherService.getListFromIdService(PositionDTOResponse.class, Constant.PATH.GET_POSITION_MULTIPLE_ID, listPositionId, requestHttp);
        }
        if(listLevelId.size() > 0) {
            levelLevel = CallOtherService.getListFromIdService(LevelDTOResponse.class, Constant.PATH.GET_LEVEL_MULTIPLE_ID, listLevelId, requestHttp);
        }
        if(listAssetTypeId.size() > 0) {
            listAssetType = CallOtherService.getListFromIdService(AssetTypeDTOResponse.class, Constant.PATH.GET_ASSET_TYPE_MULTIPLE_ID, listAssetTypeId, requestHttp);
        }
        if(requestDTOResponse.getRequestRecruitmentContent() != null && requestDTOResponse.getRequestRecruitmentContent().size() > 0){
            RequestRecruitmentContentResponseDTO recruitmentContentResponseDTO = requestDTOResponse.getRequestRecruitmentContent().stream().findFirst().get();
            if(recruitmentContentResponseDTO.getPositionId() != null){
                PositionDTOResponse positionDTOResponse = listPosition.stream().filter(y -> y.getPositionId().equals(recruitmentContentResponseDTO.getPositionId())).findFirst().orElse(new PositionDTOResponse());
                requestDTOResponse.getRequestRecruitmentContent().stream().findFirst().get().setPosition(positionDTOResponse);
            }
            if(recruitmentContentResponseDTO.getLevelId() != null){
                LevelDTOResponse levelDTOResponse = levelLevel.stream().filter(y -> y.getLevelId().equals(recruitmentContentResponseDTO.getLevelId())).findFirst().orElse(new LevelDTOResponse());
                requestDTOResponse.getRequestRecruitmentContent().stream().findFirst().get().setLevel(levelDTOResponse);
            }
        }
        if(requestDTOResponse.getRequestRecruitmentUserContent() != null && requestDTOResponse.getRequestRecruitmentUserContent().size() > 0){
            for (RequestRecruitmentUserContentResponseDTO recruitmentUserContentResponseDTO : requestDTOResponse.getRequestRecruitmentUserContent()){
                if(recruitmentUserContentResponseDTO.getRequestRecruitmentUserLevelId() != null){
                    LevelDTOResponse levelDTOResponse = levelLevel.stream().filter(y -> y.getLevelId().equals(recruitmentUserContentResponseDTO.getRequestRecruitmentUserLevelId())).findFirst().orElse(new LevelDTOResponse());
                    recruitmentUserContentResponseDTO.setRequestRecruitmentUserLevel(levelDTOResponse);
                }
            }
        }
        if(requestDTOResponse.getRequestBuyEquipmentContents() != null && requestDTOResponse.getRequestBuyEquipmentContents().size() > 0){
            for (RequestBuyEquipmentContentResponseDTO requestBuyEquipmentContentResponseDTO : requestDTOResponse.getRequestBuyEquipmentContents()){
                if(requestBuyEquipmentContentResponseDTO.getAssetTypeId() != null && listAssetType.size() > 0){
                    AssetTypeDTOResponse assetTypeDTOResponse = listAssetType.stream().filter(y -> y.getAssetTypeId().equals(requestBuyEquipmentContentResponseDTO.getAssetTypeId())).findFirst().orElse(new AssetTypeDTOResponse());
                    requestBuyEquipmentContentResponseDTO.setAssetTypeName(assetTypeDTOResponse.getAssetTypeName());
                }

            }
        }
        return requestDTOResponse;
    }

    private List<RequestDTOResponse> getListRequestDTO(List<Request> requests, Boolean isCreated, UserDTOResponse userDTOResponse, HttpServletRequest httpRequest){
        List<RequestDTOResponse> requestDTOS = CommonUtil.mapList(requests, RequestDTOResponse.class);
        for (RequestDTOResponse requestDTOResponse : requestDTOS){
            requestDTOResponse.setRequestStatusProcess(null);
        }
        Set<String> listPositionId = new HashSet<>();
        Set<String> listAssetTypeId = new HashSet<>();
        Set<String> listLevelId = new HashSet<>();
        Set<String> listUserId = new HashSet<>();
        for (RequestDTOResponse requestDTOResponse : requestDTOS){
            requestDTOResponse.setRequestStatusProcess(null);
        }
        int i = 0;
        for(Request request : requests){
            if(!isCreated){
                ProcessStepLog processStepLog = processStepLogRepository.getProcessStepStatusByRequestIdAndReceiveUserId(userDTOResponse.getUserId(), request.getRequestId());
                if(processStepLog != null){
                    requestDTOS.get(i).setRequestStatusProcess(processStepLog.getProcessStepStatus());
                    //chỉ có trạng thái là chờ phê duyệt và từ chối mới trả action để FE check
                    if(processStepLog.getProcessStepStatus().equals(Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL) ||
                            processStepLog.getProcessStepStatus().equals(Constant.PROCESS_STEP_LOG_STATUS.DENIED)){
                        List<ProcessConfig> processConfigs = processConfigRepository.getProcessConfigByProcessLogMaxAndRequestIdAndReceiveUserId(request.getRequestId(), userDTOResponse.getUserId());
                        if(processConfigs.size() > 0){
                            List<Integer> listAction = new ArrayList<>();
                            for (ProcessConfig processConfig : processConfigs){
                                //nếu luồng phê duyệt tiếp theo tới ông cuối cùng trong luồng thì gửi về action COMPLETE để FE check không chọn  người phê duyệt
                                if(processConfig.getProcessStepType().equals(Constant.PROCESS_CONFIG_TYPE.APPROVAL) && processConfig.getProcessStepNext().getProcessStepCode().equals(Constant.PROCESS_STEP_CONFIG.END)){
                                    listAction.add(Constant.PROCESS_CONFIG_TYPE.APPROVAL_END);
                                    // nếu luồng phê duyệt là mua trang thiết bị, thanh toán hoặc tuyển dụng luồng tiếp theo là thực hiện thì trả về 4
                                }else if(processConfig.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.BUY_EQUIPMENT) ||
                                        processConfig.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST) ||
                                        processConfig.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST)){
                                    List<Integer> lstStatus = new ArrayList<>();
                                    lstStatus.add(Constant.PROCESS_CONFIG_TYPE.PERFORM);
                                    List<ProcessConfig> list = processConfigRepository.getProcessConfigNext(processConfig.getProcessConfigId(), lstStatus);
                                    if(list != null && list.size() > 0){
                                        listAction.add(Constant.PROCESS_CONFIG_TYPE.APPROVAL_END);
                                    }else{
                                        listAction.add(processConfig.getProcessStepType());
                                    }
                                }else{
                                    listAction.add(processConfig.getProcessStepType());
                                }
                            }
                            String result = listAction.stream()
                                    .map(Object::toString)
                                    .collect(Collectors.joining(","));
                            requestDTOS.get(i).setAction(result);
                        }
                    }
                }
            }
            if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)
                    && request.getRequestWorktimeContents() != null
                    && request.getRequestWorktimeContents().size() > 0){
                Set<RequestDayOffContentResponseDTO> requestDayOffContentDTOs = new HashSet<>();
                request.getRequestWorktimeContents().forEach(x -> {
                    RequestDayOffContentResponseDTO requestDayOffContentDTO = new RequestDayOffContentResponseDTO();
                    CoppyObject.copyNonNullProperties(x, requestDayOffContentDTO);
                    requestDayOffContentDTOs.add(requestDayOffContentDTO);
                });
                requestDTOS.get(i).setRequestDayOffContents(requestDayOffContentDTOs);
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE)
                    && request.getRequestWorktimeContents() != null
                    && request.getRequestWorktimeContents().size() > 0){
                Set<RequestRemoteContentResponseDTO> requestRemoteContentDTOS = new HashSet<>();
                request.getRequestWorktimeContents().forEach(x -> {
                    RequestRemoteContentResponseDTO requestRemoteContentDTO = new RequestRemoteContentResponseDTO();
                    CoppyObject.copyNonNullProperties(x, requestRemoteContentDTO);
                    requestRemoteContentDTOS.add(requestRemoteContentDTO);
                });
                requestDTOS.get(i).setRequestRemoteContents(requestRemoteContentDTOS);
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.BUY_EQUIPMENT)
                    && request.getRequestPayContents() != null
                    && request.getRequestPayContents().size() > 0) {
                Set<RequestBuyEquipmentContentResponseDTO> requestBuyEquipmentContentDTOS = new HashSet<>();
                request.getRequestPayContents().forEach(x -> {
                    RequestBuyEquipmentContentResponseDTO requestBuyEquipmentContentDTO = new RequestBuyEquipmentContentResponseDTO();
                    CoppyObject.copyNonNullProperties(x, requestBuyEquipmentContentDTO);
                    requestBuyEquipmentContentDTOS.add(requestBuyEquipmentContentDTO);
                    if(x.getAssetTypeId() != null){
                        listAssetTypeId.add(x.getAssetTypeId());
                    }
                });
                requestDTOS.get(i).setRequestBuyEquipmentContents(requestBuyEquipmentContentDTOS);
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST)
                    && request.getRequestPayContents() != null
                    && request.getRequestPayContents().size() > 0) {
                Set<RequestPaymentContentResponseDTO> requestPaymentContentDTOS = new HashSet<>();
                request.getRequestPayContents().forEach(x -> {
                    RequestPaymentContentResponseDTO requestPaymentContentDTO = new RequestPaymentContentResponseDTO();
                    CoppyObject.copyNonNullProperties(x, requestPaymentContentDTO);
                    requestPaymentContentDTOS.add(requestPaymentContentDTO);
                });
                requestDTOS.get(i).setRequestPaymentContents(requestPaymentContentDTOS);
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.WORK_CONFIRMATION)
                    && request.getRequestWorktimeContents() != null
                    && request.getRequestWorktimeContents().size() > 0) {
                Set<RequestWorkConfirmContentResponseDTO> requestWorkConfirmContentDTOS = new HashSet<>();
                request.getRequestWorktimeContents().forEach(x -> {
                    RequestWorkConfirmContentResponseDTO requestWorkConfirmContentDTO = new RequestWorkConfirmContentResponseDTO();
                    CoppyObject.copyNonNullProperties(x, requestWorkConfirmContentDTO);
                    requestWorkConfirmContentDTOS.add(requestWorkConfirmContentDTO);
                });
                requestDTOS.get(i).setRequestWorkConfirmContent(requestWorkConfirmContentDTOS);
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_OT)
                    && request.getRequestWorktimeContents() != null
                    && request.getRequestWorktimeContents().size() > 0) {
                Set<RequestWorktimeOtContentResponseDTO> requestWorktimeOtContentDTOS = new HashSet<>();
                request.getRequestWorktimeContents().forEach(x -> {
                    RequestWorktimeOtContentResponseDTO requestWorktimeOtContentDTO = new RequestWorktimeOtContentResponseDTO();
                    CoppyObject.copyNonNullProperties(x, requestWorktimeOtContentDTO);
                    requestWorktimeOtContentDTOS.add(requestWorktimeOtContentDTO);
                });
                requestDTOS.get(i).setRequestWorktimeOtContent(requestWorktimeOtContentDTOS);
            }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST)
                    && request.getRequestRecruitmentContents() != null
                    && request.getRequestRecruitmentContents().size() > 0) {
                Set<RequestRecruitmentContentResponseDTO> requestRecruitmentContentDTOS = new HashSet<>();
                request.getRequestRecruitmentContents().forEach(x -> {
                    RequestRecruitmentContentResponseDTO requestRecruitmentContentDTO = new RequestRecruitmentContentResponseDTO();
                    CoppyObject.copyNonNullProperties(x, requestRecruitmentContentDTO);
                    requestRecruitmentContentDTOS.add(requestRecruitmentContentDTO);
                    if(x.getPositionId() != null){
                        listPositionId.add(x.getPositionId());
                    }
                    if(x.getLevelId() != null){
                        listLevelId.add(x.getLevelId());
                    }
                });
                Set<RequestRecruitmentUserContentResponseDTO> recruitmentUserContentResponseDTOS = new HashSet<>();
                if (request.getRequestRecruitmentUserContents() != null && request.getRequestRecruitmentUserContents().size() > 0) {
                    request.getRequestRecruitmentUserContents().forEach(x -> {
                        RequestRecruitmentUserContentResponseDTO recruitmentUserContentResponseDTO = new RequestRecruitmentUserContentResponseDTO();
                        BeanUtils.copyProperties(x, recruitmentUserContentResponseDTO);
                        recruitmentUserContentResponseDTOS.add(recruitmentUserContentResponseDTO);
                        if(x.getRequestRecruitmentUserLevelId() != null){
                            listLevelId.add(x.getRequestRecruitmentUserLevelId());
                        }
                    });
                    requestDTOS.get(i).setRequestRecruitmentUserContent(recruitmentUserContentResponseDTOS);
                }
                requestDTOS.get(i).setRequestRecruitmentContent(requestRecruitmentContentDTOS);
            }
            if (request.getUpdatedBy() != null) {
                listUserId.add(request.getUpdatedBy());
            }
            if (request.getCreatedBy() != null) {
                listUserId.add(request.getCreatedBy());
            }
            i++;
        }
        List<PositionDTOResponse> listPosition = new ArrayList<>();
        List<LevelDTOResponse> listLevel = new ArrayList<>();
        List<AssetTypeDTOResponse> listAssetType = new ArrayList<>();
        if(listPositionId.size() > 0 && requestDTOS.size() > 0) {
            listPosition = CallOtherService.getListFromIdService(PositionDTOResponse.class, Constant.PATH.GET_POSITION_MULTIPLE_ID, listPositionId, httpRequest);
        }
        if(listLevelId.size() > 0 && requestDTOS.size() > 0) {
            listLevel = CallOtherService.getListFromIdService(LevelDTOResponse.class, Constant.PATH.GET_LEVEL_MULTIPLE_ID, listLevelId, httpRequest);
        }
        if(listAssetTypeId.size() > 0 && requestDTOS.size() > 0) {
            listAssetType = CallOtherService.getListFromIdService(AssetTypeDTOResponse.class, Constant.PATH.GET_ASSET_TYPE_MULTIPLE_ID, listAssetTypeId, httpRequest);
        }
        for (RequestDTOResponse requestDTOResponse : requestDTOS){
            if(requestDTOResponse.getRequestRecruitmentUserContent() != null && requestDTOResponse.getRequestRecruitmentUserContent().size() > 0){
                for (RequestRecruitmentUserContentResponseDTO recruitmentUserContentResponseDTO : requestDTOResponse.getRequestRecruitmentUserContent()){
                    LevelDTOResponse levelDTOResponse = listLevel.stream().filter(y -> y.getLevelId().equals(recruitmentUserContentResponseDTO.getRequestRecruitmentUserLevelId())).findFirst().orElse(new LevelDTOResponse());
                    recruitmentUserContentResponseDTO.setRequestRecruitmentUserLevel(levelDTOResponse);
                }
            }
            if(requestDTOResponse.getRequestRecruitmentContent() != null && requestDTOResponse.getRequestRecruitmentContent().size() > 0){
                RequestRecruitmentContentResponseDTO recruitmentContentResponseDTO = requestDTOResponse.getRequestRecruitmentContent().stream().findFirst().get();
                if(recruitmentContentResponseDTO.getPositionId() != null && listPosition.size() > 0){
                    PositionDTOResponse positionDTOResponse = listPosition.stream().filter(y -> y.getPositionId().equals(recruitmentContentResponseDTO.getPositionId())).findFirst().orElse(new PositionDTOResponse());
                    requestDTOResponse.getRequestRecruitmentContent().stream().findFirst().get().setPosition(positionDTOResponse);
                }
                if(recruitmentContentResponseDTO.getLevelId() != null && listLevel.size() > 0){
                    LevelDTOResponse levelDTOResponse = listLevel.stream().filter(y -> y.getLevelId().equals(recruitmentContentResponseDTO.getLevelId())).findFirst().orElse(new LevelDTOResponse());
                    requestDTOResponse.getRequestRecruitmentContent().stream().findFirst().get().setLevel(levelDTOResponse);
                }
                if(requestDTOResponse.getRequestRecruitmentUserContent() != null && requestDTOResponse.getRequestRecruitmentUserContent().size() > 0){
                    for (RequestRecruitmentUserContentResponseDTO recruitmentUserContentResponseDTO : requestDTOResponse.getRequestRecruitmentUserContent()){
                        List<Attachment> attachmentUserContent = attachmentRepository.getByObjectIdAndAttachmentStatus(recruitmentUserContentResponseDTO.getRequestRecruitmentUserContentId());
                        if(attachmentUserContent != null && attachmentUserContent.size() > 0){
                            recruitmentUserContentResponseDTO.setListAttachment(CommonUtil.mapList(attachmentUserContent, AttachmentDTOResponse.class));
                        }
                    }
                }
            }
            if(requestDTOResponse.getRequestBuyEquipmentContents() != null && requestDTOResponse.getRequestBuyEquipmentContents().size() > 0){
                for (RequestBuyEquipmentContentResponseDTO requestBuyEquipmentContentResponseDTO : requestDTOResponse.getRequestBuyEquipmentContents()){
                    if(requestBuyEquipmentContentResponseDTO.getAssetTypeId() != null && listAssetType.size() > 0){
                        AssetTypeDTOResponse assetTypeDTOResponse = listAssetType.stream().filter(y -> y.getAssetTypeId().equals(requestBuyEquipmentContentResponseDTO.getAssetTypeId())).findFirst().orElse(new AssetTypeDTOResponse());
                        requestBuyEquipmentContentResponseDTO.setAssetTypeName(assetTypeDTOResponse.getAssetTypeName());
                    }

                }
            }
        }
        if(requestDTOS.size() > 0 && listUserId.size() > 0){
            List<UserDTOResponse> listUser = CallOtherService.getListFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_MULTIPLE_ID, listUserId, httpRequest);
            requestDTOS.forEach(x -> {
                if(listUser != null && listUser.size() > 0){
                    UserDTOResponse userCreated = listUser.stream().filter(y -> y.getUserId().equals(x.getCreatedBy())).findFirst().orElse(new UserDTOResponse());
                    x.setCreatedByName(userCreated.getFullName());
                    x.setDepartmentName(userCreated.getDepartment() != null ? userCreated.getDepartment().getDepartmentName() : null);
                    x.setPositionName(userCreated.getPosition() != null ? userCreated.getPosition().getPositionName() : null);
                    x.setUpdatedByName(listUser.stream().filter(y -> y.getUserId().equals(x.getUpdatedBy())).findFirst().orElse(new UserDTOResponse()).getFullName());
                }
            });
        }
        return requestDTOS;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, HttpServletRequest httpRequest,
                                       Integer requestWorkTimeType, Integer dayOffType, Integer requestOtType,
                                       Integer payType, String position, Boolean isCreated, Integer processType,
                                       String requestCode, String keyword, List<Integer> status,
                                       String username, Date fromDate, Date toDate, Boolean isAll) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
        List<String> listUserStr = new ArrayList<>();
        if(username != null && !username.isEmpty()){
            List<UserDTOResponse> userDTOResponses = CallOtherService.getListUserFromIdServiceByFullname(username, httpRequest);
            if(userDTOResponses != null && userDTOResponses.size() > 0){
                listUserStr = userDTOResponses.stream().map(UserDTOResponse::getUserId).collect(Collectors.toList());
            }
        }
        Page<Request> page;
        if(username != null && !username.isEmpty() && listUserStr.size() == 0){
            page = Page.empty();
        }else{
            if(isCreated){
                page = requestRepository.getAllRequestByRequestStatus(paging, processType, "%" + requestCode + "%",
                        status, fromDate, toDate, userDTOResponse.getUserId(), requestWorkTimeType, dayOffType,
                        requestOtType, payType, "%" + keyword + "%", position, listUserStr, isAll);
            }else{
                page = requestRepository.getAllRequest(paging, processType, userDTOResponse.getUserId(),
                        "%" + requestCode + "%",  status, fromDate, toDate,
                        requestWorkTimeType, dayOffType,
                        requestOtType, payType, position, listUserStr, Constant.PROCESS_STEP_CONFIG.START);
            }
        }

        if (page.hasContent()) {
            List<Request> requests = CommonUtil.mapList(page.getContent(), Request.class);
            List<RequestDTOResponse> requestDTOS = getListRequestDTO(requests, isCreated, userDTOResponse, httpRequest);
            output.put("data", requestDTOS);
            output.put("total", page.getTotalElements());
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
            output.put("countTabTotal", 0);
        }
        if(isCreated){
            Long countTabChuaGui = requestRepository.getCountAllRequestByRequestStatus(processType, Arrays.asList(Constant.REQUEST_STATUS.UNSENT), userDTOResponse.getUserId(), isAll);
            Long countTabChoPheDuyet = requestRepository.getCountAllRequestByRequestStatus(processType, Arrays.asList(Constant.REQUEST_STATUS.WAITING_APPROVAL), userDTOResponse.getUserId(), isAll);
            Long countTabDaPheDuyet = requestRepository.getCountAllRequestByRequestStatus(processType, Arrays.asList(Constant.REQUEST_STATUS.APPROVED), userDTOResponse.getUserId(), isAll);
            Long countTabTuChoi = requestRepository.getCountAllRequestByRequestStatus(processType, Arrays.asList(Constant.REQUEST_STATUS.REFUSE), userDTOResponse.getUserId(), isAll);
            Long countTabDaHoanThanh = requestRepository.getCountAllRequestByRequestStatus(processType, Arrays.asList(Constant.REQUEST_STATUS.COMPLETED), userDTOResponse.getUserId(), isAll);
            Long countTabDaHuy = requestRepository.getCountAllRequestByRequestStatus(processType, Arrays.asList(Constant.REQUEST_STATUS.CANCEL), userDTOResponse.getUserId(), isAll);
            Long countTabTotal = countTabChuaGui + countTabChoPheDuyet + countTabDaPheDuyet + countTabTuChoi + countTabDaHuy;
            if(processType.equals(Constant.PROCESS_TYPE.BUY_EQUIPMENT) ||
                    processType.equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST) ||
                    processType.equals(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST)){
                countTabTotal += countTabDaHoanThanh;
                output.put("countTabDaHoanThanh", countTabDaHoanThanh);
            }
            output.put("countTabChuaGui", countTabChuaGui);
            output.put("countTabChoPheDuyet", countTabChoPheDuyet);
            output.put("countTabDaPheDuyet", countTabDaPheDuyet);
            output.put("countTabTuChoi", countTabTuChoi);
            output.put("countTabDaHuy", countTabDaHuy);
            output.put("countTabTotal", countTabTotal);
        }else{
            Long countTabChoPheDuyet = requestRepository.getCountAllRequest(processType, userDTOResponse.getUserId(), Arrays.asList(Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL), Constant.PROCESS_STEP_CONFIG.START);
            Long countTabDaPheDuyet = requestRepository.getCountAllRequest(processType, userDTOResponse.getUserId(), Arrays.asList(Constant.PROCESS_STEP_LOG_STATUS.APPROVED), Constant.PROCESS_STEP_CONFIG.START);
            Long countTabTuChoi = requestRepository.getCountAllRequest(processType, userDTOResponse.getUserId(), Arrays.asList(Constant.PROCESS_STEP_LOG_STATUS.REFUSE), Constant.PROCESS_STEP_CONFIG.START);
            Long countTabBiTuChoi = requestRepository.getCountAllRequest(processType, userDTOResponse.getUserId(), Arrays.asList(Constant.PROCESS_STEP_LOG_STATUS.DENIED), Constant.PROCESS_STEP_CONFIG.START);
            Long countTabDaHoanThanh = requestRepository.getCountAllRequest(processType, userDTOResponse.getUserId(), Arrays.asList(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED), Constant.PROCESS_STEP_CONFIG.START);
            Long countTabTotal = countTabChoPheDuyet + countTabDaPheDuyet + countTabTuChoi + countTabBiTuChoi + countTabDaHoanThanh;
            output.put("countTabChoPheDuyet", countTabChoPheDuyet);
            output.put("countTabDaPheDuyet", countTabDaPheDuyet);
            output.put("countTabTuChoi", countTabTuChoi);
            output.put("countTabBiTuChoi", countTabBiTuChoi);
            output.put("countTabDaHoanThanh", countTabDaHoanThanh);
            output.put("countTabTotal", countTabTotal);
            if(processType.equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST)){
                List<Request> requests = requestRepository.getAllRequestWithoutPaging(processType, userDTOResponse.getUserId(),
                        Constant.PROCESS_STEP_CONFIG.START);
                if(requests != null && requests.size() > 0){
                    getDataSumRequestPayment(requests, output);
                }else{
                    output.put("countPhaiThanhToan", 0);
                    output.put("countDaThanhToan", 0);
                    output.put("countCanThanhToan", 0);
                    output.put("countHoanUng", 0);
                }
            }

        }
        output.put("page", pageNo);
        output.put("size", pageSize);
        return output;
    }

    private void getDataSumRequestPayment(List<Request> requests, Map<String, Object> data){
        AtomicReference<Double> countUnitMoney = new AtomicReference<>((double) 0);
        AtomicReference<Double> countAdvanceMoney = new AtomicReference<>((double) 0);
        AtomicReference<Double> countReceiveMonney = new AtomicReference<>((double) 0);
        AtomicReference<Double> countRemaining = new AtomicReference<>((double) 0);
        AtomicReference<Double> countCanThanhToan = new AtomicReference<>((double) 0);
        AtomicReference<Double> countHoanUng = new AtomicReference<>((double) 0);
        requests.forEach(request -> {
            AtomicReference<Double> countReceiveMonneyRequest = new AtomicReference<>((double) 0);
            AtomicReference<Double> countRemainingRequest = new AtomicReference<>((double) 0);
            if(request.getRequestPayContents() != null && request.getRequestPayContents().size() > 0){
                request.getRequestPayContents().forEach(requestPayContent -> {
                    countUnitMoney.updateAndGet(z -> z + requestPayContent.getUnitPrice());
                    countAdvanceMoney.updateAndGet(z -> z + requestPayContent.getAdvancedMoney());
                    countRemaining.updateAndGet(z -> z + requestPayContent.getRemainingMoney());
                    countRemainingRequest.updateAndGet(z -> z + requestPayContent.getRemainingMoney());
                });
                if(request.getRequestPayContents().stream().findFirst().get().getReceivedMoney() != null){
                    countReceiveMonney.updateAndGet(z -> z + request.getRequestPayContents().stream().findFirst().get().getReceivedMoney());
                    countReceiveMonneyRequest.updateAndGet(z -> z + request.getRequestPayContents().stream().findFirst().get().getReceivedMoney());
                }
            }
            if(request.getPayType().equals(Constant.PAY_TYPE.RETURN)){
                countHoanUng.updateAndGet(z -> z + (countRemainingRequest.get() - countReceiveMonneyRequest.get()));
            }else{
                countCanThanhToan.updateAndGet(z -> z + (countRemainingRequest.get() - countReceiveMonneyRequest.get()));
            }
        });
        data.put("countPhaiThanhToan", countUnitMoney.get() - countAdvanceMoney.get());
        data.put("countDaThanhToan", countReceiveMonney.get());
        data.put("countCanThanhToan", countCanThanhToan.get());
        data.put("countHoanUng", countHoanUng.get());
    }

    @Override
    public InputStreamResource export(HttpServletRequest httpRequest, Integer requestWorkTimeType, Integer dayOffType,
                                      Integer requestOtType, Integer payType, String position, Boolean isCreated,
                                      Integer processType, String requestCode, String keyword, List<Integer> status,
                                      String username, Date fromDate, Date toDate, Boolean isAll) throws IOException {
        List<Request> requests = new ArrayList<>();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
        List<String> listUserStr = new ArrayList<>();
        if(username != null && !username.isEmpty()){
            List<UserDTOResponse> userDTOResponses = CallOtherService.getListUserFromIdServiceByFullname(username, httpRequest);
            if(userDTOResponses != null && userDTOResponses.size() > 0){
                listUserStr = userDTOResponses.stream().map(UserDTOResponse::getUserId).collect(Collectors.toList());
            }
        }
        if(isCreated){
            requests = requestRepository.getAllRequestByRequestStatusExport(processType, "%" + requestCode + "%",
                    status, fromDate, toDate, userDTOResponse.getUserId(), requestWorkTimeType, dayOffType,
                    requestOtType, payType,"%" + keyword + "%", position, listUserStr, isAll);
        }else{
            requests = requestRepository.getAllRequestExport(processType, userDTOResponse.getUserId(),
                    "%" + requestCode + "%", status, fromDate, toDate,
                    requestWorkTimeType, dayOffType,
                    requestOtType, payType, position, listUserStr, Constant.PROCESS_STEP_CONFIG.START);
        }
        List<RequestDTOResponse> requestDTOS = getListRequestDTO(requests, isCreated, userDTOResponse, httpRequest);
        InputStreamResource inputStream = null;
        AtomicInteger stt = new AtomicInteger(1); // đánh stt
        InputStream templateStream;
        XSSFWorkbook workbook;
        XSSFSheet sheet;
        XSSFCellStyle style;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        switch (processType){
            case Constant.PROCESS_TYPE.BUY_EQUIPMENT:
                AtomicInteger rowIndexBuyEquipment = new AtomicInteger(3); //  tiêu đề, bắt đầu từ row 4 để thêm dữ liệu
                templateStream = RequestServiceImpl.class.getResourceAsStream("/template/" + Constant.TEMPLATE_EXPORT_BUY_EQUIPMENT);
                workbook = new XSSFWorkbook(templateStream);
                sheet = workbook.getSheetAt(0);
                style = ExcelUtil.getStyleBasic(workbook);
                style.setWrapText(true);
                for(RequestDTOResponse request : requestDTOS){
                    Row row = sheet.createRow(rowIndexBuyEquipment.getAndIncrement());

                    //stt
                    Cell cellStt = row.createCell(0);
                    cellStt.setCellValue(stt.getAndIncrement());
                    cellStt.setCellStyle(style);

                    //Mã đề xuất
                    Cell cellRequestCode = row.createCell(1);
                    cellRequestCode.setCellValue(request.getRequestCode());
                    cellRequestCode.setCellStyle(style);

                    //Nhân viên đề xuất
                    Cell cellAccountOwner = row.createCell(2);
                    cellAccountOwner.setCellValue(request.getCreatedByName());
                    cellAccountOwner.setCellStyle(style);

                    //Nội dung đề xuất
                    Cell cellBank = row.createCell(3);
                    cellBank.setCellValue(request.getContent());
                    cellBank.setCellStyle(style);

                    //Lý do
                    Cell cellAccountNumber = row.createCell(4);
                    cellAccountNumber.setCellValue(request.getReason());
                    cellAccountNumber.setCellStyle(style);

                    //Trạng thái xử lý
                    Cell cellContent = row.createCell(5);
                    cellContent.setCellValue(getStatusRequestStr(request, isCreated));
                    cellContent.setCellStyle(style);

                }
                workbook.write(outputStream);
                workbook.close();
                inputStream = new InputStreamResource(new ByteArrayInputStream(outputStream.toByteArray()));
                break;
            case Constant.PROCESS_TYPE.PAYMENT_REQUEST:
                AtomicInteger rowIndexPaymentRequest = new AtomicInteger(3); //  tiêu đề, bắt đầu từ row 4 để thêm dữ liệu
                templateStream = RequestServiceImpl.class.getResourceAsStream("/template/" + Constant.TEMPLATE_EXPORT_REQUEST_PAYMENT);
                workbook = new XSSFWorkbook(templateStream);
                sheet = workbook.getSheetAt(0);
                style = ExcelUtil.getStyleBasic(workbook);
                style.setWrapText(true);

                //style basic Pink
                XSSFCellStyle stylePink = com.blameo.erpservice.utils.ExcelUtil.getStyleBasic(workbook);
                XSSFColor colorPink = new XSSFColor(new java.awt.Color(251,201,206));
                stylePink.setFillForegroundColor(colorPink);
                stylePink.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                stylePink.setWrapText(true);

                //style basic Yellow
                XSSFCellStyle styleYellow = com.blameo.erpservice.utils.ExcelUtil.getStyleBasic(workbook);
                XSSFColor colorYellow = new XSSFColor(new java.awt.Color(252,236,155));
                styleYellow.setFillForegroundColor(colorYellow);
                styleYellow.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                styleYellow.setWrapText(true);

                //style basic Grey
                XSSFCellStyle styleGrey = com.blameo.erpservice.utils.ExcelUtil.getStyleBasic(workbook);
                XSSFColor colorGrey = new XSSFColor(new java.awt.Color(242,242,242));
                styleGrey.setFillForegroundColor(colorGrey);
                styleGrey.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                styleGrey.setWrapText(true);

                //style basic Green
                XSSFCellStyle styleGreen = com.blameo.erpservice.utils.ExcelUtil.getStyleBasic(workbook);
                XSSFColor colorGreen = new XSSFColor(new java.awt.Color(196,236,204));
                styleGreen.setFillForegroundColor(colorGreen);
                styleGreen.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                styleGreen.setWrapText(true);
                for(RequestDTOResponse request : requestDTOS){
                    Row row = sheet.createRow(rowIndexPaymentRequest.getAndIncrement());

                    //stt
                    Cell cellStt = row.createCell(0);
                    cellStt.setCellValue(stt.getAndIncrement());
                    cellStt.setCellStyle(style);

                    //Mã đề xuất
                    Cell cellRequestCode = row.createCell(1);
                    cellRequestCode.setCellValue(request.getRequestCode());
                    cellRequestCode.setCellStyle(style);

                    //Loại thanh toán
                    Cell cellRequestPaymentType = row.createCell(2);
                    if(request.getPayType().equals(Constant.PAY_TYPE.ADVANCE)){
                        cellRequestPaymentType.setCellValue(messageSource.getMessage("advance", null, locale));
                    }else if(request.getPayType().equals(Constant.PAY_TYPE.PAYMENT)){
                        cellRequestPaymentType.setCellValue(messageSource.getMessage("pay", null, locale));
                    }else{
                        cellRequestPaymentType.setCellValue(messageSource.getMessage("reimbursement", null, locale));
                    }
                    cellRequestPaymentType.setCellStyle(style);

                    //Tên nhà cung cấp
                    Cell cellAccountOwner = row.createCell(3);
                    cellAccountOwner.setCellValue(request.getAccountOwner());
                    cellAccountOwner.setCellStyle(style);

                    //Ngân hàng
                    Cell cellBank = row.createCell(4);
                    cellBank.setCellValue(request.getBank());
                    cellBank.setCellStyle(style);

                    //Số tài khoản
                    Cell cellAccountNumber = row.createCell(5);
                    cellAccountNumber.setCellValue(request.getAccountNumber());
                    cellAccountNumber.setCellStyle(style);

                    //Nội dung
                    Cell cellContent = row.createCell(6);
                    cellContent.setCellValue(request.getContent());
                    cellContent.setCellStyle(style);

                    Double totalPayment = 0.0;
                    Double advancePayment = 0.0;
                    Double remaining = 0.0;
                    Double receiveMonney = 0.0;
                    for(RequestPaymentContentResponseDTO requestPayContent : request.getRequestPaymentContents()){
                        if(requestPayContent.getUnitPrice() != null){
                            totalPayment += requestPayContent.getUnitPrice();
                        }
                        if(requestPayContent.getAdvancedMoney() != null){
                            advancePayment += requestPayContent.getAdvancedMoney();
                        }
                        if(requestPayContent.getRemainingMoney() != null){
                            remaining += requestPayContent.getRemainingMoney();
                        }
                    }
                    if(request.getRequestPaymentContents().stream().findFirst().get().getReceivedMoney() != null){
                        receiveMonney = request.getRequestPaymentContents().stream().findFirst().get().getReceivedMoney();
                    }

                    //Tổng số tiền phải thanh toán
                    Cell cellTotalPayment = row.createCell(7);
                    cellTotalPayment.setCellStyle(stylePink);
                    //Số tiền cần hoàn ứng
                    Cell cellAdvancePayment  = row.createCell(8);
                    cellAdvancePayment.setCellStyle(styleYellow);
                    if(!request.getPayType().equals(Constant.PAY_TYPE.RETURN)){
                        cellTotalPayment.setCellValue(totalPayment - advancePayment);
                    }else{
                        cellAdvancePayment.setCellValue(totalPayment - advancePayment);
                    }

                    //Đã hoàn thành/ hoàn ứng
                    Cell cellPaid = row.createCell(9);
                    cellPaid.setCellStyle(styleGrey);
                    cellPaid.setCellValue(receiveMonney);

                    //Chưa thanh toán/ hoàn ứng
                    Cell cellRemaining = row.createCell(10);
                    cellRemaining.setCellStyle(styleGreen);
                    cellRemaining.setCellValue(Math.abs(remaining - receiveMonney));

                }
                workbook.write(outputStream);
                workbook.close();
                inputStream = new InputStreamResource(new ByteArrayInputStream(outputStream.toByteArray()));
                break;
            case Constant.PROCESS_TYPE.RECRUITMENT_REQUEST:
                AtomicInteger rowIndexRecruitmentRequest = new AtomicInteger(2); //  tiêu đề, bắt đầu từ row 3 để thêm dữ liệu
                templateStream = RequestServiceImpl.class.getResourceAsStream("/template/" + Constant.TEMPLATE_EXPORT_REQUEST_RECRUITMENT);
                workbook = new XSSFWorkbook(templateStream);
                sheet = workbook.getSheetAt(0);
                style = ExcelUtil.getStyleBasic(workbook);
                style.setWrapText(true);
                for(RequestDTOResponse request : requestDTOS) {
                    if(request.getRequestRecruitmentUserContent() != null && request.getRequestRecruitmentUserContent().size() > 0){
                        int index = 1;
                        for (RequestRecruitmentUserContentResponseDTO requestRecruitmentUserContent : request.getRequestRecruitmentUserContent()){
                            Row row = sheet.createRow(rowIndexRecruitmentRequest.getAndIncrement());
                            if(index == 1){
                                //stt
                                Cell cellStt = row.createCell(0);
                                cellStt.setCellValue(stt.getAndIncrement());
                                cellStt.setCellStyle(style);

                                //Mã đề xuất
                                Cell cellRequestCode = row.createCell(1);
                                cellRequestCode.setCellValue(request.getRequestCode());
                                cellRequestCode.setCellStyle(style);

                                //Vị trí tuyển dụng
                                Cell cellPosition = row.createCell(2);
                                cellPosition.setCellValue(request.getRequestRecruitmentContent().stream().findFirst().get().getPosition().getPositionName());
                                cellPosition.setCellStyle(style);

                                //Số lượng
                                Cell cellQuantity = row.createCell(3);
                                cellQuantity.setCellValue(request.getRequestRecruitmentContent().stream().findFirst().get().getQuantity());
                                cellQuantity.setCellStyle(style);

                                //Thời gian dự kiến onboard
                                Cell cellTimeOnboard = row.createCell(4);
                                String timeOnboardStart = request.getRequestRecruitmentContent().stream().findFirst().get().getOnboardTimeStart() != null ?
                                        CommonUtil.dateToString(request.getRequestRecruitmentContent().stream().findFirst().get().getOnboardTimeStart()) : "";
                                String timeOnboardEnd = request.getRequestRecruitmentContent().stream().findFirst().get().getOnboardTimeEnd() != null ?
                                        CommonUtil.dateToString(request.getRequestRecruitmentContent().stream().findFirst().get().getOnboardTimeEnd()) : "";
                                cellTimeOnboard.setCellValue(timeOnboardStart + " - " + timeOnboardEnd);
                                cellTimeOnboard.setCellStyle(style);

                                //Trạng thái xử lý
                                Cell cellContent = row.createCell(5);
                                cellContent.setCellValue(getStatusRequestStr(request, isCreated));
                                cellContent.setCellStyle(style);
                            }else{
                                //stt
                                Cell cellStt = row.createCell(0);
                                cellStt.setCellStyle(style);

                                //Mã đề xuất
                                Cell cellRequestCode = row.createCell(1);
                                cellRequestCode.setCellStyle(style);

                                //Vị trí tuyển dụng
                                Cell cellPosition = row.createCell(2);
                                cellPosition.setCellStyle(style);

                                //Số lượng
                                Cell cellQuantity = row.createCell(3);
                                cellQuantity.setCellStyle(style);

                                //Thời gian dự kiến onboard
                                Cell cellTimeOnboard = row.createCell(4);
                                cellTimeOnboard.setCellStyle(style);

                                //Trạng thái xử lý
                                Cell cellContent = row.createCell(5);
                                cellContent.setCellStyle(style);
                            }

                            //STT
                            Cell cellSTT = row.createCell(6);
                            cellSTT.setCellValue(index);
                            cellSTT.setCellStyle(style);

                            //Tên nhân sự
                            Cell cellUsername = row.createCell(7);
                            cellUsername.setCellValue(requestRecruitmentUserContent.getRequestRecruitmentUsername());
                            cellUsername.setCellStyle(style);

                            //Level
                            Cell cellLevel = row.createCell(8);
                            cellLevel.setCellValue(requestRecruitmentUserContent.getRequestRecruitmentUserLevel().getLevelName());
                            cellLevel.setCellStyle(style);

                            //Mức lương
                            Cell cellSalary = row.createCell(9);
                            cellSalary.setCellValue(requestRecruitmentUserContent.getRequestRecruitmentUserSalary());
                            cellSalary.setCellStyle(style);

                            //Thời gian bắt đầu
                            Cell cellStarttime = row.createCell(10);
                            String startTime = CommonUtil.dateToString(requestRecruitmentUserContent.getRequestRecruitmentUserStartTime());
                            cellStarttime.setCellValue(startTime);
                            cellStarttime.setCellStyle(style);

                            //FileCV
                            Cell cellFileCV = row.createCell(11);
                            cellFileCV.setCellStyle(style);
                            if(requestRecruitmentUserContent.getListAttachment() != null && requestRecruitmentUserContent.getListAttachment().size() > 0){
                                cellFileCV.setCellValue(requestRecruitmentUserContent.getListAttachment().get(0).getAttachmentName());
                            }
                            index++;
                        }
                        if(request.getRequestRecruitmentUserContent().size() > 1){
                            for (int i = 0;i<6;i++){
                                sheet.addMergedRegion(new CellRangeAddress(rowIndexRecruitmentRequest.get() - request.getRequestRecruitmentUserContent().size(), rowIndexRecruitmentRequest.get() - 1,i,i));
                            }
                        }
                    }else{
                        Row row = sheet.createRow(rowIndexRecruitmentRequest.getAndIncrement());

                        //stt
                        Cell cellStt = row.createCell(0);
                        cellStt.setCellValue(stt.getAndIncrement());
                        cellStt.setCellStyle(style);

                        //Mã đề xuất
                        Cell cellRequestCode = row.createCell(1);
                        cellRequestCode.setCellValue(request.getRequestCode());
                        cellRequestCode.setCellStyle(style);

                        //Vị trí tuyển dụng
                        Cell cellPosition = row.createCell(2);
                        cellPosition.setCellValue(request.getRequestRecruitmentContent().stream().findFirst().get().getPosition().getPositionName());
                        cellPosition.setCellStyle(style);

                        //Số lượng
                        Cell cellQuantity = row.createCell(3);
                        cellQuantity.setCellValue(request.getRequestRecruitmentContent().stream().findFirst().get().getQuantity());
                        cellQuantity.setCellStyle(style);

                        //Thời gian dự kiến onboard
                        Cell cellTimeOnboard = row.createCell(4);
                        String timeOnboardStart = request.getRequestRecruitmentContent().stream().findFirst().get().getOnboardTimeStart() != null ?
                                CommonUtil.dateToString(request.getRequestRecruitmentContent().stream().findFirst().get().getOnboardTimeStart()) : "";
                        String timeOnboardEnd = request.getRequestRecruitmentContent().stream().findFirst().get().getOnboardTimeEnd() != null ?
                                CommonUtil.dateToString(request.getRequestRecruitmentContent().stream().findFirst().get().getOnboardTimeEnd()) : "";
                        cellTimeOnboard.setCellValue(timeOnboardStart + " - " + timeOnboardEnd);
                        cellTimeOnboard.setCellStyle(style);

                        //Trạng thái xử lý
                        Cell cellContent = row.createCell(5);
                        cellContent.setCellValue(getStatusRequestStr(request, isCreated));
                        cellContent.setCellStyle(style);

                        Cell cellSTT = row.createCell(6);
                        cellSTT.setCellStyle(style);

                        Cell cellUsername = row.createCell(7);
                        cellUsername.setCellStyle(style);

                        Cell cellLevel = row.createCell(8);
                        cellLevel.setCellStyle(style);

                        Cell cellSalary = row.createCell(9);
                        cellSalary.setCellStyle(style);

                        Cell cellStarttime = row.createCell(10);
                        cellStarttime.setCellStyle(style);

                        Cell cellFileCV = row.createCell(11);
                        cellFileCV.setCellStyle(style);

                    }
                }
                workbook.write(outputStream);
                workbook.close();
                inputStream = new InputStreamResource(new ByteArrayInputStream(outputStream.toByteArray()));
                break;
            default:
                break;
        }
        return inputStream;
    }

    private String getStatusRequestStr(RequestDTOResponse request, Boolean isCreate){
        String requestStatusStr = "";
        if(isCreate){
            switch (request.getRequestStatus()){
                case Constant.REQUEST_STATUS.UNSENT:
                    requestStatusStr = "Chưa gửi";
                    break;
                case Constant.REQUEST_STATUS.WAITING_APPROVAL:
                    requestStatusStr = "Chờ xử lý";
                    break;
                case Constant.REQUEST_STATUS.APPROVED:
                    requestStatusStr = "Đã phê duyệt";
                    break;
                case Constant.REQUEST_STATUS.REFUSE:
                    requestStatusStr = "Từ chối";
                    break;
                case Constant.REQUEST_STATUS.COMPLETED:
                    requestStatusStr = "Đã hoàn thành";
                    break;
                default:
                    break;
            }
        }else{
            switch (request.getRequestStatusProcess()){
                case Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL:
                    requestStatusStr = "Chờ xử lý";
                    break;
                case Constant.PROCESS_STEP_LOG_STATUS.APPROVED:
                    requestStatusStr = "Đã phê duyệt";
                    break;
                case Constant.PROCESS_STEP_LOG_STATUS.REFUSE:
                    requestStatusStr = "Từ chối";
                    break;
                case Constant.PROCESS_STEP_LOG_STATUS.DENIED:
                    requestStatusStr = "Bị từ chối";
                    break;
                case Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED:
                    requestStatusStr = "Đã hoàn thành";
                    break;
                default:
                    break;
            }
        }
        return requestStatusStr;
    }

    @Override
    @Transactional
    public void send(String id, HttpServletRequest httpServletRequest) {
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpServletRequest);
        Request request = requestRepository.findById(id).get();
        if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE) ||
                request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE) ||
                request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.WORK_CONFIRMATION)){
            List<RequestWorktimeContent> listRequestWorktimeContent = new ArrayList<RequestWorktimeContent>(request.getRequestWorktimeContents());
            validateSendRequestWorkTime(request.getProcess().getProcessType(), listRequestWorktimeContent, httpServletRequest, userDTOResponse);
        }
        request.setRequestStatus(Constant.REQUEST_STATUS.WAITING_APPROVAL);
        List<ProcessConfig> processConfigsNext = processConfigRepository.getFirstConfigByProcessType(request.getProcess().getProcessType(), Constant.PROCESS_STEP_CONFIG.START, Constant.PROCESS_CONFIG_TYPE.SEND);
        RequestLog requestLog = requestLogRepository.findRequestLogByRequestIdAndRequestLogAction(request.getRequestId(), Constant.REQUEST_LOG_ACTION.CREATE);
        UserDTOResponse userReceiveRequest = CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, requestLog.getRequestApprove(), httpServletRequest);
        if(userReceiveRequest == null){
            throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.USER, null, CommonUtil.getLocale()) + " " + messageSource.getMessage(Constant.NOT_EXIST, null, CommonUtil.getLocale()));
        }
        if(processConfigsNext != null && processConfigsNext.size() > 0){
            processConfigsNext.forEach(x -> {
                ProcessStepLog processStepLog = processStepLogRepository.getProcessLogMax(id);
                int orderMax = processStepLog != null ? processStepLog.getOrderProcess() : 0;
                ProcessStepLog processStepLogSave = new ProcessStepLog(request.getRequestId(), Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL, x.getProcessConfigId(), orderMax + 1, userDTOResponse.getUserId(), requestLog.getRequestApprove());
                processStepLogRepository.save(processStepLogSave);
                //lưu log requestProcess
                RequestProcessLog requestProcessLog = requestProcessLogRepository.getRequestProcessLogMax(id);
                int requestProcessLogOrderMax =requestProcessLog != null ? requestProcessLog.getOrderProcess() : 0;
                RequestProcessLog requestProcessLogSave = new RequestProcessLog(request.getRequestId(), Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL, null, userDTOResponse.getUserId(), requestLog.getRequestApprove(), requestProcessLogOrderMax  + 1);
                requestProcessLogRepository.save(requestProcessLogSave);
                //gửi notification
                NotificationDTO notificationDTO = createNotification(request.getProcess().getProcessType(), Constant.PROCESS_CONFIG_TYPE.SEND, request, userDTOResponse, requestLog.getRequestApprove());
                ResponseMessage responseMessage = CallOtherService.createNotification(notificationDTO, httpServletRequest);
                if(responseMessage.isSuccess()){

                }else{
                    throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.NOTIFICATION_ERROR, null, CommonUtil.getLocale()));
                }
                //send mail
                if(userReceiveRequest.getEmail() != null){
                    String content = getContentOrSubjectEmail(request, userDTOResponse, "send", 1, httpServletRequest);
                    String subject = getContentOrSubjectEmail(request, userDTOResponse, "send", 2, httpServletRequest);
                    sendMail(subject, content, new String[]{userReceiveRequest.getEmail()}, null, null);
                }
            });
        }else{
            throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.PROCESS, null, CommonUtil.getLocale()) + " " + messageSource.getMessage(Constant.NOT_EXIST, null, CommonUtil.getLocale()));
        }
        List<RequestLog> requestLogs = new ArrayList<>();
        requestLogs.add(new RequestLog(request.getRequestId(), userDTOResponse.getUserId(), request.getRequestStatus(), Constant.REQUEST_ACTION.SEND));
        requestLogService.save(requestLogs);
        requestRepository.save(request);
    }

    private Date increaseDate(){
        Date originalDate = new Date();
        long originalMilliseconds = originalDate.getTime();
        long newMilliseconds = originalMilliseconds + 1000;
        return new Date(newMilliseconds);
    }

    public void cancelRequest(List<String> requestCode, UserDTOResponse userDTOResponse, HttpServletRequest httpRequest){
        List<Request> requests = requestRepository.findByRequestCodeIn(requestCode);
        if(requests != null && requests.size() > 0){
            Double dayOffLastYear = 0D;
            Double dayOffCurrentYear = 0D;
            for(Request request : requests){
                if(request.getRequestStatus().equals(Constant.REQUEST_STATUS.APPROVED)){
                    if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)){
                        RequestWorktimeContent requestWorktimeContent = request.getRequestWorktimeContents().stream().findFirst().orElse(null);
                        if(requestWorktimeContent != null && requestWorktimeContent.getRequestDayOffType().equals(Constant.DAY_OFF_TYPE.ON_LEAVE)){
                            dayOffLastYear += request.getDayOffLastYear();
                            dayOffCurrentYear += request.getDayOffCurrentYear();
                        }
                    }else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.WORK_CONFIRMATION)){
                        RequestWorktimeContent requestWorktimeContent = request.getRequestWorktimeContents().stream().findFirst().orElse(null);
                        if(requestWorktimeContent != null){
                            TimekeepingDTO timekeepingDTO = new TimekeepingDTO(request.getCreatedBy(), requestWorktimeContent.getRequestWorkTimeDate(), requestWorktimeContent.getRequestWorkTimeCheckinOld(), requestWorktimeContent.getRequestWorkTimeCheckoutOld());
                            CallOtherService.backTimekeeping(timekeepingDTO, httpRequest);
                        }
                    }
                }
                request.setRequestStatus(Constant.REQUEST_STATUS.CANCEL);
                request.setUpdatedBy(userDTOResponse.getUserId());
                List<RequestLog> requestLogs = new ArrayList<>();
                processStepLogService.delete(request.getRequestId());
                requestLogs.add(new RequestLog(request.getRequestId(), userDTOResponse.getUserId(), request.getRequestStatus(), Constant.REQUEST_ACTION.CANCEL));
                requestLogService.save(requestLogs);
            }
            if(dayOffLastYear > 0 || dayOffCurrentYear > 0){
                //nếu là sau tháng 3 thì không update ngày nghỉ phép của năm cũ nữa
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MONTH, Calendar.MARCH); // Set the month to March
                calendar.set(Calendar.DAY_OF_MONTH, 1); // Set the day to 1
                if(ValueUtil.compareDate(new Date(), calendar.getTime()) > 0){
                    dayOffLastYear = 0D;
                }
                AvailableOffDTO availableOffDTO = new AvailableOffDTO(userDTOResponse.getUserId(), dayOffLastYear, dayOffCurrentYear);
                CallOtherService.updateAvailableDayOff(availableOffDTO, httpRequest);
            }

            requestRepository.saveAll(requests);
        }else{
            throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.REQUEST, null, CommonUtil.getLocale()) + " " + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));
        }
    }

    @Override
    @Transactional
    public void approve(RequestApproveDTO requestApproveDTO, String id, UserDTOResponse userDTOResponse, HttpServletRequest httpRequest) {
        List<Integer> list = new ArrayList<>();
        list.add(Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL);
        list.add(Constant.PROCESS_STEP_LOG_STATUS.DENIED);
        ProcessStepLog processStepLog = processStepLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(id, list, userDTOResponse.getUserId());
        if(processStepLog != null){
            //nếu là bị từ chối thì lưu 1 bản ghi log lại với trạng thái là đã phê duyệt
            if(processStepLog.getProcessStepStatus().equals(Constant.PROCESS_STEP_LOG_STATUS.DENIED)){
                //lưu log requestProcess
                int orderMaxRequestProcessLog = requestProcessLogRepository.getRequestProcessLogMax(id).getOrderProcess();
                RequestProcessLog requestProcessLog = new RequestProcessLog(id, Constant.PROCESS_STEP_LOG_STATUS.APPROVED, null, userDTOResponse.getUserId(), userDTOResponse.getUserId(), orderMaxRequestProcessLog + 1);
                requestProcessLogRepository.save(requestProcessLog);
            }
            processStepLog.setProcessStepStatus(Constant.PROCESS_STEP_LOG_STATUS.APPROVED);
            processStepLog.setHandleDate(new Date());
            processStepLogRepository.save(processStepLog);
        }
        //update log
        RequestProcessLog requestProcessLogOld = requestProcessLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(id, list, userDTOResponse.getUserId());
        if(requestProcessLogOld != null){
            requestProcessLogOld.setRequestProcessLogStatus(Constant.PROCESS_STEP_LOG_STATUS.APPROVED);
            requestProcessLogOld.setHandleDate(new Date());
            requestProcessLogRepository.save(requestProcessLogOld);
        }
        Request request = requestRepository.findById(id).get();
        List<Integer> lstStatus = new ArrayList<>();
        lstStatus.add(Constant.PROCESS_CONFIG_TYPE.APPROVAL);
        lstStatus.add(Constant.PROCESS_CONFIG_TYPE.PERFORM);
        List<ProcessConfig> processConfigNext = processConfigRepository.getProcessConfigNext(processStepLog.getProcessConfigId(), lstStatus);
        if(processConfigNext.size() > 0 && processConfigNext.get(0).getProcessStepNext() != null){
            if(processConfigNext.size() == 1 && !processConfigNext.get(0).getProcessStepNext().getProcessStepCode().equals(Constant.PROCESS_STEP_CONFIG.END)){
                if(requestApproveDTO.getReceiveUserId() == null || requestApproveDTO.getReceiveUserId().isEmpty()){
                    throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.SELETED_USER_APPROVER, null, CommonUtil.getLocale()));
                }

                //tạo bản ghi để đi bước tiếp theo
                int orderMax = processStepLogRepository.getProcessLogMax(id).getOrderProcess();
                int orderMaxRequestProcessLog = requestProcessLogRepository.getRequestProcessLogMax(id).getOrderProcess();
                processConfigNext.forEach(x -> {
                    ProcessStepLog processNew = new ProcessStepLog(request.getRequestId(), Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL, x.getProcessConfigId(), orderMax + 1, userDTOResponse.getUserId(), requestApproveDTO.getReceiveUserId(), increaseDate());
                    processStepLogRepository.save(processNew);

                    //lưu log requestProcess
                    RequestProcessLog requestProcessLog = new RequestProcessLog(request.getRequestId(), Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL, null, userDTOResponse.getUserId(), requestApproveDTO.getReceiveUserId(), orderMaxRequestProcessLog + 1);
                    requestProcessLogRepository.save(requestProcessLog);

                    //gửi notification
                    NotificationDTO notificationDTO = createNotification(request.getProcess().getProcessType(), Constant.PROCESS_CONFIG_TYPE.APPROVAL, request, userDTOResponse, requestApproveDTO.getReceiveUserId());
                    ResponseMessage responseMessage = CallOtherService.createNotification(notificationDTO, httpRequest);
                    if(responseMessage.isSuccess()){

                    }else{
                        throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.NOTIFICATION_ERROR, null, CommonUtil.getLocale()));
                    }
                    //send mail
                    UserDTOResponse userReceiveRequest = CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, requestApproveDTO.getReceiveUserId(), httpRequest);
                    if(userReceiveRequest != null && userReceiveRequest.getEmail() != null){
                        String content = getContentOrSubjectEmail(request, userDTOResponse, "approval", 1, httpRequest);
                        String subject = getContentOrSubjectEmail(request, userDTOResponse, "approval", 2, httpRequest);
                        sendMail(subject, content, new String[]{userReceiveRequest.getEmail()}, null, null);
                    }
                });
                // nếu phê duyệt thì sẽ xoá hết các process log cũ đã phê duyệt trước đó
//                List<String> processStepConfigDescendant = processConfigRepository.getAllDescendantProcessConfig(processConfig.get().getProcessStep().getProcessStepId());
//                List<ProcessStepLog> processStepLogDescendant = processStepLogRepository.getProcessStepLogByProcessConfig(processStepConfigDescendant, Constant.PROCESS_STEP_LOG_STATUS.APPROVED, request.getRequestId());
//                if(processStepLogDescendant.size() > 0){
//                    processStepLogDescendant.forEach(x -> {
//                        x.setIsDelete(Constant.IS_DELETE);
//                    });
//                    processStepLogRepository.saveAll(processStepLogDescendant);
//                }
            } else if(processConfigNext.size() == 1 && processConfigNext.get(0).getProcessStepNext().getProcessStepCode().equals(Constant.PROCESS_STEP_CONFIG.END)){
                //xác nhận công
                if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.WORK_CONFIRMATION)){
                    RequestDTOResponse requestDTOResponse = new RequestDTOResponse();
                    BeanUtils.copyProperties(request, requestDTOResponse);
                    if(request.getRequestWorktimeContents() != null
                            && request.getRequestWorktimeContents().size() > 0) {
                        Set<RequestWorktimeContentDTO> worktimeContentDTOS = new HashSet<>();
                        request.getRequestWorktimeContents().forEach(x -> {
                            RequestWorktimeContentDTO requestWorkConfirmContentDTO = new RequestWorktimeContentDTO();
                            CoppyObject.copyNonNullProperties(x, requestWorkConfirmContentDTO);
                            worktimeContentDTOS.add(requestWorkConfirmContentDTO);
                        });
                        requestDTOResponse.setRequestWorktimeContents(worktimeContentDTOS);
                    }
                    RequestDTOResponse requestDTOResponseReturn = CallOtherService.updateTimekeeping(Constant.PATH.UPDATE_WORKTIME_TIMEKEEPING, requestDTOResponse, httpRequest);
                    if(requestDTOResponseReturn != null){
                        request.setRequestStatus(Constant.REQUEST_STATUS.APPROVED);
                        requestRepository.save(request);
                    }else{
                        throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.UPDATE_WORKTIME_CONFIRM_ERROR, null, CommonUtil.getLocale()));
                    }
                    //xác nhận remote
                } else if(request.getProcess().getProcessType().equals(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE)){
                    //call sang worktime lấy số ngày nghỉ phép còn lại của user
                    //update vào content số ngày nghỉ được tính lương
                    RequestDTOResponse requestDTOResponse = new RequestDTOResponse();
                    BeanUtils.copyProperties(request, requestDTOResponse);
                    if(request.getRequestWorktimeContents() != null
                            && request.getRequestWorktimeContents().size() > 0) {
                        Set<RequestWorktimeContentDTO> worktimeContentDTOS = new HashSet<>();
                        request.getRequestWorktimeContents().forEach(x -> {
                            RequestWorktimeContentDTO requestWorkConfirmContentDTO = new RequestWorktimeContentDTO();
                            CoppyObject.copyNonNullProperties(x, requestWorkConfirmContentDTO);
                            worktimeContentDTOS.add(requestWorkConfirmContentDTO);
                        });
                        requestDTOResponse.setRequestWorktimeContents(worktimeContentDTOS);
                    }
                    RequestDTOResponse requestDTOResponseReturn = CallOtherService.updateTimekeeping(Constant.PATH.UPDATE_WORKTIME_TIMEKEEPING, requestDTOResponse, httpRequest);
                    if(requestDTOResponseReturn != null){
                        request.getRequestWorktimeContents().forEach(x -> {
                            RequestWorktimeContentDTO requestWorktimeContentDTO = requestDTOResponseReturn.getRequestWorktimeContents()
                                    .stream()
                                    .filter(y -> y.getRequestWorktimeContentId().equals(y.getRequestWorktimeContentId()))
                                    .findFirst()
                                    .orElse(new RequestWorktimeContentDTO());
                            if(requestWorktimeContentDTO.getCountDaySalary() != null){
                                x.setCountDaySalary(requestWorktimeContentDTO.getCountDaySalary());
                            }
                        });
                        request.setDayOffLastYear(requestDTOResponseReturn.getDayOffLastYear());
                        request.setDayOffCurrentYear(requestDTOResponseReturn.getDayOffCurrentYear());
                        request.setRequestStatus(Constant.REQUEST_STATUS.APPROVED);
                        requestRepository.save(request);
                    }else{
                        throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.UPDATE_WORKTIME_CONFIRM_ERROR, null, CommonUtil.getLocale()));
                    }
                }else{
                    request.setRequestStatus(Constant.REQUEST_STATUS.APPROVED);
                    requestRepository.save(request);
                }
                //gửi notification
                NotificationDTO notificationDTO = createNotification(request.getProcess().getProcessType(), Constant.PROCESS_CONFIG_TYPE.APPROVAL, request, userDTOResponse, request.getCreatedBy());
                ResponseMessage responseMessage = CallOtherService.createNotification(notificationDTO, httpRequest);
                if(responseMessage.isSuccess()){

                }else{
                    throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.NOTIFICATION_ERROR, null, CommonUtil.getLocale()));
                }
                //send mail
                UserDTOResponse userReceiveRequest = CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, request.getCreatedBy(), httpRequest);
                if(userReceiveRequest != null && userReceiveRequest.getEmail() != null){
                    String content = getContentOrSubjectEmail(request, userDTOResponse, "approval", 1, httpRequest);
                    String subject = getContentOrSubjectEmail(request, userDTOResponse, "approval", 2, httpRequest);
                    sendMail(subject, content, new String[]{userReceiveRequest.getEmail()}, null, null);
                }
            } else{
                throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.PROCESS_CONFIG, null, CommonUtil.getLocale()) + " " + messageSource.getMessage("error", null, CommonUtil.getLocale()));
            }

        }
        List<ProcessStepLog> processStepLogDenied = processStepLogRepository.getProcessLogByStatus(userDTOResponse.getUserId(), id, Constant.PROCESS_STEP_LOG_STATUS.DENIED);
        if(processStepLogDenied != null && processStepLogDenied.size() > 0){
            processStepLogDenied.forEach(x -> {
                x.setIsDelete(Constant.IS_DELETE);
            });
            processStepLogRepository.saveAll(processStepLogDenied);
        }
    }

    @Override
    @Transactional
    public void completePayment(String id, RequestPaymentDTO requestPaymentDTO, UserDTOResponse userDTOResponse, HttpServletRequest httpRequest) {
        Request request = requestRepository.findById(id).get();
        List<Integer> list = new ArrayList<>();
        list.add(Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL);

        NotificationDTO notificationDTO;
        ResponseMessage responseMessage;
        switch (request.getProcess().getProcessType()){
            case Constant.PROCESS_TYPE.BUY_EQUIPMENT:
                RequestProcessLog requestProcessLogBuyEquipment = requestProcessLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(request.getRequestId(), list, userDTOResponse.getUserId());
                requestProcessLogBuyEquipment.setRequestProcessLogStatus(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED);
                requestProcessLogBuyEquipment.setHandleDate(new Date());
                requestProcessLogRepository.save(requestProcessLogBuyEquipment);

                ProcessStepLog processStepLogBuyEquipment = processStepLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(request.getRequestId(), list, userDTOResponse.getUserId());
                processStepLogBuyEquipment.setProcessStepStatus(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED);
                processStepLogBuyEquipment.setHandleDate(new Date());
                processStepLogRepository.save(processStepLogBuyEquipment);

                List<ProcessStepLog> lstProcessStepLogBuyEquipment = processStepLogRepository.getAllProcessLogByStatus(request.getRequestId(), Constant.PROCESS_STEP_LOG_STATUS.APPROVED);
                lstProcessStepLogBuyEquipment.forEach(x -> {
                    x.setProcessStepStatus(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED);
                });
                processStepLogRepository.saveAll(lstProcessStepLogBuyEquipment);

                request.setRequestStatus(Constant.REQUEST_STATUS.COMPLETED);
                requestRepository.save(request);
                break;
            case Constant.PROCESS_TYPE.PAYMENT_REQUEST:
                List<RequestPayContent> requestPayContent = requestPayContentRepository.findByRequestId(request.getRequestId());
                Double totalRequest = 0.0;
                if(requestPayContent.size() > 0){
                    Double moneyAfterPayment = requestPaymentDTO.getReceivedMoney();
                    if(requestPayContent.get(0).getReceivedMoney() != null){
                        moneyAfterPayment += requestPayContent.get(0).getReceivedMoney();
                    }
                    for (RequestPayContent requestPay : requestPayContent){
                        totalRequest += requestPay.getRemainingMoney();
                        requestPay.setReceivedMoney(moneyAfterPayment);
                    }
                    if(totalRequest.equals(requestPayContent.get(0).getReceivedMoney())){
                        RequestProcessLog requestProcessLogPayment = requestProcessLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(request.getRequestId(), list, userDTOResponse.getUserId());
                        requestProcessLogPayment.setRequestProcessLogStatus(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED);
                        requestProcessLogPayment.setHandleDate(new Date());
                        requestProcessLogRepository.save(requestProcessLogPayment);

                        ProcessStepLog processStepLogPayment = processStepLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(request.getRequestId(), list, userDTOResponse.getUserId());
                        processStepLogPayment.setProcessStepStatus(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED);
                        processStepLogPayment.setHandleDate(new Date());
                        processStepLogRepository.save(processStepLogPayment);

                        List<ProcessStepLog> processStepLogsPayment = processStepLogRepository.getAllProcessLogByStatus(request.getRequestId(), Constant.PROCESS_STEP_LOG_STATUS.APPROVED);
                        processStepLogsPayment.forEach(x -> {
                            x.setProcessStepStatus(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED);
                        });
                        processStepLogRepository.saveAll(processStepLogsPayment);

                        request.setRequestStatus(Constant.REQUEST_STATUS.COMPLETED);
                        requestRepository.save(request);
                    }
                    requestPayContentRepository.saveAll(requestPayContent);
                }
                break;
            default:
                break;
        }
        //gửi notification
        notificationDTO = createNotification(request.getProcess().getProcessType(), Constant.PROCESS_CONFIG_TYPE.PERFORM, request, userDTOResponse, request.getCreatedBy());
        responseMessage = CallOtherService.createNotification(notificationDTO, httpRequest);
        if(responseMessage.isSuccess()){

        }else{
            throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.NOTIFICATION_ERROR, null, CommonUtil.getLocale()));
        }
        //send mail
        UserDTOResponse userReceiveRequest = CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, request.getCreatedBy(), httpRequest);
        if(userReceiveRequest != null && userReceiveRequest.getEmail() != null){
            String content = getContentOrSubjectEmail(request, userDTOResponse, "perform", 1, httpRequest);
            String subject = getContentOrSubjectEmail(request, userDTOResponse, "perform", 2, httpRequest);
            sendMail(subject, content, new String[]{userReceiveRequest.getEmail()}, null, null);
        }
    }

    @Override
    @Transactional
    public void completeRecruitment(String id, RequestRecruitmentPerformDTO requestRecruitmentPerformDTO, UserDTOResponse userDTOResponse, HttpServletRequest httpRequest) {
        Request request = requestRepository.findById(id).get();
        RequestRecruitmentContent requestRecruitmentContent = requestRecruitmentContentRepository.findByRequest_RequestId(request.getRequestId());
        BeanUtils.copyProperties(requestRecruitmentPerformDTO, requestRecruitmentContent);
        requestRecruitmentContentRepository.save(requestRecruitmentContent);

        saveAndDeleteUserRecruitment(requestRecruitmentPerformDTO.getRecruitmentUserContent(), id, httpRequest, id);

        attachmentService.saveAndDeleteAttachment(requestRecruitmentPerformDTO.getListAttachmentResult(), request.getRequestId(), httpRequest, id, Constant.ATTACHMENT_TYPE.RECRUITMENT_RESULT);

        Request saveRequest = requestRepository.save(request);
        for (RequestRecruitmentUserContentDTO recruitmentUserContentDTO : requestRecruitmentPerformDTO.getRecruitmentUserContent()){
            RequestRecruitmentUserContent requestRecruitmentUserContent = new RequestRecruitmentUserContent();
            CoppyObject.copyNonNullProperties(recruitmentUserContentDTO, requestRecruitmentUserContent);
            requestRecruitmentUserContent.setRequest(saveRequest);
            RequestRecruitmentUserContent saveContent = requestRecruitmentUserContentRepository.save(requestRecruitmentUserContent);
            attachmentService.saveAndDeleteAttachment(recruitmentUserContentDTO.getListAttachment(), saveContent.getRequestRecruitmentUserContentId(), httpRequest, saveContent.getRequestRecruitmentUserContentId(), Constant.ATTACHMENT_TYPE.RECRUITMENT);
        }

        if(requestRecruitmentPerformDTO.getIsComplete().equals(Constant.COMPLETE)){
            List<Integer> list = new ArrayList<>();
            list.add(Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL);

            RequestProcessLog requestProcessLog = requestProcessLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(request.getRequestId(), list, userDTOResponse.getUserId());
            requestProcessLog.setRequestProcessLogStatus(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED);
            requestProcessLog.setHandleDate(new Date());
            requestProcessLogRepository.save(requestProcessLog);

            ProcessStepLog processStepLog = processStepLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(request.getRequestId(), list, userDTOResponse.getUserId());
            processStepLog.setProcessStepStatus(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED);
            processStepLog.setHandleDate(new Date());
            processStepLogRepository.save(processStepLog);

            List<ProcessStepLog> processStepLogsRecruitment= processStepLogRepository.getAllProcessLogByStatus(request.getRequestId(), Constant.PROCESS_STEP_LOG_STATUS.APPROVED);
            processStepLogsRecruitment.forEach(x -> {
                x.setProcessStepStatus(Constant.PROCESS_STEP_LOG_STATUS.ACCOMPLISHED);
            });
            processStepLogRepository.saveAll(processStepLogsRecruitment);

            if(requestRecruitmentPerformDTO.getRecruitmentUserContent().size() == request.getRequestRecruitmentContents().stream().findFirst().get().getQuantity()){
                request.setRequestStatus(Constant.REQUEST_STATUS.COMPLETED);
            }

            //gửi notification
            NotificationDTO notificationDTO = createNotification(request.getProcess().getProcessType(), Constant.PROCESS_CONFIG_TYPE.PERFORM, request, userDTOResponse, request.getCreatedBy());
            ResponseMessage responseMessage = CallOtherService.createNotification(notificationDTO, httpRequest);
            if(responseMessage.isSuccess()){

            }else{
                throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.NOTIFICATION_ERROR, null, CommonUtil.getLocale()));
            }
            //send mail
            UserDTOResponse userReceiveRequest = CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, request.getCreatedBy(), httpRequest);
            if(userReceiveRequest != null && userReceiveRequest.getEmail() != null){
                String content = getContentOrSubjectEmail(request, userDTOResponse, "perform", 1, httpRequest);
                String subject = getContentOrSubjectEmail(request, userDTOResponse, "perform", 2, httpRequest);
                sendMail(subject, content, new String[]{userReceiveRequest.getEmail()}, null, null);
            }
        }
    }


    private void saveAndDeleteUserRecruitment(List<RequestRecruitmentUserContentDTO> requestRecruitmentUserContentDTOS, String id, HttpServletRequest request, String requestId) {
        List<RequestRecruitmentUserContentDTO> lstRecruitmentUserContentSave = new ArrayList<>();
        List<String> lstIdRecruitmentUserContentDelete = new ArrayList<>();
        List<RequestRecruitmentUserContent> requestRecruitmentUserContents = requestRecruitmentUserContentRepository.findAllByRequest_RequestId(id);
        lstIdRecruitmentUserContentDelete = requestRecruitmentUserContents.stream().map(RequestRecruitmentUserContent::getRequestRecruitmentUserContentId).collect(Collectors.toList());
        for (RequestRecruitmentUserContentDTO recruitmentUserContentDTO : requestRecruitmentUserContentDTOS){
            if(recruitmentUserContentDTO.getRequestRecruitmentUserContentId() != null && lstIdRecruitmentUserContentDelete.size() > 0 && lstIdRecruitmentUserContentDelete.contains(recruitmentUserContentDTO.getRequestRecruitmentUserContentId())){
                lstIdRecruitmentUserContentDelete.remove(recruitmentUserContentDTO.getRequestRecruitmentUserContentId());
            }else if(recruitmentUserContentDTO.getRequestRecruitmentUserContentId() == null){
                lstRecruitmentUserContentSave.add(recruitmentUserContentDTO);
            }
        }

        if(lstIdRecruitmentUserContentDelete.size() > 0 || lstRecruitmentUserContentSave.size() > 0){
            List<RequestRecruitmentUserContent> listRequestRecruitmentUserContent = new ArrayList<>();
            if(lstRecruitmentUserContentSave.size() > 0){
                for(RequestRecruitmentUserContentDTO recruitmentUserContentDTO : lstRecruitmentUserContentSave){
                    RequestRecruitmentUserContent requestRecruitmentUserContent = new RequestRecruitmentUserContent();
                    CoppyObject.copyNonNullProperties(recruitmentUserContentDTO, requestRecruitmentUserContent);
                    requestRecruitmentUserContent.setRequestRecruitmentUserStatus(Constant.ACTIVE);
                    listRequestRecruitmentUserContent.add(requestRecruitmentUserContent);
                }
            }
            if(lstIdRecruitmentUserContentDelete.size() > 0){
                List<RequestRecruitmentUserContent> listRequestRecruitmentUserContentDelete = requestRecruitmentUserContentRepository.findAllByRequestRecruitmentUserContentIdIn(lstIdRecruitmentUserContentDelete);
                for(RequestRecruitmentUserContent requestRecruitmentUserContent : listRequestRecruitmentUserContentDelete){
                    requestRecruitmentUserContent.setRequestRecruitmentUserStatus(Constant.INACTIVE);
                    listRequestRecruitmentUserContent.add(requestRecruitmentUserContent);
                }
            }
            requestRecruitmentUserContentRepository.saveAll(listRequestRecruitmentUserContent);
        }
    }

    @Override
    @Transactional
    public void refuse(String id, HttpServletRequest httpRequest, RequestRejectDTO requestRejectDTO) {
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
        Request request = requestRepository.findById(id).get();
        List<Integer> lstStatusDenied = new ArrayList<>();
        List<Integer> lstStatusApprove = new ArrayList<>();
        lstStatusDenied.add(Constant.PROCESS_STEP_LOG_STATUS.DENIED);
        lstStatusApprove.add(Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL);
        lstStatusApprove.add(Constant.PROCESS_STEP_LOG_STATUS.APPROVED);
        ProcessStepLog processStepLogsDenied = processStepLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(id, lstStatusDenied, userDTOResponse.getUserId());
        ProcessStepLog processStepLogsApprove = processStepLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(id, lstStatusApprove, userDTOResponse.getUserId());
        if(processStepLogsDenied != null){
            processStepLogsDenied.setIsDelete(Constant.IS_DELETE);
            processStepLogRepository.save(processStepLogsDenied);
        }
        if(processStepLogsApprove != null){
            processStepLogsApprove.setProcessStepStatus(Constant.PROCESS_STEP_LOG_STATUS.REFUSE);
            processStepLogsApprove.setHandleDate(new Date());
            processStepLogRepository.save(processStepLogsApprove);
            ProcessConfig processConfig = processConfigRepository.findById(processStepLogsApprove.getProcessConfigId()).get();
            //lấy config process từ chối từ step hiện tại
            ProcessConfig processConfigRefuse = processConfigRepository.getProcessStepByProcessStepType(processConfig.getProcessStepNext().getProcessStepId(), Constant.PROCESS_CONFIG_TYPE.REFUSE);
            //nếu từ chối về người tạo
            String userSend;
            Integer orderMax = processStepLogRepository.getProcessLogMax(id).getOrderProcess();
            if(processConfigRefuse.getProcessStepNext().getProcessStepCode().equals(Constant.PROCESS_STEP_CONFIG.START)){
                ProcessStepLog processStepLogStart = new ProcessStepLog(id, Constant.PROCESS_STEP_LOG_STATUS.DENIED, processConfigRefuse.getProcessConfigId(), orderMax + 1, userDTOResponse.getUserId(), request.getCreatedBy());
                userSend = request.getCreatedBy();
                request.setRequestStatus(Constant.REQUEST_STATUS.REFUSE);
                requestRepository.save(request);
                processStepLogRepository.save(processStepLogStart);
            }else{
                List<Integer> lstStatus = new ArrayList<>();
                lstStatus.add(Constant.PROCESS_CONFIG_TYPE.SEND);
                lstStatus.add(Constant.PROCESS_CONFIG_TYPE.APPROVAL);
                ProcessConfig processConfigPrevious = processConfigRepository.getProcessConfigPrevious(processConfigRefuse.getProcessConfigId(), lstStatus);
                ProcessStepLog processStepLog = processStepLogRepository.getProcessLogMaxAndProcessConfig(request.getRequestId(), processConfigPrevious.getProcessConfigId());
                ProcessStepLog processStepLogApprove = new ProcessStepLog(id, Constant.PROCESS_STEP_LOG_STATUS.DENIED, processConfigRefuse.getProcessConfigId(), orderMax + 1, userDTOResponse.getUserId(), processStepLog.getSendUserId());
                userSend = processStepLog.getSendUserId();
                processStepLogRepository.save(processStepLogApprove);
            }
            int orderMaxRequestProcessLog = requestProcessLogRepository.getRequestProcessLogMax(id).getOrderProcess();
            RequestProcessLog requestProcessLogSave = new RequestProcessLog(id, Constant.PROCESS_STEP_LOG_STATUS.REFUSE, requestRejectDTO.getRejectReason(), userDTOResponse.getUserId(), userSend, orderMaxRequestProcessLog + 1);
            requestProcessLogRepository.save(requestProcessLogSave);
            RequestProcessLog requestProcessLog = requestProcessLogRepository.getProcessByReceiveUserIdAndOrderMaxAndStatus(id, lstStatusApprove, userDTOResponse.getUserId());

            //nếu bản ghi có trạng thái là chờ phê duyệt thì update trạng thái thành từ chối, đã phê duyệt thì sinh bản ghi mới
            if(requestProcessLog != null && requestProcessLog.getRequestProcessLogStatus().equals(Constant.PROCESS_STEP_LOG_STATUS.WAITING_APPROVAL)) {
                requestProcessLogRepository.delete(requestProcessLog);
            }

            //gửi notification
            NotificationDTO notificationDTO = createNotification(request.getProcess().getProcessType(), Constant.PROCESS_CONFIG_TYPE.REFUSE, request, userDTOResponse, userSend);
            ResponseMessage responseMessage = CallOtherService.createNotification(notificationDTO, httpRequest);
            if(responseMessage.isSuccess()){

            }else{
                throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.NOTIFICATION_ERROR, null, CommonUtil.getLocale()));
            }
            //send mail
            UserDTOResponse userReceiveRequest = CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, userSend, httpRequest);
            if(userReceiveRequest != null && userReceiveRequest.getEmail() != null){
                String content = getContentOrSubjectEmail(request, userDTOResponse, "refuse", 1, httpRequest);
                String subject = getContentOrSubjectEmail(request, userDTOResponse, "refuse", 2, httpRequest);
                sendMail(subject, content, new String[]{userReceiveRequest.getEmail()}, null, null);
            }
        }else{
            throw new RuntimeExceptionCustom(messageSource.getMessage(Constant.REQUEST, null, CommonUtil.getLocale()) + " " + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));

        }
    }

    @Override
    public List<RequestDTOResponse> getAll(String keyword, List<Integer> status, Date fromDate, Date toDate, Date worktimeFromDate, Date worktimeToDate, List<Integer> processType, HttpServletRequest httpRequest,String userId) {
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
        List<Request> requests = requestRepository.getAllRequestWithoutPaging(processType, userDTOResponse.getUserId(),
                "%" + keyword + "%", status, fromDate, toDate,  worktimeFromDate, worktimeToDate);
        List<RequestDTOResponse> requestDTOResponses = new ArrayList<>();
        requests.forEach(x->{
            RequestDTOResponse requestDTOResponse = new RequestDTOResponse();
            BeanUtils.copyProperties(x,requestDTOResponse);

            List<RequestWorktimeContentDTO> requestWorktimeContentDTO;
            requestWorktimeContentDTO = CommonUtil.mapList(CollectionUtils.arrayToList(x.getRequestWorktimeContents().toArray()), RequestWorktimeContentDTO.class);
            requestDTOResponse.setRequestWorktimeContents(new HashSet<>(requestWorktimeContentDTO));

            requestDTOResponses.add(requestDTOResponse);
        });
        return requestDTOResponses;
    }

    @Override
    public List<RequestDTOResponse> getAllSchedule(String keyword, List<Integer> status, Date fromDate, Date toDate, Date worktimeFromDate, Date worktimeToDate, List<Integer> processType, String userId) {
        List<Request> requests = requestRepository.getAllRequestWithoutPaging(processType, userId,
                "%" + keyword + "%", status, fromDate, toDate,  worktimeFromDate, worktimeToDate);
        List<RequestDTOResponse> requestDTOResponses = new ArrayList<>();
        requests.forEach(x->{
            RequestDTOResponse requestDTOResponse = new RequestDTOResponse();
            BeanUtils.copyProperties(x,requestDTOResponse);

            List<RequestWorktimeContentDTO> requestWorktimeContentDTO;
            requestWorktimeContentDTO = CommonUtil.mapList(CollectionUtils.arrayToList(x.getRequestWorktimeContents().toArray()), RequestWorktimeContentDTO.class);
            requestDTOResponse.setRequestWorktimeContents(new HashSet<>(requestWorktimeContentDTO));

            requestDTOResponses.add(requestDTOResponse);
        });
        return requestDTOResponses;
    }

    @Async
    public void sendMail(String subject, String mailContent, String[] listAccount,
                         byte[][] attachmentDatas, String[] attachmentNames){
        SSLEmail.sendEmail(subject, mailContent, listAccount, attachmentDatas, attachmentNames);
    }

    @Override
    public void updateWorktimeContents(Set<RequestWorktimeContentDTO> worktimeContentDTOS, UserDTOResponse userDTOResponse, HttpServletRequest request){
        List<RequestWorktimeContent> requestWorktimeContents = new ArrayList<>();
        worktimeContentDTOS.forEach(x -> {
            RequestWorktimeContent requestWorktimeContent = new RequestWorktimeContent();
            BeanUtils.copyProperties(x, requestWorktimeContent);
            requestWorktimeContents.add(requestWorktimeContent);
        });
        requestWorktimeContentRepository.saveAll(requestWorktimeContents);
    }
}
