package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.controller.CallOtherService;
import com.blameo.erpservice.dto.request.ProcessConfigDTO;
import com.blameo.erpservice.dto.response.ProcessConfigDTOResponse;
import com.blameo.erpservice.model.*;
import com.blameo.erpservice.modeluser.RoleDTOResponse;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.modeluser.UserManagerDTOResponse;
import com.blameo.erpservice.repository.ProcessConfigRepository;
import com.blameo.erpservice.repository.ProcessStepLogRepository;
import com.blameo.erpservice.repository.RequestLogRepository;
import com.blameo.erpservice.repository.RequestRepository;
import com.blameo.erpservice.service.ProcessConfigService;
import com.blameo.erpservice.utils.CommonUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
@Transactional
public class ProcessConfigServiceImpl implements ProcessConfigService {

    @Autowired
    ProcessConfigRepository processConfigRepository;

    @Autowired
    ProcessStepLogRepository processStepLogRepository;

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    RequestLogRepository requestLogRepository;

    @Override
    public ProcessConfigDTOResponse save(ProcessConfig processConfig) {
        ProcessConfigDTOResponse processConfigDTOResponse = new ProcessConfigDTOResponse();
        ProcessConfig save = processConfigRepository.save(processConfig);
        BeanUtils.copyProperties(save, processConfigDTOResponse);
        return processConfigDTOResponse;
    }

    @Override
    public ProcessConfig convertProcessConfig(ProcessConfigDTO processConfigDTO, Map<String, ProcessStep> stringMap){
        ProcessConfig processConfig = new ProcessConfig();
        processConfig.setProcessStepType(processConfigDTO.getProcessStepType());
        processConfig.setProcessStep(stringMap.get(processConfigDTO.getProcessStepId()));
        processConfig.setProcessStepNext(stringMap.get(processConfigDTO.getProcessStepNext()));
        return processConfig;
    }

    @Override
    public List<ProcessConfigDTOResponse> getAll(String id, HttpServletRequest httpServletRequest) {
        List<ProcessConfigDTOResponse> processConfigDTOResponses = new ArrayList<>();
        List<ProcessConfig> result = processConfigRepository.getAll(id);
        Set<String> roleList = new HashSet<>();
        result.forEach(processConfig ->{
            roleList.add(processConfig.getProcessStep().getRoleId());
            if(processConfig.getProcessStepNext() != null){
                roleList.add(processConfig.getProcessStepNext().getRoleId());
            }
        });
        List<RoleDTOResponse> roleDTOResponses = CallOtherService.getListFromIdService(RoleDTOResponse.class, Constant.PATH.GET_ROLE_BY_LIST_ROLE_ID, roleList, httpServletRequest);
        if (result.size() > 0) {
            for (ProcessConfig processConfig : result){
                ProcessConfigDTOResponse processConfigDTOResponse = new ProcessConfigDTOResponse();
                BeanUtils.copyProperties(processConfig, processConfigDTOResponse);

                RoleDTOResponse roleDTO = roleDTOResponses.stream().filter(x -> x.getRoleId().equals(processConfig.getProcessStep().getRoleId())).findFirst().orElse(null);
                processConfigDTOResponse.getProcessStep().setRole(roleDTO);

                if(processConfigDTOResponse.getProcessStepNext() != null){
                    RoleDTOResponse roleDTONext = roleDTOResponses.stream().filter(x -> x.getRoleId().equals(processConfig.getProcessStepNext().getRoleId())).findFirst().orElse(null);
                    processConfigDTOResponse.getProcessStepNext().setRole(roleDTONext);
                }
                processConfigDTOResponses.add(processConfigDTOResponse);
            }

        }
        return processConfigDTOResponses;
    }

    @Override
    public void deleteAll(String processId) {
        List<ProcessConfig> result = processConfigRepository.getAll(processId);
        if (result != null && result.size() > 0) {
            processConfigRepository.deleteAll(result);
        }
    }

    @Override
    public List<UserDTOResponse> getListUserProcessConfigByRequestId(String requestId, Integer processType, HttpServletRequest httpServletRequest, Boolean isCreated) {
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpServletRequest);
        List<UserDTOResponse> userDTOResponses = new ArrayList<>();
        String roleId = "";
        // nếu người gửi request là người tạo thì lấy theo log, nếu người gửi không phải người tạo thì lấy theo luồng
        if(isCreated){
            List<ProcessConfig> processConfigs = processConfigRepository.getFirstConfigByProcessType(processType, Constant.PROCESS_STEP_CONFIG.START, Constant.PROCESS_CONFIG_TYPE.SEND);
            if(processConfigs != null && processConfigs.size() > 0){
                roleId =  processConfigs.get(0).getProcessStepNext().getRoleId();
            }
        }else{
            Optional<Request> request = requestRepository.findById(requestId);
            if(request.isPresent()){
                ProcessStepLog processStepLog = processStepLogRepository.getProcessStepStatusByRequestIdAndReceiveUserId(userDTOResponse.getUserId(), requestId);
                if(processStepLog != null){
                    List<Integer> lstStatus = new ArrayList<>();
                    lstStatus.add(Constant.PROCESS_CONFIG_TYPE.APPROVAL);
                    List<ProcessConfig> processConfigNext = processConfigRepository.getProcessConfigNext(processStepLog.getProcessConfigId(), lstStatus);
                    if(processConfigNext != null && processConfigNext.size() > 0){
                        roleId =  processConfigNext.get(0).getProcessStepNext().getRoleId();
                    }
                }
            }

        }
        if(roleId != null && !roleId.equals("")){
            userDTOResponses = CallOtherService.getListUserByRoleIdFromIdService(roleId, httpServletRequest);
        }else{
            UserDTOResponse detailUser = CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, userDTOResponse.getUserId(),  httpServletRequest);
            Set<UserManagerDTOResponse> setUserDTO = detailUser.getManagers();
            if(setUserDTO != null && setUserDTO.size() > 0){
                for (UserManagerDTOResponse userManagerDTOResponse : setUserDTO){
                    UserDTOResponse userDTO = new UserDTOResponse();
                    userDTO.setUserId(userManagerDTOResponse.getUserId());
                    userDTO.setFullName(userManagerDTOResponse.getFullName());
                    userDTOResponses.add(userDTO);
                }
            }
        }
        return userDTOResponses;
    }
}
