package com.blameo.erpservice.service;
import com.blameo.erpservice.dto.response.BuyEquipmentContentResponseDTO;
import com.blameo.erpservice.dto.response.RequestBuyEquipmentContentResponseDTO;
import com.blameo.erpservice.dto.response.RequestLogDTOResponse;
import com.blameo.erpservice.model.RequestLog;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
public interface RequestAssetService {
    List<BuyEquipmentContentResponseDTO> getAll(String requestId, HttpServletRequest httpRequest);
}
