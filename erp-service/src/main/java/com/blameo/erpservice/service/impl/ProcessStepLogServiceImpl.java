package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.controller.CallOtherService;
import com.blameo.erpservice.dto.response.ProcessStepLogDTOResponse;
import com.blameo.erpservice.dto.response.RequestDTOResponse;
import com.blameo.erpservice.model.ProcessStepLog;
import com.blameo.erpservice.model.Request;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.repository.ProcessStepLogRepository;
import com.blameo.erpservice.repository.RequestRepository;
import com.blameo.erpservice.service.ProcessStepLogService;
import com.blameo.erpservice.utils.CommonUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProcessStepLogServiceImpl implements ProcessStepLogService {

    @Autowired
    ProcessStepLogRepository processStepLogRepository;

    @Autowired
    RequestRepository requestRepository;

    @Override
    public ProcessStepLogDTOResponse save(ProcessStepLog processStepLog) {
        ProcessStepLog saved = processStepLogRepository.save(processStepLog);
        ProcessStepLogDTOResponse processStepLogDTOResponse = new ProcessStepLogDTOResponse();
        BeanUtils.copyProperties(saved, processStepLogDTOResponse);
        return processStepLogDTOResponse;
    }

    @Override
    public List<ProcessStepLogDTOResponse> delete(String requestId) {
        List<ProcessStepLog> processStepLog = processStepLogRepository.getByRequestId(requestId);
        if(processStepLog != null && processStepLog.size() > 0){
            processStepLog.forEach(x -> {
                x.setIsDelete(1);
            });
        }
        List<ProcessStepLog> saved = processStepLogRepository.saveAll(processStepLog);
        List<ProcessStepLogDTOResponse> processStepLogDTOResponse = new ArrayList<>();
        processStepLogDTOResponse = CommonUtil.mapList(saved, ProcessStepLogDTOResponse.class);
        return processStepLogDTOResponse;
    }

    @Override
    public List<ProcessStepLogDTOResponse> getAll(String requestId, HttpServletRequest request) {
        List<ProcessStepLog> processStepLog = processStepLogRepository.getAllByProcessRequestLogId(requestId);
        Set<String> listUserId = processStepLog.stream().map(ProcessStepLog::getReceiveUserId).collect(Collectors.toSet());

        List<UserDTOResponse> listUser = CallOtherService.getListFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_MULTIPLE_ID, listUserId, request);

        List<ProcessStepLogDTOResponse> res = CommonUtil.mapList(processStepLog, ProcessStepLogDTOResponse.class);
        res.stream().forEach(dto -> dto.setHandlerUser(
                listUser.stream()
                        .filter(userDTOResponse -> userDTOResponse.getUserId().equals(dto.getReceiveUserId()))
                        .findAny()
                        .orElse(new UserDTOResponse())
        ));
        return res;
    }
}
