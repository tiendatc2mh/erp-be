package com.blameo.erpservice.service.impl;
import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.controller.CallOtherService;
import com.blameo.erpservice.dto.response.ProcessStepLogDTOResponse;
import com.blameo.erpservice.dto.response.RequestProcessLogDTOResponse;
import com.blameo.erpservice.model.ProcessStepLog;
import com.blameo.erpservice.model.RequestProcessLog;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.repository.RequestProcessLogRepository;
import com.blameo.erpservice.service.RequestProcessLogService;
import com.blameo.erpservice.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RequestProcessLogServiceImpl implements RequestProcessLogService {

    @Autowired
    RequestProcessLogRepository requestProcessLogRepository;

    @Override
    public List<RequestProcessLogDTOResponse> getAll(String requestId, HttpServletRequest request) {
        List<RequestProcessLog> requestLogs = requestProcessLogRepository.findAllByRequestIdOrderByOrderProcessDesc(requestId);
        Set<String> listUserId = new HashSet<>();
        for (RequestProcessLog requestProcessLog : requestLogs){
            if(requestProcessLog.getRequestProcessLogStatus().equals(Constant.PROCESS_STEP_LOG_STATUS.REFUSE)){
                listUserId.add(requestProcessLog.getSendUserId());
            }else{
                listUserId.add(requestProcessLog.getReceiveUserId());
            }
        }
        List<UserDTOResponse> listUser = CallOtherService.getListFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_MULTIPLE_ID, listUserId, request);
        List<RequestProcessLogDTOResponse> res = CommonUtil.mapList(requestLogs, RequestProcessLogDTOResponse.class);
        for (RequestProcessLogDTOResponse requestProcessLogDTOResponse : res){
            if(requestProcessLogDTOResponse.getRequestProcessLogStatus().equals(Constant.PROCESS_STEP_LOG_STATUS.REFUSE)){
                UserDTOResponse userDTOResponse = listUser.stream().filter(t -> t.getUserId().equals(requestProcessLogDTOResponse.getSendUserId())).findAny().orElse(new UserDTOResponse());
                requestProcessLogDTOResponse.setHandlerUser(userDTOResponse);
            }else{
                UserDTOResponse userDTOResponse = listUser.stream().filter(t -> t.getUserId().equals(requestProcessLogDTOResponse.getReceiveUserId())).findAny().orElse(new UserDTOResponse());
                requestProcessLogDTOResponse.setHandlerUser(userDTOResponse);
            }
        }
        return res;
    }
}
