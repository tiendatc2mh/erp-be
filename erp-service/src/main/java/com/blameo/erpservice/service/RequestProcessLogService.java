package com.blameo.erpservice.service;
import com.blameo.erpservice.dto.response.RequestProcessLogDTOResponse;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
public interface RequestProcessLogService {

    List<RequestProcessLogDTOResponse> getAll(String requestId, HttpServletRequest request);
}
