package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.controller.CallOtherService;
import com.blameo.erpservice.dto.response.BuyEquipmentContentResponseDTO;
import com.blameo.erpservice.dto.response.ProcessConfigDTOResponse;
import com.blameo.erpservice.model.Request;
import com.blameo.erpservice.model.RequestPayContent;
import com.blameo.erpservice.modeluser.AssetDTOResponse;
import com.blameo.erpservice.modeluser.AssetTypeDTOResponse;
import com.blameo.erpservice.repository.RequestLogRepository;
import com.blameo.erpservice.repository.RequestRepository;
import com.blameo.erpservice.service.RequestAssetService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class RequestAssetServiceImpl implements RequestAssetService {

    @Autowired
    RequestLogRepository requestLogRepository;

    @Autowired
    RequestRepository requestRepository;

    @Override
    public List<BuyEquipmentContentResponseDTO> getAll(String requestId, HttpServletRequest httpRequest) {
        Optional<Request> request = requestRepository.findById(requestId);
        if(request.isPresent()){
            List<BuyEquipmentContentResponseDTO> data = new ArrayList<>();
            List<AssetDTOResponse> assetDTOResponses = CallOtherService.getListAssetFromIdService(requestId, httpRequest);
            Set<RequestPayContent> requestPayContents = request.get().getRequestPayContents();
            Set<String> assetTypeId = requestPayContents.stream().map(RequestPayContent::getAssetTypeId).collect(Collectors.toSet());
            List<AssetTypeDTOResponse> assetDTOs;
            List<BuyEquipmentContentResponseDTO> buyEquipmentContentResponseDTOS = new ArrayList<>();
            if(assetTypeId.size() > 0) {
                assetDTOs = CallOtherService.getListFromIdService(AssetTypeDTOResponse.class, Constant.PATH.GET_ASSET_TYPE_MULTIPLE_ID, assetTypeId, httpRequest);
                //thêm asset name
                if(assetDTOs != null && assetDTOs.size() > 0){
                    for (RequestPayContent requestPayContent : requestPayContents) {
                        BuyEquipmentContentResponseDTO buyEquipmentContentResponseDTO = new BuyEquipmentContentResponseDTO();
                        BeanUtils.copyProperties(requestPayContent, buyEquipmentContentResponseDTO);
                        AssetTypeDTOResponse assetTypeDTOResponse = assetDTOs.stream().filter(y -> y.getAssetTypeId().equals(requestPayContent.getAssetTypeId())).findFirst().orElse(new AssetTypeDTOResponse());
                        buyEquipmentContentResponseDTO.setAssetType(assetTypeDTOResponse);
                        buyEquipmentContentResponseDTOS.add(buyEquipmentContentResponseDTO);
                    }
                }
                //tạo ra buy equipment theo quantity
                for (BuyEquipmentContentResponseDTO buyEquipmentContentResponseDTO : buyEquipmentContentResponseDTOS){
                    for (int i=0;i<buyEquipmentContentResponseDTO.getQuantity(); i++){
                        BuyEquipmentContentResponseDTO buyEquipmentContentResponseDTOItem = new BuyEquipmentContentResponseDTO();
                        BeanUtils.copyProperties(buyEquipmentContentResponseDTO, buyEquipmentContentResponseDTOItem);
                        data.add(buyEquipmentContentResponseDTOItem);
                    }
                }
                if(assetDTOResponses != null && assetDTOResponses.size() > 0){
                    for (AssetDTOResponse assetDTOResponse : assetDTOResponses){
                        OptionalInt indexOpt = IntStream.range(0, data.size())
                                .filter(i -> assetDTOResponse.getAssetType().getAssetTypeId().equals(data.get(i).getAssetTypeId()) && data.get(i).getAsset() == null)
                                .findFirst();
                        if(indexOpt.isPresent()){
                            data.get(indexOpt.getAsInt()).setAsset(assetDTOResponse);
                        }
                    }
                }
            }
            return data;
        }
        return null;
    }
}
