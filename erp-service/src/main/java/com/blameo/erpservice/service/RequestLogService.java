package com.blameo.erpservice.service;
import com.blameo.erpservice.dto.request.RequestDTO;
import com.blameo.erpservice.dto.response.RequestDTOResponse;
import com.blameo.erpservice.dto.response.RequestLogDTOResponse;
import com.blameo.erpservice.model.RequestLog;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Component
public interface RequestLogService {
    List<RequestLogDTOResponse> save(List<RequestLog> requestLog);

    List<RequestLogDTOResponse> getAll(String requestId, HttpServletRequest httpRequest);
}
