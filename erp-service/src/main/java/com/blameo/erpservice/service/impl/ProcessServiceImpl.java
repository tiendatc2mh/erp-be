package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.controller.CallOtherService;
import com.blameo.erpservice.dto.request.ProcessConfigDTO;
import com.blameo.erpservice.dto.request.ProcessStepDTO;
import com.blameo.erpservice.dto.request.ProcessDTO;
import com.blameo.erpservice.dto.response.ProcessConfigDTOResponse;
import com.blameo.erpservice.dto.response.ProcessStepDTOResponse;
import com.blameo.erpservice.dto.response.ProcessDTOResponse;
import com.blameo.erpservice.model.Process;
import com.blameo.erpservice.model.ProcessConfig;
import com.blameo.erpservice.model.ProcessStep;
import com.blameo.erpservice.model.Request;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.repository.ProcessRepository;
import com.blameo.erpservice.repository.RequestRepository;
import com.blameo.erpservice.service.ProcessConfigService;
import com.blameo.erpservice.service.ProcessStepService;
import com.blameo.erpservice.service.ProcessService;
import com.blameo.erpservice.utils.CommonUtil;
import com.blameo.erpservice.utils.CoppyObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
@Transactional
public class ProcessServiceImpl implements ProcessService {

    @Autowired
    ProcessStepService processStepService;

    @Autowired
    ProcessConfigService processConfigService;

    @Autowired
    ProcessRepository processRepository;

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    MessageSource messageSource;

    @Override
    public ProcessDTOResponse save(Map<String, Object> processTypeDTOData, UserDTOResponse userDTOResponse) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, ProcessStep> stringMap = new HashMap<>();
        ProcessDTO processDTO = mapper.convertValue(processTypeDTOData.get("processType"), ProcessDTO.class);
        List<Object> lstConfig = Arrays.asList(((Collection<?>) processTypeDTOData.get("processConfig")).toArray());
        Process process = new Process();
        CoppyObject.copyNonNullProperties(processDTO, process);
        process.setProcessStatus(Constant.ACTIVE);
        process.setCreatedBy(userDTOResponse.getUserId());
        if(process.getIsDefault().equals(Constant.IS_DEFAULT)){
            Process resultDefault = processRepository.getByProcessTypeAndAndIsDefaultEqualsAndProcessStatusEquals(process.getProcessType(), Constant.IS_DEFAULT, Constant.ACTIVE);
            if(resultDefault != null){
                resultDefault.setIsDefault(Constant.NON_DEFAULT);
                processRepository.save(resultDefault);
            }
        }
        Process save = processRepository.save(process);
        ProcessDTOResponse processDTOResponse = new ProcessDTOResponse();
        BeanUtils.copyProperties(save, processDTOResponse);
        ProcessStepDTO processStepDTO = new ProcessStepDTO();
        ProcessConfigDTO processConfigDTO = new ProcessConfigDTO();
        ProcessConfig processConfig = new ProcessConfig();
        List<ProcessConfigDTO> processConfigDTOS = new ArrayList<>();
        List<String> processStepCodeLst = new ArrayList<>();
        if(lstConfig.size() > 0){
            for (Object o : lstConfig){
                ProcessStep processStep = new ProcessStep();
                ProcessStepDTOResponse resultStep = new ProcessStepDTOResponse();
                processStepDTO = mapper.convertValue(o, ProcessStepDTO.class);
                if(!processStepCodeLst.contains(processStepDTO.getProcessStepCode())){
                    processStepCodeLst.add(processStepDTO.getProcessStepCode());
                    String processStepId = processStepDTO.getProcessStepId();
                    resultStep = processStepService.save(processStepDTO, userDTOResponse);
                    BeanUtils.copyProperties(resultStep, processStep);
                    stringMap.put(processStepId, processStep);
                }
            }
            for (Object o : lstConfig){
                processConfigDTO = mapper.convertValue(o, ProcessConfigDTO.class);
                processConfig = processConfigService.convertProcessConfig(processConfigDTO, stringMap);
                processConfig.setProcess(save);
                processConfigService.save(processConfig);
            }
        }
        return processDTOResponse;
    }

    @Override
    public ProcessDTOResponse updateDefault(String id, UserDTOResponse userDTOResponse, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        ProcessDTOResponse processDTOResponse = new ProcessDTOResponse();
        Optional<Process> result = processRepository.findById(id);
        if(result.isPresent()){
            Process resultDefault = processRepository.getByProcessTypeAndAndIsDefaultEqualsAndProcessStatusEquals(result.get().getProcessType(), Constant.IS_DEFAULT, Constant.ACTIVE);
            //check có request nào đang sử dụng process này ở trạng thái đã phê duyệt và chờ phê duyệt không nếu có thì k cho sửa
            List<Request> requestsByProcessId = requestRepository.findByProcess_ProcessIdAndRequestStatus(resultDefault.getProcessId());
            if(requestsByProcessId != null && requestsByProcessId.size() > 0){
                throw new RuntimeException("Process " + messageSource.getMessage("inUse", null, locale));
            }
            if(resultDefault != null && !resultDefault.getProcessId().equals(result.get().getProcessId())){
                resultDefault.setIsDefault(Constant.NON_DEFAULT);
                processRepository.save(resultDefault);
            }
            result.get().setIsDefault(result.get().getIsDefault() == Constant.IS_DEFAULT ? Constant.NON_DEFAULT : Constant.IS_DEFAULT);
            result.get().setUpdatedBy(userDTOResponse.getUserId());
            Process save = processRepository.save(result.get());
            BeanUtils.copyProperties(save, processDTOResponse);
        }
        return processDTOResponse;
    }

    @Override
    public ProcessDTOResponse update(String id, Map<String, Object> processTypeDTOData, UserDTOResponse userDTOResponse, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        Optional<Process> result = processRepository.findById(id);
        ObjectMapper mapper = new ObjectMapper();
        Map<String, ProcessStep> stringMap = new HashMap<>();
        ProcessDTO processDTO = mapper.convertValue(processTypeDTOData.get("processType"), ProcessDTO.class);
        List<Object> lstConfig = Arrays.asList(((Collection<?>) processTypeDTOData.get("processConfig")).toArray());
        Process process = result.get();
        if(processDTO.getProcessName() != null){
            process.setProcessName(processDTO.getProcessName());
        }
        if(processDTO.getProcessDescription() != null){
            process.setProcessDescription(processDTO.getProcessDescription());
        }
        process.setUpdatedBy(userDTOResponse.getUserId());
        if(!process.getIsDefault().equals(processDTO.getIsDefault())){
            if(processDTO.getIsDefault().equals(Constant.IS_DEFAULT)){
                Process resultDefault = processRepository.getByProcessTypeAndAndIsDefaultEqualsAndProcessStatusEquals(process.getProcessType(), Constant.IS_DEFAULT, Constant.ACTIVE);
                //check có request nào đang sử dụng process này ở trạng thái đã phê duyệt và chờ phê duyệt không nếu có thì k cho sửa
                List<Request> requestsByProcessId = requestRepository.findByProcess_ProcessIdAndRequestStatus(resultDefault.getProcessId());
                if(requestsByProcessId != null && requestsByProcessId.size() > 0){
                    throw new RuntimeException("Process " + messageSource.getMessage("inUse", null, locale));
                }
                resultDefault.setIsDefault(Constant.NON_DEFAULT);
                processRepository.save(resultDefault);
                process.setIsDefault(Constant.IS_DEFAULT);
            }else{
                //check có request nào đang sử dụng process này ở trạng thái đã phê duyệt và chờ phê duyệt không nếu có thì k cho sửa
                List<Request> requestsByProcessId = requestRepository.findByProcess_ProcessIdAndRequestStatus(process.getProcessId());
                if(requestsByProcessId != null && requestsByProcessId.size() > 0){
                    throw new RuntimeException("Process " + messageSource.getMessage("inUse", null, locale));
                }
                process.setIsDefault(Constant.NON_DEFAULT);
            }
        }
        Process save = processRepository.save(process);
        List<ProcessConfigDTOResponse> processConfigs = processConfigService.getAll(save.getProcessId(), request);
        Set<String> lstStepId = new HashSet<>();
        if(processConfigs.size() > 0) {
            processConfigs.forEach(x -> {
                lstStepId.add(x.getProcessStep().getProcessStepId());
            });
        }
        processConfigService.deleteAll(save.getProcessId());
        processStepService.deleteAll(lstStepId);
        ProcessDTOResponse processDTOResponse = new ProcessDTOResponse();
        BeanUtils.copyProperties(save, processDTOResponse);
        ProcessStepDTO processStepDTO = new ProcessStepDTO();
        ProcessConfigDTO processConfigDTO = new ProcessConfigDTO();
        ProcessConfig processConfig = new ProcessConfig();
        List<ProcessConfigDTO> processConfigDTOS = new ArrayList<>();
        List<String> processStepCodeLst = new ArrayList<>();
        if(lstConfig.size() > 0){
            for (Object o : lstConfig){
                ProcessStep processStep = new ProcessStep();
                ProcessStepDTOResponse resultStep = new ProcessStepDTOResponse();
                processStepDTO = mapper.convertValue(o, ProcessStepDTO.class);
                if(!processStepCodeLst.contains(processStepDTO.getProcessStepCode())) {
                    processStepCodeLst.add(processStepDTO.getProcessStepCode());
                    String processStepId = processStepDTO.getProcessStepId();
                    resultStep = processStepService.save(processStepDTO, userDTOResponse);
                    BeanUtils.copyProperties(resultStep, processStep);
                    stringMap.put(processStepId, processStep);
                }
            }
            for (Object o : lstConfig){
                processConfigDTO = mapper.convertValue(o, ProcessConfigDTO.class);
                processConfig = processConfigService.convertProcessConfig(processConfigDTO, stringMap);
                processConfig.setProcess(save);
                processConfigService.save(processConfig);
            }
        }
        return processDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer processType, Integer isDefault) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<Process> page = processRepository.findAll(paging, "%" + keyword + "%", processType, isDefault);
        if (page.hasContent()) {
            List<ProcessDTOResponse> positionDTOS = CommonUtil.mapList(page.getContent(), ProcessDTOResponse.class);
            output.put("data", positionDTOS);
            output.put("total", page.getTotalElements());
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
        }
        output.put("page", pageNo);
        output.put("size", pageSize);
        return output;
    }

    @Override
    public List<ProcessDTOResponse> getAllProcessType() {
        return CommonUtil.mapList(processRepository.getAll(), ProcessDTOResponse.class);
    }

    @Override
    public ProcessDTOResponse get(Integer id, HttpServletRequest httpServletRequest) {
        ProcessDTOResponse processDTOResponse = new ProcessDTOResponse();
        Process result = processRepository.getByProcessTypeAndAndIsDefaultEqualsAndProcessStatusEquals(id, Constant.IS_DEFAULT, Constant.ACTIVE);
        if (result != null) {
            BeanUtils.copyProperties(result, processDTOResponse);
            processDTOResponse.setCreatedByName(CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, result.getCreatedBy(), httpServletRequest).getFullName());
            processDTOResponse.setUpdatedByName(CallOtherService.getDetailFromIdService(UserDTOResponse.class, Constant.PATH.GET_USER_BY_USER_ID, result.getUpdatedBy(),  httpServletRequest).getFullName());
        }
        return processDTOResponse;
    }

    @Override
    public Map<String, Object> getAll(Integer id, HttpServletRequest httpServletRequest) {
        Map<String, Object> data = new HashMap<>();
        ProcessDTOResponse processDTOResponse = this.get(id, httpServletRequest);
        List<ProcessConfigDTOResponse> processConfigDTOResponses = processConfigService.getAll(processDTOResponse.getProcessId(), httpServletRequest);
        data.put("processType", processDTOResponse);
        data.put("processConfig", processConfigDTOResponses);
        return data;
    }

    public ProcessDTOResponse getProcessByProcessType(Integer id, HttpServletRequest httpServletRequest) {
        ProcessDTOResponse processDTOResponse = this.get(id, httpServletRequest);
        return processDTOResponse;
    }

    @Override
    public Map<String, Object> getAllProcess(String id, HttpServletRequest httpServletRequest) {
        Map<String, Object> data = new HashMap<>();
        ProcessDTOResponse processDTOResponse = new ProcessDTOResponse();
        Optional<Process> processType = processRepository.findById(id);
        processType.ifPresent(type -> BeanUtils.copyProperties(type, processDTOResponse));
        List<ProcessConfigDTOResponse> processConfigDTOResponses = processConfigService.getAll(processDTOResponse.getProcessId(), httpServletRequest);
        data.put("processType", processDTOResponse);
        data.put("processConfig", processConfigDTOResponses);
        return data;
    }

    @Override
    public void delete(String id, UserDTOResponse userDTOResponse) {
        Locale locale = CommonUtil.getLocale();
        Optional<Process> result = processRepository.findById(id);
        if (result.isPresent()) {
            Process process = result.get();
            process.setProcessStatus(-1);
            process.setUpdatedBy(userDTOResponse.getUserId());
            processRepository.save(process);
        } else
            throw new RuntimeException("Process " + messageSource.getMessage("notExist", null, locale));
    }
}
