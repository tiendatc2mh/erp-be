package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.model.RequestPayContent;
import com.blameo.erpservice.model.RequestWorktimeContent;
import com.blameo.erpservice.repository.RequestWorktimeContentRepository;
import com.blameo.erpservice.service.RequestWorktimeContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class RequestWorktimeContentServiceImpl implements RequestWorktimeContentService {

    @Autowired
    RequestWorktimeContentRepository requestWorktimeContentRepository;

    @Autowired
    MessageSource messageSource;

    @Override
    public void deleteList(List<String> id) {
        List<RequestWorktimeContent> result = requestWorktimeContentRepository.findAllByRequestWorktimeContentIdIn(id);
        if (result != null && result.size() > 0) {
            result.forEach(x -> {
                x.setRequestWorktimeContentStatus(Constant.REQUEST_CONTENT_STATUS.DELETE);
            });
            requestWorktimeContentRepository.saveAll(result);
        }
    }
}
