package com.blameo.erpservice.service;
import com.blameo.erpservice.dto.request.ProcessStepDTO;
import com.blameo.erpservice.dto.response.ProcessStepLogDTOResponse;
import com.blameo.erpservice.dto.response.RequestLogDTOResponse;
import com.blameo.erpservice.model.ProcessStepLog;
import com.blameo.erpservice.model.RequestLog;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
public interface ProcessStepLogService {
    ProcessStepLogDTOResponse save(ProcessStepLog processStepLog);
    List<ProcessStepLogDTOResponse> delete(String requestId);

    List<ProcessStepLogDTOResponse> getAll(String requestId, HttpServletRequest request);
}
