package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.model.RequestPayContent;
import com.blameo.erpservice.repository.RequestPayContentRepository;
import com.blameo.erpservice.service.RequestPayContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class RequestPayContentServiceImpl implements RequestPayContentService {

    @Autowired
    RequestPayContentRepository requestPayContentRepository;

    @Autowired
    MessageSource messageSource;

    @Override
    public void deleteList(List<String> id) {
        List<RequestPayContent> result = requestPayContentRepository.findByRequestPayContentIdInAndRequestPayContentStatusEquals(id,1);
        if(result.size() > 0){
            delete(result);
        }
    }
    @Transactional
    public void delete(List<RequestPayContent> list){
        list.forEach(x -> {
            x.setRequestPayContentStatus(Constant.REQUEST_CONTENT_STATUS.DELETE);
        });
        requestPayContentRepository.saveAll(list);
    }
}
