package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.dto.request.AttachmentDTO;
import com.blameo.erpservice.model.Attachment;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.repository.AttachmentRepository;
import com.blameo.erpservice.service.AttachmentService;
import com.blameo.erpservice.utils.CommonUtil;
import com.blameo.erpservice.utils.CoppyObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AttachmentServiceImpl implements AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;

    @Override
    public void saveAndDeleteAttachment(List<AttachmentDTO> lstSave, String objectId, HttpServletRequest request, String requestId, Integer attachType) {
        List<AttachmentDTO> lstAttachmentSave = new ArrayList<>();
        List<Attachment> attachments = attachmentRepository.getByObjectIdAndAttachmentStatus(objectId);
        List<String> lstStrAttachmentDelete = attachments.stream().map(Attachment::getAttachmentId).collect(Collectors.toList());
        if(lstSave != null && lstSave.size() > 0){
            for (AttachmentDTO attachmentDTO : lstSave){
                //nếu có trong list thì bỏ id đó ra khỏi list xoá
                if(attachmentDTO.getAttachmentId() != null && lstStrAttachmentDelete.contains(attachmentDTO.getAttachmentId())){
                    lstStrAttachmentDelete.remove(attachmentDTO.getAttachmentId());
                }else if(attachmentDTO.getAttachmentId() == null){
                    lstAttachmentSave.add(attachmentDTO);
                }
            }
        }
        if(lstAttachmentSave.size() > 0 || lstStrAttachmentDelete.size() > 0){
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            List<Attachment> attachmentChange = new ArrayList<>();
            if(lstSave != null && lstSave.size() > 0){
                for(AttachmentDTO attachmentDTO : lstSave){
                    Attachment attachment = new Attachment();
                    CoppyObject.copyNonNullProperties(attachmentDTO, attachment);
                    attachment.setObjectId(requestId);
                    attachment.setCreatedBy(userDTOResponse.getUserId());
                    attachment.setAttachmentStatus(Constant.ACTIVE);
                    if(attachType.equals(Constant.PROCESS_TYPE.PAYMENT_REQUEST)){
                        attachment.setAttachmentType(attachmentDTO.getAttachmentType());
                    }else if(attachType.equals(Constant.ATTACHMENT_TYPE.RECRUITMENT)){
                        attachment.setAttachmentType(Constant.ATTACHMENT_TYPE.RECRUITMENT);
                    }else if(attachType.equals(Constant.ATTACHMENT_TYPE.RECRUITMENT_RESULT)){
                        attachment.setAttachmentType(Constant.ATTACHMENT_TYPE.RECRUITMENT_RESULT);
                    }
                    attachmentChange.add(attachment);
                }
            }
            if(lstStrAttachmentDelete != null && lstStrAttachmentDelete.size() > 0){
                List<Attachment> attachmentList = attachmentRepository.findAllByAttachmentIdIn(lstStrAttachmentDelete);
                for(Attachment attachment : attachmentList){
                    attachment.setAttachmentStatus(Constant.INACTIVE);
                    attachmentChange.add(attachment);
                }
            }
            attachmentRepository.saveAll(attachmentChange);
        }
    }
}
