package com.blameo.erpservice.service;
import com.blameo.erpservice.dto.request.*;
import com.blameo.erpservice.dto.response.RequestDTOResponse;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public interface RequestService {
    Object save(RequestDTO requestDTO, HttpServletRequest request);
    RequestDTOResponse get(String id, HttpServletRequest request);
    List<RequestDTOResponse> getRequestAdvance(HttpServletRequest request);
    RequestDTOResponse update(RequestDTO requestDTO, String id, HttpServletRequest request);
    void delete(String id, UserDTOResponse userDTOResponse);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, HttpServletRequest httpRequest,
                                Integer requestWorkTimeType, Integer dayOffType, Integer requestOtType, Integer payType,
                                String position, Boolean isCreated, Integer processType, String requestCode, String keyword,
                                List<Integer> status, String username, Date fromDate, Date toDate, Boolean isAll);
    InputStreamResource export(HttpServletRequest httpRequest,
                                Integer requestWorkTimeType, Integer dayOffType, Integer requestOtType, Integer payType,
                                String position, Boolean isCreated, Integer processType, String requestCode, String keyword,
                                List<Integer> status, String username, Date fromDate, Date toDate, Boolean isAll) throws IOException;
    void approve(RequestApproveDTO requestApproveDTO, String id, UserDTOResponse userDTOResponse, HttpServletRequest httpRequest);
    void completePayment(String id, RequestPaymentDTO requestPaymentDTO, UserDTOResponse userDTOResponse, HttpServletRequest httpRequest);
    void completeRecruitment(String id, RequestRecruitmentPerformDTO requestRecruitmentPerformDTO, UserDTOResponse userDTOResponse, HttpServletRequest httpRequest);
    void send(String id, HttpServletRequest httpRequest);
    void refuse(String id, HttpServletRequest httpRequest, RequestRejectDTO requestRejectDTO);
    List<RequestDTOResponse> getAll(String keyword, List<Integer> status, Date fromDate, Date toDate, Date worktimeFromDate,
                                    Date worktimeToDate, List<Integer> processType, HttpServletRequest httpRequest,String userId);
    List<RequestDTOResponse> getAllSchedule(String keyword, List<Integer> status, Date fromDate, Date toDate, Date worktimeFromDate,
                                    Date worktimeToDate, List<Integer> processType, String userId);
    void sendMail(String subject, String mailContent, String[] listAccount,
                  byte[][] attachmentDatas, String[] attachmentNames);

    void updateWorktimeContents(Set<RequestWorktimeContentDTO> worktimeContentDTOS, UserDTOResponse userDTOResponse, HttpServletRequest request);
    void cancelRequest(List<String> requestCode, UserDTOResponse userDTOResponse, HttpServletRequest request);
}
