package com.blameo.erpservice.service.impl;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.model.RequestRecruitmentContent;
import com.blameo.erpservice.model.RequestWorktimeContent;
import com.blameo.erpservice.repository.RequestRecruitmentContentRepository;
import com.blameo.erpservice.repository.RequestWorktimeContentRepository;
import com.blameo.erpservice.service.RequestRecruitmentContentService;
import com.blameo.erpservice.service.RequestWorktimeContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RequestRecruitmentContentServiceImpl implements RequestRecruitmentContentService {

    @Autowired
    RequestRecruitmentContentRepository requestRecruitmentContentRepository;

    @Autowired
    MessageSource messageSource;

    @Override
    public void deleteList(List<String> id) {
        List<RequestRecruitmentContent> result = requestRecruitmentContentRepository.findAllByRequestRecruitmentContentIdIn(id);
        if (result != null && result.size() > 0) {
            result.forEach(x -> {
                x.setRequestRecruitmentContentStatus(Constant.REQUEST_CONTENT_STATUS.DELETE);
            });
            requestRecruitmentContentRepository.saveAll(result);
        }
    }
}
