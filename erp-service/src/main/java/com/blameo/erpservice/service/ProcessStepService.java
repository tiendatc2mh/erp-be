package com.blameo.erpservice.service;

import com.blameo.erpservice.dto.request.ProcessStepDTO;
import com.blameo.erpservice.dto.response.ProcessStepDTOResponse;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public interface ProcessStepService {
    ProcessStepDTOResponse save(ProcessStepDTO processStepDTO, UserDTOResponse userDTOResponse);

    void deleteAll(Set<String> lstId);
}
