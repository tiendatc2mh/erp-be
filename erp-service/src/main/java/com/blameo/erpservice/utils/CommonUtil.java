package com.blameo.erpservice.utils;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/13/2023, Monday
 **/
public class CommonUtil {
    private static Logger logger = Logger.getLogger(CommonUtil.class);
    private ResourceLoader resourceLoader;

    public CommonUtil(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public static Locale getLocale(){
        Locale locale = new Locale("en");
        locale = LocaleContextHolder.getLocale().equals(locale)? new Locale("en") : new Locale("vi");
        return locale;
    }
    public static String genUUID(){
        return UUID.randomUUID().toString();
    }

    public static String dateToString(Date date){
        SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");
        String stringDate= DateFor.format(date);
        return stringDate;
    }

    public static String dateToStringRequest(Date date){
        SimpleDateFormat DateFor = new SimpleDateFormat("yyyy/MM/dd/");
        String stringDate= DateFor.format(date);
        return stringDate;
    }

    public static String dateToStringFull(Date date){
        SimpleDateFormat DateFor = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        String stringDate= DateFor.format(date);
        return stringDate;
    }

    public String getResourcePath(String fileName) throws  IOException {
        Resource resource = resourceLoader.getResource("classpath:" + fileName);
        Path path = resource.getFile().toPath();
        return path.toString();
    }

    public static UserDTOResponse getUserRequest(HttpServletRequest request) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            byte[] bytes = objectMapper.readValue(request.getHeader(Constant.KEY_HEADER_USER),byte[].class);
            return CommonUtil.stringToEntity(new String(bytes, StandardCharsets.UTF_8),UserDTOResponse.class);
        }catch (Exception e){
            logger.error("Convert user fail");
            return null;
        }
    }

    public static  <T> T stringToEntity(String string,Class<T> entity) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(string, entity);
        } catch (JsonProcessingException jsonProcessingException) {
            throw new RuntimeException("Convert json string to entity fail");
        }
    }

    public static Boolean validateFieldOfObj(Class<?> obj, String nameField) {
        Field[] fields = obj.getDeclaredFields();
        for (Field e : fields) {
            if (!e.getName().equals("SEQUENCE_NAME")
                    && !e.getName().equals("serialVersionUID")
                    && !e.getName().equals("id")
                    && e.getName().equals(nameField)) return true;
        }
        return false;
    }

    public static  <S, T> T mapObject(S source, Class<T> targetClass) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
        return modelMapper.map(source,targetClass);
    }

    public static  <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
        return source
                .stream()
                .map(element -> modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }

    public static Map<String, String> StringToMapByComma(String s) {
        Map<String, String> map = new HashMap<>();
        Arrays.stream(s.split(",")).map(String::trim).collect(Collectors.toList()).forEach(s1 -> {
            List<String> keyValues = Arrays.stream(s1.split(":")).map(String::trim).collect(Collectors.toList());
            map.put(keyValues.get(0), keyValues.get(1));
        });
        return map;
    }

}
