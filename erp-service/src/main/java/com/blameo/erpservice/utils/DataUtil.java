package com.blameo.erpservice.utils;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DataUtil {

    public static String makeLikeQuery(String s) {
        if (StringUtils.isEmpty(s)) return null;
        s =
            s
                .trim()
                .toLowerCase()
                .replace("!", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "!")
                .replace("%", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "%")
                .replace("_", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "_");
        return  s ;
    }

    public static List<Long> parseMultiValueRequestParam(String param) {
        List<Long> listParam = new ArrayList<>();
        try{
            if(param.contains(",")){
                String[] list = param.split(",");
                for (String s: list) {
                    listParam.add(Long.valueOf(s));
                }
            }else {
                listParam.add(Long.valueOf(param));
            }
        }catch (Exception e){
            return new ArrayList<>();
        }
        return listParam;
    }

    public static long calculateDaysBetween(Date dateStart, Date dateEnd) {
        if(dateStart.before(dateEnd)){
            LocalDate localdateStart = dateStart.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate localdateEnd = dateEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            return ChronoUnit.DAYS.between(localdateStart, localdateEnd);
        }
        return 0;
    }

    public static int countWeekendDays(Date dateStart, Date dateEnd) {
        int totalWeekendDays = 0;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateStart);

        while (!calendar.getTime().after(dateEnd)) {
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
                totalWeekendDays++;
            }
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        return totalWeekendDays;
    }

    public static long countDatesInRange(List<Date> dateList, Date start, Date end) {
        LocalDate localStart = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localEnd = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        long count = dateList.stream()
                .map(date -> date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .filter(localDate -> !localDate.isBefore(localStart) && !localDate.isAfter(localEnd))
                .count();

        return count;
    }

    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date atStartOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(startOfDay);
    }

    public static Date atEndOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return localDateTimeToDate(endOfDay);
    }
}
