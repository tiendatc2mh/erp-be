package com.blameo.erpservice.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 5/10/2023, Wednesday
 **/
public class FileUtil {
    public final static String EXT_PDF = "pdf";
    public final static String EXT_OFFICE = "xls,xlsx,doc,docx";
    public final static Pattern PATTERN = Pattern.compile("^[a-zA-Z0-9-_]+$");

    public static Boolean checkImportFile(MultipartFile file) {
        String nameFile = file.getOriginalFilename();
        if (EXT_PDF.contains(FilenameUtils.getExtension(nameFile)) ||
                EXT_OFFICE.contains(FilenameUtils.getExtension(nameFile))) {
            return true;
        }
        return false;
    }
}
