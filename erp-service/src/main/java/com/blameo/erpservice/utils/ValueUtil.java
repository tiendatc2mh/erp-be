package com.blameo.erpservice.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/26/2023, Wednesday
 **/

public class ValueUtil {

    private ValueUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static String getStringByObject(Object obj) {
        if (obj == null) {
            return null;
        }
        return obj.toString().trim();
    }

    public static Date getDateByObject(Object obj) {
        if (obj == null) {
            return null;
        }
        return (Date) obj;
    }

    public static Long getLongByObject(Object obj) {
        if (obj == null || obj.toString().isEmpty()) {
            return null;
        }
        return Long.valueOf(obj.toString());
    }

    public static Double getDoubleByObject(Object obj) {
        if (obj == null || obj.toString().isEmpty()) {
            return null;
        }
        return Double.valueOf(obj.toString());
    }

    public static Float getFloatByObject(Object obj) {
        if (obj == null || obj.toString().isEmpty()) {
            return null;
        }
        return Float.valueOf(obj.toString());
    }

    public static Integer getIntegerByObject(Object obj) {
        if (obj == null || obj.toString().isEmpty()) {
            return null;
        }
        return Integer.valueOf(obj.toString());
    }

    public static Boolean getBooleanByObject(Object obj) {
        return (obj != null && "1,true".contains(obj.toString()));
    }

    public static Double parseObjectToDouble(Object o) {
        try {
            return Double.parseDouble(o.toString());
        } catch (Exception e){
            return Double.NaN;
        }
    }
    public static Integer parseObjectToInteger(Object o) {
        try {
            if (o.getClass().equals(Integer.class))
                return (Integer) o;
            else {
                return Integer.parseInt(o.toString());
            }
        } catch (Exception e){
            return null;
        }
    }
    public static Date setEndDay(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        // Thiết lập giờ, phút, giây và mili giây thành cuối ngày
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);

        return calendar.getTime();
    }

    public static int compareTimes(Date d1, Date d2){
        int     t1;
        int     t2;
        t1 = (int) (d1.getTime() % (24*60*60*1000L));
        t2 = (int) (d2.getTime() % (24*60*60*1000L));
        return (t1 - t2);
    }

    public static int compareDate(Date d1, Date d2){
        LocalDate localDate1 = d1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localDate2 = d2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate1.compareTo(localDate2);
    }
}

