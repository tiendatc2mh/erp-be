package com.blameo.erpservice.utils;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@Component
@Data
public class EmailUtil {
    private static Logger logger = LoggerFactory.getLogger(EmailUtil.class);
    private static String mailSender;


    private static String personal;

    @Autowired
    public EmailUtil(@Value("${erp.mail.sender}") String mailSender,
                     @Value("${erp.mail.personal}") String personal) {
        this.mailSender = mailSender;
        this.personal = personal;
    }


    public static void sendMail(Session session, String[] toList, String[] ccList, String[] bccList, String subject,
                                String content, byte[][] attachmentDatas, String[] attachmentNames) throws Exception {

        try {

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mailSender, personal));

            if (toList != null) {
                for (String s : toList) {
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(s.trim()));
                }
            }
            if (ccList != null) {
                for (String s : ccList) {
                    message.addRecipient(Message.RecipientType.CC, new InternetAddress(s.trim()));
                }
            }
            if (bccList != null) {
                for (String s : bccList) {
                    message.addRecipient(Message.RecipientType.BCC, new InternetAddress(s.trim()));
                }
            }
            message.setSubject(subject, "utf-8");

            //Create the message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();

            //Fill the message
            messageBodyPart.setContent(content, "text/html;charset=utf-8");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            //Set text message part
            multipart.addBodyPart(messageBodyPart);

            if (attachmentDatas != null && attachmentNames != null && attachmentDatas.length == attachmentNames.length) {
                for (int i = 0; i < attachmentDatas.length; i++) {
                    byte[] attachmentData = attachmentDatas[i];
                    if (attachmentData != null) {
                        messageBodyPart = new MimeBodyPart();
                        messageBodyPart.setDataHandler(new DataHandler(attachmentData, "application/octet-stream"));
                        messageBodyPart.setFileName(attachmentNames[i]);
                        multipart.addBodyPart(messageBodyPart);
                    }
                }
            }

            message.setContent(multipart);
            Transport.send(message);
        } catch (Throwable t) {
            logger.error(t.getMessage(), t);
            throw t;
        }
    }

}
