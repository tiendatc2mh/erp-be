package com.blameo.erpservice.constant;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

public class Constant {
    private Constant() {
        throw new IllegalStateException("Utility class");
    }

    public static final String ASC = "asc";
    public static final String DESC = "desc";
    public static final String UNKNOWN = "unknownError";
    public static final String IS_EXIST = "isExist";
    public static final String CREATE_SUCCESS = "createSuccess";
    public static final String UPDATE_SUCCESS = "updateSuccess";
    public static final String DELETE_SUCCESS = "deleteSuccess";
    public static final String APPROVE_SUCCESS = "approveSuccess";
    public static final String SEND_SUCCESS = "sendSuccess";
    public static final String REFUSE_SUCCESS = "refuseSuccess";
    public static final String COMPLETE_SUCCESS = "completeSuccess";
    public static final String CANCEL_SUCCESS = "cancelSuccess";
    public static final String NOT_EXIST = "notExist";
    public static final String IS_USE = "inUse";
    public static final String GET_SUCCESS = "getDataSuccess";
    public  static final String KEY_HEADER_USER = "user";
    public  static final String SELETED_USER_APPROVER = "seletedUserApprover";
    public  static final String PROCESS = "process";
    public  static final String NOTIFICATION_ERROR = "notificationError";
    public  static final String UPDATE_WORKTIME_CONFIRM_ERROR = "updateWorktimeConfirmError";
    public  static final String UPDATE_REMOTE_ERROR = "updateRemoteError";
    public  static final String REQUEST = "request";
    public  static final String PROCESS_CONFIG = "processConfig";
    public  static final String USER = "user";
    public  static final String URL_SYSTEM = "http://erp.blameo.work/";

    public static final String TEMPLATE_EXPORT_REQUEST_PAYMENT = "Đề xuất thanh toán.xlsx";
    public static final String TEMPLATE_EXPORT_BUY_EQUIPMENT = "Danh sách mua trang thiết bị.xlsx";
    public static final String TEMPLATE_EXPORT_REQUEST_RECRUITMENT = "Danh sách tuyển dụng.xlsx";

    public static final int ACTIVE = 1;
    public static final int INACTIVE = 0;
    public static final int IS_DELETE = 1;
    public static final int IS_DEFAULT = 1;
    public static final int NON_DEFAULT = 0;
    public static final int COMPLETE = 1;
    public static final String URL_GATE_WAY = "http://192.168.0.196";
    public static final String PORT_GATE_WAY = "8090";

    public static class PATH {
        public static final String GET_USER_BY_USER_ID = "/api/id/user/";
        public static final String GET_USER_BY_ROLE_ID = "/api/id/role/detail/";
        public static final String GET_ROLE_BY_LIST_ROLE_ID = "/api/id/role/multiple";
        public static final String UPDATE_WORKTIME_TIMEKEEPING = "/api/worktime/timekeeping";
        public static final String CREATE_NOTIFICATION = "/api/id/notification";
        public static final String GET_USER_MULTIPLE_ID = "/api/id/user/multiple-id";
        public static final String GET_LEVEL_MULTIPLE_ID = "/api/id/level/multiple-id";
        public static final String GET_ASSET_TYPE_MULTIPLE_ID = "/api/id/asset-type/multiple-id";
        public static final String GET_POSITION_MULTIPLE_ID = "/api/id/position/multiple-id";
        public static final String GET_USER_BY_FULLNAME = "/api/id/user/all";
        public static final String GET_ASSET_BY_REQUEST_ID = "/api/id/asset/all";
        public static final String GET_WORKTIME_BY_USER_DATE= "/api/worktime/timekeeping/timekeeping-user";
        public static final String GET_WORKTIME_IN_DAY= "/api/worktime/worktime";
        public static final String BACK_TIMEKEEPING= "/api/worktime/timekeeping/back-timekeeping";
        public static final String UPDATE_AVAILABLE_DAY= "/api/worktime/avaiable-day-off/update_day_off";
        public static final String GET_LIST_HOLIDAY= "/api/worktime/holiday/all";
    }

    public static class REQUEST_WORK_TIME_TYPE {
        public static final int FORGOT_CHECKIN = 1;
        public static final int FORGOT_CHECKOUT = 2;
        public static final int FORGOT_CHECKIN_AND_CHECKOUT = 3;
    }

    public static class DAY_OFF_TYPE {
        public static final int ON_LEAVE = 1;
        public static final int UNAUTHORIZED_LEAVE = 2;
        public static final int MATERNITY_LEAVE = 3;
        public static final int FUNERAL_LEAVE = 4;
        public static final int MARRIAGE_LEAVE = 5;
    }

    public static class OT_TYPE {
        public static final int IN_WEEK = 1; // Trong tuần
        public static final int WEEKEND = 2; // Cuối tuần
        public static final int HOLIDAY = 3; // Nghỉ lễ
    }

    public static class DAY_OFF_SHIFT {
        public static final Integer ALL = 0; // Cả ngày
        public static final Integer AM = 1; // Sáng
        public static final Integer PM = 2; // chiều
    }

    public static class REMOTE_TYPE {
        public static final Integer ALL = 0; // Cả ngày
        public static final Integer AM = 1; // Sáng
        public static final Integer PM = 2; // chiều
    }

    public static class PROCESS_STEP_CONFIG {
        public static final String START = "1692327015862"; // Bắt đầu
        public static final String END = "1692327015863"; // Kết thúc
    }

    public static class REQUEST_LOG_STATUS {
        public static final int UNSENT = 0; // chưa gửi
        public static final int WAITING_APPROVE = 1; // chờ phê duyệt
        public static final int APPROVED = 2; // đã phê duyệt
        public static final int REJECT = 3; // từ chối
    }

    public static class REQUEST_LOG_ACTION {
        public static final int CREATE = 1; // tạo
        public static final int UPDATE = 2; // chỉnh sửa
        public static final int DELETE = 3; // xóa
        public static final int SEND_REQUEST = 4; // gửi yêu cầu
        public static final int APPROVE = 5; // phê duyệt
        public static final int REJECT = 6; // từ chối
    }

    public static class PROCESS_TYPE {
        public static final int REGISTER_FOR_LEAVE = 1; // đăng ký nghỉ phép
        public static final int REGISTER_FOR_OT = 2; // đăng ký OT
        public static final int REGISTER_FOR_REMOTE = 3; // đăng ký remote
        public static final int BUY_EQUIPMENT = 4; // mua trang thiết bị
        public static final int PAYMENT_REQUEST = 5; // đề nghị thanh toán
        public static final int WORK_CONFIRMATION = 6; // xác nhận công
        public static final int RECRUITMENT_REQUEST = 7; // đề nghị tuyển dụng
    }

    public static class REQUEST_STATUS {
        public static final int UNSENT = 0; // CHƯA GỬI
        public static final int WAITING_APPROVAL = 1; // CHỜ PHÊ DUYỆT
        public static final int APPROVED = 2; // ĐÃ PHÊ DUYỆT
        public static final int REFUSE = 3; // TỪ CHỐI
        public static final int DELETE = 4; // XOÁ
        public static final int COMPLETED = 5; // ĐÃ HOÀN THÀNH
        public static final int CANCEL = 6; // HUỶ
    }

    public static class REQUEST_ACTION {
        public static final int CREATE = 1; // TẠO
        public static final int EDIT = 2; // CHỈNH SỬA
        public static final int DELETE = 3; // XOÁ
        public static final int SEND = 4; // GỬI YÊU CẦU
        public static final int APPROVED = 5; // PHÊ DUYỆT
        public static final int CANCEL = 6; // HUỶ
    }

    public static class REQUEST_SEND {
        public static final int NO_SEND = 0; // CHƯA GỬI
        public static final int SEND = 1; // GỬI
    }

    public static class REQUEST_CONTENT_STATUS{
        public static final int DELETE = 0; // Xoá
        public static final int ACTIVE = 1;
    }

    public static class PROCESS_STEP_LOG_STATUS{
        public static final int WAITING_APPROVAL = 1; // chờ phê duyệt
        public static final int APPROVED = 2; //phê duyệt
        public static final int REFUSE = 3; //từ chối
        public static final int DENIED = 4; //bị từ chối
        public static final int ACCOMPLISHED = 5; //đã hoàn thành
    }

    public static class PROCESS_CONFIG_TYPE{
        public static final int SEND = 0; // Gửi
        public static final int APPROVAL = 1; // Phê duyệt
        public static final int REFUSE = 2; // Từ chối
        public static final int PERFORM = 3; // Thực hiện
        public static final int APPROVAL_END = 4; // Phê duyệt cuối cùng
    }

    public static class ATTACHMENT_TYPE{
        public static final int CONTRACT = 1; // hợp đồng
        public static final int INVOICE = 2; // hoá đơn
        public static final int DELIVERY_BILL = 3; // phiếu xuất kho
        public static final int OTHER_DOCUMENTS = 4; // chứng từ khác
        public static final int RECRUITMENT = 5; // file tuyển dụng
        public static final int RECRUITMENT_RESULT = 6; // file kết quả tuyển dụng
    }

    public static class PAY_TYPE{
        public static final int ADVANCE = 1; // tạm ứng
        public static final int PAYMENT = 2; // thanh toán
        public static final int RETURN = 3; // hoàn ứng
    }

    public static class NOTIFICATION_CODE {
        public static final String REGISTER_FOR_LEAVE = "${registerForLeave}"; // đăng ký nghỉ phép
        public static final String REGISTER_FOR_OT = "${registerForOt}"; // đăng ký OT
        public static final String REGISTER_FOR_REMOTE = "${registerForRemote}"; // đăng ký remote
        public static final String BUY_EQUIPMENT = "${buyEquipment}"; // mua trang thiết bị
        public static final String PAYMENT_REQUEST = "${paymentRequest}"; // đề nghị thanh toán
        public static final String WORK_CONFIRMATION = "${workConfirmation}"; // xác nhận công
        public static final String RECRUITMENT_REQUEST = "${recruitmentRequest}"; // đề nghị tuyển dụng
        public static final String SEND = "${send}"; // Gửi yêu cầu
        public static final String APPROVAL = "${approval}"; // Phê duyệt
        public static final String REFUSE = "${refuse}"; // Từ chối
        public static final String PERFORM = "${perform}"; // Thực hiện
        public static final String CODE = "${code}"; // Mã
    }
}
