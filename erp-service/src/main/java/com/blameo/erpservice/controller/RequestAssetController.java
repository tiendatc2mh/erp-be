package com.blameo.erpservice.controller;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.dto.ResponseMessage;
import com.blameo.erpservice.dto.response.BuyEquipmentContentResponseDTO;
import com.blameo.erpservice.dto.response.RequestBuyEquipmentContentResponseDTO;
import com.blameo.erpservice.dto.response.RequestDTOResponse;
import com.blameo.erpservice.dto.response.RequestLogDTOResponse;
import com.blameo.erpservice.service.RequestAssetService;
import com.blameo.erpservice.service.RequestLogService;
import com.blameo.erpservice.utils.CommonUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/erp/request-asset")
public class RequestAssetController {

    @Autowired
    RequestAssetService requestAssetService;

    @Autowired
    MessageSource messageSource;

    Locale locale = CommonUtil.getLocale();

    @GetMapping("")
    public ResponseEntity<Object> getLstAssetInRequest(@RequestParam(defaultValue = "") String requestId,
                                                       HttpServletRequest request){
        try {
            List<BuyEquipmentContentResponseDTO> result = requestAssetService.getAll(requestId, request);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
