package com.blameo.erpservice.controller;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.dto.ResponseMessage;
import com.blameo.erpservice.dto.response.RequestDTOResponse;
import com.blameo.erpservice.dto.response.RequestLogDTOResponse;
import com.blameo.erpservice.service.RequestLogService;
import com.blameo.erpservice.utils.CommonUtil;
import com.blameo.erpservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@RestController
@RequestMapping("/api/erp/request-log")
public class RequestLogController {

    @Autowired
    RequestLogService requestLogService;

    @Autowired
    MessageSource messageSource;

    Logger logger = Logger.getLogger(RequestLogController.class);


    @GetMapping("/all")
    public ResponseEntity<Object> getAll(@Valid @RequestParam(required = false) String requestId, HttpServletRequest httpRequest) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all department");
            List<RequestLogDTOResponse> result = requestLogService.getAll(requestId, httpRequest);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
