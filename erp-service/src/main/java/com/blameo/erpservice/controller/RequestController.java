package com.blameo.erpservice.controller;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.dto.ResponseMessage;
import com.blameo.erpservice.dto.request.*;
import com.blameo.erpservice.dto.response.RequestDTOResponse;
import com.blameo.erpservice.model.Request;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.repository.RequestRepository;
import com.blameo.erpservice.service.RequestService;
import com.blameo.erpservice.utils.CommonUtil;
import com.blameo.erpservice.utils.DataUtil;
import com.blameo.erpservice.utils.ValueUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

@RestController
@RequestMapping("/api/erp/request")
public class RequestController {

    @Autowired
    RequestService requestService;

    @Autowired
    RequestRepository requestRepository;

    Logger logger = Logger.getLogger(ProcessController.class);

    private final MessageSource messageSource;

    public RequestController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    Locale locale = CommonUtil.getLocale();

    @PostMapping({"/register-for-leave", "/register-for-ot", "/register-for-remote", "/buy-equipment", "/payment-request", "/work-confirmation", "/recruitment-request"})
    public ResponseEntity<Object> create(@Valid @RequestBody RequestDTO data, HttpServletRequest request) {
        //Get ngôn ngữ
        try {
            requestService.save(data, request);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.CREATE_SUCCESS, null, locale), null), HttpStatus.CREATED);
        } catch (RuntimeException runtimeException) {
            //với case bắt validate request
            Object dataError = null;
            if(runtimeException.getMessage().contains("\"")){
                String requestCode = runtimeException.getMessage().substring(runtimeException.getMessage().indexOf("\"") + 1, runtimeException.getMessage().lastIndexOf("\""));
                dataError = requestCode.split(", ");
            }
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage(), dataError), HttpStatus.BAD_REQUEST);
        }catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping({"/register-for-leave/send/{id}", "/register-for-ot/send/{id}", "/register-for-remote/send/{id}", "/buy-equipment/send/{id}", "/payment-request/send/{id}", "/work-confirmation/send/{id}", "/recruitment-request/send/{id}"})
    public ResponseEntity<Object> sendRequest(@PathVariable("id") String id, HttpServletRequest httpRequest) {
        try {
            requestService.send(id, httpRequest);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.SEND_SUCCESS, null, locale)), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            Object dataError = null;
            if(runtimeException.getMessage().contains("\"")){
                String requestCode = runtimeException.getMessage().substring(runtimeException.getMessage().indexOf("\"") + 1, runtimeException.getMessage().lastIndexOf("\""));
                dataError = requestCode.split(", ");
            }
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage(), dataError), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping({"/register-for-leave/{id}", "/register-for-ot/{id}", "/register-for-remote/{id}", "/buy-equipment/{id}", "/payment-request/{id}", "/work-confirmation/{id}", "/recruitment-request/{id}"})
    public ResponseEntity<Object> get(@PathVariable("id") String id, HttpServletRequest request) {
        try {
            if (!requestRepository.existsByRequestId(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.REQUEST, null, CommonUtil.getLocale()) + " " + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            RequestDTOResponse result = requestService.get(id, request);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping({"/register-for-leave/{id}", "/register-for-ot/{id}", "/register-for-remote/{id}", "/buy-equipment/{id}", "/payment-request/{id}", "/work-confirmation/{id}", "/recruitment-request/{id}"})
    public ResponseEntity<Object> delete(@PathVariable("id") String id, HttpServletRequest requestHttp) {
        try {
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(requestHttp);
            if (!requestRepository.existsByRequestId(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.REQUEST, null, CommonUtil.getLocale()) + " " + messageSource.getMessage(Constant.IS_USE, null, locale)), HttpStatus.BAD_REQUEST);
            }

            Request request = requestRepository.findById(id).get();

            if (request.getRequestStatus() != Constant.REQUEST_STATUS.UNSENT) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.REQUEST, null, CommonUtil.getLocale()) + " " + messageSource.getMessage(Constant.IS_USE, null, locale)), HttpStatus.BAD_REQUEST);
            }

            requestService.delete(id, userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.DELETE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping({"/register-for-leave/{id}", "/register-for-ot/{id}", "/register-for-remote/{id}", "/buy-equipment/{id}", "/payment-request/{id}", "/work-confirmation/{id}", "/recruitment-request/{id}"})
    public ResponseEntity<Object> update(@Valid @RequestBody RequestDTO requestDTO, @PathVariable("id") String id, HttpServletRequest httpRequest) {
        try {
            Request request = requestRepository.findById(id).get();

            if (!List.of(Constant.REQUEST_STATUS.UNSENT, Constant.REQUEST_STATUS.REFUSE).contains(request.getRequestStatus())) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.REQUEST, null, CommonUtil.getLocale()) + " " + messageSource.getMessage(Constant.IS_USE, null, locale)), HttpStatus.BAD_REQUEST);
            }

            RequestDTOResponse result = requestService.update(requestDTO, id, httpRequest);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale), result), HttpStatus.OK);

        } catch (RuntimeException runtimeException) {
            //với case bắt validate request
            Object dataError = null;
            if(runtimeException.getMessage().contains("\"")){
                String requestCode = runtimeException.getMessage().substring(runtimeException.getMessage().indexOf("\"") + 1, runtimeException.getMessage().lastIndexOf("\""));
                dataError = requestCode.split(", ");
            }
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage(), dataError), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping({"/register-for-leave", "/register-for-ot", "/register-for-remote", "/buy-equipment", "/payment-request", "/work-confirmation", "/recruitment-request"})
    public ResponseEntity<Object> findAll(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize,
                                          @RequestParam(required = false) Integer requestWorkTimeType,
                                          @RequestParam(required = false) Integer dayOffType,
                                          @RequestParam(required = false) Integer requestOtType,
                                          @RequestParam(required = false) Integer payType,
                                          @RequestParam(required = false, defaultValue = "") String position,
                                          @RequestParam Integer processType,
                                          @RequestParam(required = false, defaultValue = "") String requestCode,
                                          @RequestParam(required = false, defaultValue = "") String keyword,
                                          @RequestParam(required = false) List<Integer> status,
                                          @RequestParam(required = false, defaultValue = "") String username,
                                          @RequestParam(required = false) Date fromDate,
                                          @RequestParam(required = false) Date toDate,
                                          @RequestParam(required = false) Boolean isExport,
                                          HttpServletRequest httpRequest) {
        try {
            if (!StringUtils.isEmpty(requestCode)) {
                requestCode = DataUtil.makeLikeQuery(requestCode);
            }
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if (!StringUtils.isEmpty(username)) {
                username = DataUtil.makeLikeQuery(username);
            }
            if (toDate != null){
                toDate = ValueUtil.setEndDay(toDate);
            }
            if (isExport!= null && isExport){
                InputStreamResource inputStream=  requestService.export(httpRequest, requestWorkTimeType,
                        dayOffType, requestOtType, payType, position, true,
                        processType, requestCode, keyword, status, username, fromDate, toDate, false);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
                headers.setContentDispositionFormData("attachment", URLEncoder.encode(Constant.TEMPLATE_EXPORT_REQUEST_PAYMENT, StandardCharsets.UTF_8).replaceAll("\\+", "%20"));
                return ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .body(inputStream);
            }
            Map<String, Object> result = requestService.findAll(page, pageSize, httpRequest, requestWorkTimeType,
                                                                dayOffType, requestOtType, payType, position, true,
                                                                processType, requestCode, keyword, status, username, fromDate, toDate, false);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping({"/waiting-process/register-for-leave", "/waiting-process/register-for-ot", "/waiting-process/register-for-remote", "/waiting-process/buy-equipment", "/waiting-process/payment-request", "/waiting-process/work-confirmation", "/waiting-process/recruitment-request"})
    public ResponseEntity<Object> findAllRequestWaitingProcess(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize,
                                          @RequestParam(required = false) Integer requestWorkTimeType,
                                          @RequestParam(required = false) Integer dayOffType,
                                          @RequestParam(required = false) Integer requestOtType,
                                          @RequestParam(required = false) Integer payType,
                                          @RequestParam(required = false, defaultValue = "") String position,
                                          @RequestParam Integer processType,
                                          @RequestParam(required = false, defaultValue = "") String requestCode,
                                          @RequestParam(required = false, defaultValue = "") String keyword,
                                          @RequestParam(required = false) List<Integer> status,
                                          @RequestParam(required = false, defaultValue = "") String username,
                                          @RequestParam(required = false) Date fromDate,
                                          @RequestParam(required = false) Date toDate,
                                          @RequestParam(required = false) Boolean isExport,
                                          HttpServletRequest httpRequest) {
        try {
            if (!StringUtils.isEmpty(requestCode)) {
                requestCode = DataUtil.makeLikeQuery(requestCode);
            }
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if (!StringUtils.isEmpty(username)) {
                username = DataUtil.makeLikeQuery(username);
            }
            if (toDate != null){
                toDate = ValueUtil.setEndDay(toDate);
            }
            if (isExport!= null && isExport){
                InputStreamResource inputStream=  requestService.export(httpRequest, requestWorkTimeType,
                        dayOffType, requestOtType, payType, position, false,
                        processType, requestCode, keyword, status, username, fromDate, toDate, false);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
                headers.setContentDispositionFormData("attachment", URLEncoder.encode(Constant.TEMPLATE_EXPORT_REQUEST_PAYMENT, StandardCharsets.UTF_8).replaceAll("\\+", "%20"));
                return ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .body(inputStream);
            }
            Map<String, Object> result = requestService.findAll(page, pageSize, httpRequest, requestWorkTimeType,
                    dayOffType, requestOtType, payType, position, false,
                    processType, requestCode, keyword, status, username, fromDate, toDate, false);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping({"/register-for-leave/approve/{id}", "/register-for-ot/approve/{id}", "/register-for-remote/approve/{id}", "/buy-equipment/approve/{id}", "/payment-request/approve/{id}", "/work-confirmation/approve/{id}", "/recruitment-request/approve/{id}"})
    public ResponseEntity<Object> approveRequest(@Valid @RequestBody(required = false) RequestApproveDTO requestApproveDTO, @PathVariable("id") String id, HttpServletRequest httpRequest) {
        try {
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
            requestService.approve(requestApproveDTO, id, userDTOResponse, httpRequest);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.APPROVE_SUCCESS, null, locale)), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping({"/register-for-leave/refuse/{id}", "/register-for-ot/refuse/{id}", "/register-for-remote/refuse/{id}", "/buy-equipment/refuse/{id}", "/payment-request/refuse/{id}", "/work-confirmation/refuse/{id}", "/recruitment-request/refuse/{id}"})
    public ResponseEntity<Object> refuseRequest(@Valid @RequestBody RequestRejectDTO requestRejectDTO, @PathVariable("id") String id, HttpServletRequest httpRequest) {
        try {
            requestService.refuse(id, httpRequest, requestRejectDTO);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.REFUSE_SUCCESS, null, locale)), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping({"/complete/buy-equipment/{id}", "/complete/payment-request/{id}"})
    public ResponseEntity<Object> completePaymentRequest(@Valid @RequestBody(required = false) RequestPaymentDTO requestPaymentDTO, @PathVariable("id") String id, HttpServletRequest httpRequest) {
        try {
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
            requestService.completePayment(id, requestPaymentDTO, userDTOResponse, httpRequest);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.COMPLETE_SUCCESS, null, locale)), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/complete/recruitment/{id}")
    public ResponseEntity<Object> completeRecruitmentRequest(@Valid @RequestBody RequestRecruitmentPerformDTO requestRecruitmentPerformDTO, @PathVariable("id") String id, HttpServletRequest httpRequest) {
        try {
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
            requestService.completeRecruitment(id, requestRecruitmentPerformDTO, userDTOResponse, httpRequest);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.COMPLETE_SUCCESS, null, locale)), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<Object> getAll(@RequestParam(required = false, defaultValue = "") String keyword,
                                         @RequestParam(required = false, defaultValue = "0") List<Integer> status,
                                         @RequestParam(required = false) Date fromDate,
                                         @RequestParam(required = false) Date toDate,
                                         @RequestParam(required = false) Date worktimeFromDate,
                                         @RequestParam(required = false) Date worktimeToDate,
                                         @RequestParam List<Integer> processType,
                                         @RequestParam(required = false,defaultValue = "") String userId,
                                         HttpServletRequest httpRequest) {
        try {
            logger.debug("REST request to get all department");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if  (Objects.equals(userId, "")){
                userId = null;
            }
            if (toDate != null){
                toDate = ValueUtil.setEndDay(toDate);
            }
            List<RequestDTOResponse> result = requestService.getAll(keyword, status, fromDate, toDate, worktimeFromDate,
                    worktimeToDate, processType, httpRequest,userId);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/schedule")
    public ResponseEntity<Object> getAllSchedule(@RequestParam(required = false, defaultValue = "") String keyword,
                                         @RequestParam(required = false, defaultValue = "0") List<Integer> status,
                                         @RequestParam(required = false) Date fromDate,
                                         @RequestParam(required = false) Date toDate,
                                         @RequestParam(required = false) Date worktimeFromDate,
                                         @RequestParam(required = false) Date worktimeToDate,
                                         @RequestParam List<Integer> processType,
                                         @RequestParam(required = false,defaultValue = "") String userId) {
        try {
            logger.debug("REST request to get all department");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if  (Objects.equals(userId, "")){
                userId = null;
            }
            if (toDate != null){
                toDate = ValueUtil.setEndDay(toDate);
            }
            List<RequestDTOResponse> result = requestService.getAllSchedule(keyword, status, fromDate, toDate, worktimeFromDate,
                    worktimeToDate, processType, userId);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //lấy ra request để ghép vào ô select trong yêu cầu xác nhận thanh toán
    @GetMapping("/payment-request/request-advance")
    public ResponseEntity<Object> getRequestAdvance(HttpServletRequest request) {
        try {
            List<RequestDTOResponse> result = requestService.getRequestAdvance(request);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //worktime call sang để update số ngày tính lương
    @PutMapping("/worktime-contents")
    public ResponseEntity<Object> updateWorktimeContents(@Valid @RequestBody Set<RequestWorktimeContentDTO> worktimeContentDTOS, HttpServletRequest httpRequest) {
        try {
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
            requestService.updateWorktimeContents(worktimeContentDTOS, userDTOResponse, httpRequest);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.COMPLETE_SUCCESS, null, locale)), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/cancel-request")
    public ResponseEntity<Object> cancelRequest(@RequestBody List<String> requestCode, HttpServletRequest httpRequest){
        try {
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(httpRequest);
            requestService.cancelRequest(requestCode, userDTOResponse, httpRequest);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.CANCEL_SUCCESS, null, locale)), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
