package com.blameo.erpservice.controller;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.dto.ResponseMessage;
import com.blameo.erpservice.dto.request.AvailableOffDTO;
import com.blameo.erpservice.dto.request.NotificationDTO;
import com.blameo.erpservice.dto.request.RequestDTO;
import com.blameo.erpservice.dto.request.TimekeepingDTO;
import com.blameo.erpservice.dto.response.HolidayDTOResponse;
import com.blameo.erpservice.dto.response.RequestDTOResponse;
import com.blameo.erpservice.dto.response.TimekeepingDTOResponse;
import com.blameo.erpservice.dto.response.WorkTimeInDayResponseDTO;
import com.blameo.erpservice.message.RequestMessage;
import com.blameo.erpservice.model.Request;
import com.blameo.erpservice.modeluser.AssetDTOResponse;
import com.blameo.erpservice.modeluser.AssetTypeDTOResponse;
import com.blameo.erpservice.modeluser.TitleDTOResponse;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.utils.CommonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Service
public class CallOtherService {
    private static String gatewayHost;
    private static String gatewayPort;

    public CallOtherService(@Value("${gateway.host}") String gatewayHost, @Value("${gateway.port}") String gatewayPort) {
        this.gatewayHost = gatewayHost;
        this.gatewayPort = gatewayPort;
    }

    public static <T> T getDetailFromIdService(Class<T> entity, String path, String userId, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+path+userId,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            return mapper.convertValue(Objects.requireNonNull(response.getBody()).getData(), entity);
        } catch (Exception e) {
            try {
                return entity.getDeclaredConstructor().newInstance();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static List<UserDTOResponse> getListUserByRoleIdFromIdService(String roleId, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+Constant.PATH.GET_USER_BY_ROLE_ID + roleId,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            List<UserDTOResponse> dataUser = new ArrayList<>();
            List<LinkedHashMap<String, Object>> list = (List<LinkedHashMap<String, Object>>) ((LinkedHashMap) response.getBody().getData()).get("users");
            for(LinkedHashMap<String, Object> t : list){
                dataUser.add(mapper.convertValue(t, UserDTOResponse.class));
            }
            return dataUser;
        } catch (Exception e) {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static <T> List<T> getListFromIdService(Class<T> entity, String path, Set<String> lstId, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            String pathParam = lstId.stream().map(Object::toString)
                    .collect(Collectors.joining(","));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+path+"?id="+pathParam,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            List<T> list = (List<T>) response.getBody().getData();
            List<T> dataLst = new ArrayList<>();
            for(T t : list){
                dataLst.add(mapper.convertValue(t, entity));
            }
            return dataLst;
        } catch (Exception e) {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static List<UserDTOResponse> getListUserFromIdServiceByFullname(String fullname, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+Constant.PATH.GET_USER_BY_FULLNAME+"?keyword="+fullname,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            List<Object> list = (List<Object>) response.getBody().getData();
            List<UserDTOResponse> dataLst = new ArrayList<>();
            for(Object t : list){
                dataLst.add(mapper.convertValue(t, UserDTOResponse.class));
            }
            return dataLst;
        } catch (Exception e) {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static TimekeepingDTOResponse getTimekeepingByUserIdAndDate(String userId, Date date, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            String dateStr = CommonUtil.dateToStringRequest(date);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+Constant.PATH.GET_WORKTIME_BY_USER_DATE+"?userId="+userId+"&date="+dateStr,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            Object object = (Object) response.getBody().getData();
            return mapper.convertValue(object, TimekeepingDTOResponse.class);
        } catch (Exception e) {
            try {
                return new TimekeepingDTOResponse();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static WorkTimeInDayResponseDTO getWorkTimeInDay(Date date, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            String dateStr = CommonUtil.dateToStringRequest(date);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+Constant.PATH.GET_WORKTIME_IN_DAY+"?date="+dateStr,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            Object object = (Object) response.getBody().getData();
            return mapper.convertValue(object, WorkTimeInDayResponseDTO.class);
        } catch (Exception e) {
            try {
                return new WorkTimeInDayResponseDTO();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static List<AssetDTOResponse> getListAssetFromIdService(String requestId, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+Constant.PATH.GET_ASSET_BY_REQUEST_ID+"?requestId="+requestId,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            List<Object> list = (List<Object>) response.getBody().getData();
            List<AssetDTOResponse> dataLst = new ArrayList<>();
            for(Object t : list){
                dataLst.add(mapper.convertValue(t, AssetDTOResponse.class));
            }
            return dataLst;
        } catch (Exception e) {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static RequestDTOResponse updateTimekeeping(String path, RequestDTOResponse requestBody, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<RequestDTOResponse> data = new HttpEntity<>(requestBody, headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+path,
                    HttpMethod.PUT,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            Object requestDTOResponse = (Object) response.getBody().getData();
            return mapper.convertValue(requestDTOResponse, RequestDTOResponse.class);
        } catch (Exception e) {
            try {
                return new RequestDTOResponse();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }

    public static void backTimekeeping(TimekeepingDTO requestBody, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<TimekeepingDTO> data = new HttpEntity<>(requestBody, headers);
            restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+Constant.PATH.BACK_TIMEKEEPING,
                    HttpMethod.PUT,
                    data,
                    ResponseMessage.class
            );
        } catch (Exception e) {
            try {
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
            }
        }
    }

    public static void updateAvailableDayOff(AvailableOffDTO availableOffDTO, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<AvailableOffDTO> data = new HttpEntity<>(availableOffDTO, headers);
            restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+Constant.PATH.UPDATE_AVAILABLE_DAY,
                    HttpMethod.PUT,
                    data,
                    ResponseMessage.class
            );
        } catch (Exception e) {
            try {
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
            }
        }
    }

    public static ResponseMessage createNotification(NotificationDTO requestBody, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            headers.setContentType(new MediaType("application", "json", StandardCharsets.UTF_8));
            headers.set("Accept-Language", httpServletRequest.getHeader("Accept-Language"));
            HttpEntity<NotificationDTO> data = new HttpEntity<>(requestBody, headers);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+Constant.PATH.CREATE_NOTIFICATION,
                    HttpMethod.POST,
                    data,
                    ResponseMessage.class
            );
            return response.getBody();
        }catch (HttpClientErrorException httpClientErrorException){
            try{
                String res = httpClientErrorException.getResponseBodyAsString();
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(res, ResponseMessage.class);
            }catch (JsonProcessingException jsonProcessingException){
                return new ResponseMessage();
            }
        }
        catch (Exception e) {
            return new ResponseMessage();
        }
    }

    public static List<HolidayDTOResponse> getListHolidayFromWorktime(Date startDate, Date endDate, HttpServletRequest httpServletRequest) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", httpServletRequest.getHeader("authorization"));
            HttpEntity<String> data = new HttpEntity<>("body", headers);
            String startDateStr = CommonUtil.dateToStringRequest(startDate);
            String endDateStr = CommonUtil.dateToStringRequest(endDate);
            ResponseEntity<ResponseMessage> response = restTemplate.exchange(
                    gatewayHost+":"+gatewayPort+Constant.PATH.GET_LIST_HOLIDAY+"?start_date="+startDateStr+"?end_date="+endDateStr,
                    HttpMethod.GET,
                    data,
                    ResponseMessage.class
            );
            ObjectMapper mapper = new ObjectMapper();
            List<Object> list = (List<Object>) response.getBody().getData();
            List<HolidayDTOResponse> dataLst = new ArrayList<>();
            for(Object t : list){
                dataLst.add(mapper.convertValue(t, HolidayDTOResponse.class));
            }
            return dataLst;
        } catch (Exception e) {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace(); // Handle or log this exception appropriately
                return null; // Or throw a custom exception if needed
            }
        }
    }
}
