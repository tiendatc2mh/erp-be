package com.blameo.erpservice.controller;

import com.blameo.erpservice.constant.Constant;
import com.blameo.erpservice.dto.ResponseMessage;
import com.blameo.erpservice.dto.response.ProcessDTOResponse;
import com.blameo.erpservice.modeluser.UserDTOResponse;
import com.blameo.erpservice.repository.ProcessRepository;
import com.blameo.erpservice.repository.RequestRepository;
import com.blameo.erpservice.service.ProcessConfigService;
import com.blameo.erpservice.service.ProcessStepService;
import com.blameo.erpservice.service.ProcessService;
import com.blameo.erpservice.utils.CommonUtil;
import com.blameo.erpservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/erp/process")
public class ProcessController {

    @Autowired
    ProcessService processService;

    @Autowired
    ProcessRepository processRepository;

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    ProcessConfigService processConfigService;

    @Autowired
    ProcessStepService processStepService;

    Logger logger = Logger.getLogger(ProcessController.class);

    private final MessageSource messageSource;

    private static final String KEY = "Process ";

    public ProcessController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    Locale locale = CommonUtil.getLocale();

    @PostMapping("")
    public ResponseEntity<Object> create(@Valid @RequestBody Map<String, Object> data, HttpServletRequest request) {
        //Get ngôn ngữ
        try {
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            processService.save(data, userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.CREATE_SUCCESS, null, locale), null), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //truyền vào id của loại quy trình get ra quy trình mặc định của loại quy trình đó
    @GetMapping("/config/{id}")
    public ResponseEntity<Object> get(@PathVariable("id") Integer id, HttpServletRequest request) {
        try {
            logger.debug("REST request to get department : {}" + id);
            Map<String, Object> result = processService.getAll(id, request);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //truyền vào id của quy trình đó
    @GetMapping("/{id}")
    public ResponseEntity<Object> getProcess(@PathVariable("id") String id, HttpServletRequest request) {
        try {
            if (!processRepository.existsByProcessIdAndProcessStatusGreaterThanEqual(id, 0)) {
                return new ResponseEntity<>(ResponseMessage.error(KEY + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            logger.debug("REST request to get department : {}" + id);
            Map<String, Object> result = processService.getAllProcess(id, request);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@PathVariable("id") String id, @Valid @RequestBody Map<String, Object> data, HttpServletRequest request) {
        try {
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            processService.update(id, data, userDTOResponse, request);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale), null), HttpStatus.CREATED);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id, HttpServletRequest request) {
        try {
            logger.debug("REST request to delete department : {}" + id);
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            if (!processRepository.existsByProcessIdAndProcessStatusGreaterThanEqual(id, 0)) {
                return new ResponseEntity<>(ResponseMessage.error(KEY + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            //check table khác sử dụng nếu cần thiết
            processService.delete(id, userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.DELETE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<Object> findAll(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize,
                                          @RequestParam(required = false, defaultValue = "") String keyword,
                                          @RequestParam(required = false) Integer processType,
                                          @RequestParam(required = false) Integer isDefault) {
        try {
            logger.debug("REST request to get all department");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            Map<String, Object> result = processService.findAll(page, pageSize, keyword, processType, isDefault);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<Object> getAll() {
        try {
            logger.debug("REST request to get all department");
            List<ProcessDTOResponse> result = processService.getAllProcessType();
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/default/{id}")
    public ResponseEntity<Object> updateProcessDefault(@PathVariable("id") String id, HttpServletRequest request) {
        try {
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            processService.updateDefault(id, userDTOResponse, request);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale), null), HttpStatus.CREATED);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/default/{id}")
    public ResponseEntity<Object> getProcessDefault(@PathVariable("id") Integer id, HttpServletRequest request) {
        try {
            ProcessDTOResponse result = processService.get(id, request);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //truyền id request lấy ra list user xử lý step tiếp theo
    @GetMapping("/user-approver")
    public ResponseEntity<Object> getProcessConfig(@RequestParam(required = false) String requestId,
                                                   @RequestParam Integer processType,
                                                   @RequestParam(defaultValue = "1") Boolean isCreated,
                                                   HttpServletRequest request) {
        try {
            logger.debug("REST request to get department : {}" + requestId);
            List<UserDTOResponse> result = processConfigService.getListUserProcessConfigByRequestId(requestId, processType, request, isCreated);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
