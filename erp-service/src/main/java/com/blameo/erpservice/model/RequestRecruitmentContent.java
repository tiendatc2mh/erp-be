package com.blameo.erpservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@AllArgsConstructor
@Getter
@Setter
@Table(name = "request_recruitment_content")
@Where(clause = "request_recruitment_content_status != 0")
public class RequestRecruitmentContent {
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "request_recruitment_content_id", nullable = false, length = 36)
    private String requestRecruitmentContentId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "request_id")
    private Request request;

    @Column(name = "position_id", length = 36)
    private String positionId;

    @Column(name = "level_id", length = 36)
    private String levelId;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "reason", nullable = false)
    private String reason;

    @Column(name = "onboard_time_start")
    private Date onboardTimeStart;

    @Column(name = "onboard_time_end")
    private Date onboardTimeEnd;

    @Column(name = "request_recruitment_content_status")
    private Integer requestRecruitmentContentStatus;

    @Column(name = "recruitment_source")
    private Integer recruitmentSource;

    @Column(name = "recruitment_cost")
    private Integer recruitmentCost;

    @Column(name = "recruitment_form")
    private Integer recruitmentForm;

    @Column(name = "recruitment_complete_date")
    private Date recruitmentCompleteDate;

    public RequestRecruitmentContent() {
        this.requestRecruitmentContentStatus = 1;
    }
}
