package com.blameo.erpservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "process_config")
public class ProcessConfig{

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "process_config_id", nullable = false)
    private String processConfigId;

    @JoinColumn(name = "process_id")
    @OneToOne
    private Process process;

    @JoinColumn(name = "process_step_id")
    @OneToOne
    private ProcessStep processStep;

    @Column(name = "process_step_type")
    private Integer processStepType;

    @JoinColumn(name = "process_step_next")
    @OneToOne
    private ProcessStep processStepNext;
}
