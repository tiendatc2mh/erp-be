package com.blameo.erpservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@AllArgsConstructor
@Getter
@Setter
@Table(name = "request_worktime_content")
@Where(clause = "request_worktime_content_status != 0")
public class RequestWorktimeContent {
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "request_worktime_content_id", nullable = false)
    private String requestWorktimeContentId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "request_id")
    private Request request;

    @Column(name = "request_work_time_date")
    private Date requestWorkTimeDate;

    @Column(name = "request_work_time_checkin")
    private Date requestWorkTimeCheckin;

    @Column(name = "request_work_time_checkout")
    private Date requestWorkTimeCheckout;

    @Column(name = "request_work_time_checkin_old")
    private Date requestWorkTimeCheckinOld;

    @Column(name = "request_work_time_checkout_old")
    private Date requestWorkTimeCheckoutOld;

    @Column(name = "request_work_time_type")
    private Integer requestWorkTimeType;

    @Column(name = "request_day_off_type")
    private Integer requestDayOffType;

    @Column(name = "day_off_type")
    private Integer dayOffType;

    @Column(name = "request_day_off_user_transfer")
    private String requestDayOffUserTransfer;

    @Column(name = "day_off_begin")
    private Date dayOffBegin;

    @Column(name = "day_off_end")
    private Date dayOffEnd;

    @Column(name = "remote_date")
    private Date remoteDate;

    @Column(name = "remote_type")
    private Integer remoteType;

    @Column(name = "request_worktime_content_status")
    private Integer requestWorktimeContentStatus;

    @Column(name = "request_ot_type")
    private Integer requestOtType;

    @Column(name = "request_ot_project")
    private String requestOtProject;

    @Column(name = "request_ot_date")
    private Date requestOtDate;

    @Column(name = "request_ot_begin")
    private Date requestOtBegin;

    @Column(name = "request_ot_end")
    private Date requestOtEnd;

    @Column(name = "count_hour")
    private Double countHour;

    @Column(name = "count_day_salary")
    private Double countDaySalary;

    public RequestWorktimeContent() {
        this.requestWorktimeContentStatus = 1;
    }
}
