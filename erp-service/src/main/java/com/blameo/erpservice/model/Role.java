package com.blameo.erpservice.model;

import lombok.Data;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/24/2023, Monday
 **/
@Data
public class Role {
    String name;
}
