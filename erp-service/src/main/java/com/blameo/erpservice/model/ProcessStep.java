package com.blameo.erpservice.model;

import com.blameo.erpservice.modeluser.RoleDTOResponse;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "process_step")
public class ProcessStep extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "process_step_id", nullable = false)
    private String processStepId;

    @Column(name = "process_step_code", length = 50)
    private String processStepCode;

    @Column(name = "process_step_name", length = 100)
    private String processStepName;

    @Column(name = "role_id")
    private String roleId;

    @Column(name = "process_handle_time")
    private Integer processStepHandleTime;

    @Column(name = "process_step_description")
    private String processStepDescription;

    @Transient
    private RoleDTOResponse role;

}
