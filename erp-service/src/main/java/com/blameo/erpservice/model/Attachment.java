package com.blameo.erpservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "attachment")
@Where(clause = "attachment_status != 0")
public class Attachment extends BaseEntity {
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "attachment_id", nullable = false, length = 36)
    private String attachmentId;

    @Column(name = "attachment_name", nullable = false, length = 100)
    private String attachmentName;

    @Column(name = "attachment_url", nullable = false)
    private String attachmentUrl;

    @Column(name = "attachment_status", nullable = false)
    private Integer attachmentStatus = 1;

    @Column(name = "attachment_type", nullable = false)
    private Integer attachmentType;

    @Column(name = "object_id", nullable = false, length = 36)
    private String objectId;
}
