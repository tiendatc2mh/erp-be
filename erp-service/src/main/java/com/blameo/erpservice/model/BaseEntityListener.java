package com.blameo.erpservice.model;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
public class BaseEntityListener {

    @PrePersist
    public void prePersist(BaseEntity entity) {
        entity.setCreatedDate(new Date());
        entity.setUpdatedDate(new Date());
    }

    @PreUpdate
    public void preUpdate(BaseEntity entity) {
        entity.setUpdatedDate(new Date());
    }
}
