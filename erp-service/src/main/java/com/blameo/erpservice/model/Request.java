package com.blameo.erpservice.model;

import com.blameo.erpservice.modeluser.RoleDTOResponse;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "request")
@Where(clause = "request_status not in (4,6)")
public class Request extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "request_id", nullable = false, length = 36)
    private String requestId;

    @OneToOne
    @JoinColumn(name = "process_id", nullable = false)
    private Process process;

    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "reason", nullable = false)
    private String reason;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "request_status", nullable = false)
    private Integer requestStatus;

    @Column(name = "request_code", nullable = false)
    private String requestCode;

    @Column(name = "department_id", nullable = false)
    private String departmentId;

    @Column(name = "pay_type", nullable = false)
    private Integer payType;

    @Column(name = "day_off_marry_type", nullable = false)
    private Integer dayOffMarryType;

    @Column(name = "pay_date", nullable = false, length = 500)
    private Date payDate;

    @Column(name = "handover_user", nullable = false)
    private String handoverUser;

    @Column(name = "account_owner", nullable = false, length = 100)
    private String accountOwner;

    @Column(name = "account_number", nullable = false, length = 100)
    private String accountNumber;

    @Column(name = "bank", nullable = false, length = 500)
    private String bank;

    @Column(name = "day_off_last_year", nullable = false)
    private Double dayOffLastYear;

    @Column(name = "day_off_current_year", nullable = false)
    private Double dayOffCurrentYear;

    @Transient
    private Integer requestStatusProcess;

    @Transient
    private String createdByName;

    @OneToMany(mappedBy="request", cascade = CascadeType.ALL)
    private Set<RequestPayContent> requestPayContents = new HashSet<>();

    @OneToMany(mappedBy="request", cascade = CascadeType.ALL)
    private Set<RequestWorktimeContent> requestWorktimeContents = new HashSet<>();

    @OneToMany(mappedBy="request", cascade = CascadeType.ALL)
    private Set<RequestRecruitmentContent> requestRecruitmentContents = new HashSet<>();

    @OneToMany(mappedBy="request", cascade = CascadeType.ALL)
    private Set<RequestRecruitmentUserContent> requestRecruitmentUserContents = new HashSet<>();

}
