package com.blameo.erpservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

@Entity
@AllArgsConstructor
@Getter
@Setter
@Table(name = "request_process_log")
public class RequestProcessLog {
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "request_process_log_id", nullable = false, length = 36)
    private String requestProcessLogId;

    @Column(name = "request_id", nullable = false, length = 36)
    private String requestId;

    @Column(name = "request_process_log_status", nullable = false)
    private Integer requestProcessLogStatus;

    @Column(name = "handle_date")
    private Date handleDate = Date.from(Instant.now());

    @Column(name = "reject_reason", length = 500)
    private String rejectReason;

    @Column(name = "receive_user_id", nullable = false, length = 36)
    private String receiveUserId;

    @Column(name = "send_user_id", length = 36)
    private String sendUserId;

    @Column(name = "order_process", nullable = false)
    private Integer orderProcess;

    public RequestProcessLog(String requestId, Integer requestProcessLogAction, String rejectReason, String sendUserId, String receiveUserId, Integer orderProcess) {
        this.requestId = requestId;
        this.requestProcessLogStatus = requestProcessLogAction;
        this.rejectReason = rejectReason;
        this.sendUserId = sendUserId;
        this.receiveUserId = receiveUserId;
        this.orderProcess = orderProcess;
    }

    public RequestProcessLog() {

    }
}
