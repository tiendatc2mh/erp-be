package com.blameo.erpservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "request_log")
public class RequestLog {

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "request_log_id", nullable = false, length = 36)
    private String requestLogId;

    @Column(name = "request_id", nullable = false)
    private String requestId;

    @Column(name = "request_log_date", nullable = false)
    private Date requestLogDate = Date.from(Instant.now());

    @Column(name = "request_log_user", nullable = false)
    private String requestLogUser;

    @Column(name = "request_log_status", nullable = false)
    private Integer requestLogStatus;

    @Column(name = "request_log_action", nullable = false)
    private Integer requestLogAction;

    @Column(name = "request_approve", nullable = false)
    private String requestApprove;

    public RequestLog(String requestId, String requestLogUser, Integer requestLogStatus, Integer requestLogAction) {
        this.requestId = requestId;
        this.requestLogUser = requestLogUser;
        this.requestLogStatus = requestLogStatus;
        this.requestLogAction = requestLogAction;
    }

    public RequestLog(String requestId, String requestLogUser, Integer requestLogStatus, Integer requestLogAction, String requestApprove) {
        this.requestId = requestId;
        this.requestLogUser = requestLogUser;
        this.requestLogStatus = requestLogStatus;
        this.requestLogAction = requestLogAction;
        this.requestApprove = requestApprove;
    }
}
