package com.blameo.erpservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

@Entity
@AllArgsConstructor
@Getter
@Setter
@Table(name = "request_recruitment_user_content")
@Where(clause = "request_recruitment_user_status != 0")
public class RequestRecruitmentUserContent {
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "request_recruitment_user_content_id", length = 36)
    private String requestRecruitmentUserContentId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "request_id")
    private Request request;

    @Column(name = "request_recruitment_username", length = 100)
    private String requestRecruitmentUsername;

    @Column(name = "request_recruitment_user_level_id", length = 36)
    private String requestRecruitmentUserLevelId;

    @Column(name = "request_recruitment_user_salary")
    private Double requestRecruitmentUserSalary;

    @Column(name = "request_recruitment_user_start_time")
    private Date requestRecruitmentUserStartTime;

    @Column(name = "request_recruitment_user_status")
    private Integer requestRecruitmentUserStatus = 1;

    public RequestRecruitmentUserContent() {

    }
}
