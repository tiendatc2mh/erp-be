package com.blameo.erpservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "process_step_log")
public class ProcessStepLog {

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "process_step_log_id", nullable = false, length = 36)
    private String processStepLogId;

    @Column(name = "request_id", nullable = false, length = 36)
    private String requestId;

    @Column(name = "process_step_status", nullable = false)
    private Integer processStepStatus;

    @Column(name = "process_config_id")
    private String processConfigId;

    @Column(name = "order_process", nullable = false)
    private Integer orderProcess;

    @Column(name = "handle_date", nullable = false)
    private Date handleDate = Date.from(Instant.now());

    @Column(name = "is_delete", nullable = false)
    private Integer isDelete;

    @Column(name = "send_user_id", nullable = false)
    private String sendUserId;

    @Column(name = "receive_user_id", nullable = false)
    private String receiveUserId;

    public ProcessStepLog(String requestId, Integer processStepStatus, String processConfigId, Integer orderProcess, String sendUserId, String receiveUserId) {
        this.isDelete = 0;
        this.requestId = requestId;
        this.processStepStatus = processStepStatus;
        this.processConfigId = processConfigId;
        this.orderProcess = orderProcess;
        this.sendUserId = sendUserId;
        this.receiveUserId = receiveUserId;
    }

    public ProcessStepLog(String requestId, Integer processStepStatus, String processConfigId, Integer orderProcess, String sendUserId, String receiveUserId, Date handleDate) {
        this.isDelete = 0;
        this.requestId = requestId;
        this.processStepStatus = processStepStatus;
        this.processConfigId = processConfigId;
        this.orderProcess = orderProcess;
        this.sendUserId = sendUserId;
        this.receiveUserId = receiveUserId;
        this.handleDate = handleDate;
    }
}
