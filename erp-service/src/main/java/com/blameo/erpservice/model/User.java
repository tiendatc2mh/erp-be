package com.blameo.erpservice.model;

import lombok.Data;

import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/24/2023, Monday
 **/
@Data
public class User {
    private String username;
    private String email;
    private Set<Role> roles;
    private String fullName;

}
