package com.blameo.erpservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "process")
public class Process extends BaseEntity{
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "process_id", nullable = false)
    private String processId;

    @Column(name = "process_name", nullable = false, length = 100)
    private String processName;

    @Column(name = "process_status", nullable = false)
    private Integer processStatus;

    @Column(name = "process_description", nullable = false, length = 500)
    private String processDescription;

    @Column(name = "process_type", nullable = false)
    private Integer processType;

    @Column(name = "is_default", nullable = false)
    private Integer isDefault;

}
