package com.blameo.erpservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@AllArgsConstructor
@Getter
@Setter
@Table(name = "request_pay_content")
@Where(clause = "request_pay_content_status != 0")
public class RequestPayContent {
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "request_pay_content_id", nullable = false, length = 36)
    private String requestPayContentId;

    @JsonBackReference(value = "request")
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "request_id")
    private Request request;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "request_pay_content_description", length = 500)
    private String requestPayContentDescription;

    @Column(name = "unit_price", length = 500)
    private Double unitPrice;

    @Column(name = "asset_type_id", length = 36)
    private String assetTypeId;

    @Column(name = "received_money", length = 100)
    private Double receivedMoney;

    @Column(name = "request_pay_content_status")
    private Integer requestPayContentStatus;

    @Column(name = "pay_content", length = 500)
    private String payContent;

    @JsonBackReference(value = "request_advance")
    @OneToMany
    @JoinTable(
            name = "request_advance",
            joinColumns = @JoinColumn(name = "request_pay_content_id"),
            inverseJoinColumns = @JoinColumn(name = "request_id"))
    private Set<Request> requestAdvance = new HashSet<>();

    @Column(name = "remaining_money", length = 100)
    private Double remainingMoney;

    @Column(name = "advanced_money", length = 100)
    private Double advancedMoney;


    public RequestPayContent() {
        this.requestPayContentStatus = 1;
    }
}
