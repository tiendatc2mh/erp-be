package com.blameo.erpservice.model;

import lombok.Data;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/21/2023, Friday
 **/
@Data
public class Test {
    private String username;
    private String password;
}
