package com.blameo.erpservice.modeluser;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data
@NoArgsConstructor
public class UserDTOResponse {

    private String userId;

    private String userCode;

    private String fullName;

    private String avatar;

    private Integer gender;

    private Date birthday;

    private String email;

    private String personalEmail;

    private String phoneNumber;

    private String permanentAddress;

    private String residence;

    private Department department;

    private Position position;

    private Title title;

    private Level level;

    private Integer userType;

    private Date internshipStartDate;

    private Date officialDate;

    private Date severanveDate;

    private Integer employeeStatus;

    private Set<RoleDTOResponse> roles;

    private Set<UserManagerDTOResponse> managers;


    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String identityCard;

    private String placeOfIdentity;

    private Date dateOfIdentity;

    private String taxCode;

    private String socialInsuranceCode;

    private Date probationaryStartDate;

    private Integer attendanceCode;

    private String bankAccountNumber;

    private String bankType;

    private Double countHolidaysOld;

    private Double countHolidaysNew;


    public UserDTOResponse(String userId, String fullName) {
        this.userId = userId;
        this.fullName = fullName;
    }

    public UserDTOResponse(String userId, String fullName, String titleName, String deptName) {
        this.userId = userId;
        this.fullName = fullName;
        this.title = new Title(titleName);
        this.department = new Department(deptName);
    }
}
