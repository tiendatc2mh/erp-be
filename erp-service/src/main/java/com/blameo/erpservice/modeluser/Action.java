package com.blameo.erpservice.modeluser;

import lombok.*;

@Data
public class Action extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    private String actionId;

    private String actionCode;

    private String actionName;

    private Integer actionStatus;
}
