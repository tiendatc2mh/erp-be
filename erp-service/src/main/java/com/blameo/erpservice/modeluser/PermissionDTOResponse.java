package com.blameo.erpservice.modeluser;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data

public class PermissionDTOResponse {

    private String permissionId;

    private String permissionCode;


    private String path;


    private Integer method;


    private Integer permissionStatus;


    private Function function;


    private Action action;


    private Date createdDate;


    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;
}
