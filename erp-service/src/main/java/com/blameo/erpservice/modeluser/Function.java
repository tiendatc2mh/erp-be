package com.blameo.erpservice.modeluser;

import lombok.Data;

@Data
public class Function extends BaseEntity {
    private String functionId;

    private String functionCode;

    private String functionName;

    private Integer functionStatus;
}
