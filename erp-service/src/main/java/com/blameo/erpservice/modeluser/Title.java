package com.blameo.erpservice.modeluser;

import lombok.Data;


/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/

@Data
public class Title extends BaseEntity {
    private static final Long serialVersionUID = 1L;


    private String titleId;


    private String titleCode;


    private String titleName;


    private String titleDescription;


    private Integer titleStatus;

    public Title() {
        super();
    }

    public Title(String titleName) {
        this.titleName = titleName;
    }
}
