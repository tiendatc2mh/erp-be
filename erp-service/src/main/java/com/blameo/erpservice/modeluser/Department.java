package com.blameo.erpservice.modeluser;

import lombok.Data;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data
public class Department extends BaseEntity{
    private static final Long serialVersionUID = 1L;


    private String departmentId;

    private String departmentName;

    private String departmentCode;

    private String departmentDescription;

    private Integer departmentStatus;

    public Department() {
    }

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }
}
