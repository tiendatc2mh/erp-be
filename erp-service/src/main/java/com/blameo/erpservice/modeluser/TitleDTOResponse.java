package com.blameo.erpservice.modeluser;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data

public class TitleDTOResponse {

    @NotNull
    private String titleId;

    @NotNull
    private String titleCode;

    @NotNull
    private String titleName;

    private String titleDescription;

    @NotNull
    private Integer titleStatus;

    
    private Date createdDate;
    
    private Date updatedDate;

    private String createdBy;
}
