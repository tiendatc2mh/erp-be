package com.blameo.erpservice.modeluser;


import lombok.Data;

import java.util.Date;

@Data
public class AssetTypeDTOResponse {

    private String assetTypeId;
    private String assetTypeCode;
    private String assetTypeName;
    private Integer assetTypeStatus;
    private String assetTypeDescription;
    private Integer isDefine;
    private Date createdDate;
    private Date updatedDate;
    private String createdBy;
    private String updatedBy;
    private String createdByName;
    private String updatedByName;

}
