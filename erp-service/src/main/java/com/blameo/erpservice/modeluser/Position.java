package com.blameo.erpservice.modeluser;

import lombok.Data;


@Data
public class Position extends BaseEntity  {


    private String positionId;


    private String positionCode;


    private String positionName;


    private Department department;

    private String positionDescription;

    private Integer positionStatus;

}
