package com.blameo.erpservice.modeluser;

import lombok.Data;

import java.util.Date;

@Data
public class AssetDTOResponse extends BaseEntityDTOResponse {

    private String assetId;
    private String assetCode;
    private String assetName;
    private AssetTypeDTOResponse assetType;
    private Integer assetStatus;
    private UserDTOResponse user;
    private String assetSupplier;
    private Date assetIncreaseDate;
    private String assetBillCode;
    private Date assetWarrantyTime;
    private Integer assetYearManufacture;
    private String assetManufacture;
    private String assetUses;
    private String assetSerial;
    private Date assetDate;
}
