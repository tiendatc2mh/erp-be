package com.blameo.erpservice.modeluser;

import com.blameo.erpservice.modeluser.DepartmentDTOResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Data
public class LevelDTOResponse {

    private String levelId;

    private String levelName;

    private String departmentId;

    private DepartmentDTOResponse department;

    private Double coefficients;

    private String levelDescription;

    private Integer levelStatus;

    private String levelCode;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;


}
