package com.blameo.erpservice.modeluser;

import lombok.Data;

import java.util.Date;

@Data
public class BaseEntityDTOResponse {
    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;
}
