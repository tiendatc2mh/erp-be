package com.blameo.erpservice.modeluser;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data

public class PositionDTOResponse {

    @NotNull
    private String positionId;

    @NotNull
    private String positionCode;

    @NotNull
    private String positionName;

    @NotNull
    private Department department;

    private String positionDescription;

    @NotNull
    private Integer positionStatus;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;


}
