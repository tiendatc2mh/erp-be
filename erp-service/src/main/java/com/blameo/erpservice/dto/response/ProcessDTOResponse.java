package com.blameo.erpservice.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class ProcessDTOResponse {

    private String processId;

    private Integer processType;

    private Integer processStatus;

    private String processName;

    private String processDescription;

    private Integer isDefault;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;

}
