package com.blameo.erpservice.dto.response;

import lombok.Data;
import java.util.Date;

@Data
public class AttachmentDTOResponse {

    private String attachmentId;

    private String attachmentName;

    private String attachmentUrl;

    private Integer attachmentStatus;

    private Integer attachmentType;

    private String objectId;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

}
