package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationDTO {

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 1000, message = "{validate.length}")
    private String message;

    @NotNull(message="{validate.notNull}")
    private String requestId;

    @NotNull(message="{validate.notNull}")
    private Integer requestStatus;

    @NotNull(message="{validate.notNull}")
    private Integer type;

    private String userRequestId;

    private String userReceiverId;

    public NotificationDTO(String message, String requestId, Integer requestStatus, Integer type, String userRequestId, String userReceiverId) {
        this.message = message;
        this.requestId = requestId;
        this.requestStatus = requestStatus;
        this.type = type;
        this.userRequestId = userRequestId;
        this.userReceiverId = userReceiverId;
    }

    public NotificationDTO() {
    }
}
