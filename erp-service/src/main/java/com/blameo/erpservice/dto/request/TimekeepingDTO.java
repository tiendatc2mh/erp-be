package com.blameo.erpservice.dto.request;

import lombok.Data;

import java.util.Date;

@Data
public class TimekeepingDTO {

    private String userId;

    private Date timekeeppingDate;

    private Date arrivalTime;

    private Date backTime;

    public TimekeepingDTO(String userId, Date timekeeppingDate, Date arrivalTime, Date backTime) {
        this.userId = userId;
        this.timekeeppingDate = timekeeppingDate;
        this.arrivalTime = arrivalTime;
        this.backTime = backTime;
    }
}
