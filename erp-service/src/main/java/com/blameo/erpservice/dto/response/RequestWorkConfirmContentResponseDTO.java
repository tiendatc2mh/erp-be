package com.blameo.erpservice.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestWorkConfirmContentResponseDTO {

    private String requestWorktimeContentId;

    private Integer requestWorkTimeType;

    @JsonFormat(timezone = "GMT+7")
    private Date requestWorkTimeDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date requestWorkTimeCheckin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date requestWorkTimeCheckout;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date requestWorkTimeCheckinOld;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date requestWorkTimeCheckoutOld;

}
