package com.blameo.erpservice.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class WorkTimeInDayResponseDTO {

    private String workTimeId;

    private Date workTimeStartTimeAm;

    private Date workTimeEndTimeAm;

    private Date workTimeStartApply;

    private Date workTimeEndApply;

    private Date workTimeEndTimePm;

    private Date workTimeStartTimePm;

    private Date workTimeLateAllow;

    private Integer workTimeLateAllowCount;

    private Integer type;
}
