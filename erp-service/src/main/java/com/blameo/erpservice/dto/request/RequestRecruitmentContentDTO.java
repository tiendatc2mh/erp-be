package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRecruitmentContentDTO {

    private String requestRecruitmentContentId;

    @NotNull(message="{validate.notNull}")
    private String positionId;

    @NotNull(message="{validate.notNull}")
    private String levelId;

    @Size(max = 500, message = "{validate.maxLength}")
    private String reason;

    @NotNull(message="{validate.notNull}")
    private Integer quantity;

    @NotNull(message="{validate.notNull}")
    private Date onboardTimeStart;

    @NotNull(message="{validate.notNull}")
    private Date onboardTimeEnd;

}
