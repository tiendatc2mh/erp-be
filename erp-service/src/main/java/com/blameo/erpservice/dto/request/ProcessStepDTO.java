package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessStepDTO {
    @NotNull(message="{validate.notNull}")
    private String processStepCode;

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 100, message = "{validate.length}")
    private String processStepName;

    private String roleId;

    @Size(max = 500, message = "{validate.maxLength}")
    private String processStepDescription;

    @NotNull(message="{validate.notNull}")
    private Integer processStepHandleTime;

    @NotNull(message="{validate.notNull}")
    private String processStepId;

    @NotNull(message="{validate.notNull}")
    private String processStepNext;
}
