package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRemoteContentDTO {

    private String requestWorktimeContentId;

    @NotNull(message="{validate.notNull}")
    private int remoteType;

    @NotNull(message="{validate.notNull}")
    private Date remoteDate;

    @NotNull(message="{validate.notNull}")
    private Double countHour;

}
