package com.blameo.erpservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRemoteContentResponseDTO {

    private String requestWorktimeContentId;

    private int remoteType;

    private Date remoteDate;

    private Double countHour;

}
