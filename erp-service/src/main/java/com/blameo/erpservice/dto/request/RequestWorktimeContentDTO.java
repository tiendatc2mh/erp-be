package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.Column;
import java.sql.Time;
import java.util.Date;

@Data
public class RequestWorktimeContentDTO {

    private String requestWorktimeContentId;

    private Date requestWorkTimeDate;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date requestWorkTimeCheckin;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date requestWorkTimeCheckout;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date requestWorkTimeCheckinOld;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date requestWorkTimeCheckoutOld;

    private Integer requestWorkTimeType;

    private Integer requestDayOffType;

    private Integer dayOffType;

    private String requestDayOffUserTransfer;

    private Date dayOffBegin;

    private Date dayOffEnd;

    private int remoteType;

    private Double countHour;

    private Date remoteDate;

    private Integer requestOtType;

    private String requestOtProject;

    private Date requestOtDate;

    private Date requestOtBegin;

    private Date requestOtEnd;

    private Double countDaySalary;
}
