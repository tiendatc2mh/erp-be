package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.modeluser.RoleDTOResponse;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class ProcessStepDTOResponse {
    private String processStepId;

    private String processStepCode;

    private String processStepName;

    private RoleDTOResponse role;

    private Integer processStepDescription;

    private Integer processHandleTime;

    private String processStepNext;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;
}
