package com.blameo.erpservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestDayOffContentResponseDTO {

    private String requestWorktimeContentId;

    private Date dayOffBegin;

    private Date dayOffEnd;

    private Double countHour;

    private Integer dayOffType;

    private Integer requestDayOffType;

    private String requestDayOffUserTransfer;

    private String requestDayOffUserTransferFullname;
}
