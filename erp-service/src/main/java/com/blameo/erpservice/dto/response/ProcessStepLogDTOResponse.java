package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.modeluser.UserDTOResponse;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class ProcessStepLogDTOResponse {
    private String processStepLogId;

    private String requestId;

    private Integer processStepStatus;

    private String processConfigId;

    private String receiveUserId;

    private UserDTOResponse handlerUser;

    private Date handleDate;

}
