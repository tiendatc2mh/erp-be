package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.modeluser.LevelDTOResponse;
import com.blameo.erpservice.modeluser.PositionDTOResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRecruitmentContentResponseDTO {

    private String requestRecruitmentContentId;

    private PositionDTOResponse position;

    private String positionId;

    private LevelDTOResponse level;

    private String levelId;

    private String reason;

    private Integer quantity;

    private Integer recruitmentSource;

    private Integer recruitmentCost;

    private Integer recruitmentForm;

    private Date recruitmentCompleteDate;

    private Date onboardTimeStart;

    private Date onboardTimeEnd;

}
