package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.dto.request.RequestBuyEquipmentContentDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestLogDTOResponse {
    private String requestLogId;

    private String requestId;

    private Date requestLogDate;

    private String requestLogUser;

    private String requestLogUserFullname;

    private Integer requestLogStatus;

    private Integer requestLogAction;


}
