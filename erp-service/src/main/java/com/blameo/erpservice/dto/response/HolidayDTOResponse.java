package com.blameo.erpservice.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class HolidayDTOResponse {

    private String holidayId;

    private Date holidayDate;

    private double holidayCoefficient;

    private String holidayDescription;

}
