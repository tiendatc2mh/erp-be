package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRecruitmentPerformDTO {

    @NotNull(message="{validate.notNull}")
    private String requestRecruitmentContentId;

    @NotNull(message="{validate.notNull}")
    private Integer recruitmentSource;

    @NotNull(message="{validate.notNull}")
    private Integer recruitmentCost;

    @NotNull(message="{validate.notNull}")
    private Integer recruitmentForm;

    @NotNull(message="{validate.notNull}")
    private Date recruitmentCompleteDate;

    @NotNull(message="{validate.notNull}")
    private Integer isComplete;

    private String reason;

    private List<AttachmentDTO> listAttachmentResult;

    private List<RequestRecruitmentUserContentDTO> recruitmentUserContent;

}
