package com.blameo.erpservice.dto.request;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Data
public class RequestWorktimeOtContentDTO {

    private String requestWorktimeContentId;

    @NotNull(message="{validate.notNull}")
    private Integer requestOtType;

    @NotNull(message="{validate.notNull}")
    private String requestOtProject;

    @NotNull(message="{validate.notNull}")
    private Date requestOtDate;

    @NotNull(message="{validate.notNull}")
    private Date requestOtBegin;

    @NotNull(message="{validate.notNull}")
    private Date requestOtEnd;

    @NotNull(message="{validate.notNull}")
    private Double countHour;
}
