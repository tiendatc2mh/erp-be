package com.blameo.erpservice.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class WorkTimeInDayDTOResponse {

    private String workTimeId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date workTimeStartTimeAm;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date workTimeEndTimeAm;

    private Date workTimeStartApply;

    private Date workTimeEndApply;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date workTimeEndTimePm;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date workTimeStartTimePm;
}
