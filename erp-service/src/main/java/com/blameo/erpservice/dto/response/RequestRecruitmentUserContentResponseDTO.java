package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.dto.request.AttachmentDTO;
import com.blameo.erpservice.modeluser.LevelDTOResponse;
import com.blameo.erpservice.modeluser.PositionDTOResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRecruitmentUserContentResponseDTO {

    private String requestRecruitmentUserContentId;

    private String requestRecruitmentUsername;

    private LevelDTOResponse requestRecruitmentUserLevel;

    private String requestRecruitmentUserLevelId;

    private Double requestRecruitmentUserSalary;

    private Date requestRecruitmentUserStartTime;

    private List<AttachmentDTOResponse> listAttachment;

}
