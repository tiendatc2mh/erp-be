package com.blameo.erpservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestBuyEquipmentContentResponseDTO {

    private String requestPayContentId;

    private String assetTypeId;

    private String assetTypeName;

    private Integer quantity;

    private String requestPayContentDescription;

    private Integer requestPayContentStatus;
}
