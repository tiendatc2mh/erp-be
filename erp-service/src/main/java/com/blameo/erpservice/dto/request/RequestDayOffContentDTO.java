package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestDayOffContentDTO {

    private String requestWorktimeContentId;

    @NotNull(message="{validate.notNull}")
    private Date dayOffBegin;

    @NotNull(message="{validate.notNull}")
    private Date dayOffEnd;

    @NotNull(message="{validate.notNull}")
    private Double countHour;

    @NotNull(message="{validate.notNull}")
    private Integer dayOffType;

    @NotNull(message="{validate.notNull}")
    private Integer requestDayOffType;

    private String requestDayOffUserTransfer;

}
