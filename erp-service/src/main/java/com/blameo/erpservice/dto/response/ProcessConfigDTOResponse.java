package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.model.ProcessStep;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ProcessConfigDTOResponse {
    private String processConfigId;

    private ProcessStep processStep;

    private ProcessStep processStepNext;

    private Integer processStepType;
}
