package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestPaymentContentResponseDTO {

    private String requestPayContentId;

    private String requestPayContentDescription;

    private Double unitPrice;

    private Set<Request> requestAdvance;

    private Double remainingMoney;

    private Double receivedMoney;

    private Double advancedMoney;

    private Integer requestPayContentStatus;

    private String payContent;
}
