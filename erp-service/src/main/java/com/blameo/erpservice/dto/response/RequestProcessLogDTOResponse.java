package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.modeluser.UserDTOResponse;
import lombok.Data;

import java.util.Date;
@Data
public class RequestProcessLogDTOResponse {
    private String requestProcessLogId;

    private String requestId;

    private Integer requestProcessLogStatus;

    private Date handleDate;

    private String rejectReason;

    private UserDTOResponse handlerUser;

    private String receiveUserId;

    private String sendUserId;
}
