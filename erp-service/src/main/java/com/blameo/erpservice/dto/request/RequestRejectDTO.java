package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRejectDTO {

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 500, message = "{validate.length}")
    private String rejectReason;

}
