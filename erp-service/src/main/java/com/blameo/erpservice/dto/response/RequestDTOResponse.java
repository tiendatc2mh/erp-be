package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.dto.request.*;
import com.blameo.erpservice.model.Process;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestDTOResponse {
    private String requestId;

    private Integer requestStatus;

    private Integer requestStatusProcess;

    private String requestCode;

    private Process process;

    private String content;

    private String reason;

    private String description;

    private String action;

    private String roleId;

    private String requestApprove;

    private String requestApproveName;

    private String departmentId;

    private String departmentName;

    private String positionName;

    private Integer payType;

    private String handlerUser;

    private String bankAccountNumber;

    private String bankType;

    private Integer dayOffMarryType;

    private String handoverUser;

    private Date payDate;

    private String accountOwner;

    private String bank;

    private String accountNumber;

    private Double dayOffLastYear;

    private Double dayOffCurrentYear;

    private List<AttachmentDTOResponse> listAttachment;

    private List<AttachmentDTOResponse> listAttachmentResult;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;

    private String handoverUsername;

    private Set<RequestDayOffContentResponseDTO> requestDayOffContents;

    private Set<RequestRemoteContentResponseDTO> requestRemoteContents;

    private Set<RequestBuyEquipmentContentResponseDTO> requestBuyEquipmentContents;

    private Set<RequestPaymentContentResponseDTO> requestPaymentContents;

    private Set<RequestWorkConfirmContentResponseDTO> requestWorkConfirmContent;

    private Set<RequestWorktimeContentDTO> requestWorktimeContents;

    private Set<RequestWorktimeOtContentResponseDTO> requestWorktimeOtContent;

    private Set<RequestRecruitmentContentResponseDTO> requestRecruitmentContent;

    private Set<RequestRecruitmentUserContentResponseDTO> requestRecruitmentUserContent;

}
