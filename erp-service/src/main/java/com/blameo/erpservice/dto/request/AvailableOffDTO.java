package com.blameo.erpservice.dto.request;

import lombok.Data;

import java.util.Date;

@Data
public class AvailableOffDTO {

    private String userId;

    private Double dayOffLastYear;

    private Double dayOffCurrentYear;

    public AvailableOffDTO(String userId, Double dayOffLastYear, Double dayOffCurrentYear) {
        this.userId = userId;
        this.dayOffLastYear = dayOffLastYear;
        this.dayOffCurrentYear = dayOffCurrentYear;
    }
}
