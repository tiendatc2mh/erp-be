package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestWorkConfirmContentDTO {

    private String requestWorktimeContentId;

    @NotNull(message="{validate.notNull}")
    private Integer requestWorkTimeType;

    @NotNull(message="{validate.notNull}")
    private Date requestWorkTimeDate;

    private Date requestWorkTimeCheckin;

    private Date requestWorkTimeCheckout;

    private Date requestWorkTimeCheckinOld;

    private Date requestWorkTimeCheckoutOld;

}
