package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessConfigDTO {
    @NotNull(message="{validate.notNull}")
    private String processConfigId;

    @NotNull(message="{validate.notNull}")
    private String processStepId;

    @NotNull(message="{validate.notNull}")
    private String processStepNext;

    @NotNull(message="{validate.notNull}")
    private String processTypeId;

    @NotNull(message="{validate.notNull}")
    private Integer processStepType;

}
