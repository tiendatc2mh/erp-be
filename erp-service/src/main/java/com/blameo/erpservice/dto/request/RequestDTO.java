package com.blameo.erpservice.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
public class RequestDTO {

    private String requestId;

    @Size(max = 50, message = "{validate.maxLength}")
    private String requestCode;

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 500, message = "{validate.length}")
    private String content;

    private Integer payType;

    private Integer dayOffMarryType;

    @Size(max = 500, message = "{validate.maxLength}")
    private String description;

    private String handoverUser;

    private Date payDate;

    @NotNull(message="{validate.notNull}")
    private String receiveUserId;

    @NotNull(message="{validate.notNull}")
    private Integer isSend;

    @Size(min = 1, max = 500, message = "{validate.length}")
    private String reason;

    @NotNull(message="{validate.notNull}")
    private Integer processType;

    @Size(max = 100, message = "{validate.maxLength}")
    private String accountOwner;

    @Size(max = 100, message = "{validate.maxLength}")
    private String accountNumber;

    @Size(max = 100, message = "{validate.maxLength}")
    private String bank;

    private List<AttachmentDTO> listAttachment;

    private Set<RequestDayOffContentDTO> requestDayOffContents;

    private Set<RequestRemoteContentDTO> requestRemoteContents;

    private Set<RequestBuyEquipmentContentDTO> requestBuyEquipmentContents;

    private Set<RequestPaymentContentDTO> requestPaymentContents;

    private Set<RequestWorkConfirmContentDTO> requestWorkConfirmContent;

    private Set<RequestWorktimeOtContentDTO> requestWorktimeOtContent;

    private Set<RequestRecruitmentContentDTO> requestRecruitmentContent;

}
