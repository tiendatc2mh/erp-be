package com.blameo.erpservice.dto.request;

import com.blameo.erpservice.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestPaymentContentDTO {

    private String requestPayContentId;

    @Size(max = 500, message = "{validate.maxLength}")
    private String requestPayContentDescription;

    private Double unitPrice;

    private Set<String> requestAdvance;

    private Double remainingMoney;

    private Double advancedMoney;

    private Integer requestPayContentStatus;

    private String payContent;
}
