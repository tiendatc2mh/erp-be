package com.blameo.erpservice.dto.response;

import lombok.Data;

@Data
public class AvaiableDayOffResponse {

    private String availableDayOffId;

    private String userId;

    private Integer month;

    private Integer year;

    private Double availableDayOffOld;

    private Double availableDayOff;

}
