package com.blameo.erpservice.dto.request;

import com.blameo.erpservice.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class RequestRecruitmentUserContentDTO {

    private String requestRecruitmentUserContentId;

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 100, message = "{validate.length}")
    private String requestRecruitmentUsername;

    @NotNull(message="{validate.notNull}")
    private String requestRecruitmentUserLevelId;

    @NotNull(message="{validate.notNull}")
    private Double requestRecruitmentUserSalary;

    @NotNull(message="{validate.notNull}")
    private Date requestRecruitmentUserStartTime;

    private List<AttachmentDTO> listAttachment;

}
