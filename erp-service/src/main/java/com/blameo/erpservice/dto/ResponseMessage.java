package com.blameo.erpservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

public class ResponseMessage implements Serializable {

    @JsonProperty("success")
    private boolean success;
    @JsonProperty("message")
    private String message;
    @JsonProperty("data")
    private Object data;

    public ResponseMessage(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public ResponseMessage(@JsonProperty("success") boolean success,@JsonProperty("message") String message,@JsonProperty("data") Object data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public static ResponseMessage ok(String message) {
        return new ResponseMessage(true, message);
    }

    public static ResponseMessage error(String message) {
        return new ResponseMessage(false, message);
    }

    public static ResponseMessage ok(String message, Object data) {
        return new ResponseMessage(true, message, data);
    }

    public static ResponseMessage error(String message, Object data) {
        return new ResponseMessage(false, message, data);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResponseMessage() {
    }
}
