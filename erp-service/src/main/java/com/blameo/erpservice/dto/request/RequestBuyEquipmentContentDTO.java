package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestBuyEquipmentContentDTO {

    private String requestPayContentId;

    @NotNull(message="{validate.notNull}")
    private String assetTypeId;

    @NotNull(message="{validate.notNull}")
    private Integer quantity;

    @Size(max = 500, message = "{validate.maxLength}")
    private String requestPayContentDescription;

    private Integer requestPayContentStatus;
}
