package com.blameo.erpservice.dto.response;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;


@Data
public class RequestWorktimeOtContentResponseDTO {

    private String requestWorktimeContentId;

    private Integer requestOtType;

    private String requestOtProject;

    private Date requestOtDate;

    private Date requestOtBegin;

    private Date requestOtEnd;

    private Double countHour;
}
