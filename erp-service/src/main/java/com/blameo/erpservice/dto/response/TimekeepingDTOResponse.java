package com.blameo.erpservice.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;

@Data
@NoArgsConstructor
public class TimekeepingDTOResponse {


    private String timekeepingId;

    private String userId;

    private Integer workShift;

    private Date timekeepingDate;

    private Integer timekeepingDateStr;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date arrivalTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Date backTime;

    private String workingTime;

    private String overTime;

    private String lateTime;

    private String earlyTime;

    private List<Integer> note = new ArrayList<>();

    private String updatedBy;

    private WorkTimeInDayDTOResponse workTimeInDay;

    private Date arrivalRemoteTime;

    private Date backRemoteTime;

    private String remoteTime;
}
