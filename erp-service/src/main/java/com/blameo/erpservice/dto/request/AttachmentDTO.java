package com.blameo.erpservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AttachmentDTO {

    private String attachmentId;

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, message = "{validate.maxLength}")
    private String attachmentName;

    @NotNull(message="{validate.notNull}")
    @Size(max = 500, message = "{validate.maxLength}")
    private String attachmentUrl;

    @NotNull(message="{validate.notNull}")
    private Integer attachmentStatus;

    @NotNull(message="{validate.notNull}")
    private Integer attachmentType;
}
