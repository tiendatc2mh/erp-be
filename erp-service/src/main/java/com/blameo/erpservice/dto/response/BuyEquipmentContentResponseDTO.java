package com.blameo.erpservice.dto.response;

import com.blameo.erpservice.modeluser.AssetDTOResponse;
import com.blameo.erpservice.modeluser.AssetTypeDTOResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BuyEquipmentContentResponseDTO {

    private String assetTypeId;

    private AssetTypeDTOResponse assetType;

    private String requestPayContentDescription;

    private AssetDTOResponse asset;

    private Integer quantity;
}
