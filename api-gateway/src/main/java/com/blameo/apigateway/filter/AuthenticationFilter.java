package com.blameo.apigateway.filter;


//import com.blameo.apigateway.util.JwtUtil;
import com.blameo.apigateway.model.ResponseMessage;
import com.blameo.apigateway.model.UserDTOResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.*;

@Component
public class AuthenticationFilter extends AbstractGatewayFilterFactory<AuthenticationFilter.Config> {

    @Autowired
    private RouteValidator validator;

    @Autowired
    ObjectMapper objectMapper;


    @Value("${ip}")
    private String ip;
    @Value("${port-id-service}")
    private String portId;
    @Value("${path-auth}")
    private String pathCheckAuth;


    private RestTemplate template = new RestTemplate();

//    @Autowired
//    private JwtUtil jwtUtil;

    Log log = LogFactory.getLog(getClass());

    public AuthenticationFilter() {
        super(Config.class);
    }

    public static class Config {
    }

    @Override
    public GatewayFilter apply(Config config) {
        return ((exchange, chain) -> {
            log.info("Incoming request " + exchange.getRequest().getPath() + " is routed to id: " + exchange.getAttribute(GATEWAY_PREDICATE_MATCHED_PATH_ROUTE_ID_ATTR));
            ServerHttpRequest request = exchange.getRequest().mutate()
                    .build();
            if (validator.isSecured.test(exchange.getRequest()) && !request.getPath().value().contains("/ws/")) {
                //header contains token or not
                if (!exchange.getRequest().getHeaders().containsKey(HttpHeaders.AUTHORIZATION)) {
                    throw new RuntimeException("Missing authorization header");
                }

                //add ACCEPT_LANGUAGE
                String acceptLanguage = "vn";
                if (exchange.getRequest().getHeaders().containsKey(HttpHeaders.ACCEPT_LANGUAGE)
                        && Objects.requireNonNull(exchange.getRequest().getHeaders().get(HttpHeaders.ACCEPT_LANGUAGE)).size()>0
                        && Objects.requireNonNull(exchange.getRequest().getHeaders().get(HttpHeaders.ACCEPT_LANGUAGE)).get(0).equals("en")) {
                    acceptLanguage = "en";
                }

                String authHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
                if (authHeader != null && authHeader.startsWith("Bearer ")) {
                    authHeader = authHeader.substring(7);
                }
                try {
//                    //REST call to AUTH service
                    UserDTOResponse user = template.getForObject(ip + ":" + portId + pathCheckAuth + "?token=" + authHeader + "&acceptLanguage=" + acceptLanguage + "&path=" + replaceUUIDWithPlaceholder(exchange.getRequest().getPath().value()) + "&method="+exchange.getRequest().getMethod(), UserDTOResponse.class);
                    System.out.println(user);
                    request = exchange.getRequest().mutate()
                            .header("user", Arrays.toString(objectMapper.writeValueAsString(user).getBytes(StandardCharsets.UTF_8)))
                            .build();
                } catch (Exception e) {
                    log.debug("invalid access...!");
                    String message = e.getMessage();
                    String[] value = e.getMessage().split(":",2);
                    ResponseMessage errorResponse = null;
                    try {
                        errorResponse = objectMapper.readValue(value[1].trim().substring(1,value[1].trim().length()-1), ResponseMessage.class);
                        ResponseEntity<ResponseMessage> responseEntity =   ResponseEntity.status(HttpStatus.valueOf(Integer.parseInt(value[0].trim()))).body(errorResponse);
                        // Send the error response entity to the client
                        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
                        exchange.getResponse().setStatusCode(HttpStatus.valueOf(Integer.parseInt(value[0].trim())));
                        // Convert response body to bytes
                        byte[] responseBytes = new ObjectMapper().writeValueAsBytes(responseEntity.getBody());

                        // Create a DataBuffer with the proper encoding
                        DataBuffer buffer = new DefaultDataBufferFactory().wrap(responseBytes);
//                        return exchange.getResponse().writeWith(Mono.just(exchange.getResponse().bufferFactory().wrap(objectMapper.writeValueAsBytes(responseEntity.getBody()))));
                        return exchange.getResponse().writeWith(Mono.just(buffer));
                    } catch (JsonProcessingException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            }

            // Upgrade the request to WebSocket if conditions are met
            if (exchange.getRequest().getHeaders().containsKey("Upgrade") &&
                    exchange.getRequest().getHeaders().get("Upgrade").get(0).equalsIgnoreCase("websocket")) {
                return chain.filter(exchange.mutate().request(exchange.getRequest().mutate().header("Sec-WebSocket-Version", "13").build()).build());
            }
            return chain.filter(exchange.mutate().request(request).build());
        });
    }

    public  String replaceUUIDWithPlaceholder(String url) throws UnsupportedEncodingException {
        return URLEncoder.encode(url, StandardCharsets.UTF_8);
    }

    private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus){
        ServerHttpResponse httpResponse = exchange.getResponse();
        httpResponse.setStatusCode(httpStatus);
        return httpResponse.setComplete();
    }


}
