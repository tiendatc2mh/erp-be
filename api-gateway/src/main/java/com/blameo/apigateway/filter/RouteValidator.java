package com.blameo.apigateway.filter;


import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;

@Component
public class RouteValidator {

    public static final List<String> openApiEndpoints = List.of(
            "/api/id/sign-in",
            "/api/id/refresh-token",
            "/api/id/sign-up",
            "/api/erp/swagger",
            "/api/erp/v3/api-docs",
            "/api/erp/v3/api-docs/swagger-config",
            "/api/id/swagger",
            "/api/id/v3/api-docs",
            "/api/id/v3/api-docs/swagger-config",
            "/api/worktime/swagger",
            "/api/worktime/v3/api-docs",
            "/api/worktime/v3/api-docs/swagger-config",
            "/api/id/user/schedule",
            "/api/erp/request/schedule",
            "/eureka"
    );

    public Predicate<ServerHttpRequest> isSecured =
            request -> openApiEndpoints
                    .stream()
                    .noneMatch(uri -> request.getURI().getPath().contains(uri));

}
