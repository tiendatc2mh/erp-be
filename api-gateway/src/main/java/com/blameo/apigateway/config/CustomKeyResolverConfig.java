package com.blameo.apigateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/27/2023, Thursday
 **/
@Configuration
public class CustomKeyResolverConfig {

    @Bean
    public CustomKeyResolver customKeyResolver() {
        return new CustomKeyResolver();
    }
}