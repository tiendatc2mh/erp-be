package com.blameo.apigateway.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/27/2023, Thursday
 **/
public class CustomKeyResolver implements KeyResolver {
    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        String ipAddress = exchange.getRequest().getRemoteAddress().getAddress().getHostAddress();
        return Mono.just(ipAddress);
    }
}
