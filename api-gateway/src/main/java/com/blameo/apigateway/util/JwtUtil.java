//package com.blameo.apigateway.util;
//
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.io.Decoders;
//import io.jsonwebtoken.security.Keys;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import java.security.Key;
//
//@Component
//public class JwtUtil {
//
//
//    @Value("${blameo.app.jwtSecret}")
//    public String SECRET;
//
//
//    public Boolean validateToken(final String token) {
//        boolean returnValue = true;
//
//        String subject =  Jwts.parserBuilder().setSigningKey(getSignKey()).build().parseClaimsJws(token).getBody().getSubject();
//        if(subject == null || subject.isEmpty()) {
//            returnValue = false;
//        }
//        return returnValue;
//    }
//
//
//
//    private Key getSignKey() {
//        byte[] keyBytes = Decoders.BASE64.decode(SECRET);
//        return Keys.hmacShaKeyFor(keyBytes);
//    }
//}
