package com.blameo.apigateway.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleDTOResponse {

    private String roleId;


    private String roleName;


    private String roleCode;


    private Integer roleStatus;

    
    private Date createdDate;


    private Date updatedDate;

    private String createdBy;


    private Set<PermissionDTOResponse> permissions;

}
