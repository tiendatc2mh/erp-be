package com.blameo.apigateway.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Data
public class Position {


    private String positionId;


    private String positionCode;


    private String positionName;


    private Department department;

    private String positionDescription;

    private Integer positionStatus;

}
