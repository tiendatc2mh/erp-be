package com.blameo.apigateway.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;


@Data
public class PermissionDTOResponse {

    private String permissionId;

    private String permissionCode;


    private String path;


    private String method;


    private Integer permissionStatus;

    
    private Date createdDate;

    
    private Date updatedDate;

    private String createdBy;
}
