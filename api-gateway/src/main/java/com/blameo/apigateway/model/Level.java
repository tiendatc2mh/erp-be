package com.blameo.apigateway.model;

import lombok.*;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/2/2023, Wednesday
 **/
@Data
public class Level  {
    private static final Long serialVersionUID = 1L;


    private String levelId;

    private String levelName;

    private Department department;

    private Double coefficients;

    private String levelDescription;

    private Integer levelStatus;

    private String levelCode;
}
