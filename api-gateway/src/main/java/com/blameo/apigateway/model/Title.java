package com.blameo.apigateway.model;

import lombok.*;


/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/

@Data
public class Title  {
    private static final Long serialVersionUID = 1L;


    private String titleId;


    private String titleCode;


    private String titleName;


    private String titleDescription;


    private Integer titleStatus;

}
