package com.blameo.idservice.utils;//package com.blameo.erpservice.utils;
//
//import com.blameo.cvdb.constant.Constant;
//import com.itextpdf.text.*;
//import com.itextpdf.text.pdf.BaseFont;
//import com.itextpdf.text.pdf.PdfPCell;
//import com.itextpdf.text.pdf.PdfPTable;
//import com.itextpdf.text.pdf.PdfWriter;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.*;
//import java.util.stream.Collectors;
//
//public class PdfUtil {
//
//
//
//    public static InputStream getPdfType2And3And7(Map<String,Object> outputListKN, Long exportType, Long nhom) throws DocumentException, IOException {
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        Document document = new Document(PageSize.A4);
//        PdfWriter.getInstance(document, outputStream);
//        document.open();
//        document.setPageSize(PageSize.A4.rotate());
//        document.newPage();
//
//        // Set UTF-8 font
//        BaseFont baseFont = BaseFont.createFont("/font/RobotoSlab-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//        Font vietnameseFont = new Font(baseFont);
//
//        Font vietnameseFontBold = new Font(baseFont);
//        vietnameseFontBold.setStyle(Font.BOLD);
//
//        Font vietnameseFontBItalic = new Font(baseFont, 10, Font.ITALIC);
//
//        List<String> listKey = new ArrayList<>();
//        List<String> listHeader = new ArrayList<>();
//        List<Float> listWidth = new ArrayList<>();
//        Integer tongSoCot = 0;
//
//        if(exportType.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.KET_QUA_DA_GIAI_QUYET.getValue())) {
//            if(nhom.equals(Constant.DMNhom.BAN_HANH_VAN_BAN.getValue())){
//                Paragraph paragraph2 = new Paragraph("Danh mục ..... kiến nghị cử tri gửi tới tới kỳ họp thứ 4 và kỳ họp bất thường lần thứ 2,", vietnameseFontBold);
//                paragraph2.setAlignment(Element.ALIGN_CENTER);
//                paragraph2.setSpacingAfter(10);
//                document.add(paragraph2);
//
//                Paragraph paragraph3 = new Paragraph("Quốc hội khóa XV đã giải quyết", vietnameseFontBold);
//                paragraph3.setAlignment(Element.ALIGN_CENTER);
//                document.add(paragraph3);
//
//                Paragraph paragraph3_1 = new Paragraph("(Kèm theo Báo cáo số ....../BC-...... ngày   /  của ..........)", vietnameseFontBItalic);
//                paragraph3_1.setAlignment(Element.ALIGN_CENTER);
//                paragraph3_1.setSpacingAfter(10);
//                document.add(paragraph3_1);
//
//                Paragraph paragraph4 = new Paragraph("BẢNG 1: Ban hành văn bản", vietnameseFontBold);
//                paragraph4.setAlignment(Element.ALIGN_CENTER);
//                paragraph4.setSpacingAfter(30);
//                document.add(paragraph4);
//
//
//                listKey = Arrays.asList("sttVB", "noiDungKN_DP", "coQuanBanHanh", "stt", "noiDungKN_DP", "so_KyHieuCVTL");
//                listHeader = Arrays.asList("STTVB", "Tên văn bản ban hành", "Cơ quan ban hành/trình", "STT", "Nội dung kiến nghị/địa phương", "Số, ký hiệu CV trả lời");
//                listWidth = Arrays.asList(9F, 18F, 18F, 5F, 30F, 18F);
//                tongSoCot = listKey.size();
//
//            }else if(nhom.equals(Constant.DMNhom.THANH_TRA_KIEM_TRA.getValue())){
//                Paragraph paragraph2 = new Paragraph("BẢNG 2: Thanh tra, kiểm tra", vietnameseFontBold);
//                paragraph2.setAlignment(Element.ALIGN_CENTER);
//                paragraph2.setSpacingAfter(30);
//                document.add(paragraph2);
//
//                listKey = Arrays.asList("stt", "noiDungKN_DP", "coQuanBanHanh", "so_KyHieuCVTL");
//                listHeader = Arrays.asList("STT", "Nội dung kiến nghị/địa phương", "Cơ quan thanh tra, kiểm tra", "Số, ký hiệu CV trả lời");
//                listWidth = Arrays.asList(5F, 40F, 30F, 25F);
//                tongSoCot = listKey.size();
//            } else if (nhom.equals(Constant.DMNhom.TO_CHUC_THUC_HIEN.getValue())) {
//                Paragraph paragraph2 = new Paragraph("BẢNG 3: Tổ chức thực hiện chính sách pháp luật", vietnameseFontBold);
//                paragraph2.setAlignment(Element.ALIGN_CENTER);
//                paragraph2.setSpacingAfter(30);
//                document.add(paragraph2);
//
//                listKey = Arrays.asList("stt", "noiDungKN_DP", "coQuanBanHanh", "so_KyHieuCVTL");
//                listHeader = Arrays.asList("STT", "Nội dung kiến nghị/địa phương", "Cơ quan tổ chức, thực hiện", "Số, ký hiệu CV trả lời");
//                listWidth = Arrays.asList(5F, 40F, 30F, 25F);
//                tongSoCot = listKey.size();
//            }
//        } else if(exportType.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.DUOC_NGHIEN_CUU_TIEP_THU_GIAI_QUYET.getValue())){
//            if(nhom.equals(Constant.DMNhom.DA_NGHIEN_CUU_DA_GIAI_TRINH_CP_BAN_HANH_VB.getValue())){
//                Paragraph paragraph1 = new Paragraph("Phụ lục số 1.2", vietnameseFontBold);
//                paragraph1.setAlignment(Element.ALIGN_CENTER);
//                paragraph1.setSpacingAfter(10);
//                document.add(paragraph1);
//
//                Paragraph paragraph2 = new Paragraph("Danh mục ..... kiến nghị cử tri gửi tới tới kỳ họp thứ ..... và kỳ họp bất thường lần thứ .....,", vietnameseFontBold);
//                paragraph2.setAlignment(Element.ALIGN_CENTER);
//                paragraph2.setSpacingAfter(10);
//                document.add(paragraph2);
//
//                Paragraph paragraph3 = new Paragraph("Quốc hội khóa ....... ", vietnameseFontBold);
//                paragraph3.setSpacingAfter(10);
//                paragraph3.setAlignment(Element.ALIGN_CENTER);
//                document.add(paragraph3);
//
//                Paragraph paragraph3_2 = new Paragraph("được nghiên cứu, tiếp thu giải quyết trong thời gian tới ", vietnameseFontBold);
//                paragraph3_2.setAlignment(Element.ALIGN_CENTER);
//                paragraph3_2.setSpacingAfter(10);
//                document.add(paragraph3_2);
//
//                Paragraph paragraph3_1 = new Paragraph("(Kèm theo Báo cáo số ....../BC-...... ngày   /  của ..........)", vietnameseFontBItalic);
//                paragraph3_1.setAlignment(Element.ALIGN_CENTER);
//                paragraph3_1.setSpacingAfter(10);
//                document.add(paragraph3_1);
//
//                Paragraph paragraph4 = new Paragraph("BẢNG 1: Các kiến nghị đã nghiên cứu trình CP ban hành văn bản trong thời gian tới", vietnameseFontBold);
//                paragraph4.setAlignment(Element.ALIGN_CENTER);
//                paragraph4.setSpacingAfter(30);
//                document.add(paragraph4);
//
//
//                listKey = Arrays.asList("stt", "noiDungKN_DP", "coQuanBanHanh", "so_KyHieuCVTL", "tenVB_NDTL");
//                listHeader = Arrays.asList("STT", "Nội dung kiến nghị/địa phương", "Cơ quan ban hành/trình", "Số, ký hiệu CV trả lời", "Tên văn bản/Nội dung công việc");
//                listWidth = Arrays.asList(10F, 25F, 20F, 25F, 20F);
//                tongSoCot = listKey.size();
//
//            }else if(nhom.equals(Constant.DMNhom.KN_DANG_GIAI_QUYET.getValue())){
//                Paragraph paragraph2 = new Paragraph("BẢNG 2: Các kiến nghị đang giải quyết(... kiến nghị)", vietnameseFontBold);
//                paragraph2.setAlignment(Element.ALIGN_CENTER);
//                paragraph2.setSpacingAfter(30);
//                document.add(paragraph2);
//
//                listKey = Arrays.asList("stt", "noiDungKN_DP", "coQuanBanHanh", "so_KyHieuCVTL", "tenVB_NDTL", "loTrinhGiaiQuyet");
//                listHeader = Arrays.asList("STT", "Nội dung kiến nghị/địa phương", "Cơ quan ban hành/trình/giải quyết", "Số, ký hiệu CV trả lời", "Tên văn bản/Nội dung công việc", "Lộ trình giải quyết");
//                listWidth = Arrays.asList(5F, 25F, 15F, 15F, 20F, 20F);
//                tongSoCot = listKey.size();
//            } else if (nhom.equals(Constant.DMNhom.KN_SE_GIAI_QUYET.getValue())) {
//                Paragraph paragraph2 = new Paragraph("BẢNG 3: Các kiến nghị sẽ giải quyết(... kiến nghị)", vietnameseFontBold);
//                paragraph2.setAlignment(Element.ALIGN_CENTER);
//                paragraph2.setSpacingAfter(30);
//                document.add(paragraph2);
//
//                listKey = Arrays.asList("stt", "noiDungKN_DP", "coQuanBanHanh", "so_KyHieuCVTL", "tenVB_NDTL", "loTrinhGiaiQuyet");
//                listHeader = Arrays.asList("STT", "Nội dung kiến nghị/địa phương", "Cơ quan ban hành/trình/thực hiện", "Số, ký hiệu CV trả lời", "Tên, loại văn bản về ban hành giải quyết kiến nghị/Công việc thực hiện", "Lộ trình giải quyết");
//                listWidth = Arrays.asList(5F, 25F, 15F, 15F, 20F, 20F);
//                tongSoCot = listKey.size();
//            }
//        } else if(exportType.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.CHUA_GIAI_QUYET_TU_KY_HOP_TRUOC_DA_GIAI_QUYET.getValue())) {
//            if(nhom.equals(Constant.DMNhom.BAN_HANH_VAN_BAN.getValue())){
//                Paragraph paragraph1 = new Paragraph("Phụ lục số 2.1", vietnameseFontBold);
//                paragraph1.setAlignment(Element.ALIGN_CENTER);
//                paragraph1.setSpacingAfter(10);
//                document.add(paragraph1);
//
//                Paragraph paragraph2 = new Paragraph("Danh mục ..... kiến nghị chưa được giải quyết xong từ các kỳ họp trước đã giải quyết", vietnameseFontBold);
//                paragraph2.setAlignment(Element.ALIGN_CENTER);
//                paragraph2.setSpacingAfter(10);
//                document.add(paragraph2);
//
//                Paragraph paragraph3_1 = new Paragraph("(Kèm theo Báo cáo số ....../BC-...... ngày   /  của ..........)", vietnameseFontBItalic);
//                paragraph3_1.setAlignment(Element.ALIGN_CENTER);
//                paragraph3_1.setSpacingAfter(10);
//                document.add(paragraph3_1);
//
//                Paragraph paragraph4 = new Paragraph("BẢNG 1: Ban hành văn bản", vietnameseFontBold);
//                paragraph4.setAlignment(Element.ALIGN_CENTER);
//                paragraph4.setSpacingAfter(30);
//                document.add(paragraph4);
//                listKey = Arrays.asList("stt", "tenVBBH", "coQuanBanHanh_Trinh", "stt_KN", "noiDungKN_DP", "thoiDiemKN");
//                listHeader = Arrays.asList("STT", "Tên văn bản", "Cơ quan ban hành/trình", "STTKN", "Nội dung kiến nghị/địa phương", "Thời điểm kiến nghị");
//                listWidth = Arrays.asList(9F, 18F, 18F, 5F, 30F, 18F);
//                tongSoCot = listKey.size();
//
//            }else if(nhom.equals(Constant.DMNhom.THANH_TRA_KIEM_TRA.getValue())){
//                Paragraph paragraph2 = new Paragraph("BẢNG 2: Thanh tra kiểm tra", vietnameseFontBold);
//                paragraph2.setAlignment(Element.ALIGN_CENTER);
//                paragraph2.setSpacingAfter(30);
//                document.add(paragraph2);
//
//                listKey = Arrays.asList("stt", "noiDungKN_DP", "coQuanThanhTra", "thoiDiemKN");
//                listHeader = Arrays.asList("STT", "Nội dung kiến nghị/địa phương", "Cơ quan thanh tra", "Thời điểm kiến nghị");
//                listWidth = Arrays.asList(5F, 40F, 30F, 25F);
//                tongSoCot = listKey.size();
//            } else if (nhom.equals(Constant.DMNhom.TO_CHUC_THUC_HIEN.getValue())) {
//                Paragraph paragraph2 = new Paragraph("BẢNG 3: Tổ chức thực hiện", vietnameseFontBold);
//                paragraph2.setAlignment(Element.ALIGN_CENTER);
//                paragraph2.setSpacingAfter(30);
//                document.add(paragraph2);
//
//                listKey = Arrays.asList("stt", "noiDungKN_DP", "coQuanTCTH", "thoiDiemKN");
//                listHeader = Arrays.asList("STT", "Nội dung kiến nghị/địa phương", "Cơ quan tổ chức thực hiện", "Thời điểm kiến nghị");
//                listWidth = Arrays.asList(5F, 40F, 30F, 25F);
//                tongSoCot = listKey.size();
//            }
//        } else if (exportType.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.CHUA_GIAI_QUYET_TU_KY_HOP_TRUOC_SE_GIAI_QUYET.getValue())) {
//            Paragraph paragraph1 = new Paragraph("Phụ lục số 2.3", vietnameseFontBold);
//            paragraph1.setAlignment(Element.ALIGN_CENTER);
//            paragraph1.setSpacingAfter(10);
//            document.add(paragraph1);
//
//            Paragraph paragraph2 = new Paragraph("Danh mục ..... kiến nghị cử tri gửi chưa được giải quyết xong từ", vietnameseFontBold);
//            paragraph2.setAlignment(Element.ALIGN_CENTER);
//            paragraph2.setSpacingAfter(10);
//            document.add(paragraph2);
//
//            Paragraph paragraph3 = new Paragraph("các kỳ họp trước giải trình, cung cấp thông tin", vietnameseFontBold);
//            paragraph3.setSpacingAfter(10);
//            paragraph3.setAlignment(Element.ALIGN_CENTER);
//            document.add(paragraph3);
//
//            Paragraph paragraph3_1 = new Paragraph("(Kèm theo Báo cáo số ....../BC-...... ngày   /  của ..........)", vietnameseFontBItalic);
//            paragraph3_1.setAlignment(Element.ALIGN_CENTER);
//            paragraph3_1.setSpacingAfter(10);
//            document.add(paragraph3_1);
//
//            listKey = Arrays.asList("stt", "noiDungKN_DP", "thoiDiemKN", "coQuanTH", "tomtatKQGQ", "so_KyHieuCVTL");
//            listHeader = Arrays.asList("STT", "Nội dung kiến nghị/địa phương", "Thời điểm kiến nghị", "Cơ quan thực hiện", "Tóm tắt kết quả giải quyết", "Số, ký hiệu CV trả lời");
//            listWidth = Arrays.asList(10F, 20F, 20F, 15F, 20F, 15F);
//            tongSoCot = listKey.size();
//        }
//
//
//        // lấy tổng số cột
//        float[] width = new float[tongSoCot];
//        for(int i = 0; i < tongSoCot; i++) {
//            width[i] = listWidth.get(i);
//        }
//        PdfPTable table = new PdfPTable(tongSoCot);
//        table.setWidthPercentage(100);
//        table.setWidths(width);
//        for(int i = 0; i< tongSoCot; i++){
//            PdfPCell firstRowTotal = new PdfPCell(new Paragraph(listHeader.get(i), vietnameseFontBold));
//            firstRowTotal.setHorizontalAlignment(Element.ALIGN_CENTER);
//            firstRowTotal.setVerticalAlignment(Element.ALIGN_MIDDLE);
//            table.addCell(firstRowTotal);
//        }
//        if(outputListKN != null && outputListKN.values().toArray().length > 0 && ((ArrayList) outputListKN.values().toArray()[0]).size() > 0){
//            for(int row = 0; row < ((ArrayList) outputListKN.values().toArray()[0]).size(); row ++) {
//                for(int i = 0; i < tongSoCot; i++){
//                    PdfPCell firstRowTotal = new PdfPCell(new Paragraph((String) ((HashMap) ((ArrayList) outputListKN.values().toArray()[0]).get(row)).get(listKey.get(i)), vietnameseFont));
//                    firstRowTotal.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    firstRowTotal.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                    table.addCell(firstRowTotal);
//                }
//            }
//        }
//        // Add the header table to the document
//        document.add(table);
//        document.close();
//        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
//        // Tạo Resource từ file pdf trong bộ nhớ tạm
//        return inputStream;
//    }
//
//    public static InputStream getPdfType3(Map<String,Object> outputListKN, Long type) throws DocumentException, IOException {
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        Document document = new Document(PageSize.A4);
//        PdfWriter.getInstance(document, outputStream);
//        document.open();
//        document.setPageSize(PageSize.A4.rotate());
//        document.newPage();
//
//        // Set UTF-8 font
//        BaseFont baseFont = BaseFont.createFont("/font/RobotoSlab-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//        Font vietnameseFont = new Font(baseFont);
//        Font vietnameseFontBold = new Font(baseFont);
//        vietnameseFontBold.setStyle(Font.BOLD);
//        Paragraph paragraph1;
//        if(type.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.GIAI_TRINH_CUNG_CAP_THONG_TIN.getValue())){
//            paragraph1 = new Paragraph("Phụ lục số 1.3", vietnameseFont);
//        }else{
//            paragraph1 = new Paragraph("BÁO CÁO TỔNG HỢP LỜI HỨA", vietnameseFont);
//        }
//        paragraph1.setAlignment(Element.ALIGN_CENTER);
//        paragraph1.setSpacingAfter(10);
//        document.add(paragraph1);
//
//        if(type.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.GIAI_TRINH_CUNG_CAP_THONG_TIN.getValue())){
//            Paragraph paragraph2;
//            paragraph2 = new Paragraph("Danh mục ..... kiến nghị cử tri gửi tới tới kỳ họp thứ ...... và kỳ họp bất thường lần thứ .....,", vietnameseFont);
//            paragraph2.setAlignment(Element.ALIGN_CENTER);
//            paragraph2.setSpacingAfter(10);
//            document.add(paragraph2);
//            Paragraph paragraph3;
//            paragraph3 = new Paragraph("Quốc hội khóa ..... giải trình, cung cấp thông tin", vietnameseFont);
//            paragraph3.setAlignment(Element.ALIGN_CENTER);
//            paragraph3.setSpacingAfter(10);
//            document.add(paragraph3);
//            Paragraph paragraph4;
//            paragraph4 = new Paragraph("(Kèm theo Báo cáo số ...../BC-..... ngày / của.....)", vietnameseFont);
//            paragraph4.setAlignment(Element.ALIGN_CENTER);
//            paragraph4.setSpacingAfter(10);
//            document.add(paragraph4);
//        }
//
//        List<String> listKey;
//        List<String> listHeader;
//        List<Float> listWidth;
//        Integer tongSoCot = 0;
//        if(type.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.GIAI_TRINH_CUNG_CAP_THONG_TIN.getValue())){
//            listKey = Arrays.asList("stt", "noiDungKn", "so_KyHieuCVTL", "tomTatKQGQ");
//            listHeader = Arrays.asList("STT", "Nội dung kiến nghị", "Số, ký hiệu CV trả lời", "Tóm tắt kết quả giải quyết");
//            listWidth = Arrays.asList(5F, 40F, 15F, 40F);
//            tongSoCot = 4;
//        }else{
//            listKey = Arrays.asList("stt", "tenDBQH", "tenDoanDBQH", "noiDungChatVan", "noiDungTraLoi","loiHuaCuaBo", "tienDoGiaiQuyet");
//            listHeader = Arrays.asList("STT", "Tên đại biểu quốc hội", "Tên đoàn đại biểu Quốc hội", "Nội dung chất vấn", "Nội dung trả lời", "Lời hứa của Bộ trưởng", "Tiến độ giải quyết");
//            listWidth = Arrays.asList(5F, 25F, 15F, 15F, 15F,15F, 20F);
//            tongSoCot = 7;
//        }
//        List<Map<String,Object>> list = new ArrayList<>();
//        if(type.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.CHAT_VAN.getValue())){
//            list = ((List<Map<String,Object>>) outputListKN.get("listChatVan"));
//        }else{
//            list = ((List<Map<String,Object>>) outputListKN.get("listKn"));
//        }
//        // lấy tổng số cột
//        float[] width = new float[tongSoCot];
//        for(int i = 0; i < tongSoCot; i++) {
//            width[i] = listWidth.get(i);
//        }
//        PdfPTable table = new PdfPTable(tongSoCot);
//        table.setWidthPercentage(100);
//        table.setWidths(width);
//        for(int i = 0; i< tongSoCot; i++){
//            PdfPCell firstRowTotal = new PdfPCell(new Paragraph(listHeader.get(i), vietnameseFontBold));
//            firstRowTotal.setHorizontalAlignment(Element.ALIGN_CENTER);
//            firstRowTotal.setVerticalAlignment(Element.ALIGN_MIDDLE);
//            table.addCell(firstRowTotal);
//        }
//        if(list != null && list.size() > 0){
//            for(int row = 0; row < list.size(); row ++) {
//                for(int i = 0; i < tongSoCot; i++){
//                    PdfPCell firstRowTotal = new PdfPCell(new Paragraph((String) list.get(row).get(listKey.get(i)), vietnameseFont));
//                    firstRowTotal.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    firstRowTotal.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                    table.addCell(firstRowTotal);
//                }
//            }
//        }
//        // Add the header table to the document
//        document.add(table);
//        document.close();
//        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
//        // Tạo Resource từ file pdf trong bộ nhớ tạm
//        return inputStream;
//    }
//
//
//    public static InputStream getPdfLoiHua(Map<String,Object> outputListKN, Long type) throws DocumentException, IOException {
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        Document document = new Document(PageSize.A4);
//        PdfWriter.getInstance(document, outputStream);
//        document.open();
//        document.setPageSize(PageSize.A4.rotate());
//        document.newPage();
//
//        // Set UTF-8 font
//        BaseFont baseFont = BaseFont.createFont("/font/RobotoSlab-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//        Font vietnameseFont = new Font(baseFont);
//        Font vietnameseFontBold = new Font(baseFont);
//        vietnameseFontBold.setStyle(Font.BOLD);
//        Paragraph paragraph1;
//        paragraph1 = new Paragraph("BÁO CÁO TỔNG HỢP LỜI HỨA", vietnameseFont);
//        paragraph1.setAlignment(Element.ALIGN_CENTER);
//        paragraph1.setSpacingAfter(10);
//        document.add(paragraph1);
//
//        List<String> listKey;
//        List<String> listHeader;
//        List<Float> listWidth;
//        listKey = Arrays.asList("stt", "cauHoi", "diaPhuong", "donVi", "cauTraLoi_LoiHua", "Tình trạng giải quyết", "Tình trạng giải quyết");
//        listHeader = Arrays.asList("STT", "Câu hỏi", "Địa phương", "Đơn vị", "Câu trả lời/Lời hứa", "Ban hành văn bản", "Đang giải quyết");
//        listWidth = Arrays.asList(5F, 15F, 15F, 15F, 20F, 15F, 15F);
//        Integer tongSoCot = listHeader.size();
//        List<Map<String,Object>> list = new ArrayList<>();
//        list = ((List<Map<String,Object>>) outputListKN.get("listKn"));
//        // lấy tổng số cột
//        float[] width = new float[tongSoCot];
//        for(int i = 0; i < tongSoCot; i++) {
//            width[i] = listWidth.get(i);
//        }
//        PdfPTable table = new PdfPTable(tongSoCot);
//        table.setWidthPercentage(100);
//        table.setWidths(width);
//
//        //        =================================== row 1 ==========================================
//        for(int i = 0; i< tongSoCot - 2; i++){
//            PdfPCell firstRowTotal = new PdfPCell(new Paragraph(listHeader.get(i), vietnameseFontBold));
//            firstRowTotal.setHorizontalAlignment(Element.ALIGN_CENTER);
//            firstRowTotal.setVerticalAlignment(Element.ALIGN_MIDDLE);
//            firstRowTotal.setRowspan(2);
//            table.addCell(firstRowTotal);
//        }
//        PdfPCell firstRowTotal1 = new PdfPCell(new Paragraph(listKey.get(tongSoCot -2), vietnameseFontBold));
//        firstRowTotal1.setHorizontalAlignment(Element.ALIGN_CENTER);
//        firstRowTotal1.setVerticalAlignment(Element.ALIGN_MIDDLE);
//        firstRowTotal1.setColspan(2);
//        table.addCell(firstRowTotal1);
//
//        //        =================================== row 2 ==========================================
//        List<String> keySetChild = ((Map<String, ?>) ((Map<String, ?>) list.get(0)).get("Tình trạng giải quyết")).keySet().stream().collect(Collectors.toList());
//        for (int rowPhanLoaiCon = 0; rowPhanLoaiCon < keySetChild.size(); rowPhanLoaiCon++) {
//            PdfPCell cell = new PdfPCell(new Paragraph(keySetChild.get(rowPhanLoaiCon), vietnameseFontBold));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//            table.addCell(cell);
//        }
//
//        //        =================================== row 3 ==========================================
//        if(list != null && list.size() > 0){
//            for(int row = 0; row < list.size(); row ++) {
//                for(int i = 0; i < tongSoCot; i++) {
//                    PdfPCell cell = new PdfPCell();
//                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                    if(i == tongSoCot - 2){
//                        cell = new PdfPCell(new Paragraph(((Map<String, ?>) ((Map<String, ?>) list.get(row)).get(listKey.get(i))).get(keySetChild.get(0)).toString(), vietnameseFont));
//                    }else if(i == tongSoCot - 1){
//                        cell = new PdfPCell(new Paragraph(((Map<String, ?>) ((Map<String, ?>) list.get(row)).get(listKey.get(i))).get(keySetChild.get(1)).toString(), vietnameseFont));
//                    }else{
//                        cell = new PdfPCell(new Paragraph((String) list.get(row).get(listKey.get(i)), vietnameseFont));
//                    }
//                    table.addCell(cell);
//                }
//            }
//        }
//        // Add the header table to the document
//        document.add(table);
//        document.close();
//        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
//        // Tạo Resource từ file pdf trong bộ nhớ tạm
//        return inputStream;
//    }
//
//    public static InputStream getPdfType1(Map<String,Object> outputListKN, Long type) throws DocumentException, IOException {
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        Document document = new Document(PageSize.A4);
//        PdfWriter.getInstance(document, outputStream);
//        document.open();
//        document.setPageSize(PageSize.A4.rotate());
//        document.newPage();
//
//        // Set UTF-8 font
//        BaseFont baseFont = BaseFont.createFont("/font/RobotoSlab-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//        Font vietnameseFont = new Font(baseFont);
//        Paragraph paragraph1;
//        if(type.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.KET_QUA_CU_TRI.getValue())){
//            paragraph1 = new Paragraph("Phụ lục số 1", vietnameseFont);
//        }else{
//            paragraph1 = new Paragraph("Phụ lục 2", vietnameseFont);
//        }
//        paragraph1.setAlignment(Element.ALIGN_CENTER);
//        paragraph1.setSpacingAfter(10);
//        document.add(paragraph1);
//        Paragraph paragraph2;
//        if(type.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.KET_QUA_CU_TRI.getValue())){
//            paragraph2 = new Paragraph("Bảng tổng hợp kết quả giải quyết, trả lời kiến nghị của cử tri", vietnameseFont);
//        }else{
//            paragraph2 = new Paragraph("Bảng tổng hợp số liệu kết quả giải quyết đối với … kiến nghị chưa được giải quyết xong", vietnameseFont);
//        }
//        paragraph2.setAlignment(Element.ALIGN_CENTER);
//        paragraph2.setSpacingAfter(10);
//        document.add(paragraph2);
//        Paragraph paragraph3;
//        if(type.equals(Constant.ExportTypeBaoCaoThongKeKienNghi.KET_QUA_CU_TRI.getValue())){
//            paragraph3 = new Paragraph("gửi tới tới kỳ họp thứ 4 và kỳ họp bất thường lần thứ 2, Quốc hội khóa XV của ….", vietnameseFont);
//        }else{
//            paragraph3 = new Paragraph("từ các kỳ họp trước", vietnameseFont);
//        }
//        paragraph3.setAlignment(Element.ALIGN_CENTER);
//        paragraph3.setSpacingAfter(10);
//        document.add(paragraph3);
//
//        Paragraph paragraph4 = new Paragraph("(Kèm theo Báo cáo số ....../BC-....... ngày / của.......)", vietnameseFont);
//        paragraph4.setAlignment(Element.ALIGN_CENTER);
//        paragraph4.setSpacingAfter(10);
//        document.add(paragraph4);
//
//        // lấy tổng số kiến nghị
//        Long totalKienNghi = Long.valueOf(outputListKN.remove("Tổng số kiến nghị").toString());
//
//        // lấy tổng số cột
//        Integer tongSoCot = outputListKN.keySet().stream().collect(Collectors.toList()).size() + 1;
//        List<String> keySet = outputListKN.keySet().stream().collect(Collectors.toList());
//
//        float[] width = new float[tongSoCot];
//        width[0] = 10F;
//        for(int i = 1; i < tongSoCot; i++) {
//            width[i] = Float.valueOf(90F/(tongSoCot-1));
//        }
//        PdfPTable table = new PdfPTable(tongSoCot);
//        table.setWidthPercentage(100);
//
//        table.setWidths(width);
//        for(int row = 0; row <3; row ++) {
//            // hàng đầu tiên show tên phân loại trả lời cha
//            if(row == 0){
//                for(int i = 0; i < tongSoCot; i++){
//                    if(i == 0){
//                        PdfPCell firstRowTotal = new PdfPCell(new Paragraph("Tổng số kiến nghị", vietnameseFont));
//                        firstRowTotal.setRowspan(2);
//                        firstRowTotal.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        firstRowTotal.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                        table.addCell(firstRowTotal);
//                    }else {
//                        PdfPCell rowOneShowGocName = new PdfPCell();
//                        if(!(outputListKN.get(keySet.get(i-1)) instanceof LinkedHashMap)){
//                            rowOneShowGocName.setRowspan(2);
//                        }
//                        Paragraph elements = new Paragraph(keySet.get(i-1), vietnameseFont);
//                        elements.setAlignment(Element.ALIGN_CENTER);
//                        rowOneShowGocName.addElement(elements);
//                        rowOneShowGocName.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        rowOneShowGocName.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                        table.addCell(rowOneShowGocName);
//                    }
//                }
//            }
//
//            // hàng thứ 2 show tên phân loại con ứng với từng phân loại trả lời cha
//            if(row == 1){
//                for(int i = 1; i < tongSoCot; i++){
//                    if(outputListKN.get(keySet.get(i-1)) instanceof LinkedHashMap) {
//                        List<String> keySetChild = ((LinkedHashMap<String, ?>) outputListKN.get(keySet.get(i - 1))).keySet().stream().collect(Collectors.toList());
//                        // lấy ra tổng số phân loại con ứng với phân loại cha
//                        PdfPTable tablePhanLoaiCon = new PdfPTable(keySetChild.size());
//                        for (int rowPhanLoaiCon = 0; rowPhanLoaiCon < keySetChild.size(); rowPhanLoaiCon++) {
//                            PdfPCell cell = new PdfPCell(new Paragraph(keySetChild.get(rowPhanLoaiCon), vietnameseFont));
//                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                            tablePhanLoaiCon.addCell(cell);
//                        }
//                        PdfPCell rowTwoShowChild = new PdfPCell(tablePhanLoaiCon);
//                        table.addCell(rowTwoShowChild);
//                    }
//                }
//            }
//
//            // hàng thứ 3 show count phân loại con ứng với từng phân loại trả lời cha
//            if(row == 2){
//                // cột đầu tiên là tổng số kiến nghị
//                PdfPCell total = new PdfPCell(new Paragraph(totalKienNghi.toString()));
//                total.setHorizontalAlignment(Element.ALIGN_CENTER);
//                total.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                table.addCell(total);
//                // cột tiếp theo là data tương ứng với các phân loại con
//                for(int i = 1; i < tongSoCot; i++){
//                    if(outputListKN.get(keySet.get(i-1)) instanceof LinkedHashMap){
//                        List<String> keySetChild = ((LinkedHashMap<String, ?>) outputListKN.get(keySet.get(i-1))).keySet().stream().collect(Collectors.toList());
//                        // lấy ra tổng số phân loại con ứng với phân loại cha
//                        PdfPTable tablePhanLoaiCon = new PdfPTable(keySetChild.size());
//                        for(int rowPhanLoaiCon = 0; rowPhanLoaiCon < keySetChild.size(); rowPhanLoaiCon ++){
//                            PdfPCell cell = new PdfPCell(new Paragraph(((LinkedHashMap<String, ?>) outputListKN.get(keySet.get(i-1))).get(keySetChild.get(rowPhanLoaiCon)).toString(), vietnameseFont));
//                            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                            tablePhanLoaiCon.addCell(cell);
//                        }
//                        PdfPCell rowTwoShowChild = new PdfPCell(tablePhanLoaiCon);
//                        table.addCell(rowTwoShowChild);
//                    }else{
//                        PdfPCell rowTwoShowChild = new PdfPCell(new Paragraph(outputListKN.get(keySet.get(i-1)).toString(), vietnameseFont));
//                        rowTwoShowChild.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        rowTwoShowChild.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                        table.addCell(rowTwoShowChild);
//                    }
//                }
//            }
//
//        }
//
//        // Add the header table to the document
//        document.add(table);
//        document.close();
//        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
//        // Tạo Resource từ file pdf trong bộ nhớ tạm
//        return inputStream;
//    }
//}
