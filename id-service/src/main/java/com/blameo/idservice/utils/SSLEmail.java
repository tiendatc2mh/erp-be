package com.blameo.idservice.utils;


import com.blameo.idservice.config.LanguageConfig;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;


@Component
@Data
public class SSLEmail {
    /**
     * Outgoing Mail (SMTP) Server
     * requires TLS or SSL: smtp.gmail.com (use authentication)
     * Use Authentication: Yes
     * Port for SSL: 465
     */


    static Logger logger = LoggerFactory.getLogger(SSLEmail.class);


    private static String mailSender;


    private static String mailPassword;


    private static String smtpHost;


    private static String smtpPort;

    private static String socketFactoryPort;




    public SSLEmail(
            @Value("${erp.mail.sender}") String mailSender,
            @Value("${erp.mail.pass}") String mailPassword,
            @Value("${erp.mail.smtp.host}") String smtpHost,
            @Value("${erp.mail.smtp.port}") String smtpPort,
            @Value("${erp.mail.smtp.socketFactory.port}") String socketFactoryPort) {
        this.mailSender = mailSender;
        this.mailPassword = mailPassword;
        this.smtpHost = smtpHost;
        this.smtpPort = smtpPort;
        this.socketFactoryPort = socketFactoryPort;
    }

    public static void sendEmail(String subject, String mailContent, String[] listAccount,
                                 byte[][] attachmentDatas, String[] attachmentNames) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(LanguageConfig.class);
        MessageSource messageSource = context.getBean(MessageSource.class);
        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", smtpHost); // SMTP Host
            props.put("mail.smtp.port", smtpPort);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
//            props.put("mail.smtp.ssl.protocols", "TLSv1.2");
//            props.put("mail.smtp.starttls.enable", "true"); // TLS

            Authenticator auth = new Authenticator() {
                //override the getPasswordAuthentication method
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mailSender, mailPassword);
                }
            };

            Session session = Session.getDefaultInstance(props, auth);
            String[] ccList = null;
//        if (!isViettel){
//            listAccount = Arrays.stream(listAccount).filter(item -> !item.contains("viettel")).toArray(String[]::new);
//            ccList = new String[]{"datvt@blameo.com"};
//        }
            if (listAccount.length > 0) {
                EmailUtil.sendMail(session, listAccount, ccList, null, subject, mailContent, attachmentDatas, attachmentNames);
                logger.info(String.format("====Gui mail thanh cong tu %s den %s tai khoan ", mailSender, listAccount.length));
            } else {
                logger.info(String.format("====Gui mail khong thanh cong tu %s den %s tai khoan ", mailSender, listAccount.length));
            }
        } catch (Exception e) {
            throw new RuntimeException(messageSource.getMessage("sendMailFail", null, CommonUtil.getLocale()));
        }
    }


}
