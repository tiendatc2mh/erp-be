package com.blameo.idservice.utils;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class DataUtil {

    public static String makeLikeQuery(String s) throws UnsupportedEncodingException {
        if (StringUtils.isEmpty(s)) return null;
        s = URLDecoder.decode(s, "UTF-8");
        s =
            s
                .trim()
                .toLowerCase()
                .replace("!", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "!")
                .replace("%", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "%")
                .replace("_", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "_");
        return  s ;
    }

    public static String makeLikeQueryDefault(String s) {
        if (StringUtils.isEmpty(s)) return null;
        s =
                s
                        .trim()
                        .toLowerCase()
                        .replace("\\", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "\\")
                        .replace("!", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "!")
                        .replace("%", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "%")
                        .replace("_", Constants.DEFAULT_ESCAPE_CHAR_QUERY + "_");
        return "%" + s + "%";
    }

    public static List<Long> parseMultiValueRequestParam(String param) {
        List<Long> listParam = new ArrayList<>();
        try{
            if(param.contains(",")){
                String[] list = param.split(",");
                for (String s: list) {
                    listParam.add(Long.valueOf(s));
                }
            }else {
                listParam.add(Long.valueOf(param));
            }
        }catch (Exception e){
            return new ArrayList<>();
        }
        return listParam;
    }
}
