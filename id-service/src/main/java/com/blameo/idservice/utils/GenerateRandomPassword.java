package com.blameo.idservice.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/10/2023, Thursday
 **/
public class GenerateRandomPassword {
    public static String generateRandomPassword(int length) {
        String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+";
        return RandomStringUtils.random(length, characters);
    }
}
