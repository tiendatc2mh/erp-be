package com.blameo.idservice.utils;


import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {

    public static XSSFCellStyle getStyleBasic(XSSFWorkbook workbook){
        //create font
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.valueOf((short) 1));
        style.setBorderTop(BorderStyle.valueOf((short) 1));
        style.setBorderRight(BorderStyle.valueOf((short) 1));
        style.setBorderLeft(BorderStyle.valueOf((short) 1));
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setVerticalAlignment(VerticalAlignment.TOP);
        style.setFont(font);
        return style;
    }

    public static XSSFCellStyle getStyleCenterBasic(XSSFWorkbook workbook){
        //create font
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.valueOf((short) 1));
        style.setBorderTop(BorderStyle.valueOf((short) 1));
        style.setBorderRight(BorderStyle.valueOf((short) 1));
        style.setBorderLeft(BorderStyle.valueOf((short) 1));
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(font);
        return style;
    }

    public static XSSFCellStyle getStyleCenterBasicBold(XSSFWorkbook workbook){
        //create font
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.valueOf((short) 1));
        style.setBorderTop(BorderStyle.valueOf((short) 1));
        style.setBorderRight(BorderStyle.valueOf((short) 1));
        style.setBorderLeft(BorderStyle.valueOf((short) 1));
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(font);
        return style;
    }

    public static XSSFCellStyle getStyleDateBasic(XSSFWorkbook workbook, String dateFormat){
        //create CreationHelper
        CreationHelper createHelper = workbook.getCreationHelper();
        //create font
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.valueOf((short) 1));
        style.setBorderTop(BorderStyle.valueOf((short) 1));
        style.setBorderRight(BorderStyle.valueOf((short) 1));
        style.setBorderLeft(BorderStyle.valueOf((short) 1));
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setVerticalAlignment(VerticalAlignment.TOP);
        style.setFont(font);
        style.setDataFormat(createHelper.createDataFormat().getFormat(dateFormat));
        return style;
    }

}
