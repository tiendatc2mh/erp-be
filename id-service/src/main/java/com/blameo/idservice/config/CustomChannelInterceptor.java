package com.blameo.idservice.config;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 9/25/2023, Monday
 **/
@Component
public class CustomChannelInterceptor  extends ChannelInterceptorAdapter {
    public static Map<String,String> localClientSocket = new HashMap<>();
    @Override
    public Message<?> preSend(Message<?> message, org.springframework.messaging.MessageChannel channel) {
        SimpMessageHeaderAccessor accessor = SimpMessageHeaderAccessor.wrap(message);

        // Access and check headers here
        // For example, you can access headers like this:
        if (Objects.equals(accessor.getMessageType(), SimpMessageType.CONNECT)){
           String localClientLocale = Objects.equals(accessor.getFirstNativeHeader("locale"), "vi") ?"vi":"en";
            localClientSocket.put(accessor.getFirstNativeHeader("userId"),localClientLocale);
        }else if (Objects.equals(accessor.getMessageType(), SimpMessageType.DISCONNECT)){
            localClientSocket.remove(accessor.getFirstNativeHeader("userId"));
        }
        // You can do your checks here
        return message;
    }
}
