package com.blameo.idservice.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.stereotype.Component;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/26/2023, Wednesday
 **/
@Component
public class SwaggerCustomizer implements OpenApiCustomiser {


    @Override
    public void customise(OpenAPI openApi) {
        SecurityScheme securityScheme = new SecurityScheme()
                .type(SecurityScheme.Type.HTTP)
                .scheme("bearer")
                .bearerFormat("JWT");
        openApi.getComponents().addSecuritySchemes("bearer-key", securityScheme);
        SecurityRequirement securityRequirement = new SecurityRequirement();
        securityRequirement.addList("bearer-key");
        openApi.addSecurityItem(securityRequirement);

        openApi.getPaths().forEach((path, pathItem) -> {
            if (path.startsWith("/api/id/sign-in") || path.startsWith("/api/id/sign-up") || path.startsWith("/api/id/refresh-token")) {
                pathItem.readOperations().forEach(operation -> {
                    operation.setSecurity(null);
                    operation.setDescription("This API does not require authentication.");
                });
            }
        });
    }
}
