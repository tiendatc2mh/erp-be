package com.blameo.idservice.repository;

import com.blameo.idservice.model.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, String> {
    Optional<Department> findByDepartmentName(String name);

    Optional<Department> findByDepartmentId(String id);
    @Query(value = " select * from department d where (lower(d.department_name) like lower(?1)  ESCAPE '!' or lower(d.department_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or d.department_status = ?2) and d.department_status != -1 order by d.created_date desc", nativeQuery = true)
    Page<Department> findAll(Pageable paging, String keyword, Integer status);

    @Query(value = "select d from Department d where (?1 is null or ?1 = d.departmentStatus)  order by d.createdDate desc ")
    List<Department> getAll(Integer status);

    Boolean existsByDepartmentCodeAndDepartmentStatusGreaterThanEqual(String code, Integer status);

    Boolean existsByDepartmentIdAndDepartmentStatusGreaterThanEqual(String id, Integer status);
}
