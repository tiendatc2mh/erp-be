package com.blameo.idservice.repository;

import com.blameo.idservice.model.Title;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TitleRepository extends JpaRepository<Title, String> {
    Optional<Title> findByTitleName(String name);

    @Query(value = " select * from id.title t where (lower(t.title_name) like lower(?1)  ESCAPE '!' or lower(t.title_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or t.title_status = ?2) and t.title_status != -1 order by t.created_date desc", nativeQuery = true)
    Page<Title> findAll(Pageable paging, String keyword, Integer status);

    @Query(value = "select * from id.title t where (lower(t.title_name) like lower(?1)  ESCAPE '!' or lower(t.title_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or t.title_status = ?2) and t.title_status!=-1 order by t.created_date desc ", nativeQuery = true)
    List<Title> getAll(String keyword, Integer status);

    @Query(value = "select * from id.title t where t.title_id in (?1) and t.title_status!=-1", nativeQuery = true)
    List<Title> getAllListTitleId(List<String> titleId);

    Boolean existsByTitleCodeAndTitleStatusGreaterThanEqual(String code, Integer status);

    Boolean existsByTitleIdAndTitleStatusGreaterThanEqual(String id, Integer status);
}
