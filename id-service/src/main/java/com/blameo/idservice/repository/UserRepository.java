package com.blameo.idservice.repository;

import com.blameo.idservice.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    Optional<User> findByEmail(String username);

    Optional<User> findByUserIdAndEmployeeStatusGreaterThanEqual(String id, Integer status);

    Boolean existsByEmailAndEmployeeStatusGreaterThanEqual(String email, Integer status);
    Boolean existsByIdentityCard(String identityCard);
    Boolean existsByPersonalEmail(String personalEmail);

    Boolean existsByUserCodeAndEmployeeStatusGreaterThanEqual(String email, Integer status);

    Boolean existsByLevel_LevelIdAndEmployeeStatusGreaterThanEqual(String levelId, Integer status);

    Boolean existsByDepartment_DepartmentIdAndEmployeeStatusGreaterThanEqual(String departmentId, Integer status);

    Boolean existsByPosition_PositionIdAndEmployeeStatusGreaterThanEqual(String positionId, Integer status);

    Boolean existsByTitle_TitleIdAndEmployeeStatusGreaterThanEqual(String titleId, Integer status);

    @Query(value = "select if(COUNT(u.user_id)>0,'true','false') from user u inner join user_role ur on u.user_id = ur.user_id and u.employee_status <> -1 " +
                                        "inner join role r on r.role_id=ur.role_id and r.role_status<>-1 where r.role_id = ?1", nativeQuery = true)
    Boolean existsByRole(String id);

    Boolean existsByEmail(String email);

    Boolean existsByAttendanceCode(Integer attendanceCode);
    Boolean existsByPhoneNumber(String phone);
    Boolean existsBySocialInsuranceCode(String socialInsuranceCode);
    Boolean existsByTaxCode(String taxCode);
    Boolean existsByBankTypeAndBankAccountNumber(String bankType,String bankNumber);

    @Query(value = " select u from User u where 1=1 " +
            "and (?1 is null or u.department.departmentId = ?1) " +
            "and (lower(u.fullName) like lower(?2)  ESCAPE '!') " +
            "and (?3 is null or u.employeeStatus = ?3) and (coalesce(?4) is null or u.userType in ?4) " +
            "and u.email!=?5 and (lower(u.userCode) like lower(?6)  ESCAPE '!') and (u.employeeStatus = 1 or (?7 is null or DATE_FORMAT(u.severanveDate, '%m-%y') >= DATE_FORMAT(?7, '%m-%y'))) " +
            "and u.employeeStatus != -1 order by u.createdDate desc")
//    @Query(value = "select * from `user` as u where (?1 is null or u.department_id = ?1) " +
//            "and (lower(u.full_name) like lower(?2)  ESCAPE '!') " +
//            "and (?3 is null or u.employee_status = ?3) and (coalesce(?4) is null or u.user_type in ?4) " +
//            "and u.email != ?5 and (lower(u.user_code) like lower(?6)  ESCAPE '!') and ((?7 is null and u.employee_status != -1) or (?7 is not null and ?7 <= u.severanve_date)) order by u.created_date desc",nativeQuery = true)
    Page<User> findAll(Pageable paging, String department, String keyword, Integer status, List<Integer> type, String superAdminEmail, String userCode, Date severanveDate);

    @Query(value = " select u from User u where 1=1 " +
            "and (?1 is null or u.department.departmentId = ?1) " +
            "and (lower(u.fullName) like lower(?2)  ESCAPE '!') " +
            "and (?3 is null or u.employeeStatus = ?3) and u.email!=?4 and (lower(u.userCode) like lower(?5)  ESCAPE '!')  and u.employeeStatus != -1 order by u.createdDate desc")
    List<User> getAll(String department, String keyword, Integer status, String superAdminEmail,String userCode);

    @Query(value = " select u from User u where 1=1 " +
            "and u.email!=?1")
    List<User> getAllSchedule(String superAdminEmail);

    @Query(value = "select u.full_name from user u where 1=1 and u.user_id = ?1",nativeQuery = true)
    String getFullNameByUserId(String userId);

    @Query(value = " select u,a from User u left join Asset a on a.userId = u.userId and a.assetStatus = 1 where 1=1 " +
            "and (?1 is null or u.department.departmentId = ?1) " +
            "and (lower(u.fullName) like lower(?2)  ESCAPE '!') " +
            "and (?3 is null or u.employeeStatus = ?3) and (coalesce(?4) is null or u.userType in ?4) and u.email!=?5 and (lower(u.userCode) like lower(?6)  ESCAPE '!') and u.employeeStatus != -1 order by u.createdDate desc")
    List<Object[]> findAllToExport(String department, String keyword, Integer status, List<Integer> type, String superAdminEmail,String userCode);

    @Query(value = "select count(*) from user", nativeQuery = true)
    Long countUser();

    List<User> getAllByUserIdIn(List<String> userId);

}
