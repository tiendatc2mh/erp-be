package com.blameo.idservice.repository;

import com.blameo.idservice.model.AssetLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AssetLogRepository extends JpaRepository<AssetLog, String> {

    @Query("select al, u1.fullName, u2.fullName ,u3.fullName from AssetLog al left join User u1 on al.assetLogSource = u1.userId " +
            "left join User u2 on al.assetLogReceive = u2.userId left join User u3 on al.assetLogDecision = u3.userId " +
            "where (?1 is null or al.asset.assetId = ?1) order by al.assetLogAssignDate asc")
    Page<Object[]> findAll(Pageable pageable, String assetId);

    @Query("select al from AssetLog al where (?1 is null or al.asset.assetId = ?1) order by al.assetLogAssignDate asc")
    List<AssetLog> getAll(String assetId);

}