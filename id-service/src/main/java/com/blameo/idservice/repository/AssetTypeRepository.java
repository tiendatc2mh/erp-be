package com.blameo.idservice.repository;

import com.blameo.idservice.model.AssetType;
import com.blameo.idservice.model.Level;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AssetTypeRepository extends JpaRepository<AssetType, String> {

    Boolean existsAssetTypeByAssetTypeCodeAndAndAssetTypeStatusGreaterThanEqual(String assetTypeCode, Integer assetStatus);


    @Query("select at from AssetType at where " +
            "(lower(at.assetTypeCode) like lower(?1) ESCAPE '|' or (lower(at.assetTypeName) like lower(?1) ESCAPE '|')) and " +
            "(?2 is null or at.assetTypeStatus = ?2) and at.assetTypeStatus != -1 order by at.createdDate desc ")
    Page<AssetType> findAll(Pageable pageable, String keyword, Integer status);

    List<AssetType> getAllByAssetTypeIdIn(List<String> assetTypeIds);

    @Query("select at from AssetType at where " +
            "(?1 is null or at.assetTypeStatus = ?1) and at.assetTypeStatus != -1 order by at.createdDate desc ")
    List<AssetType> getAll(Integer status);

    Boolean existsAssetTypeByAssetTypeIdAndAndAssetTypeStatusGreaterThanEqual(String assetTypeId, Integer assetStatus);

}
