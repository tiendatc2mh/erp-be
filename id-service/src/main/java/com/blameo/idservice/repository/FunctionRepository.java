package com.blameo.idservice.repository;

import com.blameo.idservice.model.Function;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface FunctionRepository extends JpaRepository<Function, String> {
    Optional<Function> findByFunctionName(String name);

    Optional<Function> findByFunctionId(String id);

    @Query(value = " select * from function f where (lower(f.function_name) like lower(?1)  ESCAPE '!' or lower(f.function_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or f.function_status = ?2) and (?3 is null or ?3 <= f.created_date) and (?4 is null or ?4 >= f.created_date) " +
            "and f.function_status != -1 order by f.created_date desc", nativeQuery = true)
    Page<Function> findAll(Pageable paging, String keyword, Integer status, Date fromDate, Date toDate);

    @Query(value = "select * from function f where (lower(f.function_name) like lower(?1)  ESCAPE '!' or lower(f.function_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or f.function_status = ?2) and f.function_status!=-1 order by f.created_date desc ", nativeQuery = true)
    List<Function> getAll(String keyword, Integer status);

    Boolean existsByFunctionCodeAndFunctionStatusGreaterThanEqual(String code, Integer status);

    Boolean existsByFunctionIdAndFunctionStatusGreaterThanEqual(String id, Integer status);
}
