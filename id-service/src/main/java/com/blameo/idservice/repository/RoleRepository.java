package com.blameo.idservice.repository;

import com.blameo.idservice.model.Role;
import com.blameo.idservice.model.Title;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
    Optional<Role> findByRoleName(String name);

    @Query(value = " select r from Role r where 1=1 " +
            "and (lower(r.roleName) like lower(?1)  ESCAPE '!' or lower(r.roleCode) like lower(?1)  ESCAPE '!') "+
            "and (?2 is null or r.roleStatus = ?2) and r.roleCode != ?3 and (?4 is null or DATE_FORMAT(r.createdDate, '%d/%m/%y') = DATE_FORMAT(?4, '%d/%m/%y')) " +
            "and r.roleStatus != -1 order by r.createdDate desc")
    Page<Role> findAll(Pageable paging, String keyword, Integer status, String roleSuperAdmin, Date createdDate);

    @Query(value = "select r from Role r where r.roleStatus!=-1 " +
            "and (?1 is null or r.roleStatus = ?1) and r.roleCode != ?2 order by r.createdDate desc ")
    List<Role> getAll(Integer status, String roleSuperAdmin);

    @Query(value = "select r from Role r where r.roleId in (?1) and r.roleStatus <> -1")
    List<Role> getAllListRoleId(List<String> roleId);

    Boolean existsByRoleCodeAndRoleStatusGreaterThanEqual(String code,Integer status);
    Boolean existsByRoleIdAndRoleStatusGreaterThanEqual(String id,Integer status);
    Boolean existsByRoleId(String id);
    Boolean existsByRoleCode(String code);

    @Query(value = "select if(COUNT(r.role_id)>0,'true','false') from role r inner join role_permission rp on r.role_id = rp.role_id and r.role_status <> -1 " +
            "inner join permission p on p.permission_id=rp.permission_id and p.permission_status <> -1 where p.permission_id = ?1", nativeQuery = true)
    Boolean existsByPermission(String id);
}
