package com.blameo.idservice.repository;

import com.blameo.idservice.model.Level;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface LevelRepository extends JpaRepository<Level, String> {
    Optional<Level> findByLevelName(String name);

    @Query(value = " select * from level l where (lower(l.level_name) like lower(?1)  ESCAPE '!' or lower(l.level_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or l.level_status = ?2) and l.level_status != -1 order by l.created_date desc", nativeQuery = true)
    Page<Level> findAll(Pageable paging, String keyword, Integer status);

    @Query(value = " select * from level l where (lower(l.level_name) like lower(?1)  ESCAPE '!' or lower(l.level_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or l.level_status = ?2) and l.level_status!=-1 order by l.created_date desc ", nativeQuery = true)
    List<Level> getAll(String keyword, Integer status);

    List<Level> getAllByLevelIdIn(List<String> levelIds);

    Boolean existsByLevelCodeAndLevelStatusGreaterThanEqual(String code, Integer status);

    Boolean existsByLevelIdAndLevelStatusGreaterThanEqual(String id, Integer status);
    Boolean existsByDepartment_DepartmentId(String id);
}
