package com.blameo.idservice.repository;

import com.blameo.idservice.model.NoteBook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteBookRepository extends JpaRepository<NoteBook, String> {
    Boolean existsByNoteBookId(String id);

    @Query(value = " select nb from NoteBook nb where (lower(nb.noteBookName) like lower(?1)  ESCAPE '!') and (lower(nb.noteBookContent) like lower(?2)  ESCAPE '!')" +
            "and (?3 is null or nb.status = ?3) and (?4 is null or nb.noteBookType = ?4) order by nb.createdDate desc")
    Page<NoteBook> findAll(Pageable paging, String noteBookName, String noteBookContent, Integer status, Integer type);

    @Query(value = " select nb from NoteBook nb where (lower(nb.noteBookName) like lower(?1)  ESCAPE '!') and (lower(nb.noteBookContent) like lower(?2)  ESCAPE '!')" +
            "and (?3 is null or nb.status = ?3) and (?4 is null or nb.noteBookType = ?4) order by nb.createdDate desc")
    List<NoteBook> getAll(String noteBookName, String noteBookContent, Integer status, Integer type);
}