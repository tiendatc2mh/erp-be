package com.blameo.idservice.repository;

import com.blameo.idservice.model.Action;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ActionRepository extends JpaRepository<Action, String> {
    Optional<Action> findByActionName(String name);

    Optional<Action> findByActionId(String id);

    @Query(value = " select * from action a where (lower(a.action_name) like lower(?1)  ESCAPE '!' or lower(a.action_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or a.action_status = ?2) and (?3 is null or ?3 <= a.created_date) and (?4 is null or ?4 >= a.created_date) " +
            "and a.action_status != -1 order by a.created_date desc", nativeQuery = true)
    Page<Action> findAll(Pageable paging, String keyword, Integer status, Date fromDate, Date toDate);

    @Query(value = "select * from action a where (lower(a.action_name) like lower(?1)  ESCAPE '!' or lower(a.action_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or a.action_status = ?2) and  a.action_status!=-1 order by a.created_date desc ", nativeQuery = true)
    List<Action> getAll(String s, Integer status);

    Boolean existsByActionCodeAndActionStatusGreaterThanEqual(String code, Integer status);

    Boolean existsByActionIdAndActionStatusGreaterThanEqual(String id, Integer status);
}
