package com.blameo.idservice.repository;

import com.blameo.idservice.model.Asset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AssetRepository extends JpaRepository<Asset, String> {

    Boolean existsAssetByAssetType_AssetTypeIdAndAssetStatusGreaterThanEqual(String assetTypeId, Integer assetStatus);

    Boolean existsAssetByAssetCodeAndAssetStatusGreaterThanEqual(String assetCode, Integer assetStatus);

    Boolean existsAssetByAssetSerialAndAssetStatusGreaterThanEqual(String assetCode, Integer assetStatus);

    Long countAssetByAssetType_AssetTypeId(String assetTypeId);

    @Query("select a, u.fullName from Asset a left join User u on a.userId = u.userId where " +
            "(lower(a.assetName) like lower(:keyword) ESCAPE '|' or lower(u.fullName) like lower(:keyword) ESCAPE '|') and " +
            "(:assetStatus is null or a.assetStatus = :assetStatus) and " +
            "(coalesce(:assetTypeId) is null or a.assetType.assetTypeId in (:assetTypeId)) and " +
            "(:requestId is null or a.requestId = :requestId) and " +
            "(:userId is null or a.userId = :userId) order by a.createdDate desc ")
    Page<Object[]> findAll(Pageable pageable, @Param("keyword") String keyword, @Param("assetStatus") Integer assetStatus,
                          @Param("assetTypeId") List<String> assetTypeId, @Param("userId") String userId, @Param("requestId") String requestId);


    @Query("select a, u.fullName from Asset a left join User u on a.userId = u.userId where " +
            "(lower(a.assetName) like lower(:keyword) ESCAPE '|' or lower(u.fullName) like lower(:keyword) ESCAPE '|') and " +
            "(:assetStatus is null or a.assetStatus = :assetStatus) and " +
            "(coalesce(:assetTypeId) is null or a.assetType.assetTypeId in (:assetTypeId)) and " +
            "(:requestId is null or a.requestId = :requestId) and " +
            "(:userId is null or a.userId = :userId) order by a.createdDate desc ")
    List<Object[]> getAll(@Param("keyword") String keyword, @Param("assetStatus") Integer assetStatus,
                          @Param("assetTypeId") List<String> assetTypeId, @Param("userId") String userId, @Param("requestId") String requestId);

    @Modifying
    @Query("update Asset set userId = null , assetStatus = 0 where userId = ?1")
    void resetAssetByUserId(String userId);

    @Query("select a, u.fullName from Asset a left join User u on a.userId = u.userId where " +
            "(:userId is null or a.userId = :userId) and a.assetStatus != -1 order by a.createdDate desc ")
    List<Object[]> getAllByUserId(@Param("userId") String userId);
}
