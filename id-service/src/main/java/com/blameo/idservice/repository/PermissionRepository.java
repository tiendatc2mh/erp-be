package com.blameo.idservice.repository;

import com.blameo.idservice.model.Permission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PermissionRepository extends JpaRepository<Permission, String> {

    Optional<Permission> findByPermissionId(String id);
    Optional<Permission> findByPermissionIdAndPermissionStatusGreaterThanEqual(String id,Integer status);

    @Query(value = " select p from Permission p where " +
            "lower(p.permissionCode) like lower(?1)  ESCAPE '!' " +
            "and lower(p.function.functionName) like lower(?2)  ESCAPE '!' "+
            "and lower(p.action.actionName) like lower(?3)  ESCAPE '!' "+
            "and (?4 is null or p.method = ?4) "+
            "and (?5 is null or p.permissionStatus = ?5) and p.permissionStatus != -1 order by p.createdDate desc")
    Page<Permission> findAll(Pageable paging, String keyword, String functionId, String actionId, Integer method, Integer status);

    @Query(value = "select * from permission p where (?1 is null or p.permission_status=?1) and p.permission_status!=-1 order by p.created_date desc ", nativeQuery = true)
    List<Permission> getAll(Integer status);

    Boolean existsByPermissionCodeAndPermissionStatusGreaterThanEqual(String code, Integer status);

    Boolean existsByPermissionIdAndPermissionStatusGreaterThanEqual(String id, Integer status);

    Boolean existsByAction_ActionIdAndPermissionStatusGreaterThanEqual(String actionId, Integer status);
    Boolean existsByFunction_FunctionIdAndPermissionStatusGreaterThanEqual(String actionId, Integer status);
}
