package com.blameo.idservice.repository;

import com.blameo.idservice.model.Role;
import com.blameo.idservice.model.TypeDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@Repository
public interface TypeDocumentRepository extends JpaRepository<TypeDocument, String> {
    Optional<TypeDocument> findByTypeDocumentName(String name);

    @Query(value = " select t from TypeDocument t where 1=1 " +
            "and (lower(t.typeDocumentName) like lower(?1)  ESCAPE '!' or lower(t.typeDocumentCode) like lower(?1)  ESCAPE '!') "+
            "and (?2 is null or t.typeDocumentStatus = ?2) and t.typeDocumentStatus != -1 order by t.createdDate desc")
    Page<TypeDocument> findAll(Pageable paging, String keyword, Integer status);

    @Query(value = "select t from TypeDocument t where t.typeDocumentStatus!=-1 " +
            "and (?1 is null or t.typeDocumentStatus = ?1) order by t.createdDate desc ")
    List<TypeDocument> getAll(Integer status);

    Boolean existsByTypeDocumentCodeAndTypeDocumentStatusGreaterThanEqual(String code,Integer status);
    Boolean existsByTypeDocumentIdAndTypeDocumentStatusGreaterThanEqual(String id,Integer status);
}
