package com.blameo.idservice.repository;

import com.blameo.idservice.model.DepartmentManager;
import com.blameo.idservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 9/14/2023, Thursday
 **/
public interface DepartmentManagerRepository extends JpaRepository<DepartmentManager, String> {
    List<DepartmentManager> findAllByDepartmentId(String departmentId);
    void deleteAllByDepartmentId(String departmentId);
}
