package com.blameo.idservice.repository;

import com.blameo.idservice.model.Permission;
import com.blameo.idservice.model.PersonalFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonalFileRepository extends JpaRepository<PersonalFile, String> {
    @Query(value = "select * from id.personal_file p where p.user_id=?1", nativeQuery = true)
    List<PersonalFile> getAll(String id);

    @Query(value = "select p,t.typeDocumentName from PersonalFile p join TypeDocument t on t.typeDocumentId = p.typeDocumentId where p.userId=?1")
    List<Object[]> getAllWithDocumentName(String id);

    Boolean existsByTypeDocumentId(String id);

}
