package com.blameo.idservice.repository;

import com.blameo.idservice.model.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PositionRepository extends JpaRepository<Position, String> {

    @Query(value = " select * from id.position p where (lower(p.position_name) like lower(?1)  ESCAPE '!' or lower(p.position_code) like lower(?1)  ESCAPE '!')  " +
            "and (?2 is null or p.position_status = ?2) and p.position_status != -1 order by p.created_date desc", nativeQuery = true)
    Page<Position> findAll(Pageable paging, String keyword, Integer status);

    @Query(value = "select * from id.position p where (lower(p.position_name) like lower(?1)  ESCAPE '!' or lower(p.position_code) like lower(?1)  ESCAPE '!') " +
            "and (?2 is null or p.position_status = ?2) and p.position_status != -1 order by p.created_date desc ", nativeQuery = true)
    List<Position> getAll(String s, Integer status);

    Boolean existsByPositionCodeAndPositionStatusEquals(String name, Integer status);
    Boolean existsByPositionCodeAndPositionStatusGreaterThanEqual(String name, Integer status);
    List<Position> getAllByPositionIdIn(List<String> positionIds);

    Boolean existsByPositionIdAndPositionStatusGreaterThanEqual(String name, Integer status);

    Boolean existsByDepartment_DepartmentIdAndPositionStatusEquals(String id, Integer status);

    Boolean existsByDepartment_DepartmentId(String id);
}
