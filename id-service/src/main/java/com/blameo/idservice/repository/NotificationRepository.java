package com.blameo.idservice.repository;

import com.blameo.idservice.model.Notification;
import com.blameo.idservice.model.TypeDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification, String> {

    @Query(value = " select n from Notification n where 1=1 " +
            "and (n.userReceiverId = :userId) " +
            "and (lower(n.message) like lower(:keyword)  ESCAPE '!') " +
            "and (:isRead is null or n.read = :isRead) " +
            "and (:type is null or n.type = :type) " +
            "order by n.createdDate desc")
    Page<Notification> findAll(Pageable paging, @Param("userId") String userId,
                               @Param("keyword") String keyword,
                               @Param("isRead") Integer isRead,
                               @Param("type") Integer type);

    @Query(value = " select n from Notification n where 1=1 " +
            "and (n.userReceiverId = :userId) " +
            "and (lower(n.message) like lower(:keyword)  ESCAPE '!') " +
            "and (:isRead is null or n.read = :isRead) " +
            "and (:type is null or n.type = :type) " +
            "order by n.createdDate desc")
    List<Notification> getAll(@Param("userId") String userId,
                              @Param("keyword") String keyword,
                              @Param("isRead") Integer isRead,
                              @Param("type") Integer type);

    @Query(value = "SELECT *\n" +
            "FROM (\n" +
            "    SELECT \n" +
            "        *,\n" +
            "        ROW_NUMBER() OVER (PARTITION BY type ORDER BY created_date DESC) AS row_num\n" +
            "    FROM notification n where user_receiver_id = :userId" +
            ") AS ranked\n" +
            "WHERE row_num <= 5",nativeQuery = true)
    List<Notification> getNotification5Record1Type(@Param("userId") String userId);

    Long countByTypeAndUserReceiverIdAndRead(Integer type,String userId,Integer read);

    @Query(value = " select n from Notification n where 1=1 " +
            "and (n.userReceiverId = :userId) " +
            "and (n.read = 0) " +
            "and (:type is null or n.type = :type) " +
            "order by n.createdDate desc")
    List<Notification> getAllNotReadByType(@Param("userId")String userId,@Param("type") Integer type);
}