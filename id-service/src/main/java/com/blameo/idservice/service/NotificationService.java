package com.blameo.idservice.service;

import com.blameo.idservice.config.CustomChannelInterceptor;
import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.NotificationDTO;
import com.blameo.idservice.dto.response.NotificationDTOResponse;
import com.blameo.idservice.dto.response.TypeDocumentDTOResponse;
import com.blameo.idservice.model.Notification;
import com.blameo.idservice.model.TypeDocument;
import com.blameo.idservice.model.User;
import com.blameo.idservice.repository.NotificationRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.security.services.UserDetailsImpl;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static com.blameo.idservice.constant.Constant.REGISTER_FOR_LEAVE;
import static com.blameo.idservice.constant.Constant.REGISTER_FOR_OT;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/23/2023, Wednesday
 **/
@Service
public class NotificationService {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    MessageSource messageSource;

    @Autowired
    UserRepository userRepository;


    public void createNotification(NotificationDTO notificationDTO) {
        Locale locale = CommonUtil.getLocale();
        // Lưu thông báo vào cơ sở dữ liệu
        Notification notification = new Notification();
        CoppyObject.copyNonNullProperties(notificationDTO, notification);
        notification.setCreatedDate(new Date());
        notification.setRead(0);
        Notification notificationSaved = notificationRepository.save(notification);
        NotificationDTOResponse notificationDTOResponse = new NotificationDTOResponse();
        BeanUtils.copyProperties(notificationSaved, notificationDTOResponse);
        notificationDTOResponse.setMessage(replaceMessage(notificationSaved.getMessage(),new Locale(StringUtils.isEmpty(CustomChannelInterceptor.localClientSocket.get(notificationDTO.getUserReceiverId()))?
                "vi" : CustomChannelInterceptor.localClientSocket.get(notificationDTO.getUserReceiverId()))));
        notificationDTOResponse.setUserRequestAvatar(userRepository.findById(notificationDTOResponse.getUserRequestId()).orElse(new User()).getAvatar());
        // Gửi thông báo thời gian thực đến người dùng
            messagingTemplate.convertAndSendToUser(notification.getUserReceiverId(), "/notifications", notificationDTOResponse);
    }

    public Map<String, Object> findByUserId(Integer pageNo, Integer pageSize, String userId, String keyword, Integer read, Integer type) {
        Locale locale = CommonUtil.getLocale();
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<Notification> page = notificationRepository.findAll(paging, userId, "%" + keyword + "%", read, type);
        if (page.hasContent()) {
            List<NotificationDTOResponse> notificationDTOS = CommonUtil.mapList(page.getContent(), NotificationDTOResponse.class);
            notificationDTOS.forEach(notificationDTOResponse -> {
                User requestUser = userRepository.findById(notificationDTOResponse.getUserRequestId()).orElse(new User());
                notificationDTOResponse.setMessage(replaceMessage(notificationDTOResponse.getMessage(),locale));
                notificationDTOResponse.setUserRequestAvatar(requestUser.getAvatar());
                notificationDTOResponse.setUserRequestEmail(requestUser.getEmail());
            });
            output.put("data", notificationDTOS);
            output.put("total", page.getTotalElements());
            output.put("page", pageNo);
            output.put("size", pageSize);
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
            output.put("page", pageNo);
            output.put("size", pageSize);
        }
        return output;
    }

    public List<NotificationDTOResponse> getAllByUserId(String userId, String keyword, Integer read, Integer type) {
        Locale locale = CommonUtil.getLocale();
        List<Notification> notificationList = notificationRepository.getAll(userId, "%" + keyword + "%", read, type);
        List<NotificationDTOResponse> notificationDTOResponses = CommonUtil.mapList(notificationList, NotificationDTOResponse.class);
        notificationDTOResponses.forEach(notificationDTOResponse -> {
            notificationDTOResponse.setMessage(replaceMessage(notificationDTOResponse.getMessage(),locale));
            User receiverUser = userRepository.findById(notificationDTOResponse.getUserRequestId()).orElse(new User());
            notificationDTOResponse.setUserRequestAvatar(receiverUser.getAvatar());
            notificationDTOResponse.setUserRequestEmail(receiverUser.getEmail());
        });
        return notificationDTOResponses;
    }

    public void changeStatus(String id) {
        Locale locale = CommonUtil.getLocale();
        String userId = SecurityContextHolder.getContext()
                .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal()).getUserId() : null;
        Notification notification = notificationRepository.findById(id).get();
        assert userId != null;
        if (userId.equals(notification.getUserReceiverId())) {
            // Nếu chưa đọc thì thêm, không thì thôi
                notification.setRead(1);
                notificationRepository.save(notification);
        } else {
            throw new RuntimeException(messageSource.getMessage("notificationMismatch", null, locale));
        }
    }

    public Map<String,Object> getNotification2Record1Type(String userId) {
        Locale locale = CommonUtil.getLocale();
        List<Notification> notificationList = notificationRepository.getNotification5Record1Type(userId);
        List<NotificationDTOResponse> notificationDTOResponses = CommonUtil.mapList(notificationList, NotificationDTOResponse.class);
        notificationDTOResponses.forEach(notificationDTOResponse -> {
            User receiverUser = userRepository.findById(notificationDTOResponse.getUserRequestId()).orElse(new User());
            notificationDTOResponse.setMessage(replaceMessage(notificationDTOResponse.getMessage(),locale));
            notificationDTOResponse.setUserRequestAvatar(receiverUser.getAvatar());
            notificationDTOResponse.setUserRequestEmail(receiverUser.getEmail());
        });
        Map<String,Object> result = new HashMap<>();
        Map<String,List<NotificationDTOResponse>> mapByType = notificationDTOResponses.stream().collect(Collectors.groupingBy(notificationDTOResponse -> {
            switch (notificationDTOResponse.getType()){
                case 1:
                    return "registerForLeave";
                case 2:
                    return "registerForOT";
                case 3:
                    return "registerForRemote";
                case 4:
                    return "buyEquipment";
                case 5:
                    return "paymentRequest";
                case 6:
                    return "workConfirmation";
                case 7:
                    return "recruitmentRequest";
                default:
                    return "other";
            }
        }));
        List<String> allTypes = Arrays.asList("registerForLeave", "registerForOT", "registerForRemote", "buyEquipment"
                , "paymentRequest", "workConfirmation", "recruitmentRequest", "other");
        for (String type : allTypes) {
            mapByType.putIfAbsent(type, new ArrayList<>());
        }
        result.put("data",mapByType);
        result.put("countRegisterForLeave",notificationRepository.countByTypeAndUserReceiverIdAndRead(Constant.PROCESS_TYPE.REGISTER_FOR_LEAVE,userId,Constant.NON_READ));
        result.put("countRegisterForOT",notificationRepository.countByTypeAndUserReceiverIdAndRead(Constant.PROCESS_TYPE.REGISTER_FOR_OT,userId,Constant.NON_READ));
        result.put("countRegisterForRemote",notificationRepository.countByTypeAndUserReceiverIdAndRead(Constant.PROCESS_TYPE.REGISTER_FOR_REMOTE,userId,Constant.NON_READ));
        result.put("countBuyEquipment",notificationRepository.countByTypeAndUserReceiverIdAndRead(Constant.PROCESS_TYPE.BUY_EQUIPMENT,userId,Constant.NON_READ));
        result.put("countPaymentRequest",notificationRepository.countByTypeAndUserReceiverIdAndRead(Constant.PROCESS_TYPE.PAYMENT_REQUEST,userId,Constant.NON_READ));
        result.put("countWorkConfirmation",notificationRepository.countByTypeAndUserReceiverIdAndRead(Constant.PROCESS_TYPE.WORK_CONFIRMATION,userId,Constant.NON_READ));
        result.put("countRecruitmentRequest",notificationRepository.countByTypeAndUserReceiverIdAndRead(Constant.PROCESS_TYPE.RECRUITMENT_REQUEST,userId,Constant.NON_READ));
        result.put("countOther",notificationRepository.countByTypeAndUserReceiverIdAndRead(Constant.PROCESS_TYPE.OTHER,userId,Constant.NON_READ));
        return result;
    }

    @Transactional
    public void readAll(String userId,Integer type) {
        List<Notification> notificationList = notificationRepository.getAllNotReadByType(userId,type);
        notificationList.forEach(notification -> {
            notification.setRead(1);
            notificationRepository.save(notification);
        });
    }

    public String replaceMessage(String resource,Locale locale){
        return resource
                .replace(Constant.REGISTER_FOR_LEAVE,messageSource.getMessage("registerForLeave",null,locale))
                .replace(Constant.REGISTER_FOR_OT,messageSource.getMessage("registerForOt",null,locale))
                .replace(Constant.REGISTER_FOR_REMOTE,messageSource.getMessage("registerForRemote",null,locale))
                .replace(Constant.BUY_EQUIPMENT,messageSource.getMessage("buyEquipment",null,locale))
                .replace(Constant.PAYMENT_REQUEST,messageSource.getMessage("paymentRequest",null,locale))
                .replace(Constant.WORK_CONFIRMATION,messageSource.getMessage("workConfirmation",null,locale))
                .replace(Constant.RECRUITMENT_REQUEST,messageSource.getMessage("recruitmentRequest",null,locale))
                .replace(Constant.SEND,messageSource.getMessage("send",null,locale))
                .replace(Constant.APPROVAL,messageSource.getMessage("approval",null,locale))
                .replace(Constant.REFUSE,messageSource.getMessage("refuse",null,locale))
                .replace(Constant.PERFORM,messageSource.getMessage("perform",null,locale))
                .replace(Constant.CODE,messageSource.getMessage("code",null,locale));
    }
}
