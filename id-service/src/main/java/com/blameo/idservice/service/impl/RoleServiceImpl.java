package com.blameo.idservice.service.impl;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.RoleDTO;
import com.blameo.idservice.dto.response.PermissionDTOResponse;
import com.blameo.idservice.dto.response.RoleDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.Permission;
import com.blameo.idservice.model.Role;
import com.blameo.idservice.model.User;
import com.blameo.idservice.repository.PermissionRepository;
import com.blameo.idservice.repository.RoleRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.RoleService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.*;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PermissionRepository permissionRepository;

    @Autowired
    MessageSource messageSource;

    @Autowired
    EntityManager entityManager;
    @Override
    public RoleDTOResponse save(RoleDTO roleDTO, UserDTOResponse userDTOResponse) {
        Role role = new Role();
        CoppyObject.copyNonNullProperties(roleDTO,role);
        Locale locale = CommonUtil.getLocale();
// add user
        Set<User>  users = new HashSet<>();
        if (roleDTO.getUsers() != null && roleDTO.getUsers().size()>0){
            roleDTO.getUsers().forEach(x->{
                User user = userRepository.findByUserIdAndEmployeeStatusGreaterThanEqual(x,0).orElseThrow(
                        ()->  new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale))
                );
                users.add(user);
            });
        }
        role.setUsers(users);
// add permission
        Set<Permission>  permissions = new HashSet<>();
        if (roleDTO.getPermissions() != null && roleDTO.getPermissions().size()>0){
            roleDTO.getPermissions().forEach(x->{
                Permission permission = permissionRepository.findByPermissionIdAndPermissionStatusGreaterThanEqual(x,1).orElseThrow(
                        ()->  new RuntimeException(messageSource.getMessage("permission",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale))
                );
                permissions.add(permission);
            });
        }
        role.setPermissions(permissions);
        //fake
        role.setCreatedBy(userDTOResponse.getUserId());
        role.setUpdatedBy(userDTOResponse.getUserId());
        Role saved =  roleRepository.save(role);
        RoleDTOResponse roleDTOResponse = new RoleDTOResponse();
        BeanUtils.copyProperties(saved,roleDTOResponse);
        roleDTOResponse = setEntityRelationship(saved,roleDTOResponse);
        return roleDTOResponse;
    }

    @Override
    public RoleDTOResponse update(RoleDTO roleDTO, String id, UserDTOResponse userDTOResponse)  {
        Role role= roleRepository.findById(id).get();
        Locale locale = CommonUtil.getLocale();
        if (!role.getRoleCode().equals(roleDTO.getRoleCode())) {
            if (roleRepository.existsByRoleCodeAndRoleStatusGreaterThanEqual(roleDTO.getRoleCode(),0)){
                throw new RuntimeException(messageSource.getMessage("role.code",null,locale)+ SPACE +messageSource.getMessage("isExist",null,locale));
            }
        }
        if (role.getRoleCode().equals(Constant.SUPER_ADMIN_ROLE)){
            throw new RuntimeException(messageSource.getMessage("role",null,locale)+ SPACE + messageSource.getMessage("notModified", null, locale));
        }
        if (roleDTO.getRoleCode() != null) {
            role.setRoleCode(roleDTO.getRoleCode());
        }
        if (roleDTO.getRoleName() != null) {
            role.setRoleName(roleDTO.getRoleName());
        }

        if (roleDTO.getRoleStatus() != null) {
            role.setRoleStatus(roleDTO.getRoleStatus());
        }
// add user
        Set<User>  users = new HashSet<>();
        if (roleDTO.getUsers() != null && roleDTO.getUsers().size()>0){
            roleDTO.getUsers().forEach(x->{
                User user = userRepository.findByUserIdAndEmployeeStatusGreaterThanEqual(x,0).orElseThrow(
                        ()->  new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale))
                );
                users.add(user);
            });
        }
        role.setUsers(users);
// add permission
        Set<Permission>  permissions = new HashSet<>();
        if (roleDTO.getPermissions() != null && roleDTO.getPermissions().size()>0){
            roleDTO.getPermissions().forEach(x->{
                Permission permission = permissionRepository.findByPermissionIdAndPermissionStatusGreaterThanEqual(x,1).orElseThrow(
                        ()->  new RuntimeException(messageSource.getMessage("permission",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale))
                );
                permissions.add(permission);
            });
        }
        role.setPermissions(permissions);
        //
        role.setUpdatedBy(userDTOResponse.getUserId());
        Role saved =  roleRepository.save(role);
        RoleDTOResponse roleDTOResponse = new RoleDTOResponse();
        BeanUtils.copyProperties(saved,roleDTOResponse);
        roleDTOResponse = setEntityRelationship(saved,roleDTOResponse);
        return roleDTOResponse;
    }

    @Override
    public RoleDTOResponse get(String id) {
        Role role = roleRepository.findById(id).get();
        RoleDTOResponse roleDTOResponse = new RoleDTOResponse();
        BeanUtils.copyProperties(role,roleDTOResponse);

        roleDTOResponse = setEntityRelationship(role,roleDTOResponse);
        roleDTOResponse.setCreatedByName(userRepository.getFullNameByUserId(roleDTOResponse.getCreatedBy()));
        roleDTOResponse.setUpdatedByName(userRepository.getFullNameByUserId(roleDTOResponse.getUpdatedBy()));
        return roleDTOResponse;
    }

    public  RoleDTOResponse setEntityRelationship(Role role, RoleDTOResponse roleDTOResponse){
        List<User> users = new ArrayList<>(role.getUsers());
        List<Permission> permissions = new ArrayList<>(role.getPermissions());
        //detach relationship
        entityManager.detach(role);
//user
        users.forEach(x->x.setRoles(null));
        Set<UserDTOResponse> userDTOResponses = new HashSet<>(CommonUtil.mapList(users, UserDTOResponse.class));
        roleDTOResponse.setUsers(userDTOResponses);

//permission
        Set<PermissionDTOResponse> permissionDTOResponses = new HashSet<>(CommonUtil.mapList(permissions, PermissionDTOResponse.class));
        roleDTOResponse.setPermissions(permissionDTOResponses);
        return roleDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status, Date createdDate) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo-1, pageSize);
        Page<Role> page = roleRepository.findAll(paging,"%"+keyword+"%",status,Constant.ROLE_SUPER_ADMIN,createdDate);
        if (page.hasContent()){
            List<Role> roles = page.getContent();
            List<RoleDTOResponse> roleDTOResponses = new ArrayList<>();
            for (Role role:roles){
                entityManager.detach(role);
                role.setUsers(null);
                RoleDTOResponse roleDTOResponse = new RoleDTOResponse();
                BeanUtils.copyProperties(role,roleDTOResponse);
                roleDTOResponses.add(roleDTOResponse);
            }
            output.put("data", roleDTOResponses);
            output.put("total", page.getTotalElements());
            output.put("page", pageNo);
            output.put("size", pageSize);
        }else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
            output.put("page", pageNo);
            output.put("size", pageSize);
        }
        return output;
    }


    @Override
    public List<RoleDTOResponse> getAll(Integer status) {
        List<Role> roles = roleRepository.getAll(status,Constant.ROLE_SUPER_ADMIN);
        List<RoleDTOResponse> roleDTOResponses = new ArrayList<>();
        for (Role role:roles){
            entityManager.detach(role);
            role.setUsers(null);
            RoleDTOResponse roleDTOResponse = new RoleDTOResponse();
            BeanUtils.copyProperties(role,roleDTOResponse);
            roleDTOResponses.add(roleDTOResponse);
        }
       return roleDTOResponses;
    }

    @Override
    public List<RoleDTOResponse> getAllListRoleId(List<String> roleId) {
        List<Role> roles = roleRepository.getAllListRoleId(roleId);
        List<RoleDTOResponse> roleDTOResponses = new ArrayList<>();
        for (Role role:roles){
            entityManager.detach(role);
            role.setUsers(null);
            RoleDTOResponse roleDTOResponse = new RoleDTOResponse();
            BeanUtils.copyProperties(role,roleDTOResponse);
            roleDTOResponses.add(roleDTOResponse);
        }
        return roleDTOResponses;
    }

    @Override
    public void delete(String id, UserDTOResponse userDTOResponse) {
        Locale locale = CommonUtil.getLocale();
        Role role = roleRepository.findById(id).get();
        if (role.getRoleCode().equals(Constant.SUPER_ADMIN_ROLE)){
            throw new RuntimeException(messageSource.getMessage("role",null,locale)+ SPACE  + messageSource.getMessage("notModified", null, locale));
        }
        role.setRoleStatus(-1);
        role.setUpdatedBy(userDTOResponse.getUserId());
        roleRepository.save(role);
    }

    @Override
    public void changeStatus(String id, UserDTOResponse userDTOResponse) {
        Locale locale = CommonUtil.getLocale();
        Role role = roleRepository.findById(id).get();
        if (role.getRoleCode().equals(Constant.SUPER_ADMIN_ROLE)){
            throw new RuntimeException(messageSource.getMessage("role",null,locale)+ SPACE + messageSource.getMessage("notModified", null, locale));
        }
        role.setRoleStatus(role.getRoleStatus() == 0 ? 1 : 0);
        role.setUpdatedBy(userDTOResponse.getUserId());
        roleRepository.save(role);
    }
}
