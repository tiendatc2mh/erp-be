package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.FunctionDTO;
import com.blameo.idservice.dto.response.FunctionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/3/2023, Thursday
**/
@Component
public interface FunctionService {

    FunctionDTOResponse save(FunctionDTO functionDTO, UserDTOResponse userDTOResponse);

    FunctionDTOResponse update(FunctionDTO functionDTO, String id, UserDTOResponse userDTOResponse);
    FunctionDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status, Date fromDate, Date toDate);

    List<FunctionDTOResponse> getAll(String keyword, Integer status);

    void delete(String id, UserDTOResponse userDTOResponse);

    void changeStatus(String id, UserDTOResponse userDTOResponse);
}
