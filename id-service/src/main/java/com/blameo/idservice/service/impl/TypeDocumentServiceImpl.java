package com.blameo.idservice.service.impl;

import com.blameo.idservice.dto.request.TypeDocumentDTO;
import com.blameo.idservice.dto.response.TypeDocumentDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.TypeDocument;
import com.blameo.idservice.repository.TypeDocumentRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.TypeDocumentService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/

@Service
@Transactional
public class TypeDocumentServiceImpl implements TypeDocumentService {

    @Autowired
    TypeDocumentRepository typeDocumentRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    MessageSource messageSource;
    @Override
    public TypeDocumentDTOResponse save(TypeDocumentDTO typeDocumentDTO, UserDTOResponse userDTOResponse) {
        TypeDocument typeDocument = new TypeDocument();
        CoppyObject.copyNonNullProperties(typeDocumentDTO,typeDocument);
        typeDocument.setCreatedBy(userDTOResponse.getUserId());
        typeDocument.setUpdatedBy(userDTOResponse.getUserId());
        TypeDocument saved =  typeDocumentRepository.save(typeDocument);
        TypeDocumentDTOResponse typeDocumentDTOResponse = new TypeDocumentDTOResponse();
        BeanUtils.copyProperties(saved,typeDocumentDTOResponse);
        return typeDocumentDTOResponse;
    }

    @Override
    public TypeDocumentDTOResponse update(TypeDocumentDTO typeDocumentDTO, String id, UserDTOResponse userDTOResponse)  {
        TypeDocument typeDocument= typeDocumentRepository.findById(id).get();
        if (!typeDocument.getTypeDocumentCode().equals(typeDocumentDTO.getTypeDocumentCode())) {
            if (typeDocumentRepository.existsByTypeDocumentCodeAndTypeDocumentStatusGreaterThanEqual(typeDocumentDTO.getTypeDocumentCode(),0)){
                throw new RuntimeException(messageSource.getMessage("typeDocument.code",null,CommonUtil.getLocale())+ SPACE +messageSource.getMessage("isExist",null,CommonUtil.getLocale()));
            }
        }
        if (typeDocumentDTO.getTypeDocumentCode() != null) {
            typeDocument.setTypeDocumentCode(typeDocumentDTO.getTypeDocumentCode());
        }
        if (typeDocumentDTO.getTypeDocumentName() != null) {
            typeDocument.setTypeDocumentName(typeDocumentDTO.getTypeDocumentName());
        }

        if (typeDocumentDTO.getTypeDocumentDescription() != null) {
            typeDocument.setTypeDocumentDescription(typeDocumentDTO.getTypeDocumentDescription());
        }

        if (typeDocumentDTO.getTypeDocumentStatus() != null) {
            typeDocument.setTypeDocumentStatus(typeDocumentDTO.getTypeDocumentStatus());
        }
        typeDocument.setUpdatedBy(userDTOResponse.getUserId());
        TypeDocument saved =  typeDocumentRepository.save(typeDocument);
        TypeDocumentDTOResponse typeDocumentDTOResponse = new TypeDocumentDTOResponse();
        BeanUtils.copyProperties(saved,typeDocumentDTOResponse);
        return typeDocumentDTOResponse;
    }

    @Override
    public TypeDocumentDTOResponse get(String id) {
        TypeDocument typeDocument = typeDocumentRepository.findById(id).get();
        TypeDocumentDTOResponse typeDocumentDTOResponse = new TypeDocumentDTOResponse();
        BeanUtils.copyProperties(typeDocument,typeDocumentDTOResponse);
        typeDocumentDTOResponse.setCreatedByName(userRepository.getFullNameByUserId(typeDocumentDTOResponse.getCreatedBy()));
        typeDocumentDTOResponse.setUpdatedByName(userRepository.getFullNameByUserId(typeDocumentDTOResponse.getUpdatedBy()));
        return typeDocumentDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo-1, pageSize);
        Page<TypeDocument> page = typeDocumentRepository.findAll(paging,"%"+keyword+"%",status);
        if (page.hasContent()){
            List<TypeDocumentDTOResponse> typeDocumentDTOS =  CommonUtil.mapList(page.getContent(),TypeDocumentDTOResponse.class);
            output.put("data", typeDocumentDTOS);
            output.put("total", page.getTotalElements());
            output.put("page", pageNo);
            output.put("size", pageSize);
        }else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
            output.put("page", pageNo);
            output.put("size", pageSize);
        }
        return output;
    }


    @Override
    public List<TypeDocumentDTOResponse> getAll(Integer status) {
       return CommonUtil.mapList(typeDocumentRepository.getAll(status),TypeDocumentDTOResponse.class);
    }

    @Override
    public void delete(String id, UserDTOResponse userDTOResponse) {
        TypeDocument typeDocument = typeDocumentRepository.findById(id).get();
        typeDocument.setTypeDocumentStatus(-1);
        typeDocument.setUpdatedBy(userDTOResponse.getUserId());
        typeDocumentRepository.save(typeDocument);
    }

    @Override
    public void changeStatus(String id, UserDTOResponse userDTOResponse) {
        TypeDocument typeDocument = typeDocumentRepository.findById(id).get();
        typeDocument.setTypeDocumentStatus(typeDocument.getTypeDocumentStatus() == 0 ? 1 : 0);
        typeDocument.setUpdatedBy(userDTOResponse.getUserId());
        typeDocumentRepository.save(typeDocument);
    }
}
