package com.blameo.idservice.service.impl;

import com.blameo.idservice.dto.request.PermissionDTO;
import com.blameo.idservice.dto.response.PermissionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.Action;
import com.blameo.idservice.model.Function;
import com.blameo.idservice.model.Permission;
import com.blameo.idservice.repository.ActionRepository;
import com.blameo.idservice.repository.FunctionRepository;
import com.blameo.idservice.repository.PermissionRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.PermissionService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/3/2023, Thursday
 **/
@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    PermissionRepository permissionRepository;
    @Autowired
    ActionRepository actionRepository;
    @Autowired
    FunctionRepository functionRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    MessageSource messageSource;
    @Override
    public PermissionDTOResponse save(PermissionDTO permissionDTO, UserDTOResponse userDTOResponse) {
        Permission permission = new Permission();
        CoppyObject.copyNonNullProperties(permissionDTO,permission);
        Action action = actionRepository.findByActionId(permissionDTO.getActionId()).orElseThrow(
                ()-> new RuntimeException(messageSource.getMessage("action",null,CommonUtil.getLocale())+ SPACE +messageSource.getMessage("notExist",null, CommonUtil.getLocale()))
        );
        permission.setAction(action);
        Function function = functionRepository.findByFunctionId(permissionDTO.getFunctionId()).orElseThrow(
                ()-> new RuntimeException(messageSource.getMessage("function",null,CommonUtil.getLocale())+ SPACE +messageSource.getMessage("notExist",null, CommonUtil.getLocale()))
        );
        permission.setFunction(function);
        permission.setCreatedBy(userDTOResponse.getUserId());
        permission.setUpdatedBy(userDTOResponse.getUserId());
        Permission saved =  permissionRepository.save(permission);
        PermissionDTOResponse permissionDTOResponse = new PermissionDTOResponse();
        BeanUtils.copyProperties(saved,permissionDTOResponse);
        return permissionDTOResponse;
    }

    @Override
    public PermissionDTOResponse update(PermissionDTO permissionDTO, String id, UserDTOResponse userDTOResponse)  {
        Permission permission= permissionRepository.findById(id).get();
        if (!permission.getPermissionCode().equals(permissionDTO.getPermissionCode())) {
            if (permissionRepository.existsByPermissionCodeAndPermissionStatusGreaterThanEqual(permissionDTO.getPermissionCode(),0)){
                throw new RuntimeException(messageSource.getMessage("permission.code",null,CommonUtil.getLocale())+ SPACE +messageSource.getMessage("isExist",null, CommonUtil.getLocale()));
            }
        }
        if (permissionDTO.getPermissionCode() != null) {
            permission.setPermissionCode(permissionDTO.getPermissionCode());
        }

        if (permissionDTO.getPath() != null) {
            permission.setPath(permissionDTO.getPath());
        }
        if (permissionDTO.getMethod() != null) {
            permission.setMethod(permissionDTO.getMethod());
        }
        if (permissionDTO.getActionId() != null) {
            Action action = actionRepository.findByActionId(permissionDTO.getActionId()).orElseThrow(
                    ()-> new RuntimeException(messageSource.getMessage("action",null,CommonUtil.getLocale())+ SPACE +messageSource.getMessage("notExist",null, CommonUtil.getLocale()))
            );
            permission.setAction(action);
        }
        if (permissionDTO.getFunctionId() != null) {
            Function function = functionRepository.findByFunctionId(permissionDTO.getFunctionId()).orElseThrow(
                    ()-> new RuntimeException(messageSource.getMessage("function",null,CommonUtil.getLocale())+ SPACE +messageSource.getMessage("notExist",null, CommonUtil.getLocale()))
            );
            permission.setFunction(function);
        }
        if (permissionDTO.getPermissionStatus() != null) {
            permission.setPermissionStatus(permissionDTO.getPermissionStatus());
        }
        permission.setUpdatedBy(userDTOResponse.getUserId());
        Permission saved =  permissionRepository.save(permission);
        PermissionDTOResponse permissionDTOResponse = new PermissionDTOResponse();
        BeanUtils.copyProperties(saved,permissionDTOResponse);
        return permissionDTOResponse;
    }

    @Override
    public PermissionDTOResponse get(String id) {
        Permission permission = permissionRepository.findById(id).get();
        PermissionDTOResponse permissionDTOResponse = new PermissionDTOResponse();
        BeanUtils.copyProperties(permission,permissionDTOResponse);
        permissionDTOResponse.setCreatedByName(userRepository.getFullNameByUserId(permissionDTOResponse.getCreatedBy()));
        permissionDTOResponse.setUpdatedByName(userRepository.getFullNameByUserId(permissionDTOResponse.getUpdatedBy()));
        return permissionDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword,String function, String action, Integer method, Integer status) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo-1, pageSize);
        Page<Permission> page = permissionRepository.findAll(paging,"%"+keyword+"%","%"+function+"%","%"+action+"%",method,status);
        if (page.hasContent()){
            List<PermissionDTOResponse> permissionDTOResponses =  CommonUtil.mapList(page.getContent(),PermissionDTOResponse.class);
            output.put("data", permissionDTOResponses);
            output.put("total", page.getTotalElements());
            output.put("page", pageNo);
            output.put("size", pageSize);
        }else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
            output.put("page", pageNo);
            output.put("size", pageSize);
        }
        return output;
    }


    @Override
    public List<PermissionDTOResponse> getAll(Integer status) {
        return CommonUtil.mapList(permissionRepository.getAll(status),PermissionDTOResponse.class);
    }

    @Override
    public void delete(String id, UserDTOResponse userDTOResponse) {
        Permission permission = permissionRepository.findById(id).get();
        permission.setPermissionStatus(-1);
        permission.setUpdatedBy(userDTOResponse.getUserId());
        permissionRepository.save(permission);
    }

    @Override
    public void changeStatus(String id, UserDTOResponse userDTOResponse) {
        Permission permission = permissionRepository.findById(id).get();
        permission.setPermissionStatus(permission.getPermissionStatus() == 0 ? 1 : 0);
        permission.setUpdatedBy(userDTOResponse.getUserId());
        permissionRepository.save(permission);
    }
}
