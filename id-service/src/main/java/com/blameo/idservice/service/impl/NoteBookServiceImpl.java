package com.blameo.idservice.service.impl;

import com.blameo.idservice.dto.request.NoteBookDTO;
import com.blameo.idservice.dto.response.NoteBookDTOResponse;
import com.blameo.idservice.model.NoteBook;
import com.blameo.idservice.repository.NoteBookRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.NoteBookService;
import com.blameo.idservice.utils.CommonUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 9/26/2023, Tuesday
 **/
@Service
@Transactional
public class NoteBookServiceImpl implements NoteBookService {

    private final NoteBookRepository noteBookRepository;
    private final UserRepository userRepository;

    public NoteBookServiceImpl(NoteBookRepository noteBookRepository,
                               UserRepository userRepository) {
        this.noteBookRepository = noteBookRepository;
        this.userRepository = userRepository;
    }

    @Override
    public NoteBookDTOResponse save(NoteBookDTO noteBookDTO) {
        NoteBook noteBook = CommonUtil.mapObject(noteBookDTO, NoteBook.class);
        if (noteBook.getListAttachment()!= null && noteBook.getListAttachment().size()>0){
            noteBook.getListAttachment().forEach(x -> {
                x.setNoteBook(noteBook);
            });
        }
        return CommonUtil.mapObject(noteBookRepository.save(noteBook), NoteBookDTOResponse.class);

    }

    @Override
    public NoteBookDTOResponse update(NoteBookDTO noteBookDTO, String id) {
        NoteBook noteBook = CommonUtil.mapObject(noteBookDTO, NoteBook.class);
        if (noteBook.getListAttachment()!= null && noteBook.getListAttachment().size()>0){
            noteBook.getListAttachment().forEach(x -> {
                x.setNoteBook(noteBook);
            });
        }
        noteBook.setNoteBookId(id);
        return CommonUtil.mapObject(noteBookRepository.save(noteBook), NoteBookDTOResponse.class);
    }

    @Override
    public NoteBookDTOResponse get(String id) {
        NoteBook noteBook = noteBookRepository.findById(id).get();
        NoteBookDTOResponse noteBookDTOResponse = CommonUtil.mapObject(noteBook, NoteBookDTOResponse.class);
        noteBookDTOResponse.setCreatedBy(userRepository.getFullNameByUserId(noteBook.getCreatedBy()));
        noteBookDTOResponse.setUpdatedBy(userRepository.getFullNameByUserId(noteBook.getUpdatedBy()));
        return noteBookDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String noteBookName, String noteBookContent, Integer status, Integer type) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<NoteBook> page = noteBookRepository.findAll(paging, "%" + noteBookName + "%", "%" + noteBookContent + "%", status,type);
        if (page.hasContent()) {
            List<NoteBookDTOResponse> noteBookDTOResponses = CommonUtil.mapList(page.getContent(), NoteBookDTOResponse.class);
            output.put("data", noteBookDTOResponses);
            output.put("total", page.getTotalElements());
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
        }
        output.put("page", pageNo);
        output.put("size", pageSize);
        return output;
    }

    @Override
    public List<NoteBookDTOResponse> getAll(String noteBookName, String noteBookContent, Integer status, Integer type) {
        List<NoteBook> noteBooks = new ArrayList<>();
        noteBooks = noteBookRepository.getAll("%" + noteBookName + "%", "%" + noteBookContent + "%", status,type);
        return CommonUtil.mapList(noteBooks,NoteBookDTOResponse.class);
    }

    @Override
    public void delete(String id) {
        NoteBook noteBook = noteBookRepository.findById(id).get();
        noteBook.setStatus(-1);
        noteBookRepository.save(noteBook);
    }

    @Override
    public void changeStatus(String id) {
        NoteBook noteBook = noteBookRepository.findById(id).get();
        noteBook.setStatus(noteBook.getStatus() == 0 ? 1 : 0);
        noteBookRepository.save(noteBook);
    }
}
