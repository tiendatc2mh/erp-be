package com.blameo.idservice.service.impl;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.UserDTO;
import com.blameo.idservice.dto.response.RoleDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.dto.response.UserManagerDTOResponse;
import com.blameo.idservice.model.*;
import com.blameo.idservice.repository.*;
import com.blameo.idservice.service.UserService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.ExcelUtil;
import com.blameo.idservice.utils.GenerateRandomPassword;
import com.blameo.idservice.utils.SSLEmail;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/2/2023, Wednesday
 **/
@Service
@EnableAsync
public class UserServiceImpl implements UserService {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    TitleRepository titleRepository;

    @Autowired
    LevelRepository levelRepository;

    @Autowired
    MessageSource messageSource;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    EntityManager entityManager;


    @Override
    public Map<String, Object> save(UserDTO userDTO, UserDTOResponse userRequest) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        Set<Role> roles = new HashSet<>();
        // get role and set
        for (String r : userDTO.getRoles()) {
            Role role = roleRepository.findById(r).orElseThrow(
                    () -> new RuntimeException(messageSource.getMessage("role",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
            );
            roles.add(role);
        }
        Set<User> users = new HashSet<>();
        if (userDTO.getManagers() != null && userDTO.getManagers().size() > 0) {
            for (String u : userDTO.getManagers()) {
                User user = userRepository.findById(u).orElseThrow(
                        () -> new RuntimeException(messageSource.getMessage("user.manager", null, locale) + SPACE + messageSource.getMessage("notExist", null, locale))
                );
                users.add(user);
            }
        }
        User user = new User();
        BeanUtils.copyProperties(userDTO, user);
        //department
        Department department = departmentRepository.findByDepartmentId(userDTO.getDepartmentId()).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("department",null,locale)+ SPACE  + messageSource.getMessage("notExist", null, locale))
        );
        user.setDepartment(department);
        //position
        Position position = positionRepository.findById(userDTO.getPositionId()).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("position",null,locale)+ SPACE  + messageSource.getMessage("notExist", null, locale))
        );
        user.setPosition(position);
        //title
        Title title = titleRepository.findById(userDTO.getTitleId()).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("title",null,locale)+ SPACE  + messageSource.getMessage("notExist", null, locale))
        );
        user.setTitle(title);
        //level
        Level level = levelRepository.findById(userDTO.getLevelId()).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("level",null,locale)+ SPACE  + messageSource.getMessage("notExist", null, locale))
        );
        user.setLevel(level);
        //set role
        user.setRoles(roles);
        //set manager
        user.setManagers(users);
        //set password default
        String passwordGenerate = GenerateRandomPassword.generateRandomPassword(8);
        user.setPassword(encoder.encode(passwordGenerate));
        //set code
        user.setUserCode(Constant.PREFIX_USER_CODE + String.format("%04d",userRepository.countUser()));
        user.setCreatedBy(userRequest.getUserId());
        user.setUpdatedBy(userRequest.getUserId());
        User saved = userRepository.save(user);
        UserDTOResponse userDTOResponse = new UserDTOResponse();
        BeanUtils.copyProperties(saved, userDTOResponse);
        userDTOResponse = setEntityRelationship(saved, userDTOResponse);
        Map<String,Object> data = new HashMap<>();
        data.put("user",userDTOResponse);
        data.put("password",passwordGenerate);
        return data;
    }

    @Async
    public void sendMail(String subject, String mailContent, String[] listAccount,
                         byte[][] attachmentDatas, String[] attachmentNames){
        SSLEmail.sendEmail(subject, mailContent, listAccount, attachmentDatas, attachmentNames);
    }

    @Override
    public UserDTOResponse update(UserDTO userDTO, String id, UserDTOResponse userRequest) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        User user = userRepository.findById(id).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
        );
        //check attendance
        if (!user.getAttendanceCode().equals(userDTO.getAttendanceCode())) {
            if (userRepository.existsByAttendanceCode(userDTO.getAttendanceCode())) {
                throw new RuntimeException(messageSource.getMessage("user.attendanceCode",null,locale)+ SPACE + messageSource.getMessage("isExist", null, locale));
            }
        }
        //check email
        if (!user.getEmail().equals(userDTO.getEmail())) {
            if (userRepository.existsByEmailAndEmployeeStatusGreaterThanEqual(userDTO.getEmail(), 0)) {
                throw new RuntimeException(messageSource.getMessage("validate.duplicateEmailCompany", null, locale));
            }
        }
        //check cccd
        if(!Objects.equals(userDTO.getIdentityCard(),user.getIdentityCard())){
            if (!StringUtils.isEmpty(userDTO.getIdentityCard()) && userRepository.existsByIdentityCard(userDTO.getIdentityCard())) {
                throw new RuntimeException(messageSource.getMessage("validate.duplicateIdentity", null, locale));
            }
        }
        //check phone number
        if(!user.getPhoneNumber().equals(userDTO.getPhoneNumber())){
            if (userRepository.existsByPhoneNumber(userDTO.getPhoneNumber())) {
                throw new RuntimeException(messageSource.getMessage("validate.duplicatePhoneNumber", null, locale));
            }
        }
        //check bhxh
        if(!Objects.equals(userDTO.getSocialInsuranceCode(),user.getSocialInsuranceCode())){
            if (!StringUtils.isEmpty(userDTO.getSocialInsuranceCode()) && userRepository.existsBySocialInsuranceCode(userDTO.getSocialInsuranceCode())) {
                throw new RuntimeException(messageSource.getMessage("validate.duplicateSocialInsuranceCode", null, locale));
            }
        }

        //check personal email
        if(!Objects.equals(userDTO.getPersonalEmail(),user.getPersonalEmail())){
            if (!StringUtils.isEmpty(userDTO.getPersonalEmail()) && userRepository.existsByPersonalEmail(userDTO.getPersonalEmail())) {
                throw new RuntimeException(messageSource.getMessage("validate.duplicateEmailPersonal", null, locale));
            }
        }

        //check tax code
        if(!Objects.equals(userDTO.getTaxCode(),user.getTaxCode())){
            if (!StringUtils.isEmpty(userDTO.getTaxCode()) && userRepository.existsByTaxCode(userDTO.getTaxCode())) {
                throw new RuntimeException(messageSource.getMessage("validate.duplicateTaxCode", null, locale));
            }
        }

        //check attendance
        if (!user.getBankType().equals(userDTO.getBankType())||!user.getBankAccountNumber().equals(userDTO.getBankAccountNumber())) {
            if (userRepository.existsByBankTypeAndBankAccountNumber(userDTO.getBankType(),userDTO.getBankAccountNumber())) {
                throw new RuntimeException(messageSource.getMessage("validate.duplicateBank",null,locale));
            }
        }
        //check admin user
        if (user.getEmail().equals(Constant.ADMIN_EMAIL)){
            throw new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notModified", null, locale));
        }

        //copy DTO > Entity
        BeanUtils.copyProperties(userDTO, user);

        if (!userDTO.getDepartmentId().isEmpty()) {
            //department
            Department department = departmentRepository.findByDepartmentId(userDTO.getDepartmentId()).orElseThrow(
                    () -> new RuntimeException(messageSource.getMessage("department",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
            );
            user.setDepartment(department);
        }
        if (!userDTO.getPositionId().isEmpty()) {
            //position
            Position position = positionRepository.findById(userDTO.getPositionId()).orElseThrow(
                    () -> new RuntimeException(messageSource.getMessage("position",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
            );
            user.setPosition(position);
        }
        if (!userDTO.getTitleId().isEmpty()) {
            //title
            Title title = titleRepository.findById(userDTO.getTitleId()).orElseThrow(
                    () -> new RuntimeException(messageSource.getMessage("title",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
            );
            user.setTitle(title);
        }

        if (!userDTO.getLevelId().isEmpty()) {
            //level
            Level level = levelRepository.findById(userDTO.getLevelId()).orElseThrow(
                    () -> new RuntimeException(messageSource.getMessage("level",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
            );
            user.setLevel(level);
        }
        if (userDTO.getRoles().size() > 0) {
            Set<Role> roles = new HashSet<>();
            // get role and set
            for (String r : userDTO.getRoles()) {
                Role role = roleRepository.findById(r).orElseThrow(
                        () -> new RuntimeException(messageSource.getMessage("role",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
                );
                roles.add(role);
            }
            //set role
            user.setRoles(roles);
        }

        if (userDTO.getManagers() != null && userDTO.getManagers().size() > 0) {
            userDTO.getManagers().remove(id);
            Set<User> managers = new HashSet<>();
            for (String u : userDTO.getManagers()) {
                User manager = userRepository.findById(u).orElseThrow(
                        () -> new RuntimeException(messageSource.getMessage("user.manager", null, locale) + SPACE + messageSource.getMessage("notExist", null, locale))
                );
                managers.add(manager);
            }
            user.setManagers(managers);
        }else {user.setManagers(new HashSet<>());}
        user.setUpdatedBy(userRequest.getUserId());
        User saved = userRepository.save(user);
        UserDTOResponse userDTOResponse = new UserDTOResponse();
        BeanUtils.copyProperties(saved, userDTOResponse);
        userDTOResponse = setEntityRelationship(saved, userDTOResponse);
        return userDTOResponse;
    }

    @Override
    public UserDTOResponse get(String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        User user = userRepository.findById(id).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
        );
        UserDTOResponse userDTOResponse = new UserDTOResponse();
        BeanUtils.copyProperties(user, userDTOResponse);

        userDTOResponse = setEntityRelationship(user, userDTOResponse);

        return userDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String department, String keyword, Integer status, List<Integer> type, String userCode, Date severanveDate) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        List<User> users = userRepository.getAll(null, "%%", Constant.ACTIVE, Constant.ADMIN_EMAIL,"%%");
        Page<User> page = userRepository.findAll(paging, department, "%" + keyword + "%", status, type, Constant.ADMIN_EMAIL,"%" + userCode + "%",severanveDate);
        if (page.hasContent()) {
            List<UserDTOResponse> userDTOResponses = new ArrayList<>();
            page.getContent().forEach(x -> {
                UserDTOResponse userDTOResponse = new UserDTOResponse();
                BeanUtils.copyProperties(x,userDTOResponse);
                userDTOResponses.add(setEntityRelationship(x,userDTOResponse));
            });
            output.put("data", userDTOResponses);
            output.put("total", page.getTotalElements());
            output.put("totalActive", (long) users.size());
            output.put("official", users.stream().filter(x -> x.getUserType() == 1).count());
            output.put("probationary", users.stream().filter(x -> x.getUserType() == 2).count());
            output.put("intern", users.stream().filter(x -> x.getUserType() == 3).count());
            output.put("collaborators", users.stream().filter(x -> x.getUserType() == 4).count());
            output.put("page", pageNo);
            output.put("size", pageSize);
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
            output.put("totalActive", (long) users.size());
            output.put("official", users.stream().filter(x -> x.getUserType() == 1).count());
            output.put("probationary", users.stream().filter(x -> x.getUserType() == 2).count());
            output.put("intern", users.stream().filter(x -> x.getUserType() == 3).count());
            output.put("collaborators", users.stream().filter(x -> x.getUserType() == 4).count());
            output.put("page", pageNo);
            output.put("size", pageSize);
        }
        return output;
    }

    @Override
    public List<UserDTOResponse> getAll(String department, Integer status, String keyword, String userCode) {
        List<User> users = userRepository.getAll(department, "%" +keyword + "%", status, Constant.ADMIN_EMAIL,"%" +userCode + "%");
        List<UserDTOResponse> userDTOResponses = new ArrayList<>();
        users.forEach(x -> {
            UserDTOResponse userDTOResponse = new UserDTOResponse();
            BeanUtils.copyProperties(x,userDTOResponse);
            userDTOResponses.add(setEntityRelationship(x,userDTOResponse));
        });
        return userDTOResponses;
    }

    @Override
    public List<UserDTOResponse> getAllSchedule() {
        List<User> users = userRepository.getAllSchedule(Constant.ADMIN_EMAIL);
        List<UserDTOResponse> userDTOResponses = new ArrayList<>();
        users.forEach(x -> {
            UserDTOResponse userDTOResponse = new UserDTOResponse();
            BeanUtils.copyProperties(x,userDTOResponse);
            userDTOResponses.add(setEntityRelationship(x,userDTOResponse));
        });
        return userDTOResponses;
    }

    @Override
    public List<UserDTOResponse> getAllMultipleId(List<String> listUserId) {
        List<User> users = userRepository.getAllByUserIdIn(listUserId);
        List<UserDTOResponse> userDTOResponses = new ArrayList<>();
        users.forEach(x -> {
            UserDTOResponse userDTOResponse = new UserDTOResponse();
            BeanUtils.copyProperties(x,userDTOResponse);
            userDTOResponses.add(setEntityRelationship(x,userDTOResponse));
        });
        return userDTOResponses;
    }

    @Override
    public void delete(String id, UserDTOResponse userRequest) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        User user = userRepository.findById(id).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
        );
        if (user.getEmail().equals(Constant.ADMIN_EMAIL)){
            throw new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notModified", null, locale));
        }
        user.setEmployeeStatus(-1);
        user.setUpdatedBy(userRequest.getUserId());
        userRepository.save(user);
    }

    @Override
    public void changeStatus(String id, UserDTOResponse userRequest) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        User user = userRepository.findById(id).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
        );
        if (user.getEmail().equals(Constant.ADMIN_EMAIL)){
            throw new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notModified", null, locale));
        }
        user.setEmployeeStatus(user.getEmployeeStatus() == 0 ? 1 : 0);
        user.setUpdatedBy(userRequest.getUserId());
        userRepository.save(user);

    }

    @Override
    public UserDTOResponse findByEmail(String email) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        User user = userRepository.findByEmail(email).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
        );
        UserDTOResponse userDTOResponse = new UserDTOResponse();
        BeanUtils.copyProperties(user, userDTOResponse);
        userDTOResponse = setEntityRelationship(user, userDTOResponse);
        return userDTOResponse;
    }

    @Override
    public boolean isValidOldPassword(String username, String oldPassword) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        User user = userRepository.findByEmail(username).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
        );
        return encoder.matches(oldPassword, user.getPassword());
    }

    @Override
    public void changePassword(String username, String newPassword) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        User user = userRepository.findByEmail(username).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale))
        );
        user.setPassword(encoder.encode(newPassword));
        userRepository.save(user);
    }

    @Override
    public InputStreamResource export(String department, String keyword, Integer status, List<Integer> type, String userCode) {
        Locale locale = CommonUtil.getLocale();
        try {
            List<Object[]> userAsset = userRepository.findAllToExport(department, "%" + keyword + "%", status, type,Constant.ADMIN_EMAIL,"%"+userCode+"%");
            Map<Department, Map<User, List<Asset>>> departmentUserAssetHashMap = userAsset.stream()
                    .collect(Collectors.groupingBy(o -> ((User) o[0]).getDepartment(),
                            Collectors.groupingBy(o -> (User) o[0], Collectors.mapping(o -> (Asset) o[1], Collectors.toList()))));
//            long totalAssetCount = departmentUserAssetMap.values().stream()
//                    .flatMap(userAssetMap -> userAssetMap.values().stream())
//                    .mapToLong(List::size)
//                    .sum();
            TreeMap<Department, Map<User, List<Asset>>> departmentUserAssetMap = new TreeMap<>(departmentUserAssetHashMap);
            InputStream templateStream = UserServiceImpl.class.getResourceAsStream("/template/" + Constant.TEMPLATE_USER);
            XSSFWorkbook workbook = new XSSFWorkbook(templateStream);

            //create CreationHelper
            CreationHelper createHelper = workbook.getCreationHelper();

            //style basic
            XSSFCellStyle style = ExcelUtil.getStyleBasic(workbook);
            style.setWrapText(true);

            //style basic Blue
            XSSFCellStyle styleLightBlue = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorBlue = new XSSFColor(new java.awt.Color(188, 212, 236));
            styleLightBlue.setFillForegroundColor(colorBlue);
            styleLightBlue.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleLightBlue.setWrapText(true);

            //style basic Pink
            XSSFCellStyle stylePink = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorPink = new XSSFColor(new java.awt.Color(252, 236, 228));
            stylePink.setFillForegroundColor(colorPink);
            stylePink.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            stylePink.setWrapText(true);

            //style basic Yellow
            XSSFCellStyle styleYellow = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorYellow = new XSSFColor(new java.awt.Color(255, 242, 204));
            styleYellow.setFillForegroundColor(colorYellow);
            styleYellow.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleYellow.setWrapText(true);

            //style basic Green
            XSSFCellStyle styleGreen = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorGreen = new XSSFColor(new java.awt.Color(228, 236, 220));
            styleGreen.setFillForegroundColor(colorGreen);
            styleGreen.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleGreen.setWrapText(true);

            //style basic Grey
            XSSFCellStyle styleGrey = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorGrey = new XSSFColor(new java.awt.Color(236, 236, 236));
            styleGrey.setFillForegroundColor(colorGrey);
            styleGrey.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleGrey.setWrapText(true);

            //Date style basic
            XSSFCellStyle styleDate = ExcelUtil.getStyleBasic(workbook);
            styleDate.setDataFormat(createHelper.createDataFormat().getFormat("dd-mm-yyyy"));

            //Date style Yellow
            XSSFCellStyle styleDateYellow = ExcelUtil.getStyleBasic(workbook);
            styleDateYellow.setFillForegroundColor(colorYellow);
            styleDateYellow.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleDateYellow.setDataFormat(createHelper.createDataFormat().getFormat("dd-mm-yyyy"));

            //Date style Orange
            XSSFCellStyle styleDateOrange = ExcelUtil.getStyleBasic(workbook);
            XSSFColor colorOrange = new XSSFColor(new java.awt.Color(252, 204, 172));
            styleDateOrange.setFillForegroundColor(colorOrange);
            styleDateOrange.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleDateOrange.setDataFormat(createHelper.createDataFormat().getFormat("dd-mm-yyyy"));

            //Date style Grey
            XSSFCellStyle styleDateGrey = ExcelUtil.getStyleBasic(workbook);
            styleDateGrey.setFillForegroundColor(colorGrey);
            styleDateGrey.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleDateGrey.setDataFormat(createHelper.createDataFormat().getFormat("dd-mm-yyyy"));

            XSSFSheet sheet = workbook.getSheetAt(0);
            AtomicInteger rowIndex = new AtomicInteger(4); //  tiêu đề, bắt đầu từ row 5 để thêm dữ liệu
            AtomicInteger stt = new AtomicInteger(1); // đánh stt

            departmentUserAssetMap.forEach((key, value) -> {

                AtomicBoolean isFirstDepartmentCell = new AtomicBoolean(true);
                AtomicInteger countRowInDepartment = new AtomicInteger(0);
                TreeMap<User, List<Asset>> TreeMapValue = new TreeMap<>(value);
                TreeMapValue.forEach((keyUserAsset, valueUserAsset) -> {
                    AtomicInteger countRowUser = new AtomicInteger(0);
                    for (int i = 0; i < valueUserAsset.size(); i++) {
                        Row row = sheet.createRow(rowIndex.get());
                        //stt
                        Cell cellStt = row.createCell(0);
                        cellStt.setCellStyle(styleLightBlue);
                        //department
                        Cell cellDePartment = row.createCell(1);
                        cellDePartment.setCellStyle(stylePink);
                        //user code
                        Cell cellUserCode = row.createCell(2);
                        cellUserCode.setCellStyle(stylePink);
                        //user name
                        Cell cellUserName = row.createCell(3);
                        cellUserName.setCellStyle(style);
                        //position
                        Cell cellUserPosition = row.createCell(4);
                        cellUserPosition.setCellStyle(style);
                        //phone
                        Cell cellUserPhone = row.createCell(5);
                        cellUserPhone.setCellStyle(styleLightBlue);
                        //email
                        Cell cellUserEmail = row.createCell(6);
                        cellUserEmail.setCellStyle(styleLightBlue);
                        //Birth
                        Cell cellUserBirthDay = row.createCell(7);
                        cellUserBirthDay.setCellStyle(styleDateYellow);
                        //Identity card
                        Cell cellUserIdentityCard = row.createCell(8);
                        cellUserIdentityCard.setCellStyle(styleYellow);
                        //Identity date
                        Cell cellUserIdentityCardDate = row.createCell(9);
                        cellUserIdentityCardDate.setCellStyle(styleDateYellow);
                        //Identity place
                        Cell cellUserIdentityPlace = row.createCell(10);
                        cellUserIdentityPlace.setCellStyle(styleYellow);
                        //Residence
                        Cell cellUserResidence = row.createCell(11);
                        cellUserResidence.setCellStyle(styleGreen);
                        //Permanent Address
                        Cell cellUserPermanentAddress = row.createCell(12);
                        cellUserPermanentAddress.setCellStyle(styleGreen);
                        //Tax Code
                        Cell cellUserTaxCode = row.createCell(13);
                        cellUserTaxCode.setCellStyle(styleLightBlue);
                        //Social insurance code
                        Cell cellUserSocialInsuranceCode = row.createCell(14);
                        cellUserSocialInsuranceCode.setCellStyle(styleLightBlue);
                        //Bank type
                        Cell cellBankType = row.createCell(15);
                        cellBankType.setCellStyle(styleLightBlue);
                        //Bank number
                        Cell cellBankNumber = row.createCell(16);
                        cellBankNumber.setCellStyle(styleLightBlue);
                        //Internship start date
                        Cell cellUserInternshipStartDate = row.createCell(17);
                        cellUserInternshipStartDate.setCellStyle(styleDateOrange);
                        //Probationary start date
                        Cell cellUserProbationaryStartDate = row.createCell(18);
                        cellUserProbationaryStartDate.setCellStyle(styleDateOrange);
                        //Office date
                        Cell cellUserOfficeDate = row.createCell(19);
                        cellUserOfficeDate.setCellStyle(styleDateOrange);
                        //Asset 20-21
                        Asset asset = new Asset();
                        if (!valueUserAsset.isEmpty()){
                            asset = valueUserAsset.get(i);
                        }
                        Cell cellAssetCode = row.createCell(20);
                        cellAssetCode.setCellStyle(styleGrey);
                        Cell cellAssetDate = row.createCell(21);
                        cellAssetDate.setCellStyle(styleDateGrey);
                        if (asset!=null){
                            cellAssetCode.setCellValue(asset.getAssetCode());
                            cellAssetDate.setCellValue(asset.getAssetDate());
                        }

                        if (i == 0) {
                            cellStt.setCellValue(stt.get());
                            if (isFirstDepartmentCell.get()){
                                cellDePartment.setCellValue(key.getDepartmentName());
                            }
                            cellUserCode.setCellValue(keyUserAsset.getUserCode());
                            cellUserName.setCellValue(keyUserAsset.getFullName());
                            cellUserPosition.setCellValue(keyUserAsset.getPosition().getPositionName());
                            cellUserPhone.setCellValue(keyUserAsset.getPhoneNumber());
                            cellUserEmail.setCellValue(keyUserAsset.getEmail());
                            cellUserBirthDay.setCellValue(keyUserAsset.getBirthday());
                            cellUserIdentityCard.setCellValue(keyUserAsset.getIdentityCard());
                            cellUserIdentityCardDate.setCellValue(keyUserAsset.getDateOfIdentity());
                            cellUserIdentityPlace.setCellValue(keyUserAsset.getPlaceOfIdentity());
                            cellUserResidence.setCellValue(keyUserAsset.getResidence());
                            cellUserPermanentAddress.setCellValue(keyUserAsset.getPermanentAddress());
                            cellUserTaxCode.setCellValue(keyUserAsset.getTaxCode());
                            cellUserSocialInsuranceCode.setCellValue(keyUserAsset.getSocialInsuranceCode());
                            cellBankType.setCellValue(keyUserAsset.getBankType());
                            cellBankNumber.setCellValue(keyUserAsset.getBankAccountNumber());
                            cellUserInternshipStartDate.setCellValue(keyUserAsset.getInternshipStartDate());
                            cellUserProbationaryStartDate.setCellValue(keyUserAsset.getProbationaryStartDate());
                            cellUserOfficeDate.setCellValue(keyUserAsset.getOfficialDate());
                        }
                        rowIndex.getAndIncrement();
                        countRowInDepartment.getAndIncrement();
                        countRowUser.getAndIncrement();
                    }
                    if (countRowUser.get()>1){
                        //merge user
                        sheet.addMergedRegion(new CellRangeAddress(rowIndex.get() - countRowUser.get(), rowIndex.get() -1,0,0));
                        for (int i = 2;i<21;i++){
                            sheet.addMergedRegion(new CellRangeAddress(rowIndex.get() - countRowUser.get(), rowIndex.get() -1,i,i));
                        }
                    }
                    stt.getAndIncrement();
                    isFirstDepartmentCell.set(false);
                });
                if(countRowInDepartment.get() > 1){
                    //merge department
                    sheet.addMergedRegion(new CellRangeAddress(rowIndex.get() - countRowInDepartment.get(), rowIndex.get() -1,1,1));
                }


            });

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            workbook.close();
            InputStreamResource inputStream = new InputStreamResource(new ByteArrayInputStream(outputStream.toByteArray()));
            // Tạo Resource từ file Excel trong bộ nhớ tạm
            return inputStream;
        } catch (Exception e) {
            throw new RuntimeException(messageSource.getMessage("exportFail", null, locale));
        }
    }

    public UserDTOResponse setEntityRelationship(User user, UserDTOResponse userDTOResponse) {
        List<Role> roles = new ArrayList<>(user.getRoles());
        roles.forEach(x -> {
            Set<Permission> permissionSet = new HashSet<>(x.getPermissions());
            entityManager.detach(x);
            x.setPermissions(permissionSet);
            x.setUsers(null);
        });
        Set<RoleDTOResponse> roleDTOResponses = new HashSet<>(CommonUtil.mapList(roles, RoleDTOResponse.class));
        userDTOResponse.setRoles(roleDTOResponses);

        Set<UserManagerDTOResponse> userManagerDTOResponses = new HashSet<>(CommonUtil.mapList(new ArrayList<>(user.getManagers()), UserManagerDTOResponse.class));
        userDTOResponse.setManagers(userManagerDTOResponses);
        return userDTOResponse;
    }
}

