package com.blameo.idservice.service.impl;

import com.blameo.idservice.dto.request.LevelDTO;
import com.blameo.idservice.dto.response.LevelDTOResponse;
import com.blameo.idservice.exception.RuntimeExceptionCustom;
import com.blameo.idservice.model.Department;
import com.blameo.idservice.model.Level;
import com.blameo.idservice.repository.DepartmentRepository;
import com.blameo.idservice.repository.LevelRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.LevelService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/2/2023, Wednesday
 **/
@Service
@Transactional
public class LevelServiceImpl implements LevelService {
    LevelRepository levelRepository;

    UserRepository userRepository;

    MessageSource messageSource;
    private final DepartmentRepository departmentRepository;

    public LevelServiceImpl(LevelRepository levelRepository, UserRepository userRepository, MessageSource messageSource,
                            DepartmentRepository departmentRepository) {
        this.levelRepository = levelRepository;
        this.userRepository = userRepository;
        this.messageSource = messageSource;
        this.departmentRepository = departmentRepository;
    }


    @Override
    public LevelDTOResponse save(LevelDTO levelDTO) {
        Level level = new Level();
        CoppyObject.copyNonNullProperties(levelDTO, level);
        Department department = departmentRepository.findByDepartmentId(levelDTO.getDepartmentId()).get();
        level.setDepartment(department);
        Level saved = levelRepository.save(level);
        LevelDTOResponse levelDTOResponse = new LevelDTOResponse();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
//        BeanUtils.copyProperties(saved, levelDTOResponse);
        levelDTOResponse =  modelMapper.map(saved,LevelDTOResponse.class);
        return levelDTOResponse;
    }

    @Override
    public LevelDTOResponse update(LevelDTO levelDTO, String id) {
        Optional<Level> result = levelRepository.findById(id);
        Level level;
        LevelDTOResponse levelDTOResponse = new LevelDTOResponse();
        if (result.isPresent()) {
            level = result.get();
            if (!level.getLevelCode().equals(levelDTO.getLevelCode()) && Boolean.TRUE.equals((levelRepository.existsByLevelCodeAndLevelStatusGreaterThanEqual(levelDTO.getLevelCode(), 0)))) {
                throw new RuntimeExceptionCustom(messageSource.getMessage("level.code", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("isExist", null, CommonUtil.getLocale()));
            }
            level.setLevelCode(levelDTO.getLevelCode());

            level.setLevelName(levelDTO.getLevelName());

            level.setLevelStatus(levelDTO.getLevelStatus());

            level.setCoefficients(levelDTO.getCoefficients());

            level.setLevelDescription(levelDTO.getLevelDescription());

            if (levelDTO.getDepartmentId() != null) {
                Department department = departmentRepository.findByDepartmentId(levelDTO.getDepartmentId()).orElseThrow(
                        ()-> new RuntimeException(messageSource.getMessage("department",null,CommonUtil.getLocale())+ SPACE +messageSource.getMessage("notExist",null, CommonUtil.getLocale()))
                );
                level.setDepartment(department);
            }

            Level saved = levelRepository.save(level);
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.getConfiguration()
                    .setFieldMatchingEnabled(true)
                    .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
            modelMapper.map(saved, levelDTOResponse);
        }
        return levelDTOResponse;
    }

    @Override
    public LevelDTOResponse get(String id) {
        LevelDTOResponse levelDTOResponse = new LevelDTOResponse();
        Optional<Level> result = levelRepository.findById(id);
        if (result.isPresent()) {
            Level level = result.get();
//            BeanUtils.copyProperties(level, levelDTOResponse);
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.getConfiguration()
                    .setFieldMatchingEnabled(true)
                    .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
            modelMapper.map(level, levelDTOResponse);
        }
        String createdBy = userRepository.getFullNameByUserId(levelDTOResponse.getCreatedBy());
        String updatedBy = userRepository.getFullNameByUserId(levelDTOResponse.getUpdatedBy());
        levelDTOResponse.setCreatedBy(createdBy);
        levelDTOResponse.setUpdatedBy(updatedBy);
        return levelDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<Level> page = levelRepository.findAll(paging, "%" + keyword + "%", status);
        if (page.hasContent()) {
            List<LevelDTOResponse> departmentDTOS = CommonUtil.mapList(page.getContent(), LevelDTOResponse.class);
            output.put("data", departmentDTOS);
            output.put("total", page.getTotalElements());
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
        }
        output.put("page", pageNo);
        output.put("size", pageSize);
        return output;
    }

    @Override
    public List<LevelDTOResponse> getAllMultipleId(List<String> listLevelId) {
        List<Level> levels = levelRepository.getAllByLevelIdIn(listLevelId);
        return CommonUtil.mapList(levels, LevelDTOResponse.class);
    }

    @Override
    public List<LevelDTOResponse> getAll(String keyword, Integer status) {
        return CommonUtil.mapList(levelRepository.getAll("%"+keyword+"%",status), LevelDTOResponse.class);
    }

    @Override
    public void delete(String id) {
        Optional<Level> result = levelRepository.findById(id);
        if (result.isPresent()) {
            Level level = result.get();
            level.setLevelStatus(-1);
            levelRepository.save(level);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("level", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));
    }

    @Override
    public void changeStatus(String id) {
        Optional<Level> result = levelRepository.findById(id);
        if (result.isPresent()) {
            Level level = result.get();
            level.setLevelStatus(level.getLevelStatus() == 0 ? 1 : 0);
            levelRepository.save(level);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("level", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));

    }
}
