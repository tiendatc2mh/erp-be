package com.blameo.idservice.service;


import com.blameo.idservice.dto.request.AssetDTO;
import com.blameo.idservice.dto.request.AssetUpdateDTO;
import com.blameo.idservice.dto.response.AssetDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


@Component
public interface AssetService {

    AssetDTOResponse save(AssetDTO assetDTO, UserDTOResponse currentUser);

    AssetDTOResponse update(AssetUpdateDTO assetDTO, String assetId, UserDTOResponse currentUser);
    AssetDTOResponse get(String assetId);

    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer assetStatus, List<String> assetTypeId, String userId, String requestId);
    InputStreamResource export(String keyword, Integer assetStatus, List<String> assetTypeId, String userId, String requestId);

    List<AssetDTOResponse> getAll(String keyword, Integer assetStatus, List<String> assetTypeId, String userId, String requestId);

    void delete(String assetId, AssetDTO assetDTO, UserDTOResponse currentUser);
    // gán tài sản
    AssetDTOResponse assignAsset(String assetId, AssetDTO assetDTO, UserDTOResponse currentUser);
    void assignAssetByUserId(String userId, AssetDTO assetDTO, UserDTOResponse currentUser);

    // thu hồi tải sản
    AssetDTOResponse retrieveAsset(String assetId, AssetDTO assetDTO, UserDTOResponse currentUser);


    List<AssetDTOResponse> getAllByUserId(String userId);
}
