package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.TypeDocumentDTO;
import com.blameo.idservice.dto.response.TypeDocumentDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/

@Component
public interface TypeDocumentService {

    TypeDocumentDTOResponse save(TypeDocumentDTO typeDocumentDTO, UserDTOResponse userDTOResponse);
    TypeDocumentDTOResponse update(TypeDocumentDTO typeDocumentDTO, String id, UserDTOResponse userDTOResponse);
    TypeDocumentDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status);

    List<TypeDocumentDTOResponse> getAll(Integer status);

    void delete(String id, UserDTOResponse userDTOResponse);

    void changeStatus(String id, UserDTOResponse userDTOResponse);
}
