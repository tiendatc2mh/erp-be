package com.blameo.idservice.service.impl;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.AssetDTO;
import com.blameo.idservice.dto.request.AssetTypeDTO;
import com.blameo.idservice.dto.request.AssetUpdateDTO;
import com.blameo.idservice.dto.response.AssetDTOResponse;
import com.blameo.idservice.dto.response.AssetLogDTOResponse;
import com.blameo.idservice.dto.response.AssetTypeDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.Asset;
import com.blameo.idservice.model.AssetLog;
import com.blameo.idservice.model.AssetType;
import com.blameo.idservice.repository.AssetLogRepository;
import com.blameo.idservice.repository.AssetRepository;
import com.blameo.idservice.repository.AssetTypeRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.AssetLogService;
import com.blameo.idservice.service.AssetService;
import com.blameo.idservice.service.AssetTypeService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import com.blameo.idservice.utils.ExcelUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.blameo.idservice.constant.Constant.SPACE;

@Service
@Transactional
public class AssetServiceImpl implements AssetService {

    @Autowired
    AssetTypeRepository assetTypeRepository;

    @Autowired
    AssetTypeService assetTypeService;

    @Autowired
    AssetLogRepository assetLogRepository;

    @Autowired
    AssetRepository assetRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AssetLogService assetLogService;

    @Autowired
    MessageSource messageSource;

    @Override
    public AssetDTOResponse save(AssetDTO assetDTO, UserDTOResponse currentUser) {
        AssetType assetType = assetTypeRepository.findById(assetDTO.getAssetType()).get();
        Long countByAssetType = assetRepository.countAssetByAssetType_AssetTypeId(assetDTO.getAssetType());
        Asset assets = Asset.convertAssetDTO(assetDTO, assetType, currentUser, countByAssetType);
        Asset s = assetRepository.save(assets);
        AssetDTOResponse sDTO = new AssetDTOResponse();
        BeanUtils.copyProperties(s, sDTO);
        // luu log dieu chuyen asset_log
        if(assets.getUserId() != null && assets.getUserId() != ""){
            AssetLog assetLog= AssetLog.AssetLogFirstAssign(s, assetDTO, currentUser);
            AssetLogDTOResponse saveLog = assetLogService.save(assetLog, currentUser);
        }
        return sDTO;

    }

    @Override
    public AssetDTOResponse update(AssetUpdateDTO assetDTO, String assetId, UserDTOResponse currentUser) {
        Locale locale = CommonUtil.getLocale();
        Asset asset = assetRepository.findById(assetId).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("asset",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale))
        );
        if(!Objects.equals(assetDTO.getAssetSerial(), asset.getAssetSerial())){
            if( !StringUtils.isEmpty(assetDTO.getAssetSerial()) && assetRepository.existsAssetByAssetSerialAndAssetStatusGreaterThanEqual(assetDTO.getAssetSerial(), 0)){
                throw  new RuntimeException("Serial "+messageSource.getMessage("isExist",null,locale));
            }
        }
        if(!Objects.equals(assetDTO.getAssetCode(), asset.getAssetCode())){
            if(assetDTO.getAssetCode() != null && assetRepository.existsAssetByAssetCodeAndAssetStatusGreaterThanEqual(assetDTO.getAssetSerial(), 0)){
                throw  new RuntimeException(messageSource.getMessage("asset.code",null,locale)+ SPACE +messageSource.getMessage("isExist",null,locale));
            }
        }
        if(!asset.getAssetStatus().equals(-1)) {
            AssetType assetType = assetTypeRepository.findById(assetDTO.getAssetType()).get();
            asset.setAssetType(assetType);
            asset.setAssetCode(assetDTO.getAssetCode());
            asset.setAssetDate(assetDTO.getAssetDate());
            asset.setUpdatedBy(currentUser.getUserId());
            asset.setAssetName(assetDTO.getAssetName());
            asset.setAssetSupplier(assetDTO.getAssetSupplier());
            asset.setAssetIncreaseDate(assetDTO.getAssetIncreaseDate());
            asset.setAssetBillCode(assetDTO.getAssetBillCode());
            asset.setAssetWarrantyTime(assetDTO.getAssetWarrantyTime());
            asset.setAssetYearManufacture(assetDTO.getAssetYearManufacture());
            asset.setAssetManufacture(assetDTO.getAssetManufacture());
            asset.setAssetUses(assetDTO.getAssetUses());
            asset.setAssetSerial(assetDTO.getAssetSerial());
            Asset asset1 = assetRepository.save(asset);
            AssetDTOResponse dto = new AssetDTOResponse();
            BeanUtils.copyProperties(asset1, dto);
            return dto;
        }else{
            throw new RuntimeException(messageSource.getMessage("asset",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale));
        }
    }

    @Override
    public AssetDTOResponse get(String assetId) {
        Locale locale = CommonUtil.getLocale();
        AssetDTOResponse assetDTOResponse = new AssetDTOResponse();
        Asset asset = assetRepository.findById(assetId).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("asset",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale))
        );
        BeanUtils.copyProperties(asset, assetDTOResponse);
        assetDTOResponse.setAssetType(new AssetTypeDTOResponse());
        BeanUtils.copyProperties(asset.getAssetType(), assetDTOResponse.getAssetType());
        String createdByName = userRepository.getFullNameByUserId(asset.getCreatedBy());
        String updatedByName = userRepository.getFullNameByUserId(asset.getUpdatedBy());
        if(asset.getUserId() != null){
            String userFullname = userRepository.getFullNameByUserId(asset.getUserId());
            assetDTOResponse.setUser(new UserDTOResponse(asset.getUserId(), userFullname));
        }
        assetDTOResponse.setCreatedByName(createdByName);
        assetDTOResponse.setUpdatedByName(updatedByName);
        return assetDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer assetStatus, List<String> assetTypeId, String userId, String requestId) {
        Map<String, Object> output = new HashMap<>();
        Pageable pageable = PageRequest.of(pageNo-1, pageSize);
        Page<Object[]> page = assetRepository.findAll(pageable, "%" + keyword + "%", assetStatus, assetTypeId, userId, requestId);
        if(page.hasContent()){
            List<AssetDTOResponse> responseList = new ArrayList<>();
            page.getContent().forEach(objects -> {
                AssetDTOResponse assetDTOResponse = new AssetDTOResponse();
                Asset asset = (Asset) objects[0];
                BeanUtils.copyProperties(asset, assetDTOResponse);
                assetDTOResponse.setAssetType(new AssetTypeDTOResponse());
                BeanUtils.copyProperties(asset.getAssetType(), assetDTOResponse.getAssetType());
                assetDTOResponse.setUser(new UserDTOResponse(asset.getUserId(), (String) objects[1]));
                responseList.add(assetDTOResponse);
            });
            output.put("data", responseList);
            output.put("total", page.getTotalElements());
            output.put("page", pageNo);
            output.put("size", pageSize);

        }else{
            output.put("data", new ArrayList<>());
            output.put("total", 0);
            output.put("page", pageNo);
            output.put("size", pageSize);
        }
        return output;
    }

    @Override
    public InputStreamResource export(String keyword, Integer assetStatus, List<String> assetTypeId, String userId, String requestId) {
        Locale locale = CommonUtil.getLocale();
        try{
            List<AssetDTOResponse> assetList = getAll(keyword, assetStatus, assetTypeId, userId, requestId);
            List<AssetTypeDTOResponse> assetTypeList = assetTypeService.getAll(null);
            if(assetTypeId != null && assetTypeId.size() > 0){
                assetTypeList = assetTypeList.stream().filter(dto -> assetTypeId.contains(dto.getAssetTypeId())).collect(Collectors.toList());
                assetList = assetList.stream().filter(asset -> assetTypeId.contains(asset.getAssetType().getAssetTypeId())).collect(Collectors.toList());
            }

            List<String> header = Arrays.asList(
                    messageSource.getMessage("header.t1",null,locale),
                    messageSource.getMessage("header.t2",null,locale),
                    messageSource.getMessage("header.t3",null,locale),
                    messageSource.getMessage("header.t4",null,locale),
                    messageSource.getMessage("header.t5",null,locale),
                    messageSource.getMessage("header.t6",null,locale),
                    messageSource.getMessage("header.t7",null,locale),
                    messageSource.getMessage("header.t8",null,locale)

            );
            // refactor data to export
            Map<String, List<AssetDTOResponse>> mapDataSheet =  assetList.stream().collect(Collectors.groupingBy(asset -> asset.getAssetType().getAssetTypeId()));

            InputStream templateStream = AssetServiceImpl.class.getResourceAsStream("/template/" + Constant.TEMPLATE_ASSET);
            XSSFWorkbook workbook = new XSSFWorkbook(templateStream);

            //style basic
            XSSFCellStyle style = ExcelUtil.getStyleBasic(workbook);
            style.setWrapText(true);

            //style center basic
            XSSFCellStyle styleCenter = ExcelUtil.getStyleCenterBasic(workbook);
            styleCenter.setWrapText(true);

            //style center basic
            XSSFCellStyle styleCenterBold = ExcelUtil.getStyleCenterBasicBold(workbook);
            XSSFColor colorBlue = new XSSFColor(new java.awt.Color(133, 181, 232));
            styleCenterBold.setFillForegroundColor(colorBlue);
            styleCenterBold.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleCenterBold.setWrapText(true);
            styleCenter.setWrapText(true);

            //style date basic
            XSSFCellStyle styleDate = ExcelUtil.getStyleDateBasic(workbook, "dd-mm-yyyy");

            XSSFSheet sheet = workbook.getSheetAt(0);
            AtomicInteger rowIndex = new AtomicInteger(5); //  tiêu đề, bắt đầu từ row 5 để thêm dữ liệu
            AtomicInteger stt = new AtomicInteger(1); // đánh stt

            List<AssetDTOResponse> finalAssetList = assetList;
            assetTypeList.forEach(assetType -> {
                Row row = sheet.createRow(rowIndex.getAndIncrement());
                //  stt
                Cell cellStt = row.createCell(0);
                cellStt.setCellStyle(styleCenter);
                cellStt.setCellValue(stt.getAndIncrement());

                //assetTypeName
                Cell cellName = row.createCell(1);
                cellName.setCellStyle(style);
                cellName.setCellValue(assetType.getAssetTypeName());

                //count
                Cell cellCount= row.createCell(2);
                cellCount.setCellStyle(styleCenter);
                cellCount.setCellValue(finalAssetList.stream().filter(asset -> asset.getAssetType().getAssetTypeId().equals(assetType.getAssetTypeId())).count());
            });
            mapDataSheet.forEach((s, assetDTOResponses) -> {
                XSSFSheet newSheet = workbook.createSheet(assetDTOResponses.get(0).getAssetType().getAssetTypeName());
                Row row1 = newSheet.createRow(0);
                for(int i=0; i< header.size(); i++){
                    Cell cell = row1.createCell(i);
                    cell.setCellStyle(styleCenterBold);
                    cell.setCellValue(header.get(i));
                    newSheet.setColumnWidth(i, 4000);
                }
                AtomicInteger rowIndexSheet = new AtomicInteger(1);
                AtomicInteger sttSheet = new AtomicInteger(1);
                assetDTOResponses.forEach(asset -> {
                    Row row = newSheet.createRow(rowIndexSheet.getAndIncrement());
                    //  stt
                    Cell cellStt = row.createCell(0);
                    cellStt.setCellStyle(styleCenter);
                    cellStt.setCellValue(sttSheet.getAndIncrement());

                    //assetCode
                    Cell cellCode = row.createCell(1);
                    cellCode.setCellStyle(style);
                    cellCode.setCellValue(asset.getAssetCode());

                    //assetName
                    Cell cellName = row.createCell(2);
                    cellName.setCellStyle(style);
                    cellName.setCellValue(asset.getAssetName());

                    //assetUser
                    Cell cellUser = row.createCell(3);
                    cellUser.setCellStyle(style);
                    cellUser.setCellValue(asset.getUser() == null ? "" : asset.getUser().getFullName());

                    //manufacture
                    Cell manufacture = row.createCell(4);
                    manufacture.setCellStyle(style);
                    manufacture.setCellValue(asset.getAssetManufacture());

                    //increaseDate
                    Cell increaseDate = row.createCell(5);
                    increaseDate.setCellStyle(styleDate);
                    increaseDate.setCellValue(asset.getAssetIncreaseDate());

                    //warranty
                    Cell warranty = row.createCell(6);
                    warranty.setCellStyle(styleDate);
                    warranty.setCellValue(asset.getAssetWarrantyTime());

                    //billCode
                    Cell billCode = row.createCell(7);
                    billCode.setCellStyle(style);
                    billCode.setCellValue(asset.getAssetBillCode());
                });
            });


            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            workbook.close();
            InputStreamResource inputStream = new InputStreamResource(new ByteArrayInputStream(outputStream.toByteArray()));
            // Tạo Resource từ file Excel trong bộ nhớ tạm
            return inputStream;
        }catch (Exception e){
            throw new RuntimeException(messageSource.getMessage("exportFail", null, locale));
        }
    }

    @Override
    public List<AssetDTOResponse> getAll(String keyword, Integer assetStatus, List<String> assetTypeId, String userId, String requestId) {
        List<Object[]> page = assetRepository.getAll("%" + keyword + "%", assetStatus, assetTypeId, userId, requestId);
        List<AssetDTOResponse> responseList = new ArrayList<>();
        page.forEach(objects -> {
            AssetDTOResponse assetDTOResponse = new AssetDTOResponse();
            Asset asset = (Asset) objects[0];
            BeanUtils.copyProperties(asset, assetDTOResponse);
            assetDTOResponse.setAssetType(new AssetTypeDTOResponse());
            BeanUtils.copyProperties(asset.getAssetType(), assetDTOResponse.getAssetType());
            assetDTOResponse.setUser(new UserDTOResponse(asset.getUserId(), (String) objects[1]));
            responseList.add(assetDTOResponse);
        });
        return responseList;
    }

    @Override
    public void delete(String assetId, AssetDTO assetDTO, UserDTOResponse currentUser) {
        Locale locale = CommonUtil.getLocale();
        Asset asset = assetRepository.findById(assetId).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("asset",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale))
        );
        if(asset.getAssetStatus().equals(0) && (asset.getUserId() == null || asset.getUserId().equals(""))){
            asset.setAssetStatus(-1);
            asset.setUpdatedBy(currentUser.getUserId());
            Asset save = assetRepository.save(asset);
            // log asset bought
            AssetLog assetLog = AssetLog.AssetLogBought(asset, assetDTO, currentUser);
            assetLogService.save(assetLog, currentUser);
        }else{
            throw new RuntimeException(messageSource.getMessage("usedErrorAsset",null,locale));
        }
    }

    @Override
    public AssetDTOResponse assignAsset(String assetId, AssetDTO assetDTO, UserDTOResponse currentUser) {
        Locale locale = CommonUtil.getLocale();
        Asset asset = assetRepository.findById(assetId).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("asset",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale))
        );
        if(!asset.getAssetStatus().equals(Constant.DELETED)){
            // log asset assign
            AssetLog assetLog = AssetLog.AssetLogAssign(asset, assetDTO, currentUser);
            asset.setUserId(assetDTO.getUserId());
            asset.setAssetStatus(Constant.ACTIVE);
            asset.setAssetDate(assetDTO.getAssetDate());
            asset.setUpdatedBy(currentUser.getUserId());
            Asset save = assetRepository.save(asset);
            assetLogService.save(assetLog, currentUser);
            AssetDTOResponse assetDTOResponse = new AssetDTOResponse();
            BeanUtils.copyProperties(save, assetDTOResponse);
            return assetDTOResponse;
        }else{
            throw new RuntimeException(messageSource.getMessage("asset",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale));
        }
    }

    @Override
    public void assignAssetByUserId(String userId, AssetDTO assetDTO, UserDTOResponse currentUser) {
        Locale locale = CommonUtil.getLocale();
        // reset tai san cua user
        assetRepository.resetAssetByUserId(userId);
        // set lai tai san tu DTO
        if(assetDTO.getListAssetUsers() != null && assetDTO.getListAssetUsers().size() > 0){
            assetDTO.getListAssetUsers().forEach(listAssetUser -> {
                Asset asset = assetRepository.findById(listAssetUser.getAssetId()).orElseThrow(
                        () -> new RuntimeException(messageSource.getMessage("asset",null,locale)+ SPACE + messageSource.getMessage("notExist",null,locale))
                );
                if(asset.getAssetStatus().equals(Constant.INACTIVE)){
                    AssetDTO assetDTO1 = new AssetDTO();
                    assetDTO1.setAssetDate(listAssetUser.getAssetDate());
                    assetDTO1.setUserId(userId);
                    // log asset assign
                    AssetLog assetLog = AssetLog.AssetLogAssign(asset, assetDTO1, currentUser);

                    asset.setUserId(userId);
                    asset.setAssetStatus(Constant.ACTIVE);
                    asset.setAssetDate(listAssetUser.getAssetDate());
                    asset.setUpdatedBy(currentUser.getUserId());
                    assetRepository.save(asset);
                    assetLogService.save(assetLog, currentUser);
                }else{
                    throw new RuntimeException(messageSource.getMessage("assignAssetErrorStatus",null,locale));
                }
            });
        }
    }

    @Override
    public AssetDTOResponse retrieveAsset(String assetId, AssetDTO assetDTO, UserDTOResponse currentUser) {
        Locale locale = CommonUtil.getLocale();
        Asset asset = assetRepository.findById(assetId).orElseThrow(
                () -> new RuntimeException("Asset "+ messageSource.getMessage("notExist",null,locale))
        );
        if(asset.getAssetStatus().equals(Constant.ACTIVE)) {
            // log asset retrieve
            AssetLog assetLog = AssetLog.AssetLogRetrieve(asset, assetDTO, currentUser);
            asset.setUserId(assetDTO.getUserId());
            asset.setAssetStatus(Constant.INACTIVE);
            asset.setAssetDate(null);
            asset.setUpdatedBy(currentUser.getUserId());
            Asset save = assetRepository.save(asset);
            assetLogService.save(assetLog, currentUser);
            AssetDTOResponse assetDTOResponse = new AssetDTOResponse();
            BeanUtils.copyProperties(save, assetDTOResponse);
            return assetDTOResponse;
        }else{
            throw new RuntimeException(messageSource.getMessage("retrieveAssetNotAssign",null,locale));
        }
    }

    @Override
    public List<AssetDTOResponse> getAllByUserId(String userId) {
        List<Object[]> page = assetRepository.getAllByUserId(userId);
        List<AssetDTOResponse> responseList = new ArrayList<>();
        page.forEach(objects -> {
            AssetDTOResponse assetDTOResponse = new AssetDTOResponse();
            Asset asset = (Asset) objects[0];
            BeanUtils.copyProperties(asset, assetDTOResponse);
            assetDTOResponse.setAssetType(new AssetTypeDTOResponse());
            BeanUtils.copyProperties(asset.getAssetType(), assetDTOResponse.getAssetType());
            assetDTOResponse.setUser(new UserDTOResponse(asset.getUserId(), (String) objects[1]));
            responseList.add(assetDTOResponse);
        });
        return responseList;
    }
}
