package com.blameo.idservice.service.impl;

import com.blameo.idservice.dto.request.ListPersonalFileDTO;
import com.blameo.idservice.dto.request.PersonalFileDTO;
import com.blameo.idservice.dto.response.PermissionDTOResponse;
import com.blameo.idservice.dto.response.PersonalFileDTOResponse;
import com.blameo.idservice.dto.response.PositionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.exception.RuntimeExceptionCustom;
import com.blameo.idservice.model.PersonalFile;
import com.blameo.idservice.model.Position;
import com.blameo.idservice.repository.PersonalFileRepository;
import com.blameo.idservice.service.PersonalFileService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PersonalFileServiceImpl implements PersonalFileService {

    @Autowired
    PersonalFileRepository personalFileRepository;

    @Override
    public List<PersonalFileDTOResponse> save(ListPersonalFileDTO personalFileDTO, String id, UserDTOResponse userDTOResponse) {
        List<PersonalFile> result = personalFileRepository.getAll(id);
        if(result != null && result.size() > 0){
            personalFileRepository.deleteAll(result);
        }
        List<PersonalFileDTOResponse> personalFileDTOResponses = new ArrayList<>();
        List<PersonalFile> lstPersonalFile = new ArrayList<>();
        if(personalFileDTO.getPersonalFiles() != null && personalFileDTO.getPersonalFiles().size() > 0){
            for(PersonalFileDTO personalFile : personalFileDTO.getPersonalFiles()){
                PersonalFile personalFile1 = new PersonalFile();
                CoppyObject.copyNonNullProperties(personalFile,personalFile1);
                personalFile1.setCreatedBy(userDTOResponse.getUserId());
                personalFile1.setUserId(id);
                lstPersonalFile.add(personalFile1);
            }
            List<PersonalFile> saveAll = personalFileRepository.saveAll(lstPersonalFile);
            if(saveAll.size() > 0){
                for(PersonalFile file : saveAll){
                    PersonalFileDTOResponse personalFileDTOResponse = new PersonalFileDTOResponse();
                    BeanUtils.copyProperties(file, personalFileDTOResponse);
                    personalFileDTOResponses.add(personalFileDTOResponse);
                }
            }
        }
        return personalFileDTOResponses;
    }

    @Override
    public List<PersonalFileDTOResponse> get(String id) {
        List<PersonalFileDTOResponse> result = new ArrayList<>();
        List<Object[]> list = personalFileRepository.getAllWithDocumentName(id);
        list.forEach(x->{
            PersonalFile personalFile = (PersonalFile) x[0];
            String nameType = (String) x[1];
            PersonalFileDTOResponse personalFileDTOResponse =  CommonUtil.mapObject(personalFile, PersonalFileDTOResponse.class);
            personalFileDTOResponse.setTypeDocumentName(nameType);
            result.add(personalFileDTOResponse);
        });
        return result;
    }

    @Override
    public void delete(String id, UserDTOResponse userDTOResponse) {
        List<PersonalFile> result = personalFileRepository.getAll(id);
        if (result != null && result.size() > 0) {
            personalFileRepository.deleteAll(result);
        }
    }
}
