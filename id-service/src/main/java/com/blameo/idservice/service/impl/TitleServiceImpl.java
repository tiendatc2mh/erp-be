package com.blameo.idservice.service.impl;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.TitleDTO;
import com.blameo.idservice.dto.response.TitleDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.exception.RuntimeExceptionCustom;
import com.blameo.idservice.model.Title;
import com.blameo.idservice.repository.TitleRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.TitleService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Service
@Transactional
public class TitleServiceImpl implements TitleService {
    TitleRepository titleRepository;

    MessageSource messageSource;

    @Autowired
    UserRepository userRepository;

    public TitleServiceImpl(TitleRepository titleRepository, MessageSource messageSource) {
        this.titleRepository = titleRepository;
        this.messageSource = messageSource;
    }


    @Override
    public TitleDTOResponse save(TitleDTO titleDTO, UserDTOResponse userDTOResponse) {
        Title title = new Title();
        CoppyObject.copyNonNullProperties(titleDTO, title);
        title.setCreatedBy(userDTOResponse.getUserId());
        title.setUpdatedBy(userDTOResponse.getUserId());
        Title saved = titleRepository.save(title);
        TitleDTOResponse titleDTOResponse = new TitleDTOResponse();
        BeanUtils.copyProperties(saved, titleDTOResponse);
        return titleDTOResponse;
    }

    @Override
    public TitleDTOResponse update(TitleDTO titleDTO, String id) {
        Optional<Title> result = titleRepository.findById(id);
        Title title;
        TitleDTOResponse titleDTOResponse = new TitleDTOResponse();
        if (result.isPresent()) {
            title = result.get();
            if (!title.getTitleCode().equals(titleDTO.getTitleCode()) && Boolean.TRUE.equals((titleRepository.existsByTitleCodeAndTitleStatusGreaterThanEqual(titleDTO.getTitleCode(), 0)))) {
                throw new RuntimeExceptionCustom(messageSource.getMessage("title.code", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage(Constant.IS_EXIST, null, CommonUtil.getLocale()));
            }
            if (titleDTO.getTitleCode() != null) {
                title.setTitleCode(titleDTO.getTitleCode());
            }
            if (titleDTO.getTitleName() != null) {
                title.setTitleName(titleDTO.getTitleName());
            }

            if (titleDTO.getTitleDescription() != null) {
                title.setTitleDescription(titleDTO.getTitleDescription());
            }

            if (titleDTO.getTitleStatus() != null) {
                title.setTitleStatus(titleDTO.getTitleStatus());
            }

            Title saved = titleRepository.save(title);
            BeanUtils.copyProperties(saved, titleDTOResponse);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("title", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, CommonUtil.getLocale()));
        return titleDTOResponse;
    }

    @Override
    public TitleDTOResponse get(String id) {
        TitleDTOResponse titleDTOResponse = new TitleDTOResponse();
        Optional<Title> result = titleRepository.findById(id);
        if (result.isPresent()) {
            Title title = result.get();
            BeanUtils.copyProperties(title, titleDTOResponse);
        }
        titleDTOResponse.setCreatedByName(userRepository.getFullNameByUserId(titleDTOResponse.getCreatedBy()));
        titleDTOResponse.setUpdatedByName(userRepository.getFullNameByUserId(titleDTOResponse.getUpdatedBy()));
        return titleDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<Title> page = titleRepository.findAll(paging, "%" + keyword + "%", status);
        if (page.hasContent()) {
            List<TitleDTOResponse> departmentDTOS = CommonUtil.mapList(page.getContent(), TitleDTOResponse.class);
            output.put("data", departmentDTOS);
            output.put("total", page.getTotalElements());
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
        }
        output.put("page", pageNo);
        output.put("size", pageSize);
        return output;
    }


    @Override
    public List<TitleDTOResponse> getAll(String keyword, Integer status) {
        return CommonUtil.mapList(titleRepository.getAll("%"+keyword+"%",status), TitleDTOResponse.class);
    }

    @Override
    public List<TitleDTOResponse> getAllListTitleId(List<String> titleId) {
        return CommonUtil.mapList(titleRepository.getAllListTitleId(titleId), TitleDTOResponse.class);
    }

    @Override
    public void delete(String id) {
        Optional<Title> result = titleRepository.findById(id);
        if (result.isPresent()) {
            Title title = result.get();
            title.setTitleStatus(-1);
            titleRepository.save(title);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("title", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));
    }

    @Override
    public void changeStatus(String id) {
        Optional<Title> result = titleRepository.findById(id);
        if (result.isPresent()) {
            Title title = result.get();
            title.setTitleStatus(title.getTitleStatus() == 0 ? 1 : 0);
            titleRepository.save(title);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("title", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));

    }
}
