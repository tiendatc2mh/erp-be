package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.PositionDTO;
import com.blameo.idservice.dto.response.PositionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface PositionService {
    PositionDTOResponse save(PositionDTO positionDTO, UserDTOResponse userDTOResponse);
    PositionDTOResponse update(PositionDTO departmentDTO,String id, UserDTOResponse userDTOResponse);
    PositionDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status);
    List<PositionDTOResponse> getAllMultipleId(List<String> listUserId);
    List<PositionDTOResponse> getAll(String keyword, Integer status);

    void delete(String id, UserDTOResponse userDTOResponse);

    void changeStatus(String id, UserDTOResponse userDTOResponse);
}
