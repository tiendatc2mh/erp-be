package com.blameo.idservice.service.impl;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.DepartmentDTO;
import com.blameo.idservice.dto.response.DepartmentDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.dto.response.UserManagerDTOResponse;
import com.blameo.idservice.exception.RuntimeExceptionCustom;
import com.blameo.idservice.model.Department;
import com.blameo.idservice.model.DepartmentManager;
import com.blameo.idservice.model.User;
import com.blameo.idservice.repository.DepartmentManagerRepository;
import com.blameo.idservice.repository.DepartmentRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.DepartmentService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    DepartmentRepository departmentRepository;

    MessageSource messageSource;

    @Autowired
    UserRepository userRepository;
    @Autowired
    private DepartmentManagerRepository departmentManagerRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository, MessageSource messageSource) {
        this.departmentRepository = departmentRepository;
        this.messageSource = messageSource;
    }


    @Override
    public DepartmentDTOResponse save(DepartmentDTO departmentDTO, UserDTOResponse userDTOResponse) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        Department department = new Department();
        CoppyObject.copyNonNullProperties(departmentDTO, department);
        department.setCreatedBy(userDTOResponse.getUserId());
        department.setUpdatedBy(userDTOResponse.getUserId());

        Department saved = departmentRepository.save(department);
        //save manager-department
        List<DepartmentManager> departmentManagers = new ArrayList<>();
        if (departmentDTO.getManagers() != null && departmentDTO.getManagers().size() > 0) {
            for (String u : departmentDTO.getManagers()) {
                User user = userRepository.findById(u).orElseThrow(
                        () -> new RuntimeException(messageSource.getMessage("user.manager", null, locale) + SPACE + messageSource.getMessage("notExist", null, locale))
                );
                DepartmentManager departmentManager = new DepartmentManager();
                departmentManager.setDepartmentId(saved.getDepartmentId());
                departmentManager.setManagerId(u);
                departmentManagers.add(departmentManager);
            }
        }
        //
        departmentManagerRepository.saveAll(departmentManagers);

        return setManagers(saved);
    }

    @Override
    public DepartmentDTOResponse update(DepartmentDTO departmentDTO, String id, UserDTOResponse userDTOResponse) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        Optional<Department> result = departmentRepository.findById(id);
        Department department;
        DepartmentDTOResponse departmentDTOResponse = new DepartmentDTOResponse();
        if (result.isPresent()) {
            department = result.get();
            if (!department.getDepartmentCode().equals(departmentDTO.getDepartmentCode()) && Boolean.TRUE.equals((departmentRepository.existsByDepartmentCodeAndDepartmentStatusGreaterThanEqual(departmentDTO.getDepartmentCode(), 0)))) {
                throw new RuntimeExceptionCustom(messageSource.getMessage("department.code", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage(Constant.IS_EXIST, null, CommonUtil.getLocale()));
            }
            if (departmentDTO.getDepartmentCode() != null) {
                department.setDepartmentCode(departmentDTO.getDepartmentCode());
            }
            if (departmentDTO.getDepartmentName() != null) {
                department.setDepartmentName(departmentDTO.getDepartmentName());
            }

            if (departmentDTO.getDepartmentStatus() != null) {
                department.setDepartmentStatus(departmentDTO.getDepartmentStatus());
            }
            if (departmentDTO.getDepartmentDescription() != null) {
                department.setDepartmentDescription(departmentDTO.getDepartmentDescription());
            }

            department.setUpdatedDate(new Date());
            department.setUpdatedBy(userDTOResponse.getUserId());
            Department saved = departmentRepository.save(department);


            //save manager-department

            departmentManagerRepository.deleteAllByDepartmentId(id);
            List<DepartmentManager> departmentManagers = new ArrayList<>();
            if (departmentDTO.getManagers() != null && departmentDTO.getManagers().size() > 0) {
                for (String u : departmentDTO.getManagers()) {
                    User user = userRepository.findById(u).orElseThrow(
                            () -> new RuntimeException(messageSource.getMessage("user.manager", null, locale) + SPACE + messageSource.getMessage("notExist", null, locale))
                    );
                    DepartmentManager departmentManager = new DepartmentManager();
                    departmentManager.setDepartmentId(saved.getDepartmentId());
                    departmentManager.setManagerId(u);
                    departmentManagers.add(departmentManager);
                }
            }
            //
            departmentManagerRepository.saveAll(departmentManagers);

            return setManagers(saved);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("department", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, CommonUtil.getLocale()));
    }

    @Override
    public DepartmentDTOResponse get(String id) {
        Optional<Department> result = departmentRepository.findById(id);
        DepartmentDTOResponse departmentDTOResponse = setManagers(result.get());
        departmentDTOResponse.setCreatedByName(userRepository.getFullNameByUserId(departmentDTOResponse.getCreatedBy()));
        departmentDTOResponse.setUpdatedByName(userRepository.getFullNameByUserId(departmentDTOResponse.getUpdatedBy()));
        return departmentDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<Department> page = departmentRepository.findAll(paging, "%" + keyword + "%", status);
        if (page.hasContent()) {
            List<DepartmentDTOResponse> departmentDTOS = new ArrayList<>();
            page.getContent().forEach(x -> {
                departmentDTOS.add(setManagers(x));
            });
            output.put("data", departmentDTOS);
            output.put("total", page.getTotalElements());
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
        }
        output.put("page", pageNo);
        output.put("size", pageSize);
        return output;
    }


    @Override
    public List<DepartmentDTOResponse> getAll(Integer status) {
        List<Department> departments = departmentRepository.getAll(status);
        List<DepartmentDTOResponse> departmentDTOS = new ArrayList<>();
        departments.forEach(x -> {
            departmentDTOS.add(setManagers(x));
        });
        return departmentDTOS;
    }

    @Override
    public void delete(String id) {
        Optional<Department> result = departmentRepository.findById(id);
        if (result.isPresent()) {
            Department department = result.get();
            department.setDepartmentStatus(-1);
            departmentRepository.save(department);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("department", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));
    }

    @Override
    public void changeStatus(String id) {
        Optional<Department> result = departmentRepository.findById(id);
        if (result.isPresent()) {
            Department department = result.get();
            department.setDepartmentStatus(department.getDepartmentStatus() == 0 ? 1 : 0);
            departmentRepository.save(department);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("department", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));

    }

    public DepartmentDTOResponse setManagers(Department department){
     Set<UserManagerDTOResponse> userManagerDTOResponses = new HashSet<>();
     DepartmentDTOResponse departmentDTOResponse = new DepartmentDTOResponse();
     departmentManagerRepository.findAllByDepartmentId(department.getDepartmentId()).forEach(x->{
         userRepository.findById(x.getManagerId()).ifPresent(user -> {
             UserManagerDTOResponse userManagerDTOResponse = new UserManagerDTOResponse();
             userManagerDTOResponse.setUserId(user.getUserId());
             userManagerDTOResponse.setUserCode(user.getUserCode());
             userManagerDTOResponse.setFullName(user.getFullName());
             userManagerDTOResponses.add(userManagerDTOResponse);
         });
     });
     BeanUtils.copyProperties(department,departmentDTOResponse);
     departmentDTOResponse.setManagers(userManagerDTOResponses);
     return departmentDTOResponse;
    }
}
