package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.DepartmentDTO;
import com.blameo.idservice.dto.response.DepartmentDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Component
public interface DepartmentService {
    DepartmentDTOResponse  save(DepartmentDTO departmentDTO, UserDTOResponse userDTOResponse);
    DepartmentDTOResponse update(DepartmentDTO departmentDTO, String id, UserDTOResponse userDTOResponse);
    DepartmentDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status);

    List<DepartmentDTOResponse> getAll(Integer status);

    void delete(String id);

    void changeStatus(String id);
}
