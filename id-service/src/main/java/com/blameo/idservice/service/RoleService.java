package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.RoleDTO;
import com.blameo.idservice.dto.response.RoleDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/

@Component
public interface RoleService {

    RoleDTOResponse save(RoleDTO roleDTO, UserDTOResponse userDTOResponse);
    RoleDTOResponse update(RoleDTO roleDTO, String id, UserDTOResponse userDTOResponse);
    RoleDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status, Date createdDate);

    List<RoleDTOResponse> getAll(Integer status);

    List<RoleDTOResponse> getAllListRoleId(List<String> roleId);

    void delete(String id, UserDTOResponse userDTOResponse);

    void changeStatus(String id, UserDTOResponse userDTOResponse);
}
