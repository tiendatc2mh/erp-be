package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.ListPersonalFileDTO;
import com.blameo.idservice.dto.request.PersonalFileDTO;
import com.blameo.idservice.dto.response.PersonalFileDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface PersonalFileService {

    List<PersonalFileDTOResponse> save(ListPersonalFileDTO personalFileDTO, String id, UserDTOResponse userDTOResponse);
    List<PersonalFileDTOResponse> get(String userId);
    void delete(String id, UserDTOResponse userDTOResponse);
}
