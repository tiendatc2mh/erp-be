package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.PermissionDTO;
import com.blameo.idservice.dto.response.PermissionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/

@Component
public interface PermissionService {

    PermissionDTOResponse save(PermissionDTO permissionDTO, UserDTOResponse userDTOResponse);
    PermissionDTOResponse update(PermissionDTO permissionDTO, String id, UserDTOResponse userDTOResponse);
    PermissionDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, String function,String action,Integer method, Integer status);

    List<PermissionDTOResponse> getAll(Integer status);

    void delete(String id, UserDTOResponse userDTOResponse);

    void changeStatus(String id, UserDTOResponse userDTOResponse);
}
