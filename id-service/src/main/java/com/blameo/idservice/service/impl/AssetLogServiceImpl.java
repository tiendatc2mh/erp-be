package com.blameo.idservice.service.impl;

import com.blameo.idservice.dto.response.AssetLogDTOResponse;
import com.blameo.idservice.dto.response.AssetTypeDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.AssetLog;
import com.blameo.idservice.model.AssetType;
import com.blameo.idservice.repository.AssetLogRepository;
import com.blameo.idservice.service.AssetLogService;
import com.blameo.idservice.utils.CommonUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class AssetLogServiceImpl implements AssetLogService {

    @Autowired
    AssetLogRepository assetLogRepository;

    @Override
    public AssetLogDTOResponse save(AssetLog assetLog, UserDTOResponse userDTOResponse) {
        AssetLogDTOResponse assetLogDTOResponse = new AssetLogDTOResponse();
        AssetLog save = assetLogRepository.save(assetLog);
        BeanUtils.copyProperties(save, assetLogDTOResponse);
        return assetLogDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String assetId) {
        Map<String, Object> output = new HashMap<>();
        Pageable pageable = PageRequest.of(pageNo-1, pageSize);
        Page<Object[]> page = assetLogRepository.findAll(pageable, assetId);
        if(page.hasContent()){
            List<AssetLogDTOResponse> responseList = new ArrayList<>();
            page.getContent().forEach(objects -> {
                AssetLogDTOResponse assetLogDTOResponse = new AssetLogDTOResponse();
                AssetLog assetLog = (AssetLog) objects[0];
                BeanUtils.copyProperties(assetLog, assetLogDTOResponse);
                assetLogDTOResponse.setAssetLogSource((String) objects[1]);
                assetLogDTOResponse.setAssetLogReceive((String) objects[2]);
                assetLogDTOResponse.setAssetLogDecision((String) objects[3]);
                responseList.add(assetLogDTOResponse);
            });
            output.put("data", responseList);
            output.put("total", page.getTotalElements());
            output.put("page", pageNo);
            output.put("size", pageSize);

        }else{
            output.put("data", new ArrayList<>());
            output.put("total", 0);
            output.put("page", pageNo);
            output.put("size", pageSize);
        }
        return output;
    }

    @Override
    public List<AssetLogDTOResponse> getAll(String assetId) {
        return CommonUtil.mapList(assetLogRepository.getAll(assetId), AssetLogDTOResponse.class);

    }
}
