package com.blameo.idservice.service.impl;

import com.blameo.idservice.dto.request.AssetTypeDTO;
import com.blameo.idservice.dto.response.AssetTypeDTOResponse;
import com.blameo.idservice.dto.response.PositionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.AssetType;
import com.blameo.idservice.model.Position;
import com.blameo.idservice.repository.AssetTypeRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.AssetTypeService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

@Service
@Transactional
public class AssetTypeServiceImpl implements AssetTypeService {
    @Autowired
    AssetTypeRepository assetTypeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MessageSource messageSource;

    @Override
    public AssetTypeDTOResponse save(AssetTypeDTO assetTypeDTO, UserDTOResponse currentUser) {
        AssetType assetType = new AssetType();
        CoppyObject.copyNonNullProperties(assetTypeDTO, assetType);
        assetType.setCreatedBy(currentUser.getUserId());
        AssetType save = assetTypeRepository.save(assetType);
        AssetTypeDTOResponse assetTypeDTOResponse = new AssetTypeDTOResponse();
        BeanUtils.copyProperties(save, assetTypeDTOResponse);
        return assetTypeDTOResponse;

    }

    @Override
    public AssetTypeDTOResponse update(AssetTypeDTO assetTypeDTO, String assertId, UserDTOResponse currentUser) {
        AssetType assetType = assetTypeRepository.findById(assertId).get();
        if(!assetType.getAssetTypeCode().equals(assetTypeDTO.getAssetTypeCode())){
            if(assetTypeRepository.existsAssetTypeByAssetTypeCodeAndAndAssetTypeStatusGreaterThanEqual(assetTypeDTO.getAssetTypeCode(), 0)){
                throw new RuntimeException(messageSource.getMessage("assetType.code",null,CommonUtil.getLocale())+ SPACE + messageSource.getMessage("isExist",null, CommonUtil.getLocale()));
            }
        }
        CoppyObject.copyNonNullProperties(assetTypeDTO, assetType);
        if(assetType.getAssetTypeDescription() == null){
            assetType.setAssetTypeDescription(null);
        }
        assetType.setUpdatedBy(currentUser.getUserId());

        AssetType update = assetTypeRepository.save(assetType);
        AssetTypeDTOResponse assetTypeDTOResponse = new AssetTypeDTOResponse();
        BeanUtils.copyProperties(update, assetTypeDTOResponse);
        return assetTypeDTOResponse;
    }

    @Override
    public AssetTypeDTOResponse get(String assertId) {
        AssetType assetType = assetTypeRepository.findById(assertId).get();
        AssetTypeDTOResponse assetTypeDTOResponse = new AssetTypeDTOResponse();
        BeanUtils.copyProperties(assetType, assetTypeDTOResponse);
        String createdByName = userRepository.getFullNameByUserId(assetType.getCreatedBy());
        String updatedByName = userRepository.getFullNameByUserId(assetType.getUpdatedBy());
        assetTypeDTOResponse.setCreatedByName(createdByName);
        assetTypeDTOResponse.setUpdatedByName(updatedByName);
        return assetTypeDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status) {
        Map<String, Object> output = new HashMap<>();
        Pageable pageable = PageRequest.of(pageNo-1, pageSize);
        Page<AssetType> page = assetTypeRepository.findAll(pageable, "%" + keyword + "%", status);
        if(page.hasContent()){
            List<AssetTypeDTOResponse> responseList = CommonUtil.mapList(page.getContent(), AssetTypeDTOResponse.class);
            output.put("data", responseList);
            output.put("total", page.getTotalElements());
            output.put("page", pageNo);
            output.put("size", pageSize);

        }else{
            output.put("data", new ArrayList<>());
            output.put("total", 0);
            output.put("page", pageNo);
            output.put("size", pageSize);
        }
        return output;
    }

    @Override
    public List<AssetTypeDTOResponse> getAll(Integer status) {
        return CommonUtil.mapList(assetTypeRepository.getAll(status), AssetTypeDTOResponse.class);
    }


    @Override
    public List<AssetTypeDTOResponse> getAllMultipleId(List<String> listAssetTypeId) {
        List<AssetType> assetTypes = assetTypeRepository.getAllByAssetTypeIdIn(listAssetTypeId);
        return CommonUtil.mapList(assetTypes, AssetTypeDTOResponse.class);
    }

    @Override
    public void delete(String assertId, UserDTOResponse currentUser) {
        AssetType assetType = assetTypeRepository.findById(assertId).get();
        assetType.setAssetTypeStatus(-1);
        assetType.setUpdatedBy(currentUser.getUserId());
        assetTypeRepository.save(assetType);
    }

    @Override
    public void changeStatus(String assertId, UserDTOResponse currentUser) {
        AssetType assetType = assetTypeRepository.findById(assertId).get();
        assetType.setAssetTypeStatus(assetType.getAssetTypeStatus() == 0 ? 1 : 0);
        assetType.setUpdatedBy(currentUser.getUserId());
        assetTypeRepository.save(assetType);
    }


}
