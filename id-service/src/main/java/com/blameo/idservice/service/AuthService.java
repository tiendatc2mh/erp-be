package com.blameo.idservice.service;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.response.RoleDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.Permission;
import com.blameo.idservice.model.Role;
import com.blameo.idservice.model.User;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.utils.CommonUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/28/2023, Friday
 **/
@Service
public class AuthService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    MessageSource messageSource;

    @Autowired
    EntityManager entityManager;

    private static List<OpenPermission> LIST_OPEN_PERMISSION = Arrays.asList(
            new OpenPermission("/api/id/user/multiple-id","GET")
            ,new OpenPermission("/api/id/user/all","GET")
            ,new OpenPermission("/api/id/user/{id}","GET")
            ,new OpenPermission("/api/id/change-password","POST")
            ,new OpenPermission("/api/id/personal_file/self","GET")
            ,new OpenPermission("/api/id/asset/self","GET")
            ,new OpenPermission("/api/worktime/timekeeping","PUT")
            ,new OpenPermission("/api/id/asset/all","GET")
            ,new OpenPermission("/api/id/note-book/all","GET")
            ,new OpenPermission("/api/id/department/all","GET")
            ,new OpenPermission("/api/id/title/multiple","GET")
            ,new OpenPermission("/api/id/title/all","GET")
            ,new OpenPermission("/api/id/type-document/all","GET")
            ,new OpenPermission("/api/id/level/multiple-id","GET")
            ,new OpenPermission("/api/id/level/all","GET")
            ,new OpenPermission("/api/id/position/multiple-id","GET")
            ,new OpenPermission("/api/id/position/all","GET")
            ,new OpenPermission("/api/id/asset-type/multiple-id","GET")
            ,new OpenPermission("/api/id/asset-type/all","GET")
            ,new OpenPermission("/api/id/function/all","GET")
            ,new OpenPermission("/api/id/role/multiple","GET")
            ,new OpenPermission("/api/id/role/all","GET")
            ,new OpenPermission("/api/id/action/all","GET")
            ,new OpenPermission("/api/id/permission/all","GET")
            ,new OpenPermission("/api/erp/process/config/{id}","GET")
            ,new OpenPermission("/api/erp/process/user-approver","GET")
            ,new OpenPermission("/api/erp/process/default/{id}","PUT")
            ,new OpenPermission("/api/erp/process/default/{id}","GET")
            ,new OpenPermission("/api/erp/request-log/all","GET")
            ,new OpenPermission("/api/worktime/holiday","GET")
            ,new OpenPermission("/api/worktime/holiday/all","GET")
            ,new OpenPermission("/api/worktime/worktime","GET")
            ,new OpenPermission("/api/id/asset-log/all","GET")
            ,new OpenPermission("/api/id/asset-log","GET")
            ,new OpenPermission("/api/id/notification/readAll","PATCH")
            ,new OpenPermission("/api/id/notification/index","GET")
            ,new OpenPermission("/api/id/notification/all","GET")
            ,new OpenPermission("/api/id/notification/{id}","PATCH")
            ,new OpenPermission("/api/id/notification","POST")
            ,new OpenPermission("/api/id/notification","GET")
            ,new OpenPermission("/api/id/upload","POST")
            ,new OpenPermission("/api/id/user/schedule","GET")
            ,new OpenPermission("/api/id/user/schedule","GET")
            ,new OpenPermission("/api/worktime/avaiable-day-off","POST")
            ,new OpenPermission("/api/worktime/avaiable-day-off/all","GET")
            ,new OpenPermission("/api/erp/request/schedule","GET")
            ,new OpenPermission("/api/erp/request-asset","GET")
            ,new OpenPermission("/api/erp/process-step-log/all","GET")
            ,new OpenPermission("/api/erp/request/worktime-contents","PUT")
            ,new OpenPermission("/api/worktime/timekeeping/timekeeping-user","GET")
            ,new OpenPermission("/api/erp/request/worktime-contents","PUT")
            ,new OpenPermission("/api/id/role/detail/{id}","GET")
            ,new OpenPermission("/api/erp/request/payment-request/request-advance","GET")
            ,new OpenPermission("/api/erp/request/all","GET")
            ,new OpenPermission("/api/erp/request/cancel-request","PUT")
            ,new OpenPermission("/api/worktime/timekeeping/back-timekeeping","PUT")
            ,new OpenPermission("/api/worktime/avaiable-day-off/update_day_off","PUT")
    );

    private static List<OpenPermission> LIST_OPEN_PERMISSION_MY = Arrays.asList(
            new OpenPermission("/api/worktime/timekeeping/user/{id}","GET")
    );

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private static class OpenPermission{
        private String path;
        private String method;

    }
    public Boolean checkPermission(String email, String path, String method, Locale locale) throws UnsupportedEncodingException {
        if (email.equals(Constant.ADMIN_EMAIL)) {
            return true;
        } else {
            //Get ngôn ngữ
            User user = userRepository.findByEmail(email).orElseThrow(
                    () -> new RuntimeException("User " + messageSource.getMessage("notExist", null, locale))
            );
            UserDTOResponse userDTOResponse = new UserDTOResponse();
            BeanUtils.copyProperties(user, userDTOResponse);
            userDTOResponse = setEntityRelationship(user, userDTOResponse);
            Set<RoleDTOResponse> roleDTOResponse = userDTOResponse.getRoles();
            String pathFormat = replaceUUIDWithPlaceholder(path);
            if(LIST_OPEN_PERMISSION_MY.stream().anyMatch(x -> (x.getPath().equals(pathFormat) || (x.getPath() + "/").equals(pathFormat))
                    && x.getMethod().equalsIgnoreCase(method))){
                String userId = findUUIDFromPath(path);
                return userId.equals(userDTOResponse.getUserId());
            } else if(LIST_OPEN_PERMISSION.stream().anyMatch(x -> (x.getPath().equals(pathFormat) || (x.getPath() + "/").equals(pathFormat))
                    && x.getMethod().equalsIgnoreCase(method))
                    ||
                    roleDTOResponse.stream().flatMap(role -> role.getPermissions().stream()).anyMatch(permission -> (permission.getPath().equals(pathFormat) || (permission.getPath() + "/").equals(pathFormat))
                            && Constant.getMethodName(permission.getMethod()).equalsIgnoreCase(method))) {
                return true;
            }
            return false;
        }
    }

    public  String replaceUUIDWithPlaceholder(String url) throws UnsupportedEncodingException {
        String uuidPattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}";
        return url.replaceAll(uuidPattern, "{id}");
    }

    public  String findUUIDFromPath(String url) throws UnsupportedEncodingException {
        Pattern p = Pattern.compile("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}");
        Matcher m = p.matcher(url);
        if (m.find()) {
            return m.group(0);
        }
        return null;
    }

    public UserDTOResponse setEntityRelationship(User user, UserDTOResponse userDTOResponse) {
        List<Role> roles = new ArrayList<>(user.getRoles());
        roles.forEach(x -> {
            Set<Permission> permissionSet = new HashSet<>(x.getPermissions());
            entityManager.detach(x);
            x.setPermissions(permissionSet);
            x.setUsers(null);
        });
        Set<RoleDTOResponse> roleDTOResponses = new HashSet<>(CommonUtil.mapList(roles, RoleDTOResponse.class));
        userDTOResponse.setRoles(roleDTOResponses);
        return userDTOResponse;
    }
}
