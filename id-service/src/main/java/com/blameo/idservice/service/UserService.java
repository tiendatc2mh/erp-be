package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.UserDTO;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/

@Component
public interface UserService {

    Map<String,Object> save(UserDTO userDTO, UserDTOResponse userRequest);
    void sendMail(String subject, String mailContent, String[] listAccount,
                         byte[][] attachmentDatas, String[] attachmentNames);
    UserDTOResponse update(UserDTO userDTO, String id, UserDTOResponse userRequest);
    UserDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String department, String keyword, Integer status, List<Integer> type, String userCode, Date severanveDate);

    List<UserDTOResponse> getAll(String department, Integer status, String keyword, String userCode);
    List<UserDTOResponse> getAllSchedule();
    List<UserDTOResponse> getAllMultipleId(List<String> listUserId);

    void delete(String id, UserDTOResponse userRequest);

    void changeStatus(String id, UserDTOResponse userRequest);
    UserDTOResponse findByEmail(String email);

    boolean isValidOldPassword(String username, String oldPassword);

    void changePassword(String username, String newPassword);

    InputStreamResource export(String department, String keyword, Integer status, List<Integer> type, String userCode);
}
