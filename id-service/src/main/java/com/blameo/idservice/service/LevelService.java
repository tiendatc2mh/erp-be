package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.LevelDTO;
import com.blameo.idservice.dto.response.LevelDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/2/2023, Wednesday
 **/
@Component
public interface LevelService {
    LevelDTOResponse save(LevelDTO levelDTO);
    LevelDTOResponse update(LevelDTO levelDTO,String id);
    LevelDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status);
    List<LevelDTOResponse> getAllMultipleId(List<String> listUserId);
    List<LevelDTOResponse> getAll(String keyword, Integer status);

    void delete(String id);

    void changeStatus(String id);
}
