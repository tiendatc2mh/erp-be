package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.AssetTypeDTO;
import com.blameo.idservice.dto.response.AssetTypeDTOResponse;
import com.blameo.idservice.dto.response.LevelDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface AssetTypeService {

    AssetTypeDTOResponse save(AssetTypeDTO assetTypeDTO, UserDTOResponse currentUser);
    AssetTypeDTOResponse update(AssetTypeDTO assetTypeDTO, String assertId, UserDTOResponse currentUser);
    AssetTypeDTOResponse get(String assertId);

    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status);

    List<AssetTypeDTOResponse> getAll(Integer status);

    List<AssetTypeDTOResponse> getAllMultipleId(List<String> listAssetTypeId);

    void delete(String assertId,  UserDTOResponse currentUser);

    void changeStatus(String assertId,  UserDTOResponse currentUser);
}
