package com.blameo.idservice.service.impl;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.ActionDTO;
import com.blameo.idservice.dto.response.ActionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.exception.RuntimeExceptionCustom;
import com.blameo.idservice.model.Action;
import com.blameo.idservice.repository.ActionRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.ActionService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/3/2023, Thursday
 **/
@Service
@Transactional
public class ActionServiceImpl implements ActionService {
    ActionRepository actionRepository;

    MessageSource messageSource;

    @Autowired
    UserRepository userRepository;

    public ActionServiceImpl(ActionRepository actionRepository, MessageSource messageSource) {
        this.actionRepository = actionRepository;
        this.messageSource = messageSource;
    }


    @Override
    public ActionDTOResponse save(ActionDTO actionDTO, UserDTOResponse userDTOResponse) {
        Action action = new Action();
        CoppyObject.copyNonNullProperties(actionDTO, action);
        action.setCreatedBy(userDTOResponse.getUserId());
        action.setUpdatedBy(userDTOResponse.getUserId());
        //
        Action saved = actionRepository.save(action);
        ActionDTOResponse actionDTOResponse = new ActionDTOResponse();
        BeanUtils.copyProperties(saved, actionDTOResponse);
        return actionDTOResponse;
    }

    @Override
    public ActionDTOResponse update(ActionDTO actionDTO, String id, UserDTOResponse userDTOResponse) {
        Optional<Action> result = actionRepository.findById(id);
        Action action;
        ActionDTOResponse actionDTOResponse = new ActionDTOResponse();
        if (result.isPresent()) {
            action = result.get();
            if (!action.getActionCode().equals(actionDTO.getActionCode()) && Boolean.TRUE.equals((actionRepository.existsByActionCodeAndActionStatusGreaterThanEqual(actionDTO.getActionCode(), 0)))) {
                throw new RuntimeExceptionCustom(messageSource.getMessage("action.code", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("isExist", null, CommonUtil.getLocale()));
            }
            if (actionDTO.getActionCode() != null) {
                action.setActionCode(actionDTO.getActionCode());
            }
            if (actionDTO.getActionName() != null) {
                action.setActionName(actionDTO.getActionName());
            }

            if (actionDTO.getActionStatus() != null) {
                action.setActionStatus(actionDTO.getActionStatus());
            }

            action.setUpdatedBy(userDTOResponse.getUserId());
            Action saved = actionRepository.save(action);
            BeanUtils.copyProperties(saved, actionDTOResponse);
        } else {
            throw new RuntimeExceptionCustom(messageSource.getMessage("action", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, CommonUtil.getLocale()));
        }
        return actionDTOResponse;
    }

    @Override
    public ActionDTOResponse get(String id) {
        ActionDTOResponse actionDTOResponse = new ActionDTOResponse();
        Optional<Action> result = actionRepository.findById(id);
        if (result.isPresent()) {
            Action action = result.get();
            BeanUtils.copyProperties(action, actionDTOResponse);
        }
        actionDTOResponse.setCreatedByName(userRepository.getFullNameByUserId(actionDTOResponse.getCreatedBy()));
        actionDTOResponse.setUpdatedByName(userRepository.getFullNameByUserId(actionDTOResponse.getUpdatedBy()));
        return actionDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status, Date fromDate, Date toDate) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<Action> page = actionRepository.findAll(paging, "%" + keyword + "%", status,fromDate,toDate);
        if (page.hasContent()) {
            List<ActionDTOResponse> actionDTOResponses = CommonUtil.mapList(page.getContent(), ActionDTOResponse.class);
            output.put("data", actionDTOResponses);
            output.put("total", page.getTotalElements());
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
        }
        output.put("page", pageNo);
        output.put("size", pageSize);
        return output;
    }


    @Override
    public List<ActionDTOResponse> getAll(String keyword, Integer status) {
        return CommonUtil.mapList(actionRepository.getAll("%"+keyword+"%",status), ActionDTOResponse.class);
    }

    @Override
    public void delete(String id, UserDTOResponse userDTOResponse) {
        Optional<Action> result = actionRepository.findById(id);
        if (result.isPresent()) {
            Action action = result.get();
            action.setActionStatus(-1);
            action.setUpdatedBy(userDTOResponse.getUserId());
            actionRepository.save(action);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("action", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));
    }

    @Override
    public void changeStatus(String id, UserDTOResponse userDTOResponse) {
        Optional<Action> result = actionRepository.findById(id);
        if (result.isPresent()) {
            Action action = result.get();
            action.setActionStatus(action.getActionStatus() == 0 ? 1 : 0);
            action.setUpdatedBy(userDTOResponse.getUserId());
            actionRepository.save(action);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("action", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));

    }
}
