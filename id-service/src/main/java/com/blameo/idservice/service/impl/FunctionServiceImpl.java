package com.blameo.idservice.service.impl;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.FunctionDTO;
import com.blameo.idservice.dto.response.FunctionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.exception.RuntimeExceptionCustom;
import com.blameo.idservice.model.Function;
import com.blameo.idservice.repository.FunctionRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.FunctionService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/3/2023, Thursday
 **/
@Service
@Transactional
public class FunctionServiceImpl implements FunctionService {
    
    FunctionRepository functionRepository;

    @Autowired
    UserRepository userRepository;
    
    MessageSource messageSource;

    public FunctionServiceImpl(FunctionRepository functionRepository, MessageSource messageSource) {
        this.functionRepository = functionRepository;
        this.messageSource = messageSource;
    }


    @Override
    public FunctionDTOResponse save(FunctionDTO functionDTO, UserDTOResponse userDTOResponse) {
        Function function = new Function();
        CoppyObject.copyNonNullProperties(functionDTO, function);
        function.setCreatedBy(userDTOResponse.getUserId());
        function.setUpdatedBy(userDTOResponse.getUserId());
        Function saved = functionRepository.save(function);
        FunctionDTOResponse functionDTOResponse = new FunctionDTOResponse();
        BeanUtils.copyProperties(saved, functionDTOResponse);
        return functionDTOResponse;
    }

    @Override
    public FunctionDTOResponse update(FunctionDTO functionDTO, String id, UserDTOResponse userDTOResponse) {
        Optional<Function> result = functionRepository.findById(id);
        Function function;
        FunctionDTOResponse functionDTOResponse = new FunctionDTOResponse();
        if (result.isPresent()) {
            function = result.get();
            if (!function.getFunctionCode().equals(functionDTO.getFunctionCode()) && Boolean.TRUE.equals((functionRepository.existsByFunctionCodeAndFunctionStatusGreaterThanEqual(functionDTO.getFunctionCode(), 0)))) {
                throw new RuntimeExceptionCustom(messageSource.getMessage("function.code", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("isExist", null, CommonUtil.getLocale()));
            }
            if (functionDTO.getFunctionCode() != null) {
                function.setFunctionCode(functionDTO.getFunctionCode());
            }
            if (functionDTO.getFunctionName() != null) {
                function.setFunctionName(functionDTO.getFunctionName());
            }

            if (functionDTO.getFunctionStatus() != null) {
                function.setFunctionStatus(functionDTO.getFunctionStatus());
            }
            function.setUpdatedBy(userDTOResponse.getUserId());
            Function saved = functionRepository.save(function);
            BeanUtils.copyProperties(saved, functionDTOResponse);
        }
        else {
            throw new RuntimeExceptionCustom(messageSource.getMessage("function", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, CommonUtil.getLocale()));
        }
        return functionDTOResponse;
    }

    @Override
    public FunctionDTOResponse get(String id) {
        FunctionDTOResponse functionDTOResponse = new FunctionDTOResponse();
        Optional<Function> result = functionRepository.findById(id);
        if (result.isPresent()) {
            Function function = result.get();
            BeanUtils.copyProperties(function, functionDTOResponse);
        }
        functionDTOResponse.setCreatedByName(userRepository.getFullNameByUserId(functionDTOResponse.getCreatedBy()));
        functionDTOResponse.setUpdatedByName(userRepository.getFullNameByUserId(functionDTOResponse.getUpdatedBy()));
        return functionDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status, Date fromDate, Date toDate) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<Function> page = functionRepository.findAll(paging, "%" + keyword + "%", status,fromDate,toDate);
        if (page.hasContent()) {
            List<FunctionDTOResponse> functionDTOResponses = CommonUtil.mapList(page.getContent(), FunctionDTOResponse.class);
            output.put("data", functionDTOResponses);
            output.put("total", page.getTotalElements());
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
        }
        output.put("page", pageNo);
        output.put("size", pageSize);
        return output;
    }


    @Override
    public List<FunctionDTOResponse> getAll(String keyword, Integer status) {
        return CommonUtil.mapList(functionRepository.getAll("%"+keyword+"%",status), FunctionDTOResponse.class);
    }

    @Override
    public void delete(String id, UserDTOResponse userDTOResponse) {
        Optional<Function> result = functionRepository.findById(id);
        if (result.isPresent()) {
            Function function = result.get();
            function.setFunctionStatus(-1);
            function.setUpdatedBy(userDTOResponse.getUserId());
            functionRepository.save(function);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("function", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("unknownError", null, CommonUtil.getLocale()));
    }

    @Override
    public void changeStatus(String id, UserDTOResponse userDTOResponse) {
        Optional<Function> result = functionRepository.findById(id);
        if (result.isPresent()) {
            Function function = result.get();
            function.setFunctionStatus(function.getFunctionStatus() == 0 ? 1 : 0);
            function.setUpdatedBy(userDTOResponse.getUserId());
            functionRepository.save(function);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("function", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("unknownError", null, CommonUtil.getLocale()));

    }
}
