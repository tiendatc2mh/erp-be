package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.TitleDTO;
import com.blameo.idservice.dto.response.TitleDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Component
public interface TitleService {
    TitleDTOResponse save(TitleDTO titleDTO, UserDTOResponse userDTOResponse);
    TitleDTOResponse update(TitleDTO titleDTO,String id);
    TitleDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status);

    List<TitleDTOResponse> getAll(String keyword, Integer status);

    List<TitleDTOResponse> getAllListTitleId(List<String> titleId);

    void delete(String id);

    void changeStatus(String id);
}
