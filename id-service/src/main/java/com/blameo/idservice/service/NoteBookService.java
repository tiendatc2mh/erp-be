package com.blameo.idservice.service;

import com.blameo.idservice.dto.request.NoteBookDTO;
import com.blameo.idservice.dto.response.NoteBookDTOResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/

@Component
public interface NoteBookService {

    NoteBookDTOResponse save(NoteBookDTO noteBookDTO);
    NoteBookDTOResponse update(NoteBookDTO noteBookDTO, String id);
    NoteBookDTOResponse get(String id);
    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String noteBookName, String noteBookContent, Integer status, Integer type);

    List<NoteBookDTOResponse> getAll(String noteBookName, String noteBookContent, Integer status, Integer type);

    void delete(String id);

    void changeStatus(String id);
}
