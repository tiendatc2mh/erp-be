package com.blameo.idservice.service.impl;

import com.blameo.idservice.dto.request.PositionDTO;
import com.blameo.idservice.dto.response.PositionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.exception.RuntimeExceptionCustom;
import com.blameo.idservice.model.Department;
import com.blameo.idservice.model.Position;
import com.blameo.idservice.repository.DepartmentRepository;
import com.blameo.idservice.repository.PositionRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.PositionService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.CoppyObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.blameo.idservice.constant.Constant.SPACE;

@Service
@Transactional
public class PositionServiceImpl implements PositionService {

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    MessageSource messageSource;


    @Override
    public PositionDTOResponse save(PositionDTO positionDTO, UserDTOResponse userDTOResponse) {
        Locale locale = CommonUtil.getLocale();
        Department department = departmentRepository.findByDepartmentId(positionDTO.getDepartmentId()).orElseThrow(
                () -> new RuntimeException(messageSource.getMessage("department", null, locale)+ SPACE + messageSource.getMessage("notExist", null, locale)
                ));
        Position position = new Position();
        CoppyObject.copyNonNullProperties(positionDTO, position);
        position.setDepartment(department);
        position.setCreatedBy(userDTOResponse.getUserId());
        Position saved = positionRepository.save(position);
        PositionDTOResponse positionDTOResponse = new PositionDTOResponse();
        BeanUtils.copyProperties(saved, positionDTOResponse);
        return positionDTOResponse;
    }

    @Override
    public PositionDTOResponse update(PositionDTO positionDTO, String id, UserDTOResponse userDTOResponse) {
        Optional<Position> result = positionRepository.findById(id);
        Position position;
        PositionDTOResponse positionDTOResponse = new PositionDTOResponse();
        if (result.isPresent()) {
            position = result.get();
            if (!position.getPositionCode().equals(positionDTO.getPositionCode()) && positionRepository.existsByPositionCodeAndPositionStatusGreaterThanEqual(positionDTO.getPositionCode(), 0)) {
                throw new RuntimeExceptionCustom(messageSource.getMessage("position.code", null, CommonUtil.getLocale())+ SPACE  + messageSource.getMessage("isExist", null, CommonUtil.getLocale()));
            }

            if (positionDTO.getPositionCode() != null) {
                position.setPositionCode(positionDTO.getPositionCode());
            }

            if (positionDTO.getPositionName() != null) {
                position.setPositionName(positionDTO.getPositionName());
            }

            Department department = departmentRepository.findByDepartmentId(positionDTO.getDepartmentId()).orElseThrow(
                    () -> new RuntimeException(messageSource.getMessage("department", null, CommonUtil.getLocale())+ SPACE + messageSource.getMessage("notExist", null, CommonUtil.getLocale())
                    ));

            position.setDepartment(department);

            if (positionDTO.getPositionDescription() != null) {
                position.setPositionDescription(positionDTO.getPositionDescription());
            }

            if (positionDTO.getPositionStatus() != null) {
                position.setPositionStatus(positionDTO.getPositionStatus());
            }
            position.setUpdatedBy(userDTOResponse.getUserId());

            Position saved = positionRepository.save(position);
            BeanUtils.copyProperties(saved, positionDTOResponse);
        }
        return positionDTOResponse;
    }

    @Override
    public PositionDTOResponse get(String id) {
        PositionDTOResponse positionDTOResponse = new PositionDTOResponse();
        Optional<Position> result = positionRepository.findById(id);
        if (result.isPresent()) {
            Position position = result.get();
            BeanUtils.copyProperties(position, positionDTOResponse);
        }
        String createdBy = userRepository.getFullNameByUserId(positionDTOResponse.getCreatedBy());
        String updatedBy = userRepository.getFullNameByUserId(positionDTOResponse.getUpdatedBy());
        positionDTOResponse.setCreatedByName(createdBy);
        positionDTOResponse.setUpdatedByName(updatedBy);
        return positionDTOResponse;
    }

    @Override
    public Map<String, Object> findAll(Integer pageNo, Integer pageSize, String keyword, Integer status) {
        Map<String, Object> output = new HashMap<>();
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<Position> page = positionRepository.findAll(paging, "%" + keyword + "%", status);
        if (page.hasContent()) {
            List<PositionDTOResponse> positionDTOS = CommonUtil.mapList(page.getContent(), PositionDTOResponse.class);
            output.put("data", positionDTOS);
            output.put("total", page.getTotalElements());
        } else {
            output.put("data", new ArrayList<>());
            output.put("total", 0);
        }
        output.put("page", pageNo);
        output.put("size", pageSize);
        return output;
    }

    @Override
    public List<PositionDTOResponse> getAllMultipleId(List<String> listPositionId) {
        List<Position> positions = positionRepository.getAllByPositionIdIn(listPositionId);
        return CommonUtil.mapList(positions, PositionDTOResponse.class);
    }

    @Override
    public List<PositionDTOResponse> getAll(String keyword, Integer status) {
        return CommonUtil.mapList(positionRepository.getAll("%"+keyword+"%",status), PositionDTOResponse.class);
    }

    @Override
    public void delete(String id, UserDTOResponse userDTOResponse) {
        Optional<Position> result = positionRepository.findById(id);
        if (result.isPresent()) {
            Position position = result.get();
            position.setPositionStatus(-1);
            position.setUpdatedBy(userDTOResponse.getUserId());
            positionRepository.save(position);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("position", null, CommonUtil.getLocale())+ SPACE  + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));
    }

    @Override
    public void changeStatus(String id, UserDTOResponse userDTOResponse) {
        Optional<Position> result = positionRepository.findById(id);
        if (result.isPresent()) {
            Position position = result.get();
            position.setPositionStatus(position.getPositionStatus() == 0 ? 1 : 0);
            position.setUpdatedBy(userDTOResponse.getUserId());
            positionRepository.save(position);
        } else
            throw new RuntimeExceptionCustom(messageSource.getMessage("position", null, CommonUtil.getLocale())+ SPACE  + messageSource.getMessage("notExist", null, CommonUtil.getLocale()));
    }
}
