package com.blameo.idservice.service;

import com.blameo.idservice.dto.response.AssetLogDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.AssetLog;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface AssetLogService {

    AssetLogDTOResponse save(AssetLog assetLog, UserDTOResponse userDTOResponse);

    Map<String, Object> findAll(Integer pageNo, Integer pageSize, String assetId);

    List<AssetLogDTOResponse> getAll(String assetId);
}
