package com.blameo.idservice.security.services;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/14/2023, Tuesday
 **/

@Service
public class LoginAttemptService {
    private final int MAX_ATTEMPTS = 5;
    private final Map<String, Integer> failedLogins = new ConcurrentHashMap<>();
    private final Map<String, ScheduledFuture<?>> scheduledTasks = new ConcurrentHashMap<>();


    public void loginSucceeded(String username, String ip) {
        String key = username + "-" + ip;
        failedLogins.clear();
        scheduledTasks.clear();
    }

    public void loginFailed(String username, String ip) {
        String key = username + "-" + ip;
        if (!scheduledTasks.containsKey(key)) {
            // Schedule a task to reset the failed login count after 5 minutes
            ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            ScheduledFuture<?> scheduledFuture = executorService.schedule(() -> {
                // Reset the failed login count for this key
                failedLogins.remove(key);
                scheduledTasks.remove(key);
            }, 20000, TimeUnit.MILLISECONDS);
            // Store the scheduled task and failed login count in maps
            scheduledTasks.put(key, scheduledFuture);
            failedLogins.put(key, 1);
        } else {
        int attemptsCount = failedLogins.getOrDefault(key, 0) + 1;
        failedLogins.put(key, attemptsCount);}
    }

    public boolean isBlocked(String username, String ip) {
        String key = username + "-" + ip;
        return failedLogins.getOrDefault(key, 0) >= MAX_ATTEMPTS;
    }

    public static String getClientIP(HttpServletRequest request) {
        String xForwardedForHeader = request.getHeader("X-Forwarded-For");
        if (xForwardedForHeader == null) {
            return request.getRemoteAddr();
        } else {
            // X-Forwarded-For header is a comma-separated list
            // of the client IP addresses.
            // The first IP in the list is the original client IP.
            String[] parts = xForwardedForHeader.split(",");
            return parts[0].trim();
        }
    }
}
