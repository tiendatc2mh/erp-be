package com.blameo.idservice.security.services;

import com.blameo.idservice.exception.TokenExpiredException;
import com.blameo.idservice.exception.TokenRefreshException;
import com.blameo.idservice.model.RefreshToken;
import com.blameo.idservice.repository.RefreshTokenRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@Service
public class RefreshTokenService {
    @Value("${blameo.app.jwtRefreshExpirationMs}")
    private Long refreshTokenDurationMs;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    MessageSource  messageSource;

    Locale locale = CommonUtil.getLocale();

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    public RefreshToken createRefreshToken(String userName) {
        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setUsername(userName);
        refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMs));
        refreshToken.setToken(UUID.randomUUID().toString());
        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new TokenExpiredException(messageSource.getMessage("refreshTokenExpired",null,locale));
        }

        return token;
    }

    @Transactional
    public int deleteByUserName(String userName) {
        return refreshTokenRepository.deleteByUsername(userRepository.findByEmail(userName).get().getEmail());
    }
}
