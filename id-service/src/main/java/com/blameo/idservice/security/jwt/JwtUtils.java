package com.blameo.idservice.security.jwt;

import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.exception.TokenExpiredException;
import com.blameo.idservice.model.RefreshToken;
import com.blameo.idservice.payload.response.JwtResponse;
import com.blameo.idservice.security.services.RefreshTokenService;
import com.blameo.idservice.security.services.UserDetailsImpl;
import com.blameo.idservice.service.UserService;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Locale;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@Component
public class JwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${blameo.app.jwtSecret}")
    private String jwtSecret;

    @Value("${blameo.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    @Value("${blameo.app.jwtExpirationMsRemember}")
    private int jwtExpirationMsRemember;

    @Autowired
    RefreshTokenService refreshTokenService;

    @Autowired
    UserService userService;

    @Autowired
    MessageSource messageSource;

    public String generateJwtToken(Authentication authentication) {

        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }
    public String generateJwtToken(String username, boolean remember) {
        if (remember) {
            return Jwts.builder()
                    .setSubject((username))
                    .setIssuedAt(new Date())
                    .setExpiration(new Date((new Date()).getTime() + jwtExpirationMsRemember))
                    .signWith(SignatureAlgorithm.HS512, jwtSecret)
                    .compact();
        } else {
            return Jwts.builder()
                    .setSubject((username))
                    .setIssuedAt(new Date())
                    .setExpiration(new Date((new Date()).getTime() + 1000L * jwtExpirationMs))
                    .signWith(SignatureAlgorithm.HS512, jwtSecret)
                    .compact();
        }
    }

    public String generateTokenFromUsername(String username) {
        return Jwts.builder()
                .setSubject((username))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    public boolean validateJwtToken(String authToken, Locale locale) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            throw new TokenExpiredException(messageSource.getMessage("expiredToken", null, locale));
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
    public JwtResponse afterAuthen(Authentication authentication, boolean remember){
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        String jwt = generateJwtToken(userDetails.getUsername(),remember);
        UserDTOResponse userDTOResponse = userService.findByEmail(userDetails.getUsername());

//        List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
//                .collect(Collectors.toList());


        RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getUsername());
         return new JwtResponse(jwt, refreshToken.getToken()
//                 , userDetails.getUsername(), userDetails.getEmail()
                 , userDTOResponse);
    }
}
