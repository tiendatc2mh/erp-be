package com.blameo.idservice.security.services;

import com.blameo.idservice.model.User;
import com.blameo.idservice.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static Logger logger = Logger.getLogger(UserDetailsServiceImpl.class);
    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found or Not Active with user: " + email));
//check active nữa
        return UserDetailsImpl.build(user);
    }

}
