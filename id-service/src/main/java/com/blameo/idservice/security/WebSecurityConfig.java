package com.blameo.idservice.security;

import com.blameo.idservice.security.jwt.AuthEntryPointJwt;
import com.blameo.idservice.security.jwt.AuthTokenFilter;
import com.blameo.idservice.security.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    private AuthEntryPointJwt unauthorizedHandler;

    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter() {
        return new AuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers("/api/id/sign-in").permitAll()
                .antMatchers("/**").permitAll()
                .antMatchers("/ws/**").permitAll()
                .antMatchers("/api/id/user/**").permitAll()
                .antMatchers("/api/id/role/**").permitAll()
                .antMatchers("/api/id/level/**").permitAll()
                .antMatchers("/api/id/department/**").permitAll()
                .antMatchers("/api/id/title/**").permitAll()
                .antMatchers("/api/id/type-document/**").permitAll()
                .antMatchers("/api/id/position/**").permitAll()
                .antMatchers("/api/id/action/**").permitAll()
                .antMatchers("/api/id/function/**").permitAll()
                .antMatchers("/api/id/permission/**").permitAll()
                .antMatchers("/api/id/check-authen").permitAll()
                .antMatchers("/api/id/refresh-token").permitAll()
                .antMatchers("/swagger-ui/**", "/v3/api-docs/**").permitAll()
                .antMatchers("/api/id/swagger","/api/id/swagger-ui/**", "/api/id/v3/api-docs/**").permitAll()
                .antMatchers("/api/test/**").permitAll()
//                .antMatchers("/api/v1/**").permitAll()
                .antMatchers("/actuator/**").permitAll()
                .anyRequest().authenticated();

        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/ws/**","/v3/api-docs/**",
                "/swagger-ui.html",
                "/swagger-ui/**","/api/id/swagger-ui/**","/api/id/swagger","/api/id/v3/api-docs/**");
    }
}
