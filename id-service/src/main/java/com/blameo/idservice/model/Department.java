package com.blameo.idservice.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "department", schema = "id")
@Where(clause = "department_status != -1")
public class Department extends BaseEntity implements Comparable<Department>{
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "department_id", nullable = false)
    private String departmentId;

    @Column(name = "department_name", length = 100)
    private String departmentName;

    @Column(name = "department_code", length = 50)
    private String departmentCode;

    @Column(name = "department_description", length = 500)
    private String departmentDescription;

    @Column(name = "department_status")
    private Integer departmentStatus;

    @Override
    public int compareTo(@NotNull Department o) {
        if (this.departmentName.equals(o.departmentName)){
            return 0;
        }else {
            return this.departmentName.compareToIgnoreCase(o.departmentName);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentId, that.departmentId) && Objects.equals(departmentName, that.departmentName) && Objects.equals(departmentCode, that.departmentCode) && Objects.equals(departmentDescription, that.departmentDescription) && Objects.equals(departmentStatus, that.departmentStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentId, departmentName, departmentCode, departmentDescription, departmentStatus);
    }
}
