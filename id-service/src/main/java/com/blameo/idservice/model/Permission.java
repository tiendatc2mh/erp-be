package com.blameo.idservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 7/31/2023, Monday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "permission", schema = "id")
@Where(clause = "permission_status != -1")
public class Permission extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "permission_id", nullable = false)
    private String permissionId;

    @Column(name = "permission_code")
    private String permissionCode;

    @Column(name = "path")
    private String path;

    @Column(name = "method")
    private Integer method;

    @Column(name = "permission_status")
    private Integer permissionStatus;

    @JoinColumn(name = "function_id")
    @OneToOne
    private Function function;

    @JoinColumn(name = "action_id")
    @OneToOne
    private Action action;

}
