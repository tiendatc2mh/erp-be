package com.blameo.idservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/3/2023, Thursday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "action", schema = "id")
@Where(clause = "action_status != -1")
public class Action extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "action_id", nullable = false)
    private String actionId;

    @Column(name = "action_code", length = 50)
    private String actionCode;

    @Column(name = "action_name", length = 100)
    private String actionName;

    @Column(name = "action_status")
    private Integer actionStatus;
}
