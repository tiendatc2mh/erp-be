package com.blameo.idservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 9/26/2023, Tuesday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "note_book")
@Where(clause = "status != -1")
public class NoteBook extends BaseEntity{
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "note_book_id", nullable = false)
    private String noteBookId;

    @Column(name = "note_book_name", length = 100)
    private String noteBookName;

    @Column(name = "note_book_content")
    private String noteBookContent;

    @Column(name = "note_book_type")
    private Integer noteBookType;

    @Column(name = "status")
    private Integer status;


    @OneToMany(mappedBy = "noteBook",cascade = CascadeType.ALL,orphanRemoval = true)
    private Set<Attachment> listAttachment = new HashSet<>();
}
