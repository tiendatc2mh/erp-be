package com.blameo.idservice.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "`position`", schema = "id")
@Where(clause = "position_status != -1")

public class Position extends BaseEntity {
    private static Logger logger = LogManager.getLogger(User.class);
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "position_id", nullable = false)
    private String positionId;

    @Column(name = "position_code", length = 50)
    private String positionCode;

    @Column(name = "position_name", length = 100)
    private String positionName;

    @JoinColumn(name = "department_id")
    @OneToOne
    private Department department;

    @Column(name = "position_description", length = 500)
    private String positionDescription;

    @Column(name = "position_status")
    private Integer positionStatus;

}
