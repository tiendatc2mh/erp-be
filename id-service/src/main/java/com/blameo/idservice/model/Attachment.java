package com.blameo.idservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "attachment")
public class Attachment{
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "attachment_id", nullable = false)
    private String attachmentId;

    @Column(name = "attachment_name", length = 1000)
    private String attachmentName;

    @Column(name = "attachment_url", length = 1000)
    private String attachmentUrl;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_note_book")
    private NoteBook noteBook;

}
