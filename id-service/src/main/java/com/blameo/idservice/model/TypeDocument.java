package com.blameo.idservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "type_document")
@Where(clause = "type_document_status != -1")
public class TypeDocument  extends BaseEntity{

    @Column(name = "type_document_id")
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    private String typeDocumentId;

    @Column(name = "type_document_name")
    private String typeDocumentName;

    @Column(name = "type_document_description")
    private String typeDocumentDescription;

    @Column(name = "type_document_code")
    private String typeDocumentCode;

    @Column(name = "type_document_status")
    private Integer typeDocumentStatus;
}
