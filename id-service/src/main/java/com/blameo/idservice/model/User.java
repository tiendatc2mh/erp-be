package com.blameo.idservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "USER")
@Where(clause = "employee_status != -1")
public class User extends BaseEntity implements Comparable<User>{
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "user_id")
    private String userId;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "user_code")
    private String userCode;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "GENDER")
    private Integer gender;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "personal_email")
    private String personalEmail;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "permanent_address")
    private String permanentAddress;

    @Column(name = "residence")
    private String residence;


    @OneToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @OneToOne
    @JoinColumn(name = "position_id")
    private Position position;

    @OneToOne
    @JoinColumn(name = "title_id")
    private Title title;

    @OneToOne
    @JoinColumn(name = "level_id")
    private Level level;

    @Column(name = "user_type")
    private Integer userType;

    @Column(name = "internship_start_date")
    private Date internshipStartDate;

    @Column(name = "official_date")
    private Date officialDate;

    @Column(name = "severanve_date")
    private Date severanveDate;

    @Column(name = "employee_status")
    private Integer employeeStatus;

    @Column(name = "identity_card")
    private String identityCard;

    @Column(name = "place_of_identity")
    private String placeOfIdentity;

    @Column(name = "date_of_identity")
    private Date dateOfIdentity;

    @Column(name = "tax_code")
    private String taxCode;

    @Column(name = "social_insurance_code")
    private String socialInsuranceCode;

    @Column(name = "probationary_start_date")
    private Date probationaryStartDate;

    @Column(name = "attendance_code")
    private Integer attendanceCode;

    @Column(name = "bank_account_number")
    private String bankAccountNumber;

    @Column(name = "bank_type")
    private String bankType;

    @Column(name = "count_holidays_old")
    private Double countHolidaysOld;

    @Column(name = "count_holidays_new")
    private Double countHolidaysNew;

    @JsonBackReference
    @OneToMany
    @JoinTable(
            name = "USER_ROLE",
            joinColumns = @JoinColumn(
                    name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(
                    name = "ROLE_ID"))
    private Set<Role> roles = new HashSet<>();

    @JsonBackReference
    @OneToMany
    @JoinTable(
            name = "user_manager",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "manager_id"))
    private Set<User> managers = new HashSet<>();

    public void addRole(Role role) {
        this.roles.add(role);
        role.getUsers().add(this);
    }

    public void removeRole(Role role) {
        this.roles.remove(role);
        role.getUsers().remove(this);
    }
    public User(String username, String email, String encode, String fullName) {
        super();
    }

    public User(String userId) {
        this.userId = userId;
    }


    @Override
    public int compareTo(@NotNull User o) {
        if (this.getCreatedDate().equals(o.getCreatedDate())){
            return 0;
        }else {
            return this.getCreatedDate().compareTo(o.getCreatedDate()) > 0 ? -1 : 1;
        }
    }
}
