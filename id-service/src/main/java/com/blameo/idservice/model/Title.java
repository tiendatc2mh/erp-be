package com.blameo.idservice.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@AllArgsConstructor
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "title", schema = "id")
@Where(clause = "title_status != -1")
public class Title extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "title_id", nullable = false)
    private String titleId;

    @Column(name = "title_code", length = 50)
    @NotNull
    private String titleCode;

    @Column(name = "title_name", length = 100)
    @NotNull
    private String titleName;

    @Column(name = "title_description", length = 500)
    private String titleDescription;

    @Column(name = "title_status")
    @NotNull
    private Integer titleStatus;

}
