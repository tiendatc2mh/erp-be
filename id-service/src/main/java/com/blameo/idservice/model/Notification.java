package com.blameo.idservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/23/2023, Wednesday
 **/


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "notification")
public class Notification {

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private String id;

    @Column(name = "message")
    private String message;

    @Column(name = "`request_id`")
    private String requestId;            //Pyc

    @Column(name = "`request_status`")
    private Integer requestStatus;            //Pyc

    @Column(name = "`type`")
    private Integer type;

    @Column(name = "`read`")
    private Integer read;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "user_request_id")
    private String userRequestId;       //Pyc

    @Column(name = "user_receiver_id")
    private String userReceiverId;      //Pyc

}