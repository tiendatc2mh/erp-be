package com.blameo.idservice.model;

import com.blameo.idservice.security.services.UserDetailsImpl;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
public class BaseEntityListener {

    @PrePersist
    public void prePersist(BaseEntity entity) {
        entity.setCreatedBy(SecurityContextHolder.getContext()
                .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal()).getUserId():"");
        entity.setUpdatedBy(SecurityContextHolder.getContext()
                .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal()).getUserId():"");
        entity.setCreatedDate(new Date());
        entity.setUpdatedDate(new Date());

    }

    @PreUpdate
    public void preUpdate(BaseEntity entity) {
        entity.setUpdatedDate(new Date());
        entity.setUpdatedBy(SecurityContextHolder.getContext()
                .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal()).getUserId():"");
    }
}
