package com.blameo.idservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/2/2023, Wednesday
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "level", schema = "id")
@Where(clause = "level_status != -1")
public class Level extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "level_id", nullable = false)
    private String levelId;

    @Column(name = "level_name", length = 100)
    private String levelName;

    @JoinColumn(name = "department_id")
    @OneToOne
    private Department department;

    @Column(name= "coefficients")
    private Double coefficients;

    @Column(name = "level_description", length = 500)
    private String levelDescription;

    @Column(name = "level_status")
    private Integer levelStatus;

    @Column(name = "level_code")
    private String levelCode;
}
