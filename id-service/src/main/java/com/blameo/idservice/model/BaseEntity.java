package com.blameo.idservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.Instant;
import java.util.Date;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@MappedSuperclass
@Getter
@Setter
@EntityListeners(BaseEntityListener.class)
public class BaseEntity {

    @Column(name = "created_date", updatable = false)
    private Date createdDate = Date.from(Instant.now());

    @Column(name = "updated_date")
    private Date updatedDate= Date.from(Instant.now());

    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;
}
