package com.blameo.idservice.model;

import com.blameo.idservice.dto.request.AssetDTO;
import com.blameo.idservice.dto.response.UserDTOResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "asset")
public class Asset extends BaseEntity {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "asset_id", nullable = false)
    private String assetId;

    @Column(name = "asset_code")
    private String assetCode;

    @Column(name = "asset_name")
    private String assetName;

    @OneToOne
    @JoinColumn(name = "asset_type_id")
    private AssetType assetType;

    @Column(name = "asset_status")
    private Integer assetStatus;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "asset_date")
    private Date assetDate;

    @Column(name = "asset_supplier", length = 100)
    private String assetSupplier;

    @Column(name = "asset_increase_date")
    private Date assetIncreaseDate;

    @Column(name = "asset_bill_code", length = 100)
    private String assetBillCode;

    @Column(name = "asset_warranty_time")
    private Date assetWarrantyTime;

    @Column(name = "asset_year_manufacture")
    private Integer assetYearManufacture;

    @Column(name = "asset_manufacture", length = 100)
    private String assetManufacture;

    @Column(name = "asset_uses", length = 100)
    private String assetUses;

    @Column(name = "asset_serial", length = 100)
    private String assetSerial;

    @Column(name = "request_id")
    private String requestId;


    public static Asset convertAssetDTO(AssetDTO assetDTO, AssetType assetType, UserDTOResponse currentUser, Long countByAssetType){
        countByAssetType = countByAssetType + 1L;
        Asset asset = new Asset();
        BeanUtils.copyProperties(assetDTO, asset);
        asset.setAssetType(assetType);
        asset.setAssetCode(assetType.getAssetTypeCode() + String.format("%04d", countByAssetType));
        asset.setAssetStatus(assetDTO.getAssetStatus());
        asset.setAssetSerial(assetDTO.getAssetSerial());
        asset.setAssetDate(assetDTO.getAssetDate());
        asset.setUserId((assetDTO.getUserId() != null && assetDTO.getUserId() != "") ? assetDTO.getUserId() : null);
        asset.setCreatedBy(currentUser.getUserId());
        asset.setRequestId(assetDTO.getRequestId());
        return asset;
    }
}
