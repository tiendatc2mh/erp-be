package com.blameo.idservice.model;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.AssetDTO;
import com.blameo.idservice.dto.response.UserDTOResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "asset_log")
public class AssetLog {
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Id
    @Column(name = "asset_log_id")
    private String assetLogId;

    @OneToOne
    @JoinColumn(name = "asset_id")
    private Asset asset;

    @Column(name = "asset_log_source")
    private String assetLogSource;

    @Column(name = "asset_log_receive")
    private String assetLogReceive;

    @Column(name = "asset_log_assign_date")
    private Date assetLogAssignDate;

    @Column(name = "asset_log_action")
    private Integer assetLogAction;

    @Column(name = "asset_log_content")
    private String assetLogContent;

    @Column(name = "asset_log_decision")
    private String assetLogDecision;

    public static AssetLog AssetLogFirstAssign(Asset asset, AssetDTO assetDTO, UserDTOResponse userDTOResponse){
        AssetLog assetLog = new AssetLog();
        assetLog.setAsset(asset);
        assetLog.setAssetLogSource(asset.getUserId());
        assetLog.setAssetLogAction(Constant.ASSET_LOG_ACTION.ASSIGN);
        assetLog.setAssetLogReceive(assetDTO.getUserId());
        assetLog.setAssetLogAssignDate(assetDTO.getAssetDate());
        assetLog.setAssetLogContent(assetDTO.getReason());
        assetLog.setAssetLogDecision(userDTOResponse.getUserId());
        return assetLog;
    }

    public static AssetLog AssetLogAssign(Asset asset, AssetDTO assetDTO, UserDTOResponse userDTOResponse){
        AssetLog assetLog = new AssetLog();
        assetLog.setAsset(asset);
        assetLog.setAssetLogSource(asset.getUserId());
        assetLog.setAssetLogAction(Constant.ASSET_LOG_ACTION.ASSIGN);
        assetLog.setAssetLogReceive(assetDTO.getUserId());
        assetLog.setAssetLogAssignDate(assetDTO.getAssetDate());
        assetLog.setAssetLogContent(assetDTO.getReason());
        assetLog.setAssetLogDecision(userDTOResponse.getUserId());
        return assetLog;
    }

    public static AssetLog AssetLogRetrieve(Asset asset, AssetDTO assetDTO, UserDTOResponse userDTOResponse){
        AssetLog assetLog = new AssetLog();
        assetLog.setAsset(asset);
        assetLog.setAssetLogSource(asset.getUserId());
        assetLog.setAssetLogAction(Constant.ASSET_LOG_ACTION.RETRIEVE);
        assetLog.setAssetLogReceive(assetDTO.getUserId());
        assetLog.setAssetLogAssignDate(new Date());
        assetLog.setAssetLogContent(assetDTO.getReason());
        assetLog.setAssetLogDecision(userDTOResponse.getUserId());
        return assetLog;
    }

    public static AssetLog AssetLogBought(Asset asset, AssetDTO assetDTO, UserDTOResponse userDTOResponse){
        AssetLog assetLog = new AssetLog();
        assetLog.setAsset(asset);
        assetLog.setAssetLogSource(null);
        assetLog.setAssetLogAction(Constant.ASSET_LOG_ACTION.BOUGHT);
        assetLog.setAssetLogReceive(null);
        assetLog.setAssetLogAssignDate(new Date());
        assetLog.setAssetLogContent(assetDTO.getReason());
        assetLog.setAssetLogDecision(userDTOResponse.getUserId());
        return assetLog;
    }
}
