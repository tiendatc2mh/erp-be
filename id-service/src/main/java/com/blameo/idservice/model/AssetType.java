package com.blameo.idservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "asset_type")
@Where(clause = "asset_type_status != -1")
public class AssetType extends BaseEntity{

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "asset_type_id", nullable = false)
    private String assetTypeId;

    @Column(name = "asset_type_code")
    private String assetTypeCode;

    @Column(name = "asset_type_name")
    private String assetTypeName;

    @Column(name = "asset_type_status")
    private Integer assetTypeStatus;

    @Column(name = "asset_type_description")
    private String assetTypeDescription;

    @Column(name = "is_define")
    private Integer isDefine;

}
