package com.blameo.idservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "personal_file")
public class PersonalFile extends BaseEntity{
    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "personal_file_id", nullable = false)
    private String personalFileId;

    @Column(name = "personal_file_name", length = 50)
    private String personalFileName;

    @Column(name = "type_document_id", length = 100)
    private String typeDocumentId;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Column(name = "personal_file_url", nullable = false, length = 500)
    private String personalFileUrl;

    @Column(name = "personal_file_description", length = 500)
    private String personalFileDescription;

}
