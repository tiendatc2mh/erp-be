package com.blameo.idservice.constant;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

public class Constant {
    private Constant() {
        throw new IllegalStateException("Utility class");
    }

    public static class PROCESS_TYPE {
        public static final int REGISTER_FOR_LEAVE = 1; // đăng ký nghỉ phép
        public static final int REGISTER_FOR_OT = 2; // đăng ký OT
        public static final int REGISTER_FOR_REMOTE = 3; // đăng ký remote
        public static final int BUY_EQUIPMENT = 4; // mua trang thiết bị
        public static final int PAYMENT_REQUEST = 5; // đề nghị thanh toán
        public static final int WORK_CONFIRMATION = 6; // xác nhận công
        public static final int RECRUITMENT_REQUEST = 7; // đề nghị tuyển dụng
        public static final int OTHER = 8; // Khác
    }

    //key message
    public static final String REGISTER_FOR_LEAVE = "${registerForLeave}"; // đăng ký nghỉ phép
    public static final String REGISTER_FOR_OT = "${registerForOt}"; // đăng ký OT
    public static final String REGISTER_FOR_REMOTE = "${registerForRemote}"; // đăng ký remote
    public static final String BUY_EQUIPMENT = "${buyEquipment}"; // mua trang thiết bị
    public static final String PAYMENT_REQUEST = "${paymentRequest}"; // đề nghị thanh toán
    public static final String WORK_CONFIRMATION = "${workConfirmation}"; // xác nhận công
    public static final String RECRUITMENT_REQUEST = "${recruitmentRequest}"; // đề nghị tuyển dụng
    public static final String SEND = "${send}"; // Gửi yêu cầu
    public static final String APPROVAL = "${approval}"; // Phê duyệt
    public static final String REFUSE = "${refuse}"; // Từ chối
    public static final String PERFORM = "${perform}"; // Thực hiện
    public static final String CODE = "${code}"; // Mã
    //

    public static final String PREFIX_USER_CODE = "NV_";
    public static final String SPACE = " ";

    public static class PATH {
        public static final String GET_TIMEKEEPING_USER = "/api/worktime/timekeeping/timekeeping-user";
    }

    //Không được bỏ ROLE_
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_SUPER_ADMIN = "SUPER_ADMIN";
    public static final String ROLE_USER = "USER";

    public static final String ASC = "asc";
    public static final String DESC = "desc";
    public static final int MAX_FILE_NAME = 215;
    public static final int OFFICE_FILE = 1;
    public static final int IMAGE_FILE = 2;
    public static final int IMAGE_PDF_FILE = 3;
    public static final int IMAGE_PDF_OFFICE_FILE = 4;
    public static final String TEMPLATE_USER = "Danh sách nhân sự.xlsx";
    public static final String TEMPLATE_ASSET = "Kiểm kê công cụ tài sản.xlsx";

    public static final String ADMIN_EMAIL = "admin@blameo.com";
    public static final String SUPER_ADMIN_ROLE = "SUPER_ADMIN";
    public static final String DEFAULT_PASSWORD = "123@blameo";
    public static final String KEY = "Department ";
    public static final String UNKNOWN = "unknownError";
    public static final String IS_EXIST = "isExist";
    public static final String CREATE_SUCCESS = "createSuccess";
    public static final String UPDATE_SUCCESS = "updateSuccess";
    public static final String DELETE_SUCCESS = "updateSuccess";
    public static final String NOT_EXIST = "notExist";
    public static final String IS_USE = "inUse";
    public static final String GET_SUCCESS = "getDataSuccess";
    public  static final String KEY_HEADER_USER = "user";

    public static final Integer ACTIVE = 1;
    public static final Integer INACTIVE = 0;
    public static final Integer NON_READ = 0;
    public static final Integer IS_READ = 1;

    public static final Integer DELETED = -1;

    public static final String REGEX_CODE = "^[A-Z0-9-_]+$";
    public static final String REGEX_PASSWORD = "^[a-zA-Z0-9!@#$%^&*()_=+-]+$";
    public static final String REGEX_ALPHA_NUMBER = "^[a-zA-Z0-9]+$";
    public static final String REGEX_ALPHA_NUMBER_QUOTA = "^[a-zA-Z0-9]*$";
    public static final String REGEX_VIETNAMESE_RESIDENCE = "^[0-9a-zA-ZÀ-ỹ\\/\\,; -]*$";
    public static final String REGEX_VIETNAMESE = "^[a-zA-ZÀ-ỹ ]*$";
    public static final String REGEX_PHONE_NUMBER = "0[0-9]{7,9}$";
    public static final String REGEX_NUMBER = "^[0-9]*$";

    public static class ASSET_LOG_ACTION {
        public static final Integer ASSIGN = 1;
        public static final Integer RETRIEVE = 2;
        public static final Integer BOUGHT = 3;
    }

    public static String getMethodName(int methodNumber) {
        switch (methodNumber) {
            case 1:
                return "GET";
            case 2:
                return "POST";
            case 3:
                return "PUT";
            case 4:
                return "PATCH";
            case 5:
                return "DELETE";
            default:
                throw new RuntimeException("Invalid method number");
        }
    }
}
