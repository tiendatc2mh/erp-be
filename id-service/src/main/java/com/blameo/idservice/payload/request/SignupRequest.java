package com.blameo.idservice.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SignupRequest {
    @NotNull
    @Size(min = 3, max = 20, message = "{validate.length}")
    private String username;
 
//    @NotBlank
//    @Size(max = 50)
//    @Email
    private String email;
    
    private Set<String> role;
    
//    @NotBlank
//    @Size(min = 6, max = 40)
    private String password;

//    @NotBlank
//    @Size(min = 1, max = 50)
    private String fullName;
}
