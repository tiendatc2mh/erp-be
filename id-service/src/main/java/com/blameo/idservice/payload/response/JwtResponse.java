package com.blameo.idservice.payload.response;

import com.blameo.idservice.dto.response.UserDTOResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@AllArgsConstructor
@NoArgsConstructor
@Data
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String refreshToken;
//	private String username;
//	private String email;
	private UserDTOResponse userDTOResponses;

	public JwtResponse(String token, String refreshToken,
//			, String username, String email,
					   UserDTOResponse userDTOResponses) {
		this.token = token;
		this.refreshToken = refreshToken;
//		this.username = username;
//		this.email = email;
		this.userDTOResponses = userDTOResponses;
	}
}
