package com.blameo.idservice.payload.request;

import lombok.Data;

import javax.validation.constraints.*;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@Data
public class LoginRequest {
	@NotNull(message = "{validate.notNull}")
	@Email(message = "{validate.email}")
	@Size(min = 0, max = 255, message = "{validate.length}")
	private String email;

	@NotNull(message = "{validate.notNull}")
	private String password;

	private boolean remember;
}

