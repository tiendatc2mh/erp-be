package com.blameo.idservice;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.model.Role;
import com.blameo.idservice.model.User;
import com.blameo.idservice.repository.RoleRepository;
import com.blameo.idservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@SpringBootApplication
@EnableAsync
@EnableConfigurationProperties
public class IdServiceApplication implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    public static void main(String[] args) {
        SpringApplication.run(IdServiceApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        if (!roleRepository.existsByRoleCode(Constant.SUPER_ADMIN_ROLE) && !userRepository.existsByEmail(Constant.ADMIN_EMAIL)){
            //role
            Role role = new Role();
            role.setRoleCode(Constant.SUPER_ADMIN_ROLE);
            role.setRoleName(Constant.SUPER_ADMIN_ROLE);
            role.setCreatedDate(new Date());
            role.setRoleStatus(1);
            Role saved =  roleRepository.save(role);

            //user
            User user = new User();
            user.setEmail(Constant.ADMIN_EMAIL);
            user.setPassword(passwordEncoder.encode(Constant.DEFAULT_PASSWORD));
            user.setUserCode("ADMIN");
            user.setFullName("ADMIN");
            user.setGender(1);
            user.setUserType(1);
            user.setCreatedDate(new Date());
            user.setEmployeeStatus(1);
            user.addRole(saved);
            userRepository.save(user);
            System.out.println("Tạo user và role admin thành công!");

        }
    }
}
