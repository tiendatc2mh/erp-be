package com.blameo.idservice.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * A DTO for the {@link com.blameo.idservice.model.Title} entity
 */
@Data

public class TitleDTOResponse {

    private String titleId;

    private String titleCode;

    private String titleName;

    private String titleDescription;

    private Integer titleStatus;

    private Date createdDate;
    
    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;
}
