package com.blameo.idservice.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
public class AssetTypeDTOResponse extends BaseEntityDTOResponse {

    private String assetTypeId;
    private String assetTypeCode;
    private String assetTypeName;
    private Integer assetTypeStatus;
    private String assetTypeDescription;
    private Integer isDefine;

}
