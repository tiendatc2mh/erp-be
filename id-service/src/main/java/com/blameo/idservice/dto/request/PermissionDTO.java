package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.blameo.idservice.model.Permission} entity
 */
@Data
public class PermissionDTO {
    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 50, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_CODE, message = "{validate.regexCode}")
    private String permissionCode;

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 255, message = "{validate.length}")
    private String path;

    @NotNull(message="{validate.notNull}")
    @Min(value = 1, message = "{validate.minInteger}")
    @Max(value = 5, message = "{validate.maxInteger}")
    private Integer method;

    @NotNull(message="{validate.notNull}")
    @Min(value = 0, message = "{validate.minInteger}")
    @Max(value = 1, message = "{validate.maxInteger}")
    private Integer permissionStatus;

    @NotNull(message="{validate.notNull}")
    private String functionId;

    @NotNull(message="{validate.notNull}")
    private String actionId;
}
