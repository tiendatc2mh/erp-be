package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.blameo.idservice.model.Function} entity
 */
@Data
public class FunctionDTO {
    @NotNull(message="{validate.notNull}")
    @Size(max = 50, min = 1, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_CODE, message = "{validate.regexCode}")
    private String functionCode;

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, min = 1, message = "{validate.length}")
    private String functionName;

    @NotNull(message="{validate.notNull}")
    @Max(value = 1, message = "{validate.maxInteger}")
    @Min(value = 0, message = "{validate.minInteger}")
    private Integer functionStatus;
}
