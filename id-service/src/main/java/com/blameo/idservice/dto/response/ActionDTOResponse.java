package com.blameo.idservice.dto.response;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * A DTO for the {@link com.blameo.idservice.model.Action} entity
 */
@Data

public class ActionDTOResponse {
    @NotNull
    private String actionId;

    @NotNull
    private String actionCode;

    @NotNull
    private String actionName;

    @NotNull
    private Integer actionStatus;

    
    private Date createdDate;

    
    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;
}
