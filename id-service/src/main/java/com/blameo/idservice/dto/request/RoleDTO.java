package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data
public class RoleDTO {

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 100, message = "{validate.length}")
    private String roleName;

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 50, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_CODE, message = "{validate.regexCode}")
    private String roleCode;

    @NotNull(message="{validate.notNull}")
    @Min(value = 0, message = "{validate.minInteger}")
    @Max(value = 1, message = "{validate.maxInteger}")
    private Integer roleStatus;

    private Set<String> users ;

    private Set<String> permissions;


}
