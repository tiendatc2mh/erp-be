package com.blameo.idservice.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 9/26/2023, Tuesday
 **/
@Data
public class AttachmentDTO {
    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 1000, message = "{validate.length}")
    private String attachmentName;
    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 1000, message = "{validate.length}")
    private String attachmentUrl;
}
