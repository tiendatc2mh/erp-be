package com.blameo.idservice.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

/**
 * A DTO for the {@link com.blameo.idservice.model.Department} entity
 */
@Data
public class DepartmentDTOResponse {

    private String departmentId;

    private String departmentName;

    private String departmentCode;

    private String departmentDescription;

    private Integer departmentStatus;
    
    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;

    private Set<UserManagerDTOResponse> managers;

}
