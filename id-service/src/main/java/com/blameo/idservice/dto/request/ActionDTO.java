package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.blameo.idservice.model.Action} entity
 */
@Data
public class ActionDTO {
    @NotNull(message="{validate.notNull}")
    @Size(max = 50, min = 1, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_CODE, message = "{validate.regexCode}")
    private String actionCode;

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, min = 1, message = "{validate.length}")
    private String actionName;

    @NotNull(message="{validate.notNull}")
    @Max(value = 1, message = "{validate.maxInteger}")
    @Min(value = 0, message = "{validate.minInteger}")
    private Integer actionStatus;
}
