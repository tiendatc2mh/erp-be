package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/3/2023, Thursday
 **/
@Data
public class ResetPasswordRequest {
    @NotNull(message="{validate.notNull}")
    @Email(message = "{validate.email}")
    @Size(min = 1, max = 255, message = "{validate.length}")
    private String email;
    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 255, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_PASSWORD, message = "{validate.password}")
    private String newPassword;


}
