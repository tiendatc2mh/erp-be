package com.blameo.idservice.dto.request;

import com.blameo.idservice.model.Attachment;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 9/26/2023, Tuesday
 **/
@Data
public class NoteBookDTO {

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, min = 1, message = "{validate.length}")
    private String noteBookName;

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, message = "{validate.length}")

    private String noteBookContent;

    @NotNull(message="{validate.notNull}")
    @Max(value = 1, message = "{validate.maxInteger}")
    @Min(value = 0, message = "{validate.minInteger}")
    private Integer status;

    @NotNull(message="{validate.notNull}")
    @Max(value = 3, message = "{validate.maxInteger}")
    @Min(value = 1, message = "{validate.minInteger}")
    private Integer noteBookType;

    @Valid
    private Set<AttachmentDTO> listAttachment;

}
