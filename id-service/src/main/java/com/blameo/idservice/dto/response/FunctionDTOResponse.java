package com.blameo.idservice.dto.response;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * A DTO for the {@link com.blameo.idservice.model.Function} entity
 */
@Data

public class FunctionDTOResponse {
    @NotNull
    private String functionId;

    @NotNull
    private String functionCode;

    @NotNull
    private String functionName;

    @NotNull
    private Integer functionStatus;

    
    private Date createdDate;

    
    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;

}
