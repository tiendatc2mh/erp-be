package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.blameo.idservice.model.Title} entity
 */
@Data
public class TitleDTO implements Serializable {

    @NotNull(message="{validate.notNull}")
    @Size(max = 50, min = 1, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_CODE, message = "{validate.regexCode}")
    private String titleCode;

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, min = 1, message = "{validate.length}")
    private String titleName;

    @Size(max = 500, min = 0, message = "{validate.length}")
    private String titleDescription;

    @NotNull(message="{validate.notNull}")
    @Max(value = 1, message = "{validate.maxInteger}")
    @Min(value = 0, message = "{validate.minInteger}")
    private Integer titleStatus;
}
