package com.blameo.idservice.dto.response;

import com.blameo.idservice.dto.request.AttachmentDTO;
import com.blameo.idservice.model.Attachment;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 9/26/2023, Tuesday
 **/
@Data
public class NoteBookDTOResponse {

    private String noteBookId;

    private String noteBookName;

    private String noteBookContent;

    private Integer noteBookType;

    private Integer status;

    private Set<Attachment> listAttachment;

    private Date createdDate;

    private String createdBy;

    private String updatedBy;

}
