package com.blameo.idservice.dto.response;

import com.blameo.idservice.model.Action;
import com.blameo.idservice.model.Function;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * A DTO for the {@link com.blameo.idservice.model.Permission} entity
 */
@Data

public class PermissionDTOResponse {

    private String permissionId;

    private String permissionCode;


    private String path;


    private Integer method;


    private Integer permissionStatus;


    private Function function;


    private Action action;

    
    private Date createdDate;

    
    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;
}
