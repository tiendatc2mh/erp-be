package com.blameo.idservice.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class AssetLogDTOResponse {
    private String assetLogId;

    private String assetLogSource;

    private String assetLogReceive;

    private Date assetLogAssignDate;

    private Integer assetLogAction;

    private String assetLogContent;

    private String assetLogDecision;
}
