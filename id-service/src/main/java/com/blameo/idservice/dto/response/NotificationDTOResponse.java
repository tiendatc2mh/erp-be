package com.blameo.idservice.dto.response;

import com.blameo.idservice.model.User;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/23/2023, Wednesday
 **/


@Data
public class NotificationDTOResponse {

    private String id;

    private String message;

    private String requestId;            //Pyc

    private Integer requestStatus;            //Pyc

    private Integer type;

    private Integer read;

    private Date createdDate;

    private String userRequestId;       //Pyc

    private String userRequestEmail;       //Pyc

    private String userRequestAvatar;       //Pyc

    private String userReceiverId;      //Pyc


}