package com.blameo.idservice.dto.response;

import com.blameo.idservice.dto.request.RoleDTO;
import com.blameo.idservice.model.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data
@NoArgsConstructor
public class UserDTOResponse {

    private String userId;


    private String userCode;


    private String fullName;


    private String avatar;


    private Integer gender;

    
    private Date birthday;


    private String email;


    private String personalEmail;


    private String phoneNumber;


    private String permanentAddress;


    private String residence;


    private Department department;


    private Position position;


    private Title title;


    private Level level;

    private Integer userType;

    
    private Date internshipStartDate;

    
    private Date officialDate;

    
    private Date severanveDate;


    private Integer employeeStatus;

    private Set<RoleDTOResponse> roles;

    private Set<UserManagerDTOResponse> managers;

    
    private Date createdDate;

    
    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String identityCard;


    private String placeOfIdentity;


    private Date dateOfIdentity;


    private String taxCode;

    private String socialInsuranceCode;

    private Date probationaryStartDate;

    private Integer attendanceCode;

    private String bankAccountNumber;

    private String bankType;

    private Double countHolidaysOld;

    private Double countHolidaysNew;
    public UserDTOResponse(String userId, String fullName) {
        this.userId = userId;
        this.fullName = fullName;
    }

    public UserDTOResponse(String userId, String fullName, String titleName, String deptName, String bankAccountNumber, String bankType) {
        this.userId = userId;
        this.fullName = fullName;
        this.bankAccountNumber = bankAccountNumber;
        this.bankType = bankType;
        Title title1 = new Title();
        title1.setTitleName(titleName);
        this.title = title1;
        Department department1 = new Department();
        department1.setDepartmentName(deptName);
        this.department = department1;
    }
}
