package com.blameo.idservice.dto.response;

import com.blameo.idservice.model.Department;
import com.blameo.idservice.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * A DTO for the {@link com.blameo.idservice.model.Title} entity
 */
@Data

public class PositionDTOResponse {

    @NotNull
    private String positionId;

    @NotNull
    private String positionCode;

    @NotNull
    private String positionName;

    @NotNull
    private Department department;

    private String positionDescription;

    @NotNull
    private Integer positionStatus;
    
    private Date createdDate;
    
    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;


}
