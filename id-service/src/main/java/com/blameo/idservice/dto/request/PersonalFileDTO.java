package com.blameo.idservice.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data
public class PersonalFileDTO {

    @NotNull(message="{validate.notNull}")
    private String typeDocumentId;

    @NotNull(message="{validate.notNull}")
    private Integer quantity;

    @NotNull(message="{validate.notNull}")
    private String personalFileUrl;

    @NotNull(message="{validate.notNull}")
    private String personalFileName;
}
