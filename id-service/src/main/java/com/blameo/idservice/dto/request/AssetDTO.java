package com.blameo.idservice.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

@Data
public class AssetDTO {

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, message = "{validate.length}")
    @NotBlank(message="{validate.notNull}")
    private String assetName;

    @NotNull(message="{validate.notNull}")
    @NotBlank(message="{validate.notNull}")
    private String assetType;

    private Date assetIncreaseDate;

    @Size(max = 100, message = "{validate.length}")
    private String assetSupplier;


    @Size(max = 100, message = "{validate.length}")
    @Pattern(regexp = "[a-zA-Z0-9!@#$%^&*()=+-_/]*$", message = "{validate.invalid}")
    private String assetBillCode;

    private Date assetWarrantyTime;

    @Min(value = 1000, message = "{validate.minInteger}")
    @Max(value = 9999, message = "{validate.maxInteger}")
    private Integer assetYearManufacture;

    @Size(max = 100, message = "{validate.length}")
    private String assetManufacture;

    @Size(max = 100, message = "{validate.length}")
    private String assetUses;

    private List<ListAssetUser> listAssetUsers;

    @Size(max = 100, message = "{validate.length}")
    private String assetSerial;

    private String userId;

    private Date assetDate;

    @Min(value = 0, message = "{validate.minInteger}")
    @Max(value = 1, message = "{validate.maxInteger}")
    private Integer assetStatus;

    @Size(max = 100, message = "{validate.length}")
    private String reason;

    private String requestId;

    @Data
    public static class ListAssetUser {
        @Size(max = 50, message = "{validate.length}")
        @NotNull(message="{validate.notNull}")
        @NotBlank(message="{validate.notNull}")
        private String assetId;

        private Date assetDate;
    }
}

