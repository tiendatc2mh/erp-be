package com.blameo.idservice.dto.response;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * A DTO for the {@link com.blameo.idservice.model.Department} entity
 */
@Data

public class PersonalFileDTOResponse {

    @NotNull
    private String personalFileId;

    @NotNull
    private String personalFileName;

    @NotNull
    private String typeDocumentId;

    private String userId;

    @NotNull
    private Integer quantity;
    
    private String personalFileUrl;

    private String personalFileDescription;

    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String typeDocumentName;

}
