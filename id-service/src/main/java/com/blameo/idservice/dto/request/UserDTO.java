package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.model.Role;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data
public class UserDTO {

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 100, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_VIETNAMESE, message = "{validate.invalid}")
    private String fullName;


    private String avatar;

    @NotNull(message="{validate.notNull}")
    @Min(value = 0, message = "{validate.minInteger}")
    @Max(value = 1, message = "{validate.maxInteger}")
    private Integer gender;

    @NotNull(message="{validate.notNull}")
    private Date birthday;

    @NotNull(message="{validate.notNull}")
    @Email(message = "{validate.email}")
    @Size(min = 1, max = 255, message = "{validate.length}")
    private String email;

    @Email(message = "{validate.email}")
    @Size(min = 0, max = 255, message = "{validate.length}")
    private String personalEmail;

    @NotNull(message="{validate.notNull}")
    @Pattern(regexp = Constant.REGEX_PHONE_NUMBER, message = "{validate.invalid}")
    private String phoneNumber;

    @Pattern(regexp = Constant.REGEX_VIETNAMESE_RESIDENCE, message = "{validate.invalid}")
    @Size(min = 0, max = 255, message = "{validate.length}")
    private String permanentAddress;

    @Pattern(regexp = Constant.REGEX_VIETNAMESE_RESIDENCE, message = "{validate.invalid}")
    @Size(min = 0, max = 255, message = "{validate.length}")
    private String residence;

    @NotNull(message="{validate.notNull}")
    private String departmentId;

    @NotNull(message="{validate.notNull}")
    private String positionId;

    @NotNull(message="{validate.notNull}")
    private String titleId;

    @NotNull(message="{validate.notNull}")
    private String levelId;

    @NotNull(message="{validate.notNull}")
    @Min(value = 1, message = "{validate.minInteger}")
    @Max(value = 4, message = "{validate.maxInteger}")
    private Integer userType;


    private Date internshipStartDate;


    private Date officialDate;


    private Date severanveDate;

    @NotNull(message="{validate.notNull}")
    @Min(value = -1, message = "{validate.minInteger}")
    @Max(value = 1, message = "{validate.maxInteger}")
    private Integer employeeStatus;

    @NotEmpty(message="{validate.notNull}")
    private Set<String> roles;

    private Set<String> managers;

    @Size(min = 0, max = 50, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_ALPHA_NUMBER_QUOTA, message = "{validate.invalid}")
    private String identityCard;

    @Size(min = 0, max = 100, message = "{validate.length}")
    private String placeOfIdentity;


    private Date dateOfIdentity;


    @Pattern(regexp = Constant.REGEX_NUMBER, message = "{validate.invalid}")
    @Size(min = 10, max = 13, message = "{validate.length}")
    private String taxCode;

    @Pattern(regexp = Constant.REGEX_NUMBER, message = "{validate.invalid}")
    @Size(min = 0, max = 50, message = "{validate.length}")
    private String socialInsuranceCode;

    private Date probationaryStartDate;

    @NotNull(message="{validate.notNull}")
    @Min(value = 1, message = "{validate.minInteger}")
    private Integer attendanceCode;

    @NotNull(message="{validate.notNull}")
    @Pattern(regexp = Constant.REGEX_NUMBER, message = "{validate.invalid}")
    @Size(min = 1, max = 50, message = "{validate.length}")
    private String bankAccountNumber;

    @NotNull(message="{validate.notNull}")
    @Size(min = 0, max = 255, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_ALPHA_NUMBER_QUOTA, message = "{validate.invalid}")
    private String bankType;
}
