package com.blameo.idservice.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
public class AssetTypeDTO {

    @NotNull(message="{validate.notNull}")
    @Size(max = 50, message = "{validate.length}")
    @NotBlank(message="{validate.notNull}")
    private String assetTypeCode;

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, message = "{validate.length}")
    @NotBlank(message="{validate.notNull}")
    private String assetTypeName;

    @NotNull(message="{validate.notNull}")
    @Min(value = 0, message = "{validate.minInteger}")
    @Max(value = 1, message = "{validate.maxInteger}")
    private Integer assetTypeStatus;

    @Size(max = 500, message = "{validate.length}")
    private String assetTypeDescription;

    @NotNull(message="{validate.notNull}")
    @Min(value = 0, message = "{validate.minInteger}")
    @Max(value = 1, message = "{validate.maxInteger}")
    private Integer isDefine;
}
