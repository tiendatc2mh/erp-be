package com.blameo.idservice.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data

public class TypeDocumentDTOResponse {

    private String typeDocumentId;

    private String typeDocumentName;

    private String typeDocumentDescription;

    private String typeDocumentCode;

    private Integer typeDocumentStatus;

    
    private Date createdDate;

    
    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;

}
