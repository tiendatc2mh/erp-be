package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.model.Department;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
public class PositionDTO {

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, min = 1, message = "{validate.length}")
    private String positionName;

    @NotNull(message="{validate.notNull}")
    @Size(max = 50, min = 1, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_CODE, message = "{validate.regexCode}")
    private String positionCode;

    @NotNull(message="{validate.notNull}")
    private String departmentId;

    @Size(max = 500, min = 0, message = "{validate.length}")
    private String positionDescription;

    @NotNull(message="{validate.notNull}")
    @Max(value = 1, message = "{validate.maxInteger}")
    @Min(value = 0, message = "{validate.minInteger}")
    private Integer positionStatus;

}
