package com.blameo.idservice.dto.request;

import lombok.Data;

import javax.validation.Valid;
import java.util.List;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 9/13/2023, Wednesday
 **/
@Data
public class ListPersonalFileDTO {
    @Valid
    private List<PersonalFileDTO> personalFiles;
}
