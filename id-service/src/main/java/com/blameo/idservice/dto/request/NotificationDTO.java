package com.blameo.idservice.dto.request;

import com.blameo.idservice.model.User;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/23/2023, Wednesday
 **/


@Data
public class NotificationDTO {

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 1000, message = "{validate.length}")
    private String message;

    private String requestId;       //Pyc

    private Integer requestStatus;       //Pyc

    @NotNull(message="{validate.notNull}")
    @Min(value = 1, message = "{validate.minInteger}")
    @Max(value = 8, message = "{validate.maxInteger}")
    private Integer type;

    @NotNull(message="{validate.notNull}")
    private String userRequestId;

    @NotNull(message="{validate.notNull}")
    private String userReceiverId;            //Pyc


    // Getter và setter

}