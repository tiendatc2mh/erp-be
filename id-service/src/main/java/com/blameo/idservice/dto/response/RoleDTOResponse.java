package com.blameo.idservice.dto.response;

import com.blameo.idservice.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@Data

public class RoleDTOResponse {

    private String roleId;


    private String roleName;


    private String roleCode;


    private Integer roleStatus;

    
    private Date createdDate;


    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

    private String createdByName;

    private String updatedByName;

    private Set<UserDTOResponse> users;

    private Set<PermissionDTOResponse> permissions;

}
