package com.blameo.idservice.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * A DTO for the {@link com.blameo.idservice.model.Level} entity
 */
@Data
public class LevelDTOResponse {

    private String levelId;

    private String levelName;

    private String departmentId;

    private DepartmentDTOResponse department;

    private Double coefficients;

    private String levelDescription;

    private Integer levelStatus;

    private String levelCode;
    
    private Date createdDate;

    private Date updatedDate;

    private String createdBy;

    private String updatedBy;

}
