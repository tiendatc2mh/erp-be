package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.blameo.idservice.model.Level} entity
 */
@Data
public class LevelDTO {

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 100, message = "{validate.length}")
    private String levelName;

    @NotNull(message="{validate.notNull}")
    private String departmentId;

    @NotNull(message="{validate.notNull}")
    private Double coefficients;

    @Size(min = 0, max = 500, message = "{validate.length}")
    private String levelDescription;

    @NotNull(message="{validate.notNull}")
    @Min(value = 0, message = "{validate.minInteger}")
    @Max(value = 1, message = "{validate.maxInteger}")
    private Integer levelStatus;

    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 50, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_CODE, message = "{validate.regexCode}")
    private String levelCode;
}
