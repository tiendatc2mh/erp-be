package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Set;

/**
 * A DTO for the {@link com.blameo.idservice.model.Department} entity
 */
@Data
public class DepartmentDTO {

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, min = 1, message = "{validate.length}")
    private String departmentName;

    @NotNull(message="{validate.notNull}")
    @Size(max = 100, min = 1, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_CODE, message = "{validate.regexCode}")
    private String departmentCode;

    @Size(max = 500, min = 0, message = "{validate.length}")
    private String departmentDescription;

    @NotNull(message="{validate.notNull}")
    @Max(value = 1, message = "{validate.maxInteger}")
    @Min(value = 0, message = "{validate.minInteger}")
    private Integer departmentStatus;

    private Set<String> managers;

}
