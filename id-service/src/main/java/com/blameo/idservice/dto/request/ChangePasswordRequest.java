package com.blameo.idservice.dto.request;

import com.blameo.idservice.constant.Constant;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/3/2023, Thursday
 **/
public class ChangePasswordRequest {
    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 255, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_PASSWORD, message = "{validate.password}")
    private String oldPassword;
    @NotNull(message="{validate.notNull}")
    @Size(min = 1, max = 255, message = "{validate.length}")
    @Pattern(regexp = Constant.REGEX_PASSWORD, message = "{validate.password}")
    private String newPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }


}
