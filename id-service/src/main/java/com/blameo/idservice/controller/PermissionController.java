package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.PermissionDTO;
import com.blameo.idservice.dto.request.PermissionDTO;
import com.blameo.idservice.dto.response.PermissionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.Function;
import com.blameo.idservice.repository.*;
import com.blameo.idservice.service.PermissionService;
import com.blameo.idservice.service.TypeDocumentService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@RestController
@RequestMapping("/api/id/permission")
public class PermissionController {
    @Autowired
    PermissionService permissionService;

    @Autowired
    PermissionRepository permissionRepository;

    @Autowired
    ActionRepository actionRepository;

    @Autowired
    FunctionRepository functionRepository;

    @Autowired
    private MessageSource messageSource;

    Logger logger = Logger.getLogger(PermissionController.class);
    @Autowired
    private RoleRepository roleRepository;

    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody PermissionDTO permissionDTO, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to save permission : {}"+ permissionDTO.toString());

            if (permissionRepository.existsByPermissionCodeAndPermissionStatusGreaterThanEqual(permissionDTO.getPermissionCode(),0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("permission.code",null,locale)+ SPACE +messageSource.getMessage("isExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            PermissionDTOResponse result = permissionService.save(permissionDTO,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("createSuccess",null,locale),result),HttpStatus.CREATED);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody PermissionDTO permissionDTO, @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to update permission : {}"+ permissionDTO.toString());

            if (!permissionRepository.existsByPermissionIdAndPermissionStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("permission",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            PermissionDTOResponse result = permissionService.update(permissionDTO,id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale),result),HttpStatus.OK);

        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer pageSize,
                                     @RequestParam(required = false,defaultValue = "") String keyword,
                                     @RequestParam(required = false,defaultValue = "") String function,
                                     @RequestParam(required = false,defaultValue = "") String action,
                                     @RequestParam(required = false) Integer method,
                                     @RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all permission");
            if (!StringUtils.isEmpty(action)) {
                action = DataUtil.makeLikeQuery(action);
            }
            if (!StringUtils.isEmpty(function)) {
                function = DataUtil.makeLikeQuery(function);
            }
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            Map<String,Object> result = permissionService.findAll(page,pageSize,keyword,function,action,method,status);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> get( @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get permission : {}"+ id);

            if (!permissionRepository.existsByPermissionIdAndPermissionStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("permission",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            PermissionDTOResponse result = permissionService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> changeStatus( @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to change status permission : {}"+ id);

            if (!permissionRepository.existsByPermissionIdAndPermissionStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("permission",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            permissionService.changeStatus(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete( @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to delete permission : {}"+ id);

            if (!permissionRepository.existsByPermissionIdAndPermissionStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("permission",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }

            //check table khác sử dụng nếu cần thiết
            if (roleRepository.existsByPermission(id)){
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("permission",null,locale)+ SPACE + messageSource.getMessage(Constant.IS_USE, null, locale)+ messageSource.getMessage("dontDelete", null, locale)), HttpStatus.BAD_REQUEST);
            }
            permissionService.delete(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("deleteSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll(@RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all permission");
            List<PermissionDTOResponse> result = permissionService.getAll(status);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
