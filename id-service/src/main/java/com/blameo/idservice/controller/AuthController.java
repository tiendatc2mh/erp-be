package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.ChangePasswordRequest;
import com.blameo.idservice.dto.request.ResetPasswordRequest;
import com.blameo.idservice.exception.TokenExpiredException;
import com.blameo.idservice.exception.TokenRefreshException;
import com.blameo.idservice.model.RefreshToken;
import com.blameo.idservice.model.Role;
import com.blameo.idservice.model.User;
import com.blameo.idservice.payload.request.LoginRequest;
import com.blameo.idservice.payload.request.SignupRequest;
import com.blameo.idservice.payload.request.TokenRefreshRequest;
import com.blameo.idservice.payload.response.MessageResponse;
import com.blameo.idservice.payload.response.TokenRefreshResponse;
import com.blameo.idservice.repository.RoleRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.security.jwt.JwtUtils;
import com.blameo.idservice.security.services.RefreshTokenService;
import com.blameo.idservice.security.services.UserDetailsImpl;
import com.blameo.idservice.service.AuthService;
import com.blameo.idservice.service.UserService;
import com.blameo.idservice.utils.CommonUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 3/15/2023, Wednesday
 **/

@RestController
@RequestMapping("/api/id")
public class AuthController {

    private final Logger logger = Logger.getLogger(AuthController.class);
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    RefreshTokenService refreshTokenService;

    @Autowired
    AuthService authService;

    @Autowired
    UserService userService;

    @Autowired
    MessageSource messageSource;



    @PostMapping("/sign-in")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            if (userRepository.existsByEmail(loginRequest.getEmail())){
                Authentication authentication = authenticationManager
                        .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
                SecurityContextHolder.getContext().setAuthentication(authentication);
                return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("loginSuccess", null, locale), jwtUtils.afterAuthen(authentication,loginRequest.isRemember())), HttpStatus.OK);
            }
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("user.email.notFound", null, locale), null), HttpStatus.BAD_REQUEST);
        } catch (DisabledException e) {
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("unknownError", null, locale), null), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("user.invalid", null, locale), null), HttpStatus.BAD_REQUEST);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError", null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @PostMapping("/sign-up")
//    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
//        if (userRepository.existsByEmail(signUpRequest.getUsername())) {
//            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
//        }
//
//        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
//            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
//        }
//
//        // Create new user's account
//        User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
//                encoder.encode(signUpRequest.getPassword()), signUpRequest.getFullName());
//
//        Set<String> strRoles = signUpRequest.getRole();
//        Set<Role> roles = new HashSet<>();
//
//        if (strRoles == null) {
//            Role userRole = roleRepository.findByRoleName(Constant.ROLE_USER)
//                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//            roles.add(userRole);
//        } else {
//            for (String role : strRoles) {
//                switch (role) {
//                    case Constant.ROLE_SUPER_ADMIN:
//                        Role adminRole = roleRepository.findByRoleName(Constant.ROLE_SUPER_ADMIN)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(adminRole);
//
//                        break;
//                    case Constant.ROLE_ADMIN:
//                        Role modRole = roleRepository.findByRoleName(Constant.ROLE_ADMIN)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(modRole);
//
//                        break;
//                    default:
//                        Role userRole = roleRepository.findByRoleName(Constant.ROLE_USER)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(userRole);
//                }
//            }
//        }
//
//        user.setRoles(roles);
//        userRepository.save(user);
//
//        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
//    }

    @PostMapping("/refresh-token")
    public ResponseEntity<?> refreshtoken(@Valid @RequestBody TokenRefreshRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            String requestRefreshToken = request.getRefreshToken();

            return refreshTokenService.findByToken(requestRefreshToken)
                    .map(refreshTokenService::verifyExpiration)
                    .map(RefreshToken::getUsername)
                    .map(user -> {
                        String token = jwtUtils.generateTokenFromUsername(user);
                        return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("refreshTokenSuccess", null, CommonUtil.getLocale()), new TokenRefreshResponse(token, requestRefreshToken)), HttpStatus.OK);
                    })
                    .orElse(new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("invalidRefreshToken", null, CommonUtil.getLocale()), null), HttpStatus.BAD_REQUEST));
        } catch (TokenExpiredException tokenExpiredException) {
            return new ResponseEntity<>(ResponseMessage.error(tokenExpiredException.getMessage()), HttpStatus.UNAUTHORIZED);
        }
        catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError", null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            // Lấy thông tin người dùng hiện tại đăng nhập
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            // Kiểm tra mật khẩu cũ của người dùng
            if (!userService.isValidOldPassword(username, request.getOldPassword())) {
                return new ResponseEntity<>(new ResponseMessage(false, messageSource.getMessage("invalidOldPassword", null, CommonUtil.getLocale()), null), HttpStatus.BAD_REQUEST);
            }
            // Thay đổi mật khẩu mới
            userService.changePassword(username, request.getNewPassword());
            return new ResponseEntity<>(new ResponseMessage(false, messageSource.getMessage("changePasswordSuccess", null, CommonUtil.getLocale()), null), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError", null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody ResetPasswordRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            //ADMIN co quyen
            userService.changePassword(request.getEmail(), request.getNewPassword());
            return new ResponseEntity<>(new ResponseMessage(false, messageSource.getMessage("resetPasswordSuccess", null, CommonUtil.getLocale()), null), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError", null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/sign-out")
    public ResponseEntity<?> logoutUser() {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = userDetails.getUsername();
            // Clear the security context
            SecurityContextHolder.clearContext();
            refreshTokenService.deleteByUserName(username);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("logoutSuccess", null, CommonUtil.getLocale()), null), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError", null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/check-authen")
    public ResponseEntity<?> checkAuthentication(@RequestParam(name = "token") String token
            , @RequestParam(name = "path") String path
            , @RequestParam(name = "method") String method
            , @RequestParam(name = "acceptLanguage") String acceptLanguage) {
        //Get ngôn ngữ
        Locale locale = new Locale(acceptLanguage);
        try {
            if (jwtUtils.validateJwtToken(token,locale)) {
                String email = jwtUtils.getUserNameFromJwtToken(token);
                path = URLDecoder.decode(path, StandardCharsets.UTF_8);
                if (authService.checkPermission(email, path, method,locale)) {
                    return ResponseEntity.ok(userService.findByEmail(email));
                } else
                    return new ResponseEntity<>(new ResponseMessage(false, messageSource.getMessage("unAuthorized", null, locale), null), HttpStatus.FORBIDDEN);
            }
            return new ResponseEntity<>(new ResponseMessage(false, messageSource.getMessage("invalidToken", null, locale), null), HttpStatus.BAD_REQUEST);
        }
        catch (TokenExpiredException tokenExpiredException) {
            return new ResponseEntity<>(ResponseMessage.error(tokenExpiredException.getMessage()), HttpStatus.UNAUTHORIZED);
        }
        catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError", null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
