package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.UserDTO;
import com.blameo.idservice.dto.request.UserDTO;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.repository.RoleRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.security.jwt.JwtUtils;
import com.blameo.idservice.security.services.RefreshTokenService;
import com.blameo.idservice.service.AuthService;
import com.blameo.idservice.service.UserService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@RestController
@RequestMapping("/api/id/user")
public class UserController {

    private final Logger logger = Logger.getLogger(UserController.class);
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    RefreshTokenService refreshTokenService;

    @Autowired
    AuthService authService;

    @Autowired
    MessageSource messageSource;

    @Value("${url_web}")
    private String urlWeb;

    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody UserDTO userDTO, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to save user : {}"+ userDTO.toString());

            if (userRepository.existsByAttendanceCode(userDTO.getAttendanceCode())) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("user.attendanceCode",null,locale)+ SPACE +messageSource.getMessage("isExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            if (userRepository.existsByEmailAndEmployeeStatusGreaterThanEqual(userDTO.getEmail(),0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("validate.duplicateEmailCompany",null,locale)), HttpStatus.BAD_REQUEST);
            }

            if (userDTO.getIdentityCard() != null && userRepository.existsByIdentityCard(userDTO.getIdentityCard())) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("validate.duplicateIdentity",null,locale)), HttpStatus.BAD_REQUEST);
            }
            if (userRepository.existsByPhoneNumber(userDTO.getPhoneNumber())) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("validate.duplicatePhoneNumber",null,locale)), HttpStatus.BAD_REQUEST);
            }
            if (userDTO.getSocialInsuranceCode() != null && userRepository.existsBySocialInsuranceCode(userDTO.getSocialInsuranceCode())) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("validate.duplicateSocialInsuranceCode",null,locale)), HttpStatus.BAD_REQUEST);
            }
            if (userDTO.getPersonalEmail() != null && userRepository.existsByPersonalEmail(userDTO.getPersonalEmail())) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("validate.duplicateEmailPersonal", null, locale)), HttpStatus.BAD_REQUEST);
            }
            if (userDTO.getTaxCode() != null && userRepository.existsByTaxCode(userDTO.getTaxCode())) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("validate.duplicateTaxCode",null,locale)), HttpStatus.BAD_REQUEST);
            }
            if (userRepository.existsByBankTypeAndBankAccountNumber(userDTO.getBankType(),userDTO.getBankAccountNumber())) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("validate.duplicateBank",null,locale)), HttpStatus.BAD_REQUEST);
            }

            Map<String,Object> result = userService.save(userDTO,userDTOResponse);

            //sendMail
            String content = "<h3>" + messageSource.getMessage("contentEmailCreateUser1", null, locale) + "</h3>\n" +
                    "    <p>" + messageSource.getMessage("contentEmailCreateUser2", null, locale) + "</p>\n" +
                    "    <ul>\n" +
                    "        <li><strong>Email:</strong> " + ((UserDTOResponse)result.get("user")).getEmail() + "</li>\n" +
                    "        <li><strong>Password:</strong> " + result.get("password") + "</li>\n" +
                    "    </ul>\n" +
                    "    <p>" + messageSource.getMessage("contentEmailCreateUser3", null, locale) + "</p>" +
                    "    <p>" + messageSource.getMessage("contentEmailCreateUser4", new Object[]{urlWeb}, locale) + "</p>";
            String subject = messageSource.getMessage("subjectMailCreateUser", null, locale) + " "+ ((UserDTOResponse)result.get("user")).getFullName();
            userService.sendMail(subject, content, new String[]{((UserDTOResponse)result.get("user")).getEmail()}, null, null);
            //
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("createSuccess",null,locale),result.get("user")),HttpStatus.CREATED);

        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody UserDTO userDTO, @PathVariable("id") String id, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to update user : {}"+ userDTO.toString());
            UserDTOResponse result = userService.update(userDTO,id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale),result),HttpStatus.OK);

        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer pageSize,
                                     @RequestParam(required = false) String department,
                                     @RequestParam(required = false,defaultValue = "") String keyword,
                                     @RequestParam(required = false,defaultValue = "") String userCode,
                                     @RequestParam(required = false) Integer status,
                                     @RequestParam(required = false) List<Integer> type,
                                     @RequestParam(required = false) Integer isExport,
                                     @RequestParam(required = false) Date severanveDate
    ) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all user");
            if (StringUtils.isEmpty(department)) {
                department = null;
            }
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if (!StringUtils.isEmpty(userCode)) {
                userCode = DataUtil.makeLikeQuery(userCode);
            }
            if (isExport!= null && isExport == 1){
                InputStreamResource inputStream=  userService.export(department,keyword,status,type,userCode);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
                headers.setContentDispositionFormData("attachment", URLEncoder.encode(Constant.TEMPLATE_USER, StandardCharsets.UTF_8).replaceAll("\\+", "%20"));
                return ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .body(inputStream);
            }
            Map<String,Object> result = userService.findAll(page,pageSize,department,keyword,status,type,userCode,severanveDate);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> get( @PathVariable("id") String id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get user : {}"+ id);
            UserDTOResponse result = userService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> changeStatus( @PathVariable("id") String id, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to change status user : {}"+ id);
            userService.changeStatus(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete( @PathVariable("id") String id, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to delete user : {}"+ id);

            //check table khác sử dụng nếu cần thiết
            userService.delete(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("deleteSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll(@RequestParam(required = false) Integer status
                                    ,@RequestParam(required = false) String department,
                                    @RequestParam(required = false,defaultValue = "") String keyword,
                                    @RequestParam(required = false,defaultValue = "") String userCode) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all user");
            if (StringUtils.isEmpty(department)) {
                department = null;
            }
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if (!StringUtils.isEmpty(userCode)) {
                userCode = DataUtil.makeLikeQuery(userCode);
            }
            List<UserDTOResponse> result = userService.getAll(department,status,keyword,userCode);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/schedule")
    public ResponseEntity<?> getAllUserSchedule() {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all user");
            List<UserDTOResponse> result = userService.getAllSchedule();
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/multiple-id")
    public ResponseEntity<?> getAllMultipleId(@RequestParam(required = false) List<String> id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all user");
            List<UserDTOResponse> result = userService.getAllMultipleId(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
