package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.DepartmentDTO;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.response.DepartmentDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.repository.DepartmentRepository;
import com.blameo.idservice.repository.LevelRepository;
import com.blameo.idservice.repository.PositionRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.DepartmentService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@RestController
@RequestMapping("/api/id/department")
@Tag(name = "Department Api (Danh sách Api phòng ban)")
public class DepartmentController {
    DepartmentService departmentService;

    DepartmentRepository departmentRepository;

    UserRepository userRepository;
    
    PositionRepository positionRepository;

    private final MessageSource messageSource;

    public DepartmentController(DepartmentService departmentService, DepartmentRepository departmentRepository, UserRepository userRepository, PositionRepository positionRepository, MessageSource messageSource,
                                LevelRepository levelRepository) {
        this.departmentService = departmentService;
        this.departmentRepository = departmentRepository;
        this.userRepository = userRepository;
        this.positionRepository = positionRepository;
        this.messageSource = messageSource;
        this.levelRepository = levelRepository;
    }

    Logger logger = Logger.getLogger(DepartmentController.class);
    private final LevelRepository levelRepository;


    @PostMapping("")
    @Operation(summary = "Tạo phòng ban", description = "Trả về thông tin phòng ban được tạo", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tạo thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = DepartmentDTOResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Mã Code phòng ban đã tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> create(@Valid @RequestBody DepartmentDTO departmentDTO, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to save department : {}" + departmentDTO.toString());
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

            if (Boolean.TRUE.equals(departmentRepository.existsByDepartmentCodeAndDepartmentStatusGreaterThanEqual(departmentDTO.getDepartmentCode(), 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("department.code", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            DepartmentDTOResponse result = departmentService.save(departmentDTO, userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.CREATE_SUCCESS, null, locale), result), HttpStatus.CREATED);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @Operation(summary = "Cập nhật phòng ban", description = "Trả về thông tin phòng ban được cập nhật", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Cập nhật thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = DepartmentDTOResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Mã Code phòng ban đã tồn tại hoặc Id phòng ban không tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> update(@Valid @RequestBody DepartmentDTO departmentDTO, @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to update department : {}" + departmentDTO.toString());
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            if (Boolean.FALSE.equals(departmentRepository.existsByDepartmentIdAndDepartmentStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("department", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            DepartmentDTOResponse result = departmentService.update(departmentDTO, id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale), result), HttpStatus.OK);

        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    @Operation(summary = "Lấy danh sách phòng ban", description = "Trả về danh sách phòng ban", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Lấy danh sách thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = DepartmentDTOResponse.class)))}),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> findAll(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize,
                                          @RequestParam(required = false, defaultValue = "") String keyword,
                                          @RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all department");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            Map<String, Object> result = departmentService.findAll(page, pageSize, keyword, status);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    @Operation(summary = "Lấy thông tin 1 phòng ban", description = "Trả về thông tin phòng ban", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Cập nhật thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = DepartmentDTOResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Id phòng ban không tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> get(@PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get department : {}" + id);

            if (Boolean.FALSE.equals(departmentRepository.existsByDepartmentIdAndDepartmentStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("department", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            DepartmentDTOResponse result = departmentService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    @Operation(summary = "Thay đổi trạng thái 1 phòng ban", description = "Trả về thông báo", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Thay đổi trạng thái thành công"),
            @ApiResponse(responseCode = "400", description = "Id phòng ban không tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> changeStatus(@PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get department : {}" + id);

            if (Boolean.FALSE.equals(departmentRepository.existsByDepartmentIdAndDepartmentStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("department", null, locale)+ SPACE  + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            departmentService.changeStatus(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Xóa 1 phòng ban", description = "Trả về thông báo", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Thay đổi trạng thái thành công"),
            @ApiResponse(responseCode = "400", description = "Id phòng ban không tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to delete department : {}" + id);
            if (Boolean.FALSE.equals(departmentRepository.existsByDepartmentIdAndDepartmentStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("department", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            //check table khác sử dụng nếu cần thiết

            if (Boolean.TRUE.equals(levelRepository.existsByDepartment_DepartmentId(id))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("department", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_USE, null, locale)+ messageSource.getMessage("dontDelete", null, locale)), HttpStatus.BAD_REQUEST);
            }

            if (Boolean.TRUE.equals(userRepository.existsByDepartment_DepartmentIdAndEmployeeStatusGreaterThanEqual(id, 0)) || Boolean.TRUE.equals(positionRepository.existsByDepartment_DepartmentId(id))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("department", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_USE, null, locale)+ messageSource.getMessage("dontDelete", null, locale)), HttpStatus.BAD_REQUEST);
            }
            departmentService.delete(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.DELETE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    @Operation(summary = "Lấy danh sách phòng ban (Không phân trang)", description = "Trả về danh sách phòng ban", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Lấy danh sách thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = DepartmentDTOResponse.class)))}),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> getAll(@RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all department");
            List<DepartmentDTOResponse> result = departmentService.getAll(status);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
