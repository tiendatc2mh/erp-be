package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.service.MinIOService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.FileUtil;
import io.minio.GetObjectArgs;
import io.minio.GetObjectResponse;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Locale;
import java.util.Objects;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/8/2023, Tuesday
 **/
@RestController
@RequestMapping("/api/id/upload")
public class UploadFileController {

    Logger logger = Logger.getLogger(UploadFileController.class);
    @Autowired
    MinioClient minioClient;

    @Autowired
    MinIOService minIOService;

    @Value("${minio.bucket.name}")
    String minioBucket;

    @Value("${minio.url}")
    String minioUrl;

    @Autowired
    MessageSource messageSource;

    @PostMapping(value = "",consumes= MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> upload(@RequestPart(value = "file") MultipartFile file, @RequestParam(value = "type",required = true) Integer type) {
        Locale locale = CommonUtil.getLocale();
        try {
            if (file.getSize() > 1024 * 1024 * 5) {
                return new ResponseEntity<>(new ResponseMessage(false, messageSource.getMessage("limitSizeUpload",null,locale), null), HttpStatus.BAD_REQUEST);
            }
            if (Objects.requireNonNull(file.getOriginalFilename()).length() > Constant.MAX_FILE_NAME){
                return new ResponseEntity<>(new ResponseMessage(false, messageSource.getMessage("maxLengthFileName",null,locale), null), HttpStatus.BAD_REQUEST);

            }
            //Check extension file
            switch (type) {
                case Constant.OFFICE_FILE:
                    if (!FileUtil.checkOfficeFile(file)) {
                        return new ResponseEntity<>(new ResponseMessage(false, messageSource.getMessage("validExtensionUpload",null,locale), null), HttpStatus.BAD_REQUEST);
                    }
                    break;
                case Constant.IMAGE_FILE:
                    if (!FileUtil.checkFileImage(file)) {
                        return new ResponseEntity<>(new ResponseMessage(false,  messageSource.getMessage("validExtensionUpload",null,locale), null), HttpStatus.BAD_REQUEST);
                    }
                    break;
                case Constant.IMAGE_PDF_FILE:
                    if (!FileUtil.checkFileImagePDF(file)) {
                        return new ResponseEntity<>(new ResponseMessage(false,  messageSource.getMessage("validExtensionUpload",null,locale), null), HttpStatus.BAD_REQUEST);
                    }
                case Constant.IMAGE_PDF_OFFICE_FILE:
                    if (!FileUtil.checkFileImageOfficePDF(file)){
                        return new ResponseEntity<>(new ResponseMessage(false,  messageSource.getMessage("validExtensionUpload",null,locale), null), HttpStatus.BAD_REQUEST);
                    }
//                default:
//                    return new ResponseEntity<>(new ResponseMessage(false, "Định dạng file không hỗ trợ", null), HttpStatus.BAD_REQUEST);

            }

            ObjectWriteResponse response = minIOService.uploadFile(file, minioBucket, file.getOriginalFilename());
            System.out.println("====upload success!");
            String url = minioUrl + "/" + minioBucket + "/" + file.getOriginalFilename() + "?versionId=" + response.versionId();
//            GetObjectResponse objectResponse = minioClient.getObject(GetObjectArgs.builder()
//                    .bucket(response.bucket())
//                    .object(response.object())
//                    .versionId(response.versionId())
//                    .build());
//            System.out.println("filePath: " + url);
//            System.out.println("fileName: " + objectResponse.object());
//            System.out.println("fileVersionId: " + response.versionId());
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("uploadSuccess", null, locale), url), HttpStatus.OK);
        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError", null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @PostMapping("/office-file")
//    public ResponseEntity<?> uploadOfficeFile(MultipartFile file) {
//        Locale locale = CommonUtil.getLocale();
//        try {
//            if (file.getSize() > 1024 * 1024 * 5) {
//                return new ResponseEntity<>(new ResponseMessage(false, "Kích thước file upload phải dưới 5MB", null), HttpStatus.BAD_REQUEST);
//            }
//            //Check extension file
//            if (!FileUtil.checkOfficeFile(file)) {
//                return new ResponseEntity<>(new ResponseMessage(false, "Định dạng file không hỗ trợ", null), HttpStatus.BAD_REQUEST);
//            }
//
//            ObjectWriteResponse response = minIOService.uploadFile(file, minioBucket, file.getOriginalFilename());
//            System.out.println("====upload success!");
//            String url = minioUrl + "/" + minioBucket + "/" + file.getOriginalFilename() + "?versionId=" + response.versionId();
////            GetObjectResponse objectResponse = minioClient.getObject(GetObjectArgs.builder()
////                    .bucket(response.bucket())
////                    .object(response.object())
////                    .versionId(response.versionId())
////                    .build());
////            System.out.println("filePath: " + url);
////            System.out.println("fileName: " + objectResponse.object());
////            System.out.println("fileVersionId: " + response.versionId());
//            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("uploadSuccess", null, locale), url), HttpStatus.OK);
//        } catch (RuntimeException runtimeException) {
//            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
//        } catch (Exception e) {
//            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError", null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
}
