package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.AssetDTO;
import com.blameo.idservice.dto.request.AssetTypeDTO;
import com.blameo.idservice.dto.request.AssetUpdateDTO;
import com.blameo.idservice.dto.response.AssetDTOResponse;
import com.blameo.idservice.dto.response.AssetLogDTOResponse;
import com.blameo.idservice.dto.response.AssetTypeDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.repository.AssetRepository;
import com.blameo.idservice.repository.AssetTypeRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.security.services.UserDetailsImpl;
import com.blameo.idservice.service.AssetService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static com.blameo.idservice.constant.Constant.SPACE;

@RestController
@RequestMapping("/api/id/asset")
public class AssetController {

    @Autowired
    AssetRepository assetRepository;

    @Autowired
    AssetTypeRepository assetTypeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AssetService assetService;

    @Autowired
    private MessageSource messageSource;

    Logger logger = Logger.getLogger(AssetController.class);

    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody AssetDTO assetDTO, HttpServletRequest request) {

        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to save asset type : {}"+ assetDTO.toString());
            if(assetDTO.getAssetSerial() != null && assetDTO.getAssetSerial() != "" && assetRepository.existsAssetByAssetSerialAndAssetStatusGreaterThanEqual(assetDTO.getAssetSerial(), 0)){
                return new ResponseEntity<>(ResponseMessage.error("Serial "+messageSource.getMessage("isExist",null,locale)), HttpStatus.BAD_REQUEST);
            }

            if(!assetTypeRepository.existsAssetTypeByAssetTypeIdAndAndAssetTypeStatusGreaterThanEqual(assetDTO.getAssetType(), 1)){
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("assetType",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }

            if (assetDTO.getUserId() != null && assetDTO.getUserId() != "" && !userRepository.existsById(assetDTO.getUserId())) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale)), HttpStatus.BAD_REQUEST);
            }
            AssetDTOResponse result = assetService.save(assetDTO, currentUser);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("createSuccess",null,locale),result),HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody AssetUpdateDTO assetDTO, @PathVariable("id") String id, HttpServletRequest request) {

        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to updaate asset type : {}"+ assetDTO.toString());
            if(!assetTypeRepository.existsAssetTypeByAssetTypeIdAndAndAssetTypeStatusGreaterThanEqual(assetDTO.getAssetType(), 1)){
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("assetType",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            AssetDTOResponse result = assetService.update(assetDTO, id, currentUser);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale),result),HttpStatus.CREATED);
        } catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/assign/{id}")
    public ResponseEntity<?> assign(@RequestBody AssetDTO assetDTO, @PathVariable("id") String id,  HttpServletRequest request) {

        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);

        try {
            if(assetDTO.getReason() != null && assetDTO.getUserId() != null){
                logger.debug("REST request to assign asset type : {}"+ assetDTO.toString());
                if (!userRepository.existsById(assetDTO.getUserId())) {
                    return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale)), HttpStatus.BAD_REQUEST);
                }
                AssetDTOResponse result = assetService.assignAsset(id, assetDTO,currentUser);
                return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("assignAssetSuccess",null,locale), result),HttpStatus.CREATED);
            }else{
                Map<String, Object> error = new HashMap<>();
                if(assetDTO.getReason() == null){
                    error.put("reason", messageSource.getMessage("validate.notNull",null,locale));
                }
                if(assetDTO.getUserId() == null){
                    error.put("userId", messageSource.getMessage("validate.notNull",null,locale));
                }
                return new ResponseEntity<>(ResponseMessage.error(null, error), HttpStatus.BAD_REQUEST);
            }
        } catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/assign/user/{id}")
    public ResponseEntity<?> assignByUserId(@RequestBody AssetDTO assetDTO, @PathVariable("id") String id,  HttpServletRequest request) {
        // id là id của user cần gán
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to assign asset to user: {}"+ assetDTO.toString());
            if (!userRepository.existsById(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("user",null,locale)+ SPACE + messageSource.getMessage("notExist", null, locale)), HttpStatus.BAD_REQUEST);
            }
            assetService.assignAssetByUserId(id, assetDTO,currentUser);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("assignAssetSuccess",null,locale)),HttpStatus.CREATED);
        } catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/retrieve/{id}")
    public ResponseEntity<?> retrieve(@RequestBody AssetDTO assetDTO, @PathVariable("id") String id,  HttpServletRequest request) {

        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to retrieve asset type : {}"+ assetDTO.toString());
            if(assetDTO.getReason() != null){
                AssetDTOResponse result = assetService.retrieveAsset(id, assetDTO,currentUser);
                return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("retrieveAssetSuccess",null,locale), result),HttpStatus.CREATED);
            }else{
                Map<String, Object> error = new HashMap<>();
                error.put("reason", messageSource.getMessage("validate.notNull",null,locale));
                return new ResponseEntity<>(ResponseMessage.error(null, error), HttpStatus.BAD_REQUEST);
            }
        } catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // api thanh lý tài sản
    @PutMapping("/liquidation/{id}")
    public ResponseEntity<?> delete(@RequestBody AssetDTO assetDTO, @PathVariable("id") String id,  HttpServletRequest request) {

        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to delete asset type : {}"+ id);
            if(assetDTO.getReason() != null){
                assetService.delete(id, assetDTO, currentUser);
                return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("deleteSuccess", null, locale)), HttpStatus.NO_CONTENT);
            }else{
                Map<String, Object> error = new HashMap<>();
                error.put("reason", messageSource.getMessage("validate.notNull",null,locale));
                return new ResponseEntity<>(ResponseMessage.error(null, error), HttpStatus.BAD_REQUEST);
            }
        } catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable("id") String id){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try{
            logger.debug("REST request to get asset : {}"+ id);
            AssetDTOResponse result = assetService.get(id);
            return new ResponseEntity<>(ResponseMessage.ok(messageSource.getMessage("getDataSuccess", null, locale), result), HttpStatus.OK);
        }catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer pageSize,
                                     @RequestParam(required = false,defaultValue = "") String keyword,
                                     @RequestParam(required = false) List<String> assetType,
                                     @RequestParam(required = false) String userId,
                                     @RequestParam(required = false) Integer status,
                                     @RequestParam(required = false) Integer isExport,
                                     @RequestParam(required = false) String requestId){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try{
            logger.debug("REST request to get all asset");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if(isExport != null && isExport == 1){
                InputStreamResource inputStream=  assetService.export(keyword,status, assetType, userId, requestId);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
                headers.setContentDispositionFormData("attachment", URLEncoder.encode(Constant.TEMPLATE_ASSET, StandardCharsets.UTF_8).replaceAll("\\+", "%20"));
                return ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .body(inputStream);
            }
            Map<String,Object> result = assetService.findAll(page,pageSize,keyword,status, assetType, userId, requestId);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll(@RequestParam(required = false,defaultValue = "") String keyword,
                                     @RequestParam(required = false) List<String> assetType,
                                     @RequestParam(required = false) String userId,
                                     @RequestParam(required = false) Integer status,
                                     @RequestParam(required = false) String requestId){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try{
            logger.debug("REST request to get all asset");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            List<AssetDTOResponse> result = assetService.getAll(keyword,status, assetType, userId, requestId);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/self")
    public ResponseEntity<?> getSelf(@RequestParam(required = false,defaultValue = "") String keyword,
                                    @RequestParam(required = false) List<String> assetType,
                                    @RequestParam(required = false) Integer status,
                                    @RequestParam(required = false) String requestId){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try{
            logger.debug("REST request to get all asset");
            String userId = SecurityContextHolder.getContext()
                    .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal()).getUserId() : null;
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            List<AssetDTOResponse> result = assetService.getAll(keyword,status, assetType, userId, requestId);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getAllAssetByUserId(@PathVariable("id") String userId){
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try{
            logger.debug("REST request to get asset");
            List<AssetDTOResponse> result = assetService.getAllByUserId(userId);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
