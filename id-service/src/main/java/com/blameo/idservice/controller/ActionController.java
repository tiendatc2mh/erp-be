package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.ActionDTO;
import com.blameo.idservice.dto.response.ActionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.repository.ActionRepository;
import com.blameo.idservice.repository.PermissionRepository;
import com.blameo.idservice.service.ActionService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import com.blameo.idservice.utils.ValueUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/3/2023, Thursday
 **/
@RestController
@RequestMapping("/api/id/action")
public class ActionController {

    ActionRepository actionRepository;

    ActionService actionService;
    PermissionRepository permissionRepository;

    private final MessageSource messageSource;

    public ActionController(ActionRepository actionRepository, ActionService actionService, PermissionRepository permissionRepository, MessageSource messageSource) {
        this.actionRepository = actionRepository;
        this.actionService = actionService;
        this.permissionRepository = permissionRepository;
        this.messageSource = messageSource;
    }

    Logger logger = Logger.getLogger(ActionController.class);


    @PostMapping("")
    public ResponseEntity<Object> create(@Valid @RequestBody ActionDTO actionDTO, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to save department : {}" + actionDTO.toString());

            if (Boolean.TRUE.equals(actionRepository.existsByActionCodeAndActionStatusGreaterThanEqual(actionDTO.getActionCode(), 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("action.code", null, locale) +SPACE+ messageSource.getMessage(Constant.IS_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            ActionDTOResponse result = actionService.save(actionDTO,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.CREATE_SUCCESS, null, locale), result), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@Valid @RequestBody ActionDTO actionDTO, @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to update department : {}" + actionDTO.toString());

            if (Boolean.FALSE.equals(actionRepository.existsByActionIdAndActionStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("action", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            ActionDTOResponse result = actionService.update(actionDTO, id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale), result), HttpStatus.OK);

        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<Object> findAll(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize,
                                          @RequestParam(required = false, defaultValue = "") String keyword,
                                          @RequestParam(required = false) Date fromDate,
                                          @RequestParam(required = false) Date toDate,
                                          @RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all department");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if (toDate != null){
                toDate = ValueUtil.setEndDay(toDate);
            }
            Map<String, Object> result = actionService.findAll(page, pageSize, keyword, status, fromDate, toDate);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get department : {}" + id);

            if (Boolean.FALSE.equals(actionRepository.existsByActionIdAndActionStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("action", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            ActionDTOResponse result = actionService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Object> changeStatus(@PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to get department : {}" + id);

            if (Boolean.FALSE.equals(actionRepository.existsByActionIdAndActionStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("action", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            actionService.changeStatus(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to delete department : {}" + id);

            if (Boolean.FALSE.equals(actionRepository.existsByActionIdAndActionStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("action", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }

            //check table khác sử dụng nếu cần thiết
            if (Boolean.TRUE.equals(permissionRepository.existsByAction_ActionIdAndPermissionStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("action", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_USE, null, locale) + messageSource.getMessage("dontDelete", null, locale)), HttpStatus.BAD_REQUEST);
            }
            actionService.delete(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.DELETE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<Object> getAll(
            @RequestParam(required = false, defaultValue = "") String keyword,
            @RequestParam(required = false) Integer status
    ) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            logger.debug("REST request to get all department");
            List<ActionDTOResponse> result = actionService.getAll(keyword,status);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
