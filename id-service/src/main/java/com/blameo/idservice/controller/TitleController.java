package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.request.TitleDTO;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.response.TitleDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.repository.TitleRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.TitleService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@RestController
@RequestMapping("/api/id/title")
@Tag(name = "Title Api (Danh sách Api đơn vị)")
public class TitleController {
    TitleService titleService;

    TitleRepository titleRepository;

    UserRepository userRepository;
    private final MessageSource messageSource;

    public TitleController(TitleService titleService, TitleRepository titleRepository, UserRepository userRepository, MessageSource messageSource) {
        this.titleService = titleService;
        this.titleRepository = titleRepository;
        this.userRepository = userRepository;
        this.messageSource = messageSource;
    }

    Logger logger = Logger.getLogger(TitleController.class);


    @PostMapping("")
    @Operation(summary = "Tạo đơn vị", description = "Trả về thông tin đơn vị được tạo", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tạo thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = TitleDTOResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Mã Code đơn vị đã tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> create(@Valid @RequestBody TitleDTO titleDTO, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to save title : {}" + titleDTO.toString());
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

            if (Boolean.TRUE.equals(titleRepository.existsByTitleCodeAndTitleStatusGreaterThanEqual(titleDTO.getTitleCode(), 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("title.code", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            TitleDTOResponse result = titleService.save(titleDTO, userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.CREATE_SUCCESS, null, locale), result), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @Operation(summary = "Cập nhật đơn vị", description = "Trả về thông tin đơn vị được cập nhật", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Cập nhật thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = TitleDTOResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Mã Code đơn vị đã tồn tại hoặc Id đơn vị không tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> update(@Valid @RequestBody TitleDTO titleDTO, @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to update title : {}" + titleDTO.toString());

            if (Boolean.FALSE.equals(titleRepository.existsByTitleIdAndTitleStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("title", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            TitleDTOResponse result = titleService.update(titleDTO, id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale), result), HttpStatus.OK);

        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    @Operation(summary = "Lấy danh sách đơn vị", description = "Trả về danh sách đơn vị", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Lấy danh sách thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = TitleDTOResponse.class)))}),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> findAll(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize,
                                          @RequestParam(required = false, defaultValue = "") String keyword,
                                          @RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all title");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            Map<String, Object> result = titleService.findAll(page, pageSize, keyword, status);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    @Operation(summary = "Lấy thông tin 1 đơn vị", description = "Trả về thông tin đơn vị", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Cập nhật thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = TitleDTOResponse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Id đơn vị không tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> get(@PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get title : {}" + id);

            if (Boolean.FALSE.equals(titleRepository.existsByTitleIdAndTitleStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("title", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            TitleDTOResponse result = titleService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PatchMapping("/{id}")
    @Operation(summary = "Thay đổi trạng thái 1 đơn vị", description = "Trả về thông báo", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Thay đổi trạng thái thành công"),
            @ApiResponse(responseCode = "400", description = "Id đơn vị không tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> changeStatus(@PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get title : {}" + id);

            if (Boolean.FALSE.equals(titleRepository.existsByTitleIdAndTitleStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("title", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            titleService.changeStatus(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Xóa 1 đơn vị", description = "Trả về thông báo", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Thay đổi trạng thái thành công"),
            @ApiResponse(responseCode = "400", description = "Id đơn vị không tồn tại"),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to delete title : {}" + id);

            if (Boolean.FALSE.equals(titleRepository.existsByTitleIdAndTitleStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("title", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            //check table khác sử dụng nếu cần thiết
            if (Boolean.TRUE.equals(userRepository.existsByTitle_TitleIdAndEmployeeStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("title", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_USE, null, locale)+ messageSource.getMessage("dontDelete", null, locale)), HttpStatus.BAD_REQUEST);
            }
            titleService.delete(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.DELETE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    @Operation(summary = "Lấy danh sách đơn vị (Không phân trang)", description = "Trả về danh sách đơn vị", parameters = {
            @Parameter(
                    in = ParameterIn.HEADER,
                    name = "Accept-Language",
                    schema = @Schema(allowableValues = { "vi", "en"})
            )
    })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Lấy danh sách thành công",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = TitleDTOResponse.class)))}),
            @ApiResponse(responseCode = "500", description = "Có gì đó không ổn")
    })
    public ResponseEntity<Object> getAll(
            @RequestParam(required = false, defaultValue = "") String keyword,
            @RequestParam(required = false) Integer status
    ) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all title");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            List<TitleDTOResponse> result = titleService.getAll(keyword,status);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/multiple")
    public ResponseEntity<Object> getAllListTitleId(@RequestParam List<String> id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all title");
            List<TitleDTOResponse> result = titleService.getAllListTitleId(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
