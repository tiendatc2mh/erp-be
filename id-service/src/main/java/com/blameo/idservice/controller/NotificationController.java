package com.blameo.idservice.controller;

import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.NotificationDTO;
import com.blameo.idservice.dto.response.NotificationDTOResponse;
import com.blameo.idservice.model.User;
import com.blameo.idservice.repository.NotificationRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.security.services.UserDetailsImpl;
import com.blameo.idservice.service.NotificationService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/23/2023, Wednesday
 **/
@RestController
@RequestMapping("/api/id/notification")
public class NotificationController {

    @Autowired
    NotificationService notificationService;

    @Autowired
    NotificationRepository notificationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MessageSource messageSource;

    Logger logger  = Logger.getLogger(NotificationController.class);


    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody NotificationDTO notificationDTO) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to save notification : {}"+ notificationDTO.toString());
            notificationService.createNotification(notificationDTO);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("createSuccess",null,locale),null),HttpStatus.CREATED);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("")
    public ResponseEntity<?> findAllNotification(@RequestParam (defaultValue = "1") Integer page,
                                                 @RequestParam (defaultValue = "10") Integer pageSize,
                                                 @RequestParam(required = false,defaultValue = "") String keyword,
                                                 @RequestParam(required = false) Integer isRead,
                                                 @RequestParam(required = false) Integer type
    ) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            String userId = SecurityContextHolder.getContext()
                    .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal()).getUserId() : null;
            if (userId == null) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
            }
            Map<String, Object> result = notificationService.findByUserId(page, pageSize, userId, keyword, isRead,type);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("getDataSuccess", null, locale), result), HttpStatus.OK);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/all")
    public ResponseEntity<?> getAllNotification(
                                                 @RequestParam(required = false,defaultValue = "") String keyword,
                                                 @RequestParam(required = false) Integer isRead,
                                                 @RequestParam(required = false) Integer type
    ) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            String userId = SecurityContextHolder.getContext()
                    .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal()).getUserId() : null;
            if (userId == null) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
            }
            List<NotificationDTOResponse> result = notificationService.getAllByUserId(userId, keyword, isRead,type);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("getDataSuccess", null, locale), result), HttpStatus.OK);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/index")
    public ResponseEntity<?> getNotification2Record1Type(
    ) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {

            String userId = SecurityContextHolder.getContext()
                    .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal()).getUserId() : null;
            if (userId == null) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
            }
            Map<String,Object> result = notificationService.getNotification2Record1Type(userId);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage("getDataSuccess", null, locale), result), HttpStatus.OK);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PatchMapping("/{id}")
    public ResponseEntity<?> updateStatusRead(@PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to change status notification : {}"+ id);

            if (!notificationRepository.existsById(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("notification",null,locale)+SPACE+messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            notificationService.changeStatus(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/readAll")
    public ResponseEntity<?> readAll(@RequestParam(required = false) Integer type) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to read all : {}"+ type);
            String userId = SecurityContextHolder.getContext()
                    .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal()).getUserId() : null;
            if (userId == null) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
            }
            notificationService.readAll(userId,type);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
