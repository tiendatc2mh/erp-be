package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.PositionDTO;
import com.blameo.idservice.dto.response.PositionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.Position;
import com.blameo.idservice.repository.PositionRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.PositionService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

@RestController
@RequestMapping("/api/id/position")
public class PositionController {

    private final Logger logger = Logger.getLogger(PositionController.class);


    @Autowired
    PositionRepository positionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PositionService positionService;

    @Autowired
    MessageSource messageSource;


    @PostMapping("")
    public ResponseEntity<Object> create(@Valid @RequestBody PositionDTO positionDTO, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to save position : {}" + positionDTO.toString());
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            if (positionRepository.existsByPositionCodeAndPositionStatusGreaterThanEqual(positionDTO.getPositionCode(), 0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("position.code", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            PositionDTOResponse result = positionService.save(positionDTO, userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.CREATE_SUCCESS, null, locale), result), HttpStatus.CREATED);
        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to delete position : {}" + id);
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            if (!positionRepository.existsByPositionIdAndPositionStatusGreaterThanEqual(id, 0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("position", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }

            if (userRepository.existsByPosition_PositionIdAndEmployeeStatusGreaterThanEqual(id, 0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("position", null, locale)+ SPACE  + messageSource.getMessage(Constant.IS_USE, null, locale) + messageSource.getMessage("dontDelete", null, locale)), HttpStatus.BAD_REQUEST);
            }

            //check table khác sử dụng nếu cần thiết
            positionService.delete(id, userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.DELETE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Object> changeStatus(@PathVariable("id") String id, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get position : {}" + id);
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            if (!positionRepository.existsByPositionIdAndPositionStatusGreaterThanEqual(id, 0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("position", null, locale)+ SPACE  + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            positionService.changeStatus(id, userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@Valid @RequestBody PositionDTO positionDTO, @PathVariable("id") String id, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to update position : {}" + positionDTO.toString());
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
            if (!positionRepository.existsByPositionIdAndPositionStatusGreaterThanEqual(id, 0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("position", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            PositionDTOResponse result = positionService.update(positionDTO, id, userDTOResponse );
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale), result), HttpStatus.OK);

        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable("id") String id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get position : {}" + id);

            if (!positionRepository.existsByPositionIdAndPositionStatusGreaterThanEqual(id, 0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("position", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            PositionDTOResponse result = positionService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<Object> findAll(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize,
                                          @RequestParam(required = false, defaultValue = "") String keyword,
                                          @RequestParam(required = false) Integer status) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all position");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            Map<String, Object> result = positionService.findAll(page, pageSize, keyword, status);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<Object> getAll(@RequestParam(required = false, defaultValue = "") String keyword,
                                         @RequestParam(required = false) Integer status) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all position");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            List<PositionDTOResponse> result = positionService.getAll(keyword,status);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/multiple-id")
    public ResponseEntity<?> getAllMultipleId(@RequestParam(required = false) List<String> id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all user");
            List<PositionDTOResponse> result = positionService.getAllMultipleId(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
