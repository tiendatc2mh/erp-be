package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.LevelDTO;
import com.blameo.idservice.dto.response.LevelDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.model.Department;
import com.blameo.idservice.repository.DepartmentRepository;
import com.blameo.idservice.repository.LevelRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.LevelService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Đô Trần Văn
 * @mailto : dox1dh@gmail.com
 * @created : 8/2/2023, Wednesday
 **/
@RestController
@RequestMapping("/api/id/level")
public class LevelController {
    LevelRepository levelRepository;

    DepartmentRepository departmentRepository;
    
    LevelService levelService;

    UserRepository userRepository;
    private final MessageSource messageSource;

    public LevelController(LevelRepository levelRepository, DepartmentRepository departmentRepository, LevelService levelService,UserRepository userRepository, MessageSource messageSource) {
        this.levelRepository = levelRepository;
        this.departmentRepository = departmentRepository;
        this.levelService = levelService;
        this.userRepository = userRepository;
        this.messageSource = messageSource;
    }

    Logger logger = Logger.getLogger(LevelController.class);

    //Get ngôn ngữ


    @PostMapping("")
    public ResponseEntity<Object> create(@Valid @RequestBody LevelDTO levelDTO) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to save department : {}" + levelDTO.toString());
            Optional<Department> data = departmentRepository.findByDepartmentId(levelDTO.getDepartmentId());
            if (data.isEmpty() || data.get().getDepartmentStatus() == -1 || Boolean.TRUE.equals(levelRepository.existsByLevelCodeAndLevelStatusGreaterThanEqual(levelDTO.getLevelCode(), 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("level.code", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            LevelDTOResponse result = levelService.save(levelDTO);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.CREATE_SUCCESS, null, locale), result), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@Valid @RequestBody LevelDTO levelDTO, @PathVariable("id") String id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to update level : {}" + levelDTO.toString());

            if (Boolean.FALSE.equals(levelRepository.existsByLevelIdAndLevelStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("level", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            LevelDTOResponse result = levelService.update(levelDTO, id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale), result), HttpStatus.OK);

        } catch (RuntimeException runtimeException) {
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<Object> findAll(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize,
                                          @RequestParam(required = false, defaultValue = "") String keyword,
                                          @RequestParam(required = false) Integer status) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all department");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            Map<String, Object> result = levelService.findAll(page, pageSize, keyword, status);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable("id") String id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get department : {}" + id);

            if (Boolean.FALSE.equals(levelRepository.existsByLevelIdAndLevelStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("level", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            LevelDTOResponse result = levelService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Object> changeStatus(@PathVariable("id") String id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get department : {}" + id);

            if (Boolean.FALSE.equals(levelRepository.existsByLevelIdAndLevelStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("level", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }
            levelService.changeStatus(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.UPDATE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to delete department : {}" + id);

            if (Boolean.FALSE.equals(levelRepository.existsByLevelIdAndLevelStatusGreaterThanEqual(id, 0))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("level", null, locale)+ SPACE + messageSource.getMessage(Constant.NOT_EXIST, null, locale)), HttpStatus.BAD_REQUEST);
            }

            //check table khác sử dụng nếu cần thiết
            if (userRepository.existsByLevel_LevelIdAndEmployeeStatusGreaterThanEqual(id, 0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("level", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_USE, null, locale)+ messageSource.getMessage("dontDelete", null, locale)), HttpStatus.BAD_REQUEST);
            }

            levelService.delete(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.DELETE_SUCCESS, null, locale)), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<Object> getAll(@RequestParam(required = false, defaultValue = "") String keyword,
                                         @RequestParam(required = false) Integer status) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all level");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            List<LevelDTOResponse> result = levelService.getAll(keyword,status);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/multiple-id")
    public ResponseEntity<?> getAllMultipleId(@RequestParam(required = false) List<String> id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all user");
            List<LevelDTOResponse> result = levelService.getAllMultipleId(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
