package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.RoleDTO;
import com.blameo.idservice.dto.response.RoleDTOResponse;
import com.blameo.idservice.dto.response.TitleDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.repository.RoleRepository;
import com.blameo.idservice.repository.UserRepository;
import com.blameo.idservice.service.RoleService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import com.blameo.idservice.utils.ValueUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import okhttp3.Headers;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@RestController
@RequestMapping("/api/id/role")
public class RoleController {
    @Autowired
    RoleService roleService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private MessageSource messageSource;

    Logger logger = Logger.getLogger(RoleController.class);
    @Autowired
    private UserRepository userRepository;

    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody RoleDTO roleDTO, HttpServletRequest request ) {
    //Get ngôn ngữ
            Locale locale = CommonUtil.getLocale();
            UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to save role : {}"+ roleDTO.toString());

            if (roleRepository.existsByRoleCodeAndRoleStatusGreaterThanEqual(roleDTO.getRoleCode(),0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("role.code",null,locale)+ SPACE +messageSource.getMessage("isExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            RoleDTOResponse result = roleService.save(roleDTO,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("createSuccess",null,locale),result),HttpStatus.CREATED);

        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody RoleDTO roleDTO, @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to update role : {}"+ roleDTO.toString());

            if (!roleRepository.existsByRoleIdAndRoleStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("role",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            RoleDTOResponse result = roleService.update(roleDTO,id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale),result),HttpStatus.OK);

        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer pageSize,
                                     @RequestParam(required = false,defaultValue = "") String keyword,
                                     @RequestParam(required = false) Date createdDate,
                                     @RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all role");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            if (createdDate != null){
                createdDate = ValueUtil.setEndDay(createdDate);
            }
            Map<String,Object> result = roleService.findAll(page,pageSize,keyword,status,createdDate);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }

        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> get( @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get role : {}"+ id);

            if (!roleRepository.existsByRoleId(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("role",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            RoleDTOResponse result = roleService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> changeStatus( @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to change status role : {}"+ id);

            if (!roleRepository.existsByRoleIdAndRoleStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("role",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            roleService.changeStatus(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete( @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to delete role : {}"+ id);

            if (!roleRepository.existsByRoleIdAndRoleStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("role",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }

            //check table khác sử dụng nếu cần thiết
            if (userRepository.existsByRole(id)){
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("role",null,locale)+ SPACE + messageSource.getMessage(Constant.IS_USE, null, locale)+ messageSource.getMessage("dontDelete", null, locale)), HttpStatus.BAD_REQUEST);
            }
            roleService.delete(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("deleteSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll(@RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all role");
            List<RoleDTOResponse> result = roleService.getAll(status);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/multiple")
    public ResponseEntity<Object> getAllListRoleId(@RequestParam List<String> id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all title");
            List<RoleDTOResponse> result = roleService.getAllListRoleId(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<?> getDetailRole( @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get role : {}"+ id);

            if (!roleRepository.existsByRoleId(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("role",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            RoleDTOResponse result = roleService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
