package com.blameo.idservice.controller;


import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.AssetTypeDTO;
import com.blameo.idservice.dto.response.AssetTypeDTOResponse;
import com.blameo.idservice.dto.response.LevelDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.repository.AssetRepository;
import com.blameo.idservice.repository.AssetTypeRepository;
import com.blameo.idservice.service.AssetTypeService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

@RestController
@RequestMapping("/api/id/asset-type")
public class AssetTypeController {

    @Autowired
    AssetTypeService assetTypeService;

    @Autowired
    AssetTypeRepository assetTypeRepository;

    @Autowired
    AssetRepository assetRepository;

    @Autowired
    private MessageSource messageSource;
    Logger logger = Logger.getLogger(AssetTypeController.class);

    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody AssetTypeDTO assetTypeDTO, HttpServletRequest request) {

        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to save asset type : {}"+ assetTypeDTO.toString());

            if (assetTypeRepository.existsAssetTypeByAssetTypeCodeAndAndAssetTypeStatusGreaterThanEqual(assetTypeDTO.getAssetTypeCode(),0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("assetType.code",null,locale)+ SPACE + messageSource.getMessage("isExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            AssetTypeDTOResponse result = assetTypeService.save(assetTypeDTO, currentUser);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("createSuccess",null,locale),result),HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody AssetTypeDTO assetTypeDTO, @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to update asset type : {}"+ assetTypeDTO.toString());
            if(!assetTypeRepository.existsAssetTypeByAssetTypeIdAndAndAssetTypeStatusGreaterThanEqual(id, 0)){
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("assetType",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            AssetTypeDTOResponse result = assetTypeService.update(assetTypeDTO, id, currentUser);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale),result),HttpStatus.OK);

        } catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer pageSize,
                                     @RequestParam(required = false,defaultValue = "") String keyword,
                                     @RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all asset type");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            Map<String,Object> result = assetTypeService.findAll(page,pageSize,keyword,status);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get( @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get asset type : {}"+ id);

            if(!assetTypeRepository.existsAssetTypeByAssetTypeIdAndAndAssetTypeStatusGreaterThanEqual(id, 0)){
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("assetType",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            AssetTypeDTOResponse result = assetTypeService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> changeStatus( @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to change status asset type : {}"+ id);

            if(!assetTypeRepository.existsAssetTypeByAssetTypeIdAndAndAssetTypeStatusGreaterThanEqual(id, 0)){
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("assetType",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            assetTypeService.changeStatus(id, currentUser);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete( @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse currentUser = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to delete assert type : {}"+ id);

            if(!assetTypeRepository.existsAssetTypeByAssetTypeIdAndAndAssetTypeStatusGreaterThanEqual(id, 0)){
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("assetType",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }

            //check table khác sử dụng nếu cần thiết
            if(assetRepository.existsAssetByAssetType_AssetTypeIdAndAssetStatusGreaterThanEqual(id, 0)){
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("usedError",null,locale)), HttpStatus.BAD_REQUEST);
            }
            assetTypeService.delete(id, currentUser);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("deleteSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll(@RequestParam(required = false, defaultValue = "1") Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all assert type");
            List<AssetTypeDTOResponse> result = assetTypeService.getAll(status);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/multiple-id")
    public ResponseEntity<?> getAllMultipleId(@RequestParam(required = false) List<String> id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all user");
            List<AssetTypeDTOResponse> result = assetTypeService.getAllMultipleId(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
