package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.NoteBookDTO;
import com.blameo.idservice.dto.request.TypeDocumentDTO;
import com.blameo.idservice.dto.response.NoteBookDTOResponse;
import com.blameo.idservice.dto.response.TypeDocumentDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.repository.AttachmentRepository;
import com.blameo.idservice.repository.NoteBookRepository;
import com.blameo.idservice.repository.PersonalFileRepository;
import com.blameo.idservice.repository.TypeDocumentRepository;
import com.blameo.idservice.service.NoteBookService;
import com.blameo.idservice.service.TypeDocumentService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@RestController
@RequestMapping("/api/id/note-book")
public class NoteBookController {
    @Autowired
    NoteBookService noteBookService;

    @Autowired
    NoteBookRepository noteBookRepository;

    @Autowired
    private MessageSource messageSource;

    Logger logger = Logger.getLogger(NoteBookController.class);

    @Autowired
    private AttachmentRepository attachmentRepository;

    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody NoteBookDTO noteBookDTO) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to save note book : {}"+ noteBookDTO.toString());
            NoteBookDTOResponse result = noteBookService.save(noteBookDTO);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("createSuccess",null,locale),result),HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody NoteBookDTO noteBookDTO, @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to update note book : {}"+ noteBookDTO.toString());

            if (!noteBookRepository.existsByNoteBookId(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("noteBook",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            NoteBookDTOResponse result = noteBookService.update(noteBookDTO,id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale),result),HttpStatus.OK);

        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
//
    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer pageSize,
                                     @RequestParam(required = false,defaultValue = "") String noteBookName,
                                     @RequestParam(required = false,defaultValue = "") String noteBookContent,
                                     @RequestParam(required = false) Integer status,
                                     @RequestParam(required = false) Integer type) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all note book");
            if (!StringUtils.isEmpty(noteBookName)) {
                noteBookName = DataUtil.makeLikeQuery(noteBookName);
            }
            if (!StringUtils.isEmpty(noteBookContent)) {
                noteBookContent = DataUtil.makeLikeQuery(noteBookContent);
            }
            Map<String,Object> result = noteBookService.findAll(page,pageSize,noteBookName,noteBookContent,status,type);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> get( @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get note book : {}"+ id);

            if (!noteBookRepository.existsByNoteBookId(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("noteBook",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            NoteBookDTOResponse result = noteBookService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
//
    @PatchMapping("/{id}")
    public ResponseEntity<?> changeStatus( @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to change status note book : {}"+ id);

            if (!noteBookRepository.existsByNoteBookId(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("noteBook",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            noteBookService.changeStatus(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete( @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to delete note book : {}" + id);
            if (!noteBookRepository.existsByNoteBookId(id)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("noteBook", null, locale) + SPACE + messageSource.getMessage("notExist", null, locale)), HttpStatus.BAD_REQUEST);
            }
            //check table khác sử dụng nếu cần thiết

            noteBookService.delete(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("deleteSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
//
    @GetMapping("/all")
    public ResponseEntity<?> getAll( @RequestParam(required = false,defaultValue = "") String noteBookName,
                                     @RequestParam(required = false,defaultValue = "") String noteBookContent,
                                     @RequestParam(required = false) Integer status,
                                     @RequestParam(required = false) Integer type) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all note book");
            if (!StringUtils.isEmpty(noteBookName)) {
                noteBookName = DataUtil.makeLikeQuery(noteBookName);
            }
            if (!StringUtils.isEmpty(noteBookContent)) {
                noteBookContent = DataUtil.makeLikeQuery(noteBookContent);
            }
            List<NoteBookDTOResponse> result = noteBookService.getAll(noteBookName,noteBookContent,status,type);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
