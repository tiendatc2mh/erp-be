package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.TypeDocumentDTO;
import com.blameo.idservice.dto.response.TypeDocumentDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.repository.PersonalFileRepository;
import com.blameo.idservice.repository.TypeDocumentRepository;
import com.blameo.idservice.service.TypeDocumentService;
import com.blameo.idservice.utils.CommonUtil;
import com.blameo.idservice.utils.DataUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.blameo.idservice.constant.Constant.SPACE;

/**
 * @author : Phạm Hòa
 * @mailto : phoavn96@gmail.com
 * @created : 8/1/2023, Tuesday
 **/
@RestController
@RequestMapping("/api/id/type-document")
public class TypeDocumentController {
    @Autowired
    TypeDocumentService typeDocumentService;

    @Autowired
    TypeDocumentRepository typeDocumentRepository;

    @Autowired
    private MessageSource messageSource;

    Logger logger = Logger.getLogger(TypeDocumentController.class);
    @Autowired
    private PersonalFileRepository personalFileRepository;

    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody TypeDocumentDTO typeDocumentDTO, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to save type document : {}"+ typeDocumentDTO.toString());

            if (typeDocumentRepository.existsByTypeDocumentCodeAndTypeDocumentStatusGreaterThanEqual(typeDocumentDTO.getTypeDocumentCode(),0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("typeDocument.code",null,locale)+ SPACE +messageSource.getMessage("isExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            TypeDocumentDTOResponse result = typeDocumentService.save(typeDocumentDTO,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("createSuccess",null,locale),result),HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody TypeDocumentDTO typeDocumentDTO, @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to update type document : {}"+ typeDocumentDTO.toString());

            if (!typeDocumentRepository.existsByTypeDocumentIdAndTypeDocumentStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("typeDocument",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            TypeDocumentDTOResponse result = typeDocumentService.update(typeDocumentDTO,id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale),result),HttpStatus.OK);

        }
        catch (RuntimeException runtimeException){
            return new ResponseEntity<>(ResponseMessage.error(runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer pageSize,
                                     @RequestParam(required = false,defaultValue = "") String keyword,
                                     @RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all type document");
            if (!StringUtils.isEmpty(keyword)) {
                keyword = DataUtil.makeLikeQuery(keyword);
            }
            Map<String,Object> result = typeDocumentService.findAll(page,pageSize,keyword,status);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> get( @PathVariable("id") String id) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get type document : {}"+ id);

            if (!typeDocumentRepository.existsByTypeDocumentIdAndTypeDocumentStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("typeDocument",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            TypeDocumentDTOResponse result = typeDocumentService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> changeStatus( @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to change status type document : {}"+ id);

            if (!typeDocumentRepository.existsByTypeDocumentIdAndTypeDocumentStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("typeDocument",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }
            typeDocumentService.changeStatus(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("updateSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete( @PathVariable("id") String id, HttpServletRequest request) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);
        try {
            logger.debug("REST request to delete type document : {}"+ id);

            if (!typeDocumentRepository.existsByTypeDocumentIdAndTypeDocumentStatusGreaterThanEqual(id,0)) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("typeDocument",null,locale)+ SPACE +messageSource.getMessage("notExist",null,locale)), HttpStatus.BAD_REQUEST);
            }

            //check table khác sử dụng nếu cần thiết
            if (Boolean.TRUE.equals(personalFileRepository.existsByTypeDocumentId(id))) {
                return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("typeDocument", null, locale)+ SPACE + messageSource.getMessage(Constant.IS_USE, null, locale)+ messageSource.getMessage("dontDelete", null, locale)), HttpStatus.BAD_REQUEST);
            }
            typeDocumentService.delete(id,userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("deleteSuccess",null,locale)),HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll(@RequestParam(required = false) Integer status) {
        //Get ngôn ngữ
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get all type document");
            List<TypeDocumentDTOResponse> result = typeDocumentService.getAll(status);
            return new ResponseEntity<>(new ResponseMessage(true,messageSource.getMessage("getDataSuccess",null,locale),result),HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
