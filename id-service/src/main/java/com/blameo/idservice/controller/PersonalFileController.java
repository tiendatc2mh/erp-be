package com.blameo.idservice.controller;

import com.blameo.idservice.constant.Constant;
import com.blameo.idservice.dto.ResponseMessage;
import com.blameo.idservice.dto.request.ListPersonalFileDTO;
import com.blameo.idservice.dto.request.PersonalFileDTO;
import com.blameo.idservice.dto.request.UserDTO;
import com.blameo.idservice.dto.response.PersonalFileDTOResponse;
import com.blameo.idservice.dto.response.PositionDTOResponse;
import com.blameo.idservice.dto.response.UserDTOResponse;
import com.blameo.idservice.security.services.UserDetailsImpl;
import com.blameo.idservice.service.PersonalFileService;
import com.blameo.idservice.utils.CommonUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/id/personal_file")
public class PersonalFileController {

    @Autowired
    PersonalFileService personalFileService;

    @Autowired
    MessageSource messageSource;

    Logger logger = Logger.getLogger(PermissionController.class);

    private static final String KEY = "Personal file ";

    @PostMapping("/{id}")
    public ResponseEntity<?> create(@Valid @RequestBody ListPersonalFileDTO personalFileDTO, @PathVariable("id") String id, HttpServletRequest request) {
        Locale locale = CommonUtil.getLocale();
        UserDTOResponse userDTOResponse = CommonUtil.getUserRequest(request);

        try {
            logger.debug("REST request to save user : {}"+ personalFileDTO.toString());
            List<PersonalFileDTOResponse> result = personalFileService.save(personalFileDTO, id, userDTOResponse);
            return new ResponseEntity<>(new ResponseMessage(true,KEY + messageSource.getMessage("createSuccess",null,locale),result),HttpStatus.CREATED);
        }
        catch (Exception e){
            return new ResponseEntity<>(ResponseMessage.error(KEY + messageSource.getMessage("unknownError",null,locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable("id") String id) {
        Locale locale = CommonUtil.getLocale();
        try {
            logger.debug("REST request to get department : {}" + id);

            List<PersonalFileDTOResponse> result = personalFileService.get(id);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/self")
    public ResponseEntity<Object> getSelf() {
        Locale locale = CommonUtil.getLocale();
        try {
            String userId = SecurityContextHolder.getContext()
                    .getAuthentication() != null ? ((UserDetailsImpl) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal()).getUserId() : null;
            List<PersonalFileDTOResponse> result = personalFileService.get(userId);
            return new ResponseEntity<>(new ResponseMessage(true, messageSource.getMessage(Constant.GET_SUCCESS, null, locale), result), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(ResponseMessage.error(messageSource.getMessage(Constant.UNKNOWN, null, locale)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
